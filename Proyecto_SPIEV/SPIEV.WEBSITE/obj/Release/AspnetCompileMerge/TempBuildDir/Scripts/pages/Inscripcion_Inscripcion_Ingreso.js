﻿$(document).ready(function () {

    $('#tabla-inscripciones-ingreso').DataTable({
        "ajax": {
            url: "/inscripcion/inscripciones-ingresadas-ajax",
            dataSrc: '',
            type: 'POST'
        },
        "language": { "url": "/Scripts/datatables/Spanish.json" },
        "columns": [
            { "data": "ID_PRIMERA_INSCRIPCION" },
            { "data": "SPIEV.ADQUIRIENTE.RUN" },
            { "data": "USUARIO.NOMBRE_USUARIO" },
            {
                "data": "SPIEV.FECHA",
                render: function (data, type, row)
                {
                    if(type === "sort" || type === "type")
                    {
                        return data;
                    }
                    return moment(data).format("DD-MM-YYYY");
                }
            },
            {
                "mRender": function (data, type, row)
                {
                    return DIV_GROUP_OPTIONS(row['ID_PRIMERA_INSCRIPCION']);
                },
                "searchable": false,
                "orderable": false
            }
        ]
    });
});

function DIV_GROUP_OPTIONS(ID_PRIMERA_INSCRIPCION)
{
    var DIV = $('<div>');
    var BTN_GROUP = $('<div>').addClass('btn-group btn-group-sm');

    var BTN_VER = $('<button>').attr({ "type": "button", "onclick": "CLICK_MODAL_DETALLE(" + ID_PRIMERA_INSCRIPCION + ")" }).addClass('btn btn-outline-primary btn-sm').html('<i class="fas fa-eye"></i>');
    //var BTN_BORRAR = $('<button>').attr('type', 'button').addClass('btn btn-outline-danger').html('<i class="fas fa-trash"></i>');

    $(BTN_GROUP).append(BTN_VER);
    //$(BTN_GROUP).append(BTN_BORRAR);

    $(DIV).append(BTN_GROUP);

    return $(DIV).html();

}

function CLICK_MODAL_DETALLE(ID_PRIMERA_INSCRIPCION)
{
    $('#span-id-inscripcion').text(ID_PRIMERA_INSCRIPCION);

    var URL = $('#inscripcion-documentos').data('url');

    //SETEAMOS LA RUTA DEL LINK
    $('#inscripcion-documentos').attr('href', URL + ID_PRIMERA_INSCRIPCION);

    //ABRIMOS EL MODAL
    $('#modal-detalle-solicitud').modal('show');

    //CARGAMOS EL DETALLE DE LA INSCRIPCION
    LOAD_DETALLE_INSCRIPCION(ID_PRIMERA_INSCRIPCION);
}

function CLICK_MODAL_ESTADO(ID_SOLICITUD)
{
    //CERRAR EL MODAL ANTERIOR
    $('#modal-detalle-solicitud').modal('hide');

    //DATOS VARIABLES
    $('#cambio-estado-id-solicitud').text(ID_SOLICITUD);

    //OBTENEMOS LOS ESTADOS DE LA SOLICITUD Y EL DETALLE DE LA MISMA 
    $.when(GET_ESTADOS_SOLICITUD(), GET_DETALLE_SOLICITUD(ID_SOLICITUD)).then(function (JSON_ESTADOS, JSON_DETALLE) {

        var ESTADOS = JSON_ESTADOS[0];
        var DETALLE = JSON_DETALLE[0];

        //SI EXISTIO ALGÚN ERROR
        if (!ESTADOS.status || !DETALLE.status) {
            return;
        }

        //OBTENEMOS EL SELECTOR
        var SELECT_CAMBIO = $('#select-estados-solicitud');

        //SE LIMPIA EL COMBOBOX
        $(SELECT_CAMBIO).empty();

        //SE RECORREN LOS ESTADOS DE LA SOLICITUD
        $.each(ESTADOS.data, function (ID, VALUE) {

            var OPCION = $('<option>').attr('value', ID.ID_ESTADO).html(VALUE.NOMBRE_ESTADO);

            $(SELECT_CAMBIO).append(OPCION);

        });

        //PONER EL COMBOBOX EN POSICION
        $(SELECT_CAMBIO).val(DETALLE.data.ID_ESTADO);

    });

    //ABRIR EL MODAL DE CAMBIO DE ESTADO
    $('#modal-cambio-estado').modal('show');
}

function CLICK_CAMBIO_ESTADO()
{
    var ID_SOLICITUD = parseInt($('#cambio-estado-id-solicitud').text());

    var ID_ESTADO = parseInt($('#select-estados-solicitud').val());

    var COMENTARIO = $('#comentario-cambio-estado').val();

    var NUEVA_HISTORIA = {
        ID_SOLICITUD: ID_SOLICITUD,
        ID_ESTADO: ID_ESTADO,
        COMENTARIO: COMENTARIO
    };

    $.when(GUARDAR_CAMBIO_ESTADO(NUEVA_HISTORIA)).then(function (JSON_RES, textStatus, jqXHR) {

        //CLASE PARA ALERTA
        var CLASE = "alert alert-dismissible fade show ";

        //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
        CLASE += JSON_RES.status ? "alert-success" : "alert-danger";

        //SE INSERTA LA ALERTA EN PANTALLA
        HTML_ALERTA(CLASE, JSON_RES.message);
    });

    console.log(NUEVA_HISTORIA);

}

function GET_ESTADOS_SOLICITUD()
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/estados-solicitud-ajax",
        dataType: "json"
    });
}

function LOAD_DETALLE_INSCRIPCION(ID_INSCRIPCION) {
    //OBTENEMOS LOS ESTADOS DE LA SOLICITUD Y EL DETALLE DE LA MISMA 
    $.when(GET_DETALLE_INSCRIPCION(ID_INSCRIPCION)).then(function (JSON_DETALLE) {

        //EN CASO DE ERROR EN LA CONSULTA...
        if (!JSON_DETALLE.status) {
            return;
        }

        var JSON_INS = JSON_DETALLE.data;

        //CARGAR LOS DATOS...
        LOAD_VEHICULO(JSON_INS.SPIEV.VEHICULO);
        LOAD_COMUNIDAD(JSON_INS.SPIEV.COMUNIDAD);
        LOAD_ADQUIRIENTE(JSON_INS.SPIEV.ADQUIRIENTE);
        LOAD_SOLICITANTE(JSON_INS.SPIEV.SOLICITANTE);
        LOAD_ESTIPULANTE(JSON_INS.SPIEV.ESTIPULANTE);
        LOAD_IMPUESTO(JSON_INS.SPIEV.IMPUESTO);
        LOAD_FACTURA(JSON_INS.SPIEV.FACTURA);
        LOAD_OBSERVACION(JSON_INS.SPIEV.OBSERVACION);
        LOAD_OPERADOR(JSON_INS.SPIEV.OPERADOR);
    });
}

function LOAD_VEHICULO(OBJ_VEHICULO) {
    $('#id-vehiculo').html(OBJ_VEHICULO.ID_VEHICULO);
    $('#codigo-informe-tecnico-vehiculo').html(OBJ_VEHICULO.CODIGO_INFORME_TECNICO);
    $('#num-asientos-vehiculo').html(OBJ_VEHICULO.NUMERO_ASIENTOS);
    $('#anho-fabricacion-vehiculo').html(OBJ_VEHICULO.AGNO_FABRICACION);
    $('#tipo-combustible-vehiculo').html(OBJ_VEHICULO.TIPO_COMBUSTIBLE.NOMBRE_COMBUSTIBLE);
    $('#marca-vehiculo').html(OBJ_VEHICULO.MARCA);
    $('#modelo-vehiculo').html(OBJ_VEHICULO.MODELO);
    $('#num-chasis-vehiculo').html(OBJ_VEHICULO.NUMERO_CHASIS);
    $('#num-motor-vehiculo').html(OBJ_VEHICULO.NUMERO_MOTOR);
    $('#num-serie-vehiculo').html(OBJ_VEHICULO.NUMERO_SERIE);
    $('#num-vin-vehiculo').html(OBJ_VEHICULO.NUMERO_VIN);
    $('#pbv-vehiculo').html(OBJ_VEHICULO.PESO_BRUTO_VEHICULAR);
    $('#num-puertas-vehiculo').html(OBJ_VEHICULO.NUMERO_PUERTAS);
    $('#tipo-vehiculo').html(OBJ_VEHICULO.TIPO_VEHICULO.NOMBRE_TIPO);
    $('#tipo-carga-vehiculo').html(OBJ_VEHICULO.TIPO_CARGA.TIPO_CARGA_DESCRIPCION);
    $('#tipo-pbv-vehiculo').html(OBJ_VEHICULO.TIPO_PBV.TIPO_CARGA_DESCRIPCION);
}

function LOAD_COMUNIDAD(OBJ_COMUNIDAD) {
    $('#id-comunidad').html(OBJ_COMUNIDAD.ID_COMUNIDAD);
    $('#opcion-comunidad').html(OBJ_COMUNIDAD.OPCION_COMUNIDAD.OPCION);
    $('#cantidad-comunidad').html(OBJ_COMUNIDAD.CANTIDAD);
}

function LOAD_ADQUIRIENTE(OBJ_ADQUIRIENTE) {
    $('#id-adquiriente').html(OBJ_ADQUIRIENTE.ID_ADQUIRIENTE);
    $('#ape-paterno-adquiriente').html(OBJ_ADQUIRIENTE.APELLIDO_PATERNO);
    $('#ape-materno-adquiriente').html(OBJ_ADQUIRIENTE.APELLIDO_MATERNO);
    $('#razon-social-adquiriente').html(OBJ_ADQUIRIENTE.RAZON_SOCIAL);
    $('#run-adquiriente').html(OBJ_ADQUIRIENTE.RUN);
    $('#calle-adquiriente').html(OBJ_ADQUIRIENTE.CALLE);
    $('#num-domicilio-adquiriente').html(OBJ_ADQUIRIENTE.NUMERO_DOMICILIO);
    $('#letra-domicilio-adquiriente').html(OBJ_ADQUIRIENTE.LETRA_DOMICILIO);
    $('#resto-domicilio-adquiriente').html(OBJ_ADQUIRIENTE.RESTO_DOMICILIO);
    $('#telefono-adquiriente').html(OBJ_ADQUIRIENTE.TELEFONO);
    $('#cod-postal-adquiriente').html(OBJ_ADQUIRIENTE.CODIGO_POSTAL);
    $('#correo-adquiriente').html(OBJ_ADQUIRIENTE.CORREO_ELECTRONICO);
    $('#comuna-adquiriente').html(OBJ_ADQUIRIENTE.COMUNA.NOMBRE);
    $('#calidad-adquiriente').html(OBJ_ADQUIRIENTE.CALIDAD_PERSONA.TIPO_PERSONA_DESCRIPCION);
}

function LOAD_SOLICITANTE(OBJ_SOLICITANTE) {
    $('#id-solicitante').html(OBJ_SOLICITANTE.ID_SOLICITANTE);
    $('#ape-paterno-solicitante').html(OBJ_SOLICITANTE.APELLIDO_PATERNO);
    $('#ape-materno-solicitante').html(OBJ_SOLICITANTE.APELLIDO_MATERNO);
    $('#razon-social-solicitante').html(OBJ_SOLICITANTE.RAZON_SOCIAL);
    $('#run-solicitante').html(OBJ_SOLICITANTE.RUN);
    $('#calle-solicitante').html(OBJ_SOLICITANTE.CALLE);
    $('#num-domicilio-solicitante').html(OBJ_SOLICITANTE.NUMERO_DOMICILIO);
    $('#letra-domicilio-solicitante').html(OBJ_SOLICITANTE.LETRA_DOMICILIO);
    $('#resto-domicilio-solicitante').html(OBJ_SOLICITANTE.RESTO_DOMICILIO);
    $('#telefono-solicitante').html(OBJ_SOLICITANTE.TELEFONO);
    $('#cod-postal-solicitante').html(OBJ_SOLICITANTE.CODIGO_POSTAL);
    $('#correo-solicitante').html(OBJ_SOLICITANTE.CORREO_ELECTRONICO);
    $('#comuna-solicitante').html(OBJ_SOLICITANTE.COMUNA.NOMBRE);
}

function LOAD_ESTIPULANTE(OBJ_ESTIPULANTE) {
    $('#id-estipulante').html(OBJ_ESTIPULANTE.ID_ESTIPULANTE);
    $('#ape-paterno-estipulante').html(OBJ_ESTIPULANTE.APELLIDO_PATERNO);
    $('#ape-materno-estipulante').html(OBJ_ESTIPULANTE.APELLIDO_MATERNO);
    $('#razon-social-estipulante').html(OBJ_ESTIPULANTE.RAZON_SOCIAL);
    $('#run-estipulante').html(OBJ_ESTIPULANTE.RUN);
    $('#calle-estipulante').html(OBJ_ESTIPULANTE.CALLE);
    $('#num-domicilio-estipulante').html(OBJ_ESTIPULANTE.NUMERO_DOMICILIO);
    $('#letra-domicilio-estipulante').html(OBJ_ESTIPULANTE.LETRA_DOMICILIO);
    $('#resto-domicilio-estipulante').html(OBJ_ESTIPULANTE.RESTO_DOMICILIO);
    $('#telefono-estipulante').html(OBJ_ESTIPULANTE.TELEFONO);
    $('#cod-postal-estipulante').html(OBJ_ESTIPULANTE.CODIGO_POSTAL);
    $('#correo-estipulante').html(OBJ_ESTIPULANTE.CORREO_ELECTRONICO);
    $('#comuna-estipulante').html(OBJ_ESTIPULANTE.COMUNA.NOMBRE);
    $('#calidad-estipulante').html(OBJ_ESTIPULANTE.CALIDAD_PERSONA.TIPO_PERSONA_DESCRIPCION);
}

function LOAD_IMPUESTO(OBJ_IMPUESTO) {
    $('#id-impuesto').html(OBJ_IMPUESTO.ID_IMPUESTO_ADICIONAL);
    $('#cod-ide-impuesto').html(OBJ_IMPUESTO.CODIGO_IDENTIFICACION);
    $('#cod-inf-tecnico').html(OBJ_IMPUESTO.CODIGO_INFORME_TECNICO);
    $('#monto-impuesto').html(OBJ_IMPUESTO.MONTO_IMPUESTO);
    $('#total-factura-impuesto').html(OBJ_IMPUESTO.TOTAL_FACTURA);
}

function LOAD_FACTURA(OBJ_FACTURA) {
    $('#id-factura').html(OBJ_FACTURA.ID_FACTURA);
    $('#num-factura').html(OBJ_FACTURA.NUMERO_FACTURA);
    $('#fecha-factura').html(OBJ_FACTURA.FECHA);
    $('#comuna-factura').html(OBJ_FACTURA.COMUNA.NOMBRE);
    $('#rut-emisor-factura').html(OBJ_FACTURA.RUT_EMISOR);
    $('#nombre-emisor-factura').html(OBJ_FACTURA.NOMBRE_EMISOR);
    $('#total-factura').html(OBJ_FACTURA.MONTO_TOTAL_FACTURA);
    $('#tipo-moneda-factura').html(OBJ_FACTURA.TIPO_MONEDA.TIPO_MONEDA_DESCRIPCION);
}

function LOAD_OBSERVACION(OBJ_OBSERVACION) {
    $('#id-observacion').html(OBJ_OBSERVACION.ID_OBSERVACION);
    $('#observacion').html(OBJ_OBSERVACION.COMENTARIO);
}

function LOAD_OPERADOR(OBJ_OPERADOR) {
    $('#id-operador').html(OBJ_OPERADOR.ID_OPERADOR);
    $('#region-operador').html(OBJ_OPERADOR.REGION.NOMBRE);
    $('#run-usuario-operador').html(OBJ_OPERADOR.RUN_USUARIO);
    $('#run-empresa-operador').html(OBJ_OPERADOR.RUN_EMPRESA);
}

function GET_DETALLE_INSCRIPCION(ID_INSCRIPCION) {
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/detalle-inscripcion-ajax",
        data: JSON.stringify({ ID_INSCRIPCION: parseInt(ID_INSCRIPCION) }),
        dataType: "json"
    });
}

function HTML_ALERTA(CLASS, MENS)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PARTIAL_ALERT",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');
        }
    });
}