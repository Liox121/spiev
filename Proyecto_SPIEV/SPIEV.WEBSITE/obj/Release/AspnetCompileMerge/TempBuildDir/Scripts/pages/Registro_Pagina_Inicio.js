﻿$(document).ready(function () {

    $.when(ResumenInscripciones()).then(function (json, textStatus, jqXHR) {
        console.log(jqXHR.status);

        //SI EXISTIO ALGÚN ERROR
        if (!json.status) {
            return;
        }

        CompletarResumenEstado(json.data);
    });
});


function getSum(total, num)
{
    return total + num;
}


function CompletarResumenEstado(data)
{
    //VARIABLES
    var SOLICITUD_INGRESADA = data.filter(function (data) {
        return data.ESTADO === 'Solicitud Ingresada';
    }).map(function (x) {
        return x.TOTAL;
    }).reduce(getSum, 0);

    var INSCRIPCION_INGRESADA = data.filter(function (data) {
        return data.ESTADO === 'Inscripcion Ingresada';
    }).map(function (x) {
        return x.TOTAL;
    }).reduce(getSum, 0);


    var DOCUMENTACION_CARGADA = data.filter(function (data) {
        return data.ESTADO === 'Documentación Cargada';
    }).map(function (x) {
        return x.TOTAL;
    }).reduce(getSum, 0);


    var INSCRIPCION_RECHAZADA = data.filter(function (data) {
        return data.ESTADO === 'Inscripcion Rechazada';
    }).map(function (x) {
        return x.TOTAL;
    }).reduce(getSum, 0);


    //SUMA DE LOS ANTERIORES 
    var INSCRIPCION_TOTAL = INSCRIPCION_INGRESADA + INSCRIPCION_RECHAZADA + DOCUMENTACION_CARGADA;

    $('#total-solicitud-ingresada').text(SOLICITUD_INGRESADA);
    $('#total-inscripcion-ingresada').text(INSCRIPCION_INGRESADA);
    $('#total-inscripcion-rechazada').text(INSCRIPCION_RECHAZADA);
    $('#total-inscripcion-cargada').text(DOCUMENTACION_CARGADA);
    $('#total-inscripcion').text(INSCRIPCION_TOTAL);

    //EFECTO DE CONTEO SOBRE LOS CUADROS DE RESUMEN
    $('span.integers').counterUp({
        delay: 10, // the delay time in ms
        time: 1000 // the speed time in ms
    });

    var JSON_GRAPHS = [];

    //SI LOS VALORES SON MAYORES A 0
    if (INSCRIPCION_INGRESADA > 0) JSON_GRAPHS.push({ "estado": "Inscripciones Ingresadas", "total": INSCRIPCION_INGRESADA, "color": "#36b9cc" });
    if (INSCRIPCION_RECHAZADA > 0) JSON_GRAPHS.push({ "estado": "Inscripciones Rechazadas", "total": INSCRIPCION_RECHAZADA, "color": "#1cc88a" });
    if (DOCUMENTACION_CARGADA > 0) JSON_GRAPHS.push({ "estado": "Documentación Cargada", "total": DOCUMENTACION_CARGADA, "color": "#e74a3b" });

    //LOS COLORCITOS...
    var COLORES = JSON_GRAPHS.map(function (graph) {
        return graph.color;
    });

    //CREAR EL GRAFICO
    CompletarResumenGrafico(JSON_GRAPHS, COLORES);
}

function CompletarResumenGrafico(json_graphs, colors)
{
    AmCharts.makeChart("chartdiv",
        {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "titleField": "estado",
            "valueField": "total",
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [],
            "colors": colors,
            "dataProvider": json_graphs
        }
    );
}

function ResumenInscripciones()
{
    return $.ajax({
        type: "POST",
        cache: false,
        contentType: "application/json; charset=utf-8",
        url: "/reportes/registro-estadistica-ajax",
        dataType: "json"
    });
}