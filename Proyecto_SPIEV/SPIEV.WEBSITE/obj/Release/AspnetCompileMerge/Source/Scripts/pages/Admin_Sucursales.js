﻿$(document).ready(function ()
{
    //SI NO EXISTE LA TABLA
    if (!$('#tabla-sucursales').length) return;

    $('#tabla-sucursales').DataTable({
        "ajax": {
            url: "/administrador/sucursales-ajax",
            dataSrc: ''
        },
        "language": { "url": "/Scripts/datatables/Spanish.json" },
        "columns": [
            { "data": "ID_SUCURSAL" },
            { "data": "NOMBRE_SUCURSAL" },
            { "data": "EMPRESA.NOMBRE_EMPRESA" },
            {
                "mRender": function (data, type, row)
                {
                    return DIV_GROUP_OPTIONS(row);
                },
                "searchable": false,
                "orderable": false
            }
        ]
    });
});

function DIV_GROUP_OPTIONS(FILA_TABLA)
{
    var ID_SUCURSAL = FILA_TABLA['ID_SUCURSAL'];

    var DIV = $('<div>');
    var DIV_BTN_GROUP = $('<div>').addClass('btn-group btn-group-sm');

    var BTN_EDITAR = $('<a>').attr('href', '/administrador/sucursales/' + ID_SUCURSAL).addClass('btn btn-outline-primary').html('<i class="fas fa-edit fa-sm"></i>');
    var BTN_BORRAR = $('<button>').attr({ 'type': 'button', 'onclick': "CLICK_BORRAR_SUCURSAL(" + ID_SUCURSAL + ")" }).addClass('btn btn-outline-danger').html('<i class="fas fa-trash fa-sm"></i>');

    $(DIV_BTN_GROUP).append(BTN_EDITAR);
    $(DIV_BTN_GROUP).append(BTN_BORRAR);

    $(DIV).append(DIV_BTN_GROUP);

    return $(DIV).html();
}

function CLICK_CREATE()
{
    var OBJETO = {};

    $("#datos-sucursal").find('[data-propiedad]').each(function () {
        //VALOR EN STRING DEL CAMPO...
        var VALOR_CAMPO = $(this).val();

        //SI ES ENTERO
        if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

        //SI ES DECIMAL
        if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

        //SI ES UN RADIO BUTTON
        if ($(this).data('radio')) VALOR_CAMPO = $("#" + $(this).attr('id')).is(':checked') ? true : false;

        //SI ES UN CHECKBOX
        if ($(this).data('checkbox')) {
            VALOR_CAMPO = Array();
            $("input[name='" + $(this).attr("name") + "']:checked").each(function () {
                VALOR_CAMPO.push(parseInt(this.value));
            });
        }

        //SE CREA EL CAMPO CON EL VALOR
        OBJETO[$(this).data('propiedad')] = VALOR_CAMPO;

    });

    CREAR_SUCURSAL(OBJETO, $('#btn-editar-sucursal'));
}

function CLICK_EDIT()
{
    var OBJETO = {};

    $("#datos-sucursal").find('[data-propiedad]').each(function () {
        //VALOR EN STRING DEL CAMPO...
        var VALOR_CAMPO = $(this).val();

        //SI ES ENTERO
        if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

        //SI ES DECIMAL
        if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

        //SI ES UN RADIO BUTTON
        if ($(this).data('radio')) VALOR_CAMPO = $("#" + $(this).attr('id')).is(':checked') ? true : false;

        //SI ES UN CHECKBOX
        if ($(this).data('checkbox')) {
            VALOR_CAMPO = Array();
            $("input[name='" + $(this).attr("name") + "']:checked").each(function () {
                VALOR_CAMPO.push(parseInt(this.value));
            });
        }

        //SE CREA EL CAMPO CON EL VALOR
        OBJETO[$(this).data('propiedad')] = VALOR_CAMPO;

    });

    EDITAR_SUCURSAL(OBJETO, $('#btn-crear-sucursal'));
}

function CREAR_SUCURSAL(OBJETO, BTN)
{
    //BOTON EN CARGANDO...
    $(BTN).html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //TEXT EN BOTON
    var TEXT_BTN = "Crear sucursal";

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/administrador/sucursales/crear-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message, BTN, TEXT_BTN);

        },
        error: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos", BTN, TEXT_BTN);
        }
    });
}

function EDITAR_SUCURSAL(OBJETO, BTN)
{
    //BOTON EN CARGANDO...
    $(BTN).html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //TEXT EN BOTON
    var TEXT_BTN = "Guardar sucursal";

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/administrador/sucursales/editar-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT) {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message, BTN, TEXT_BTN);

        },
        error: function (RESULT) {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos", BTN, TEXT_BTN);
        }
    });
}

function CLICK_BORRAR_SUCURSAL(ID_SUCURSAL)
{
    var BORRAR = confirm("¿Seguro que desea borrar la sucursal: " + ID_SUCURSAL + "?");

    if (BORRAR === false) return;

    $.when(BORRAR_SUCURSAL(ID_SUCURSAL)).then(function (JSON_RES, textStatus, jqXHR) {

        //MENSAJE CON LA RESPUESTA
        alert(JSON_RES.message);

        //SI EXISTIO ERROR
        if (!JSON_RES.status) return;

        //SE BUSCA EL ID DE LA FILA; fnFindCellRowIndexes(TEXTO_A_BUSCAR, INDICE_DE_LA_FILA)
        var ID_ROW = $('#tabla-sucursales').dataTable().fnFindCellRowIndexes(ID_SUCURSAL, 0);

        //SE BORRA LA FILA SEGÚN ID
        $('#tabla-sucursales').DataTable().row(ID_ROW).remove().draw();

    });

}

function BORRAR_SUCURSAL(ID_SUCURSAL)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/administrador/borrar-sucursal-ajax",
        data: JSON.stringify({ ID_SUCURSAL: parseInt(ID_SUCURSAL) }),
        dataType: "json"
    });
}

function HTML_ALERTA(CLASS, MENS, BTN, TEXT_BTN)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PARTIAL_ALERT",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');

            $(BTN).html(TEXT_BTN);
        }
    });
}






