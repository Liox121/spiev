﻿$(document).ready(function ()
{
    $('#tabla-perfiles').DataTable({
        "ajax": {
            url: "/administrador/perfiles-ajax",
            dataSrc: 'data'
        },
        "language": { "url": "/Scripts/datatables/Spanish.json" },
        "columns": [
            { "data": "ID_PERFIL" },
            { "data": "NOMBRE" },
            { "data": "DESCRIPCION" },
            {
                "mRender": function (data, type, row)
                {
                    return "<a href='/administrador/perfiles/" + row['ID_PERFIL'] + "' class='btn btn-outline-primary btn-sm'><i class='fas fa-edit fa-sm'></i></a>";
                },
                "searchable": false,
                "orderable": false
            }
        ]
    });
});

function CLICK_EDIT()
{
    var OBJETO = {};

    $("#datos-perfil").find('[data-propiedad]').each(function () {
        //VALOR EN STRING DEL CAMPO...
        var VALOR_CAMPO = $(this).val();

        //SI ES ENTERO
        if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

        //SI ES DECIMAL
        if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

        //SI ES UN RADIO BUTTON
        if ($(this).data('radio')) VALOR_CAMPO = $("input[name='" + $(this).attr('name') + "']:checked").val();

        //SI ES UN CHECKBOX
        if ($(this).data('checkbox')) {
            VALOR_CAMPO = Array();
            $("input[name='" + $(this).attr("name") + "']:checked").each(function () {
                VALOR_CAMPO.push(parseInt(this.value));
            });
        }

        //SE CREA EL CAMPO CON EL VALOR
        OBJETO[$(this).data('propiedad')] = VALOR_CAMPO;

    });

    EDITAR_PERFIL(OBJETO, $('#btn-editar-perfil'));
}

function EDITAR_PERFIL(OBJETO, BTN)
{
    //BOTON EN CARGANDO...
    $(BTN).html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    //TEXT EN BOTON
    var TEXT_BTN = "Guardar perfil";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/administrador/perfiles/editar-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message, BTN, TEXT_BTN);

        },
        error: function (RESULT) {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            clase_base += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos", BTN, TEXT_BTN);
        }
    });
}

function HTML_ALERTA(CLASS, MENS, BTN, TEXT_BTN)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PARTIAL_ALERT",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');

            $(BTN).html(TEXT_BTN);
        }
    });
}






