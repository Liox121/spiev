﻿function LOAD_MIS_SOLICITUDES()
{
    $('#tabla-mis-solicitudes').DataTable({
        "ajax": {
            url: "/solicitud/mis-solicitudes-ajax",
            dataSrc: '',
            type: 'POST'
        },
        "language": { "url": "/Scripts/datatables/Spanish.json" },
        "columns": [
            { "data": "ID_SOLICITUD" },
            {
                "mRender": function (data, type, row) {
                    return row["RUT_ADQUIRIENTE"] + "-" + row["DV_ADQUIRIENTE"];
                }
            },
            { "data": "ESTADO.NOMBRE_ESTADO" },
            { "data": "PRIMERA_INSCRIPCION.ESTADO.NOMBRE_ESTADO", "defaultContent": "-" },
            { "data": "SUCURSAL.NOMBRE_SUCURSAL" },
            {
                "data": "FECHA_SOLICITUD",
                render: function (data, type, row) {
                    if (type === "sort" || type === "type") {
                        return data;
                    }
                    return moment(data).format("DD-MM-YYYY");
                }
            },
            {
                "mRender": function (data, type, row) {
                    return DIV_GROUP_OPTIONS(row);
                },
                "searchable": false,
                "orderable": false
            }
        ],
        "order": [[0, "desc"]]
    });
}

function DIV_GROUP_OPTIONS(FILA_TABLA)
{
    var ID_SOLICITUD = FILA_TABLA['ID_SOLICITUD'];
    var TIENE_INSCRIPCION = FILA_TABLA['ID_PRIMERA_INSCRIPCION'] === null ? false : true;

    var DIV = $('<div>');
    var DIV_BTN_GROUP = $('<div>').addClass('btn-group btn-group-sm');

    var BTN_VER = $('<button>').attr({ "type": "button", "onclick": "CLICK_MODAL_DETALLE(" + ID_SOLICITUD + ")" }).addClass('btn btn-outline-primary').html('<i class="fas fa-eye fa-sm"></i>');
    var BTN_EDITAR = $('<a>').attr('href', '/solicitud/mis-solicitudes/' + ID_SOLICITUD).addClass('btn btn-outline-primary').html('<i class="fas fa-edit fa-sm"></i>');

    var BTN_BORRAR = $('<button>').attr({ 'type': 'button', 'onclick': "CLICK_BORRAR_MIS_SOLICITUD(" + ID_SOLICITUD + ")" }).addClass('btn btn-outline-danger').html('<i class="fas fa-trash fa-sm"></i>');

    //SI TIENE INSCRIPCIÓN DESHABILITAR EL BOTON
    if (TIENE_INSCRIPCION)
    {
        $(BTN_EDITAR).attr('disabled', true).addClass('disabled');
        $(BTN_BORRAR).attr('disabled', true).addClass('disabled');
    }

    $(DIV_BTN_GROUP).append(BTN_VER);
    $(DIV_BTN_GROUP).append(BTN_EDITAR);
    $(DIV_BTN_GROUP).append(BTN_BORRAR);

    $(DIV).append(DIV_BTN_GROUP);

    return $(DIV).html();

}

function CLICK_MODAL_DETALLE(ID_SOLICITUD)
{
    $('#span-id-solicitud').text(ID_SOLICITUD);
    $('#modal-detalle-solicitud').modal('show');

    $.when(GET_ARCHIVOS_SOLICITUD(ID_SOLICITUD)).then(function (JSON_RES, textStatus, jqXHR) {
        console.log(jqXHR.status); // Alerts 200

        //SI EXISTIO ALGÚN ERROR
        if (!JSON_RES.status) {
            return;
        }

        LOAD_ARCHIVOS(JSON_RES.data.ARCHIVOS);
        LOAD_HISTORIA_SOLICITUD(JSON_RES.data.HISTORIA_SOLICITUD);
        LOAD_HISTORIA_INSCRIPCION(JSON_RES.data.HISTORIA_INSCRIPCION);
    });

}

function LOAD_ARCHIVOS(JSON_ARCHIVOS)
{
    var T_BODY = $('#tabla-detalle-archivos').find('tbody');
    $(T_BODY).empty();

    $.each(JSON_ARCHIVOS, function (ID, VALUE) {
        var TR = $('<tr>');

        $(TR).append($('<td>').html(VALUE.ID_ARCHIVO));
        $(TR).append($('<td>').html(VALUE.TIPO_ARCHIVO_SOLICITUD.TIPO_ARCHIVO.NOMBRE));

        var LINK = $('<a>').attr('href', '/solicitud/mis-archivos-solicitud/' + VALUE.ID_ARCHIVO).html('Descargar');

        $(TR).append($('<td>').html(LINK));

        $(T_BODY).append(TR);

    });

}

function LOAD_HISTORIA_SOLICITUD(JSON_HISTORIA)
{
    var T_BODY = $('#tabla-historia-solicitud').find('tbody');
    $(T_BODY).empty();

    $.each(JSON_HISTORIA, function (ID, VALUE) {
        var TR = $('<tr>');

        $(TR).append($('<td>').html(VALUE.ID_HISTORIA));
        $(TR).append($('<td>').html(VALUE.USUARIO.NOMBRE_USUARIO));
        $(TR).append($('<td>').html(VALUE.ESTADO.NOMBRE_ESTADO));
        $(TR).append($('<td>').html(VALUE.COMENTARIO));
        $(TR).append($('<td>').html(VALUE.FECHA));

        $(T_BODY).append(TR);

    });

    //SI NO EXISTE HISTORIA ALGUNA
    if (JSON_HISTORIA.length === 0)
    {
        var TR = $('<tr>');
        $(TR).append($('<td>').attr('colspan', 4).html('Sin historia para mostrar'));
        $(T_BODY).append(tr);
    }

}

function LOAD_HISTORIA_INSCRIPCION(JSON_HISTORIA) {
    var T_BODY = $('#tabla-historia-inscripcion').find('tbody');
    $(T_BODY).empty();

    $.each(JSON_HISTORIA, function (id, value) {
        var TR = $('<tr>');

        $(TR).append($('<td>').html(value.ID_HISTORIA));
        $(TR).append($('<td>').html(value.USUARIO.NOMBRE_USUARIO));
        $(TR).append($('<td>').html(value.ESTADO.NOMBRE_ESTADO));
        $(TR).append($('<td>').html(value.COMENTARIO));
        $(TR).append($('<td>').html(value.FECHA));

        $(T_BODY).append(TR);

    });

    if (JSON_HISTORIA.length === 0)
    {
        var TR = $('<tr>');
        $(TR).append($('<td>').attr('colspan', 4).html('Sin historia para mostrar'));
        $(T_BODY).append(TR);
    }
}

function GET_ARCHIVOS_SOLICITUD(ID_SOLICITUD)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/mis-detalle-ajax",
        data: JSON.stringify({ ID_SOLICITUD: parseInt(ID_SOLICITUD) }),
        dataType: "json"
    });
}

function CLICK_BORRAR_MIS_SOLICITUD(ID_SOLICITUD)
{
    var BORRAR = confirm("¿Seguro que desea borrar la solicitud: " + ID_SOLICITUD + "?");

    if (BORRAR === false)  return;

    $.when(BORRAR_MI_SOLICITUD(ID_SOLICITUD)).then(function (JSON_RES, textStatus, jqXHR) {

        //MENSAJE CON LA RESPUESTA
        alert(JSON_RES.message);

        //SI EXISTIO ERROR
        if (!JSON_RES.status) return;

        //RECARGAR LA TABLITA CON DATOS
        $('#tabla-mis-solicitudes').DataTable().ajax.reload();

        //SE BUSCA EL ID DE LA FILA; fnFindCellRowIndexes(TEXTO_A_BUSCAR, INDICE_DE_LA_FILA)
        var ID_ROW = $('#tabla-mis-solicitudes').dataTable().fnFindCellRowIndexes(ID_SOLICITUD, 0);

        //SE BORRA LA FILA SEGÚN ID
        $('#tabla-mis-solicitudes').DataTable().row(ID_ROW).remove().draw();

    });

}

function BORRAR_MI_SOLICITUD(ID_SOLICITUD)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/borrar-solicitud-propia-ajax",
        data: JSON.stringify({ ID_SOLICITUD: parseInt(ID_SOLICITUD) }),
        dataType: "json"
    });
}