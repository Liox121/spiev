﻿$(document).ready(function () {
    localStorage.removeItem('json-tipo-archivo');

    //ID DE LA INSCRIPCION
    var ID_INSCRIPCION = $('#id-inscripcion').text();

    //CARGAR DATOS DE LA SOLICITUD
    LOAD_SOLICITUD(ID_INSCRIPCION);
});

function CLICK_LOAD_ARCHIVOS()
{
    //ESTADO CARGANDO BOTONES
    var BTN_ADD_FILE = $('#btn-add-file');
    var BTN_LOAD_FILE = $('#btn-load-from-solicitud');
    var BTN_SAVE_DOC = $('#btn-cargar-documentos');

    //BOTONES CARGANDO...
    BTN_CARGANDO(BTN_ADD_FILE);
    BTN_CARGANDO(BTN_LOAD_FILE);
    BTN_CARGANDO(BTN_SAVE_DOC);

    //ID DE LA INSCRIPCION
    var ID_INSCRIPCION = $('#id-inscripcion').text();

    //CARGAR ARCHIVOS DESDE LA SOLICITUD
    $.when(CARGAR_ARCHIVOS_SOLICITUD(ID_INSCRIPCION)).then(function (REQ_CARGA)
    {
        var JSON_CARGA = REQ_CARGA;

        if (!JSON_CARGA.status)
        {
            //ALERTA HTML
            HTML_ALERTA("alert alert-dismissible fade show alert-danger", JSON_CARGA.message);

            //ESTADO BOTONES
            BTN_NORMAL(BTN_ADD_FILE, "<i class='fas fa-plus-circle'></i> Agregar archivo");
            BTN_NORMAL(BTN_LOAD_FILE, "<i class='fas fa-sync'></i> Desde Solicitud");
            BTN_NORMAL(BTN_SAVE_DOC, "Cargar Documentos");

            return;
        }

        //TODOS LOS ARCHIVOS SUBIDOS
        $.each(JSON_CARGA.data, function (ID, ELEMENT) {
            FILE_UPDATE(ELEMENT);
        });

        //ESTADO BOTONES
        BTN_NORMAL(BTN_ADD_FILE, "<i class='fas fa-plus-circle'></i> Agregar archivo");
        BTN_NORMAL(BTN_LOAD_FILE, "<i class='fas fa-sync'></i> Desde Solicitud");
        BTN_NORMAL(BTN_SAVE_DOC, "Cargar Documentos");

    });
}


function LOAD_SOLICITUD(ID_INSCRIPCION)
{
    //CARGAR TODA LA INFO NECESARIA
    $.when(GET_TIPOS_ARCHIVOS(), GET_DOCUMENTOS_INSCRIPCION(ID_INSCRIPCION)).then(function (REQ_TIPOS_ARCHIVOS, REQ_ARCHIVOS_SOLICITUD) {

        //LOS DATOS
        var JSON_TIPOS = REQ_TIPOS_ARCHIVOS[0];
        var JSON_DOCUMENTOS = REQ_ARCHIVOS_SOLICITUD[0];

        //ALMACENAMOS LOS TIPOS DE ARCHIVOS EN SESSION HTML5
        localStorage.setItem('json-tipo-archivo', JSON.stringify(JSON_TIPOS));

        //TODOS LOS ARCHIVOS SUBIDOS
        $.each(JSON_DOCUMENTOS.data, function (ID, ELEMENT) {
            FILE_UPDATE(ELEMENT);
        });

        //ESTADO BOTONES
        BTN_NORMAL($('#btn-add-file'), "<i class='fas fa-plus-circle'></i> Agregar archivo");
        BTN_NORMAL($('#btn-load-from-solicitud'), "<i class='fas fa-sync'></i> Desde Solicitud");
        BTN_NORMAL($('#btn-cargar-documentos'), "Cargar Documentos");
    });
}

function BTN_NORMAL(BTN, TEXT_BTN)
{
    $(BTN).removeAttr('disabled');
    $(BTN).removeClass('disabled');
    $(BTN).html(TEXT_BTN);
}

function BTN_CARGANDO(BTN)
{
    $(BTN).attr('disabled', true);
    $(BTN).addClass('disabled');
    $(BTN).html("<i class='fas fa-spinner fa-spin'></i> Cargando");
}

function FILE_UPDATE(JSON_ARCHIVO)
{
    //CONTAMOS LOS ARCHIVOS AGREGADOS Y LE SUMAMOS UNO
    var NEW_ID = $(".caja-archivo").length + 1;

    //EL NUEVO ID DE LA CAJA
    var ID_ROW = "caja-archivo-" + NEW_ID;

    var DIV_COL = $('<div>')
        .addClass('col-lg-12 py-1 caja-archivo is-update')
        .attr('id', ID_ROW)
        .attr('data-archivo', JSON_ARCHIVO.ID_DOCUMENTO);

    var DIV_CARD = $('<div>').addClass('card');
    var DIV_CARD_BODY = $('<div>').addClass('card-body');

    var DIV_ROW = $('<div>').addClass('row');

    $(DIV_ROW).append(GET_DIV_DOWNLOAD(JSON_ARCHIVO.ID_DOCUMENTO));
    $(DIV_ROW).append(GET_SELECT_ARCHIVO_UPD(JSON_ARCHIVO.ID_TIPO_ARCHIVO_INSCRIPCION));
    $(DIV_ROW).append(GET_DIV_DELETE(ID_ROW));

    $(DIV_CARD).append(DIV_CARD_BODY);
    $(DIV_CARD_BODY).append(DIV_ROW);
    $(DIV_COL).append(DIV_CARD);

    $("#files").append(DIV_COL);
}

function CLICK_ADD_FILE()
{
    var NEW_ID = $(".caja-archivo").length + 1;
    var ID_ROW = "caja-archivo-" + NEW_ID;

    var DIV_COL = $('<div>').addClass('col-lg-12 py-1 caja-archivo').attr('id', ID_ROW);
    var DIV_CARD = $('<div>').addClass('card');
    var DIV_CARD_BODY = $('<div>').addClass('card-body');
    var DIV_ROW = $('<div>').addClass('row');

    $(DIV_ROW).append(GET_DIV_INPUT());
    $(DIV_ROW).append(GET_SELECT_ARCHIVO());
    $(DIV_ROW).append(GET_DIV_DELETE(ID_ROW));

    $(DIV_CARD).append(DIV_CARD_BODY);
    $(DIV_CARD_BODY).append(DIV_ROW);
    $(DIV_COL).append(DIV_CARD);

    $("#files").append(DIV_COL);
}

function GET_DIV_INPUT()
{
    var DIV = $('<div>').addClass('col-5');
    var INPUT_FILE = $('<input>').attr('type', 'file').addClass('form-control-file form-control-sm');

    $(DIV).append(INPUT_FILE);

    return DIV;
}

function GET_DIV_DOWNLOAD(ID)
{
    var DIV = $('<div>').addClass('col-5');
    var INPUT_FILE = $('<a>').addClass('btn btn-primary btn-sm btn-block').attr('href', '/solicitud/mis-archivos-solicitud/' + ID).html('<i class="fas fa-file-download"></i> Descargar');

    $(DIV).append(INPUT_FILE);

    return DIV;
}

function GET_SELECT_ARCHIVO()
{
    var DIV = $('<div>').addClass('col-5');
    var LABEL = $('<label>').addClass('col-2 col-form-label col-form-label-sm').html('Tipo');
    var SELECT = $('<select>').addClass('form-control form-control-sm');

    var DIV_SELECT = $('<div>').addClass('col-10').append(SELECT);
    var DIV_FORM_GROUP = $('<div>').addClass('form-group row').append(LABEL).append(DIV_SELECT);

    COMPLETAR_SELECT_TIPO_ARCHIVO(SELECT);

    $(DIV).append(DIV_FORM_GROUP);

    return DIV;
}

function GET_SELECT_ARCHIVO_UPD(ID_TIPO_ARCHIVO)
{
    var DIV = $('<div>').addClass('col-5');
    var LABEL = $('<label>').addClass('col-2 col-form-label col-form-label-sm').html('Tipo');
    var SELECT = $('<select>').addClass('form-control form-control-sm');

    var DIV_SELECT = $('<div>').addClass('col-10').append(SELECT);
    var DIV_FORM_GROUP = $('<div>').addClass('form-group row').append(LABEL).append(DIV_SELECT);

    COMPLETAR_SELECT_TIPO_ARCHIVO_UPD(SELECT, ID_TIPO_ARCHIVO);

    $(DIV).append(DIV_FORM_GROUP);

    return DIV;
}

function GET_DIV_DELETE(ID_CAJITA)
{
    var DIV = $('<div>').addClass('col-1');
    var BTN = $('<button>').addClass('btn btn-danger btn-sm btn-block').attr({ 'type': 'button', 'onclick': 'CLICK_BORRAR_CAJA("' + ID_CAJITA + '")' }).html('<i class="fas fa-trash-alt"></i>');

    $(DIV).append(BTN);

    return DIV;
}

function CLICK_BORRAR_CAJA(ID_DIV)
{
    $('#' + ID_DIV).remove();
}

function COMPLETAR_SELECT_TIPO_ARCHIVO(SELECT)
{
    $(SELECT).append($('<option>').html('Cargando...'));

    //SE CARGA EL SELECT SI YA HAY DATOS EN ALMACEN HTML5...                                                                   
    if (localStorage.getItem('json-tipo-archivo') !== null)
    {
        var JSON_LOCAL_STORE = JSON.parse(localStorage.getItem('json-tipo-archivo'));
        CARGAR_SELECT(JSON_LOCAL_STORE, SELECT);

        //SALIDA FORZADA...
        return;
    }

    //SI NO EXISTEN DATOS EN ALMACCEN, SE BUSCA AL SERVIDOR
    $.when(GET_TIPOS_ARCHIVOS()).then(function (JSON_TIPOS, textStatus, jqXHR)
    {
        localStorage.setItem('json-tipo-archivo', JSON.stringify(JSON_TIPOS));

        var JSON_LOCAL_STORE = JSON.parse(localStorage.getItem('json-tipo-archivo'));

        CARGAR_SELECT(JSON_LOCAL_STORE, SELECT);
    });
}

function COMPLETAR_SELECT_TIPO_ARCHIVO_UPD(SELECT, ID_TIPO_ARCHIVO)
{
    $(SELECT).append($('<option>').html('Cargando...'));

    //SE CARGA EL SELECT SI YA HAY DATOS EN ALMACEN HTML5                                                                    
    if (localStorage.getItem('json-tipo-archivo') !== null)
    {
        //OBTIENE LOS DATOS
        var JSON_TIPOS_STORE = JSON.parse(localStorage.getItem('json-tipo-archivo'));
        CARGAR_SELECT(JSON_TIPOS_STORE, SELECT);

        //PONER EL SELECT EN POSICION
        $(SELECT).val(ID_TIPO_ARCHIVO);

        //SALIDA FORZADA...
        return;
    }

    //SI NO EXISTEN DATOS EN ALMACCEN, SE BUSCA AL SERVIDOR
    $.when(GET_TIPOS_ARCHIVOS()).then(function (JSON_TIPOS, textStatus, jqXHR)
    {
        localStorage.setItem('json-tipo-archivo', JSON.stringify(JSON_TIPOS));

        var JSON_TIPOS_STORE = JSON.parse(localStorage.getItem('json-tipo-archivo'));
        CARGAR_SELECT(JSON_TIPOS_STORE, SELECT);

        //PONER EL SELECT EN POSICION
        $(SELECT).val(ID_TIPO_ARCHIVO);
    });

}

function CARGAR_SELECT(DATA, SELECT)
{
    $(SELECT).empty();

    $.each(DATA, function (ID, ELEMENT) {
        $(SELECT).append($('<option>').attr('value', ELEMENT.ID_TIPO_ARCHIVO_INSCRIPCION).html(ELEMENT.TIPO_ARCHIVO.NOMBRE));
    });
}

function GET_TIPOS_ARCHIVOS()
{
    return $.ajax({
        url: "/inscripcion/tipos-archivos-inscripcion-ajax",
        type: 'POST',
        dataType: 'json'
    });
}

function CARGAR_ARCHIVOS_SOLICITUD(ID_INSCRIPCION)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/cargar-archivos-ajax",
        data: JSON.stringify({ ID_INSCRIPCION: parseInt(ID_INSCRIPCION) }),
        dataType: "json"
    });
}

function GET_DOCUMENTOS_INSCRIPCION(ID_INSCRIPCION)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/documentos-inscripcion-ajax",
        data: JSON.stringify({ ID_INSCRIPCION: parseInt(ID_INSCRIPCION) }),
        dataType: "json"
    });
}

function CLICK_SAVE()
{
    var ID_INSCRIPCION = parseInt($('#id-inscripcion').text());

    var OBJETO = new FormData();
    var JSON_OBJECT = { ID_INSCRIPCION : ID_INSCRIPCION };
    var VALIDAR = { valid: true, message: "" };

    var ARCHIVOS_ACTUALIZAR = [];
    var TIPOS_NUEVOS = [];
    var TIPOS_ACTUALIZAR = [];

    //LOS ARCHIVOS SUBIDOS
    $.each($('.caja-archivo'), function (ID, ELEMENT)
    {
        //EL TIPO DE ARCHIVO
        var TIPO_ARCHIVO = $(ELEMENT).find('select')[0];

        //SI ES UN ARCHIVO A ACTUALIZAR
        if ($(ELEMENT).hasClass('is-update'))
        {
            //EL ID DEL ARCHIVO A ACTUALIZAR
            var ID_ARCHIVO = parseInt($(ELEMENT).data('archivo'));

            //SE AÑADEN LOS ARCHIVOS AL ARREGLO
            ARCHIVOS_ACTUALIZAR.push(ID_ARCHIVO);
            TIPOS_ACTUALIZAR.push(parseInt($(TIPO_ARCHIVO).val()));
        }
        else
        {
            //EL ARCHIVO
            var ARCHIVO = $(ELEMENT).find('input[type="file"]')[0];

            //SE AÑADEN
            VALIDAR = VALIDAR_ARCHIVO(ARCHIVO);

            //SE AÑADEN LOS ARCHIVOS AL FORM
            OBJETO.append("FILES", ARCHIVO.files[0]);
            TIPOS_NUEVOS.push(parseInt($(TIPO_ARCHIVO).val()));
        }
    });

    JSON_OBJECT.ARCHIVOS_ACTUALIZAR = ARCHIVOS_ACTUALIZAR;
    JSON_OBJECT.TIPO_ARCHIVO_ACTUALIZAR = TIPOS_ACTUALIZAR;
    JSON_OBJECT.TIPO_ARCHIVO_NUEVO = TIPOS_NUEVOS;

    //SE PASA EL OBJETO A STRING
    var JSON_OBJECT_STRING = JSON.stringify(JSON_OBJECT);

    //SE AÑADEN EL JSON CON PARAMETROS
    OBJETO.append("JSON_PARAMETROS", JSON_OBJECT_STRING);

    //SI LA VALIDACION ES FALSE
    if (VALIDAR.valid === false)
    {
        alert(VALIDAR.message);
        return;
    }

    //GUARDAR DOCUMENTACIÓN
    GUARDAR_DOCUMENTACION(OBJETO);
}

function VALIDAR_ARCHIVO(ARCHIVO)
{
    var SALIDA = {};
    SALIDA.valid = true;
    SALIDA.message = "";

    if (ARCHIVO.files.length === 0)
    {
        SALIDA.valid = false;
        SALIDA.message = "Existe campo(s) sin archivo adjunto";
    }

    return SALIDA;
}

function GUARDAR_DOCUMENTACION(OBJETO)
{
    $.ajax({
        beforeSend: function (xhr)
        {
            $('#label-status').html('<i class="fas fa-spinner fa-spin"></i> Subiendo...').removeClass();
            $('#row-upload-ajax').css('display', 'flex');
        },
        url: "/inscripcion/guardar-enviar-documentos-ajax",
        type: "POST",
        data: OBJETO,
        dataType: 'json',
        contentType: false,
        cache: false,
        processData: false,
        success: function (RES)
        {
            var CLASE = "text-danger";

            if (RES.status) CLASE = "text-success";

            $('#label-status').addClass(CLASE);
            $('#label-status').text(RES.message);
        }
    });
}

function HTML_ALERTA(CLASS, MENS)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PARTIAL_ALERT",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');
        }
    });
}

