﻿$(document).ready(function () {
    localStorage.removeItem('json-tipo-archivo');

    // FUNCION DESDE: Solicitud_Mis_Solicitudes.js
    LOAD_MIS_SOLICITUDES();
    // FUNCION DESDE: Solicitud_Mis_Solicitudes.js

    //SE AGREGAN LOS INPUTS POR DEFECTO
    LOAD_TIPOS_REQUERIDOS();
});

function LOAD_TIPOS_REQUERIDOS()
{
    //TIPOS REQUERIDOS AL SERVIDOR
    $.when(GET_TIPOS_REQUERIDOS()).then(function (JSON_REQ, textStatus, jqXHR) {
        //TIPOS A VALIDAR
        var JSON_TIPOS = JSON_REQ.data;

        $.each(JSON_TIPOS, function (ID, ELEMENT) {
            var TIPO = ELEMENT.TIPO_ARCHIVO;
            ADD_FILE_STATIC_FILE(ELEMENT.ID_TIPO_ARCHIVO_SOLICITUD, TIPO.NOMBRE);
        });

    });

}

function ADD_FILE_STATIC_FILE(VALUE, TEXT)
{
    //CONTAMOS LOS ARCHIVOS AGREGADOS Y LE SUMAMOS UNO
    var NEW_ID = $(".caja-archivo").length + 1;

    //EL NUEVO ID DE LA CAJA
    var ID_ROW = "caja-archivo-" + NEW_ID;

    var DIV_COL = $('<div>').addClass('col-lg-12 py-1 caja-archivo').attr('id', ID_ROW);
    var DIV_ROW = $('<div>').addClass('row');

    $(DIV_ROW).append(GET_DIV_INPUT());
    $(DIV_ROW).append(GET_SELECT_ARCHIVO_STATIC(VALUE, TEXT));
    //$(DIV_ROW).append(GET_DIV_DELETE(ID_ROW));

    $(DIV_COL).append(DIV_ROW);
    $("#files").append(DIV_COL);
}

function CLICK_ADD_FILE()
{
    //CONTAMOS LOS ARCHIVOS AGREGADOS Y LE SUMAMOS UNO
    var NEW_ID = $(".caja-archivo").length + 1;

    //EL NUEVO ID DE LA CAJA
    var ID_ROW = "caja-archivo-" + NEW_ID;

    var DIV_COL = $('<div>').addClass('col-lg-12 py-1 caja-archivo').attr('id', ID_ROW);
    var DIV_CARD = $('<div>').addClass('card');
    var DIV_CARD_BODY = $('<div>').addClass('card-body p-2');
    var DIV_ROW = $('<div>').addClass('row');

    $(DIV_ROW).append(GET_DIV_INPUT());
    $(DIV_ROW).append(GET_SELECT_ARCHIVO());
    $(DIV_ROW).append(GET_DIV_DELETE(ID_ROW));

    //$(DIV_CARD_BODY).append(DIV_ROW);
    //$(DIV_CARD).append(DIV_CARD_BODY);
    $(DIV_COL).append(DIV_ROW);

    $("#files").append(DIV_COL);
}

function GET_DIV_INPUT()
{
    var DIV = $('<div>').addClass('col-6');
    var INPUT = $('<input>').attr('type', 'file').addClass('form-control-file form-control-sm px-0');

    $(DIV).append(INPUT);

    return DIV;
}

function GET_SELECT_ARCHIVO_STATIC(VALUE, TEXT)
{
    var DIV = $('<div>').addClass('col-4');
    var SELECT = $('<select>').addClass('form-control form-control-sm select-text-spiev').attr('disabled', true);

    //AÑADIR OPCION ESTATICA
    $(SELECT).append($('<option>').attr('value', VALUE).html(TEXT));

    var DIV_SELECT = $('<div>').addClass('col-12').append(SELECT);
    var DIV_FORM_GROUP = $('<div>').addClass('form-group row mb-0').append(DIV_SELECT);
    $(DIV).append(DIV_FORM_GROUP);

    return DIV;
}

function GET_SELECT_ARCHIVO()
{

    var DIV = $('<div>').addClass('col-4');
    //var LABEL = $('<label>').addClass('col-2 col-form-label col-form-label-sm').html('Tipo');
    var SELECT = $('<select>').addClass('form-control form-control-sm');

    var DIV_SELECT = $('<div>').addClass('col-12').append(SELECT);
    var DIV_FORM_GROUP = $('<div>').addClass('form-group row mb-0').append(DIV_SELECT);

    COMPLETAR_SELECT_TIPO_ARCHIVO(SELECT);

    $(DIV).append(DIV_FORM_GROUP);

    return DIV;
}

function GET_DIV_DELETE(ID)
{
    var DIV = $('<div>').addClass('col-2'); 
    var BTN = $('<button>').addClass('btn btn-danger btn-sm btn-block').attr({ 'type': 'button', 'onclick': 'CLICK_BORRAR_CAJA("' + ID + '")'}).html('<i class="fas fa-trash-alt"></i>');

    $(DIV).append(BTN);

    return DIV;
}

function CLICK_BORRAR_CAJA(ID)
{
    $('#' + ID).remove();
}

function COMPLETAR_SELECT_TIPO_ARCHIVO(SELECT)
{
    $(SELECT).append($('<option>').html('Cargando...'));

    //SE CARGA EL SELECT SI YA HAY DATOS EN ALMACEN HTML5                                                                    
    if (localStorage.getItem('json-tipo-archivo') !== null)
    {
        var JSON_TIPO_ARCHIVO = JSON.parse(localStorage.getItem('json-tipo-archivo'));
        CARGAR_SELECT(JSON_TIPO_ARCHIVO, SELECT);
        return;
    }

    //SI NO SE BUSCA AL SERVIDOR
    $.when(GET_TIPOS_ARCHIVOS()).then(function (data, textStatus, jqXHR) {

        localStorage.setItem('json-tipo-archivo', JSON.stringify(data));
        var JSON_TIPO_ARCHIVO = JSON.parse(localStorage.getItem('json-tipo-archivo'));
        CARGAR_SELECT(JSON_TIPO_ARCHIVO, SELECT);

    });
}

function CARGAR_SELECT(DATA, SELECT)
{
    $(SELECT).empty();

    $.each(DATA, function (ID, ELEMENT) {
        $(SELECT).append($('<option>').attr('value', ELEMENT.ID_TIPO_ARCHIVO_SOLICITUD).html(ELEMENT.TIPO_ARCHIVO.NOMBRE));
    });
}

function GET_TIPOS_ARCHIVOS()
{
    return $.ajax({
        url: "/solicitud/tipos-archivos-ajax-crear",
        type: 'POST',
        dataType: 'json'
    });
}

function IS_IN_ARRAY(value, array)
{
    return array.indexOf(value) > -1;
}

function CLICK_CREAR()
{
    var ID_SUCURSAL = parseInt($('#Sucursales').val());
    var ID_FINANCIERA = parseInt($('#Financieras').val());
    var RUT_ADQUIRIENTE = $('#rut-adquiriente').val();
    var CORREO_ADQUIRIENTE = $('#correo-adquiriente').val();
    var COMENTARIO = $('#comentario-solicitud').val();

    var TERMINACION_OPC1 = parseInt($('#TerminacionOPC1').val());
    TERMINACION_OPC1 = isNaN(TERMINACION_OPC1) ? null : TERMINACION_OPC1;

    var TERMINACION_OPC2 = parseInt($('#TerminacionOPC2').val());
    TERMINACION_OPC2 = isNaN(TERMINACION_OPC2) ? null : TERMINACION_OPC2;

    var ARRAY_SERVICIOS = $("input[name='servicios']:checked").map(function () {
        return parseInt($(this).val());
    }).get();

    var OBJETO = new FormData();

    var JSON_OBJECT =
    {
        ID_SUCURSAL: ID_SUCURSAL,
        ID_FINANCIERA: ID_FINANCIERA === 0 ? null : ID_FINANCIERA,
        RUT_ADQUIRIENTE: RUT_ADQUIRIENTE,
        CORREO_ADQUIRIENTE: CORREO_ADQUIRIENTE,
        COMENTARIO: COMENTARIO,
        TIPO_ARCHIVO_NUEVO: null,
        SERVICIO_NUEVO: ARRAY_SERVICIOS,
        TERMINACION_OPC1: TERMINACION_OPC1,
        TERMINACION_OPC2: TERMINACION_OPC2
    };

    //TIPOS REQUERIDOS AL SERVIDOR
    $.when(GET_TIPOS_REQUERIDOS()).then(function (JSON_REQ, textStatus, jqXHR)
    {
        //TIPOS A VALIDAR
        var JSON_TIPOS = JSON_REQ.data;

        //NOMBRES DE LOS ARCHIVOS REQUERIDOS
        var JSON_NOMBRES = JSON_TIPOS.map(function (obj) {
            return obj.TIPO_ARCHIVO.NOMBRE;
        });

        var VALIDAR = { valid: false, message: "Favor adjunte " + JSON_NOMBRES.join(', ') };

        var TIPOS_NUEVOS = [];

        //LOS ARCHIVOS SUBIDOS
        $.each($('.caja-archivo'), function (ID, ELEMENT) {

            //EL ARCHIVO
            var ARCHIVO = $(ELEMENT).find('input[type="file"]')[0];

            //EL TIPO DE ARCHIVO
            var TIPO_ARCHIVO = $(ELEMENT).find('select')[0];

            //SE AÑADEN LOS TIPOS DE ARCHIVO
            TIPOS_NUEVOS.push(parseInt($(TIPO_ARCHIVO).val()));

            //SE AÑADEN
            VALIDAR = VALIDAR_ARCHIVOS(ARCHIVO);

            //SE AÑADEN LOS ARCHIVOS AL FORM
            OBJETO.append("FILES", ARCHIVO.files[0]);

        });

        //TIPOS DE ARCHIVOS QUE COINCIDEN CON LOS ADJUNTOS...
        var FILTRO = JSON_TIPOS.filter(function (ITEM) {
            return IS_IN_ARRAY(ITEM.ID_TIPO_ARCHIVO_SOLICITUD, TIPOS_NUEVOS);
        });

        //SI FALTAN ARCHIVOS NECESARIOS
        if (FILTRO.length < JSON_TIPOS.length) VALIDAR = { valid: false, message: "Favor adjunte " + JSON_NOMBRES.join(', ') };

        //SE COMPLETA EL OBJETO
        JSON_OBJECT.TIPO_ARCHIVO_NUEVO = TIPOS_NUEVOS;

        //SE PASA EL OBJETO A STRING
        var JSON_OBJECT_STRING = JSON.stringify(JSON_OBJECT);

        //SE AÑADEN EL JSON CON PARAMETROS
        OBJETO.append("JSON_PARAMETROS", JSON_OBJECT_STRING);

        //SI LA VALIDACION ES FALSE
        if (VALIDAR.valid === false)
        {
            alert(VALIDAR.message);
            return;
        }

        CREAR_SOLICITUD(OBJETO);
    });

}

function VALIDAR_ARCHIVOS(ARCHIVO)
{
    var SALIDA = {};
    SALIDA.valid = true;
    SALIDA.message = "";

    if (ARCHIVO.files.length === 0)
    {
        SALIDA.valid = false;
        SALIDA.message = "Existe mas o un campo sin archivo adjunto";
    }

    return SALIDA;
}


function CREAR_SOLICITUD(OBJETO)
{
    $.ajax({
        beforeSend: function (xhr)
        {
            $('#label-status').html('<i class="fas fa-spinner fa-spin"></i> Subiendo...').removeClass();
            $('#row-upload-ajax').css('display', 'flex');
        },
        url: "/solicitud/crear-ajax",
        type: "POST",
        data: OBJETO,
        dataType: 'json',
        contentType: false,
        cache: false,
        processData: false,
        success: function (RES)
        {
            var CLASE = "text-danger";

            if (RES.status) CLASE = "text-success";

            $('#label-status').addClass(CLASE);
            $('#label-status').text(RES.message);

            //ALERTA EN PANTALLA
            alert(RES.message);

            //RECARGAR LA PAGINA
            if(RES.status) location.reload();
        }
    });
}

function GET_TIPOS_REQUERIDOS()
{
    return $.ajax({
        url: "/solicitud/tipos-requeridos-solicitud",
        type: 'GET',
        dataType: 'json'
    });
}