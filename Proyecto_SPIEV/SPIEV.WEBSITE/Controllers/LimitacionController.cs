﻿using SPIEV.REPOS;
using SPIEV.WEBSITE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace SPIEV.WEBSITE.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("limitacion")]
    public class LimitacionController : Controller
    {
        // GET: Limitacion
        public ActionResult INDEX()
        {
            return View();
        }

        [Route("limitaciones-todas")]
        [HttpGet]
        public ActionResult LIMITACION_TODO()
        {
            return View();
        }

        // GET: Registro
        [Route("desde-solicitud/{ID_SOLICITUD:long}")]
        [HttpGet]
        public ActionResult LIMITACION_SOLICITUD(long? ID_SOLICITUD)
        {
            //DATOS PARA LA VISTA...
            SPIEV_VM MODEL = new SPIEV_VM()
            {
                ID_SOLICITUD = ID_SOLICITUD,
                LISTA_COMUNAS = COMUNA_R.TODAS_COMUNA().ToList(),

                VEHICULO = new VEHICULO_VM()
                {
                    LISTA_TIPOS_COMBUSTIBLE = TIPO_COMBUSTIBLE_VEHICULO_R.TODOS_COMBUSTIBLE().ToList(),
                    LISTA_TIPOS_VEHICULO = TIPO_VEHICULO_R.TODOS_WEB().ToList(),
                    LISTA_TIPOS_MEDIDA = TIPO_MEDIDA_VEHICULO_R.TODOS_TIPOS_MEDIDA().ToList(),
                    LISTA_COLORES_VEHICULO = COLOR_VEHICULO_R.TODOS_COLOR_VEHICULO().ToList(),
                    LISTA_TIPOS_POTENCIA = TIPO_POTENCIA_R.TODOS_TIPO_POTENCIA().ToList(),
                    LISTA_TIPOS_CARROCERIA = TIPO_CARROCERIA_R.TODOS_TIPOS_CARROCERIA().ToList()
                },

                COMUNIDAD = new COMUNIDAD_VM()
                {
                    LISTA_OPCIONES_COMUNIDAD = OPCION_COMUNIDAD_R.TODOS_OPCION_COMUNIDAD().ToList()
                },

                FACTURA = new FACTURA_VM()
                {
                    LISTA_TIPOS_MONEDAS = TIPO_MONEDA_R.TODOS_TIPO_MONEDA().ToList()
                },

                OPERADOR = new OPERADOR_VM()
                {
                    LISTA_REGIONES = REGION_R.TODOS_REGION().ToList()
                },
                SOLICITANTE = new SOLICITANTE_VM()
                {
                    LISTA_SOLICITANTE = SOLICITANTE_R.TODOS_SOLICITANTE().ToList()
                }
            };

            return View(MODEL);
        }

        [Route("limitacion-todas-ajax")]
        [HttpPost]
        public string LISTA_INSCRIPCION_AJAX()
        {
            return JsonConvert.SerializeObject(LIMITACION_R.TODO_LIMITACION());
        }


    }
}