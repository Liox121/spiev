﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.WEBSITE.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;

namespace SPIEV.WEBSITE.Controllers
{
    [RoutePrefix("")]
    public class HomeController : Controller  
    {
        [Route]
        public ActionResult INDEX()
        {
            if (TempData["ERROR_LOGIN"] != null) ViewBag.ERROR_LOGIN = TempData["ERROR_LOGIN"].ToString();

            return View();
        }


        [HttpPost] //SOLO PETICIONES POST
        //[ValidateAntiForgeryToken] //PARA ATAQUES CSRF
        public ActionResult INGRESO(USUARIO_VM MODEL)
        {
            MD5 md5Hash = MD5.Create();


            string HASHPASS = USUARIO_R.GetMd5Hash(md5Hash, MODEL.CLAVE);

            bool EXISTE_USUARIO = USUARIO_R.LOGIN(MODEL.NOMBRE_USUARIO, HASHPASS);
            

            //SI EXISTE ENVIA AL USUARIO A LA PAGINA CORRESPONDIENTE
            if (EXISTE_USUARIO)
            {
               
                //USUARIO LOGEADO
                USUARIO USER = USUARIO_R.TODOS().First(a => a.NOMBRE_USUARIO == MODEL.NOMBRE_USUARIO && a.CLAVE == HASHPASS);

                //SI ESTA INACTIVO
                if (USER.ACTIVO == false)
                {
                    TempData["ERROR_LOGIN"] = "El usuario se encuentra inactivo, contacte con el administrador";
                    return RedirectToAction("Index");
                }

                //CREAR LA SESION
                SESSION_WEB.CREATE_SESSION(MODEL.NOMBRE_USUARIO, HASHPASS);

                //PAGINA POR DEFECTO
                PAGINA PAGINA_DEFECTO = new PAGINA();

                //LISTA DE PERFILES POR USUARIO
                List<USUARIO_PERFIL_SESSION> PERFILES_USUARIO = SESSION_WEB.SESION_USER.MIS_PERFILES;

                //SI EXISTEN PERFILES ASOCIADOS AL USUARIO
                if (PERFILES_USUARIO.Any())
                {
                    //PRIMER PERFIL ASOCIADO AL USUARIO
                    USUARIO_PERFIL_SESSION PRIMER_PERFIL = PERFILES_USUARIO.First();

                    //SI EXISTE UN PERFIL POR DEFECTO
                    if (PERFILES_USUARIO.Any(n => n.POR_DEFECTO)) PRIMER_PERFIL = PERFILES_USUARIO.First(n => n.POR_DEFECTO);

                    //SI EXISTEN PAGINAS ASOCIADAS AL PERFIL
                    if (PRIMER_PERFIL.PERFIL.PAGINAS.Any())
                    {
                        PAGINA_DEFECTO = PRIMER_PERFIL.PERFIL.PAGINAS.First().PAGINA;

                        //SI EXISTE UNA PAGINA POR DEFECTO
                        if (PRIMER_PERFIL.PERFIL.PAGINAS.Any(n => n.POR_DEFECTO))
                        {
                            PAGINA_DEFECTO = PRIMER_PERFIL.PERFIL.PAGINAS.First(n => n.POR_DEFECTO).PAGINA;
                        }
                    }

                }

                return RedirectToAction(PAGINA_DEFECTO.ACCION, PAGINA_DEFECTO.CONTROLADOR);
            }

            //SI NO ENVIA A LOGIN NUEVAMENTE
            TempData["ERROR_LOGIN"] = "Usuario y/o contraseña incorrecta";
            return RedirectToAction("Index");
        }

        [SESION_VALIDA]
        [HttpPost]
        public string UPDATE_SESSION_AJAX()
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            try
            {
                //DATOS DESDE LA SESION
                USUARIO_SESSION USUARIO = SESSION_WEB.SESION_USER;

                //ACTUALIZAR LA SESION
                SESSION_WEB.CREATE_SESSION(USUARIO.NOMBRE_USUARIO, USUARIO.CLAVE);

                //SALIDA OK
                SALIDA.status = true;
            }
            catch (Exception ex)
            {
                //SE ENVIA EL ERROR DE LA APLICACION
                SALIDA.message = ex.Message;
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

        [HttpPost]
        public ActionResult PARTIAL_ALERT(string Clase, string Mensaje)
        {
            PARTIAL_ALERT_VM MODEL = new PARTIAL_ALERT_VM { Clase = Clase, Mensaje = Mensaje };
            return PartialView(MODEL);
        }

        [HttpPost]
        public ActionResult PARTIAL_ALERT_INSCRIPCION_OK(long ID_INSCRIPCION)
        {
            ViewBag.ID_INSCRIPCION = ID_INSCRIPCION;
            return PartialView();
        }

        [Route("sin-permisos")]
        public ActionResult PERMISOS_INSUFICIENTES()
        {
            return View();
        }

        [HttpPost]
        [Route("logout")]
        public ActionResult LOG_OUT()
        {
            Session.Abandon();
            return Json(new { status = "done" });
        }

    }
}