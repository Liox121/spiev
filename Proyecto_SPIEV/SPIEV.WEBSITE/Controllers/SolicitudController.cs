﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.UTIL;
using SPIEV.WEBSITE.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SPIEV.WEBSITE.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("solicitud")]
    public class SolicitudController : Controller
    {
        // GET: Solicitud
        [Route]
        public ActionResult INDEX()
        {
            //LISTAS DE OPCIONES CON TERMINACION DE PPU
            String JSON_STRING = JSON_CONFIG_R.READ("SOLICITUD_OPCION_TERMINACION").JSON_OBJETO;
            List<int?> LISTA_OPCIONES = JsonConvert.DeserializeObject<List<int?>>(JSON_STRING);

            SOLICITUD_VM MODEL = new SOLICITUD_VM
            {
                SUCURSALES = SESSION_WEB.SESION_USER.MIS_SUCURSALES,
                FINANCIERAS = FINANCIERA_R.TODOS().ToList(),
                SERVICIOS = SERVICIO_R.TODOS().ToList(),
                TERMINACIONES = LISTA_OPCIONES.Select(n => new SelectListItem { Text = $"{n}", Value = $"{n}"}).ToList()
            };

            //AÑADE LA OPCION POR DEFECTO Y ORDENA...
            MODEL.DEFAULT_OPCION_FINANCIERA();
            //AÑADE LA OPCION POR DEFECTO...
            MODEL.DEFAULT_OPCION_TERMINACION();

            return View(MODEL);
        }

        [Route("pagina-inicio")]
        public ActionResult PAGINA_INICIO()
        {
            return View();
        }

        [Route("tipos-archivos-ajax")]
        [HttpPost]
        public string TIPOS_ARCHIVOS_AJAX()
        {
            return JsonConvert.SerializeObject(TIPO_ARCHIVO_SOLICITUD_R.TODOS_WEB());
        }

        [Route("tipos-archivos-ajax-crear")]
        [HttpPost]
        public string TIPOS_ARCHIVOS_AJAX_CREAR()
        {
            return JsonConvert.SerializeObject(TIPO_ARCHIVO_SOLICITUD_R.TODOS_WEB_CREAR());
        }

        [Route("mis-solicitudes")]
        public ActionResult MIS_SOLICITUDES()
        {
            return View();
        }

        [Route("mis-solicitudes/{ID_SOLICITUD:long}")]
        [HttpGet]
        public ActionResult MIS_SOLICITUDES_EDITAR(long ID_SOLICITUD)
        {
            //SE ENVIA EL ID DE SOLICITUD A LA VISTA
            ViewBag.ID_SOLICITUD = ID_SOLICITUD;

            //LISTAS DE OPCIONES CON TERMINACION DE PPU
            String JSON_STRING = JSON_CONFIG_R.READ("SOLICITUD_OPCION_TERMINACION").JSON_OBJETO;
            List<int?> LISTA_OPCIONES = JsonConvert.DeserializeObject<List<int?>>(JSON_STRING);

            //LA LISTA DE SUCURSALES DISPONIBLES
            SOLICITUD_VM MODEL = new SOLICITUD_VM
            {
                SUCURSALES = SESSION_WEB.SESION_USER.MIS_SUCURSALES,
                FINANCIERAS = FINANCIERA_R.TODOS().ToList(),
                SERVICIOS = SERVICIO_R.TODOS().ToList(),
                TERMINACIONES = LISTA_OPCIONES.Select(n => new SelectListItem { Text = $"{n}", Value = $"{n}" }).ToList()
            };

            //AÑADE LA OPCION POR DEFECTO Y ORDENA...
            MODEL.DEFAULT_OPCION_FINANCIERA();

            //AÑADE LA OPCION POR DEFECTO...
            MODEL.DEFAULT_OPCION_TERMINACION();

            return View(MODEL);
        }

        [Route("mis-solicitudes-ajax")]
        [HttpPost]
        public string MIS_SOLICITUDES_AJAX()
        {
            return JsonConvert.SerializeObject(SOLICITUD_SPIEV_R.TODOS_SOLICITUD_X_USUARIO(SESSION_WEB.SESION_USER.ID_USUARIO));
        }

        [Route("borrar-solicitud-propia-ajax")]
        [HttpPost]
        public string BORRAR_SOLICITUD_PROPIA_AJAX(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //LEER DATOS DE LA SOLICITUD
                SOLICITUD_SPIEV SOLICITUD_SPIEV = SOLICITUD_SPIEV_R.READ(ID_SOLICITUD);

                bool TIENE_INSCRIPCION = SOLICITUD_SPIEV.ID_PRIMERA_INSCRIPCION.HasValue;
                bool TIENE_LIMITACION = SOLICITUD_SPIEV.ID_LIMITACION.HasValue;

                //SI TIENE UNA INSCRIPCIÓN ASOCIADA
                if (TIENE_INSCRIPCION)
                    throw new Exception($"La solicitud {ID_SOLICITUD}, tiene un inscripción asociada");

                //SI TIENE UNA LIMITACION ASOCIADA
                if (TIENE_LIMITACION)
                    throw new Exception($"La solicitud {ID_SOLICITUD}, tiene un inscripción asociada");

                //SI LA SOLICITUD NO CORRESPONDE AL USUARIO QUE GENERA EL BORRADO...
                if (SOLICITUD_SPIEV.ID_USUARIO != SESSION_WEB.SESION_USER.ID_USUARIO)
                    throw new Exception($"La solicitud {ID_SOLICITUD}, no corresponde al usuario");

                //GENERAR EL BORRADO LOGICO
                SOLICITUD_SPIEV_R.UPDATE(new SOLICITUD_SPIEV_R.UPDATE_SOLICITUD
                {
                    ID_SOLICITUD = ID_SOLICITUD,
                    BORRADO = false
                });

                //LA RESPUESTA
                salida.status = true;
                salida.message = "Solicitud borrada, correctamente";
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("crear-ajax")]
        public string NUEVA_SOLICITUD_AJAX(HttpPostedFileBase[] FILES, string JSON_PARAMETROS)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //VARIABLES
            List<string> PATH_UPLOAD_FILES = new List<string>();
            JSON_ARCHIVO_SOLICITUD JSON_ARCHIVOS = new JSON_ARCHIVO_SOLICITUD();

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO SOBRE LAS TRANSACCIONES CONTENIDAS
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SE CONVIERTE EL JSON RECIBIDO
                    JSON_ARCHIVOS = JsonConvert.DeserializeObject<JSON_ARCHIVO_SOLICITUD>(JSON_PARAMETROS);

                    //VALIDAR LOS DATOS
                    TryValidateModel(JSON_ARCHIVOS);

                    //FILES NULOS
                    if (FILES == null)
                        ModelState.AddModelError("", "No existen archivos para subir");

                    //SERVICIOS NULOS
                    if (JSON_ARCHIVOS.SERVICIO_NUEVO == null || JSON_ARCHIVOS.SERVICIO_NUEVO.Any() == false)
                        ModelState.AddModelError("", "Seleccione al menos un servicio");

                    //TOTAL DE ARCHIVOS PDFs
                    var ARCHIVOS_PDF = FILES.Where(n => Path.GetExtension(n.FileName).ToUpper() == ".PDF");

                    //NO TODOS LOS ARCHIVOS SON PDFs
                    if (ARCHIVOS_PDF.Count() != FILES.Count())
                        ModelState.AddModelError("", "Solo se permiten archivos PDF");

                    //ENVIAR VALIDACIÓN
                    if (!ModelState.IsValid)
                        throw new Exception(ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).First());

                    int RUT_INT = Rut_Validar.ParteNumerica(JSON_ARCHIVOS.RUT_ADQUIRIENTE);
                    char DV = Rut_Validar.Digito(RUT_INT)[0];

                    //OBJETO A CREAR...
                    SOLICITUD_SPIEV SOLICITUD = new SOLICITUD_SPIEV
                    {
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        ID_SUCURSAL = JSON_ARCHIVOS.ID_SUCURSAL,
                        ID_FINANCIERA = JSON_ARCHIVOS.ID_FINANCIERA,
                        RUT_ADQUIRIENTE = RUT_INT,
                        DV_ADQUIRIENTE = DV,
                        CORREO_ADQUIRIENTE = JSON_ARCHIVOS.CORREO_ADQUIRIENTE,
                        ID_ESTADO = 1, // ESTADO: Solicitud SPIEV Ingresada
                        COMENTARIO = JSON_ARCHIVOS.COMENTARIO,
                        FECHA_SOLICITUD = DateTime.Now,
                        TERMINACION_OPC1 = JSON_ARCHIVOS.TERMINACION_OPC1,
                        TERMINACION_OPC2 = JSON_ARCHIVOS.TERMINACION_OPC2
                    };

                    //CREAR SOLICITUD SPIEV
                    SOLICITUD_SPIEV SOLICITUD_CREADA = SOLICITUD_SPIEV_R.CREATE(SOLICITUD);

                    //LA RUTA DE DESTINO
                    String DESTINO = Server.MapPath("~/Multimedia/Solicitudes");

                    //PROCESAR LOS ARCHIVOS
                    for (int i = 0; i < FILES.Count(); i++)
                    {
                        //EL ARCHIVO
                        HttpPostedFileBase ARCHIVO = FILES[i];

                        //RUTA A GUARDAR
                        String FILE_NAME = $"{Guid.NewGuid()}{Path.GetExtension(ARCHIVO.FileName)}";

                        //EL ID DEL TIPO
                        int ID_TIPO_ARCHIVO = JSON_ARCHIVOS.TIPO_ARCHIVO_NUEVO[i];

                        //GUARDAR EL ARCHIVO
                        ARCHIVO.SaveAs($"{DESTINO}/{FILE_NAME}");

                        //SE REGISTRA EL ARCHIVO SUBIDO
                        PATH_UPLOAD_FILES.Add($"{DESTINO}/{FILE_NAME}");

                        //CREAR EL ARCHIVO ASOCIADO A LA SOLICITUD
                        ARCHIVO_SOLICITUD_SPIEV_R.CREATE(new ARCHIVO_SOLICITUD_SPIEV
                        {
                            ID_SOLICITUD = SOLICITUD_CREADA.ID_SOLICITUD,
                            ID_TIPO_ARCHIVO_SOLICITUD = ID_TIPO_ARCHIVO,
                            ARCHIVO = FILE_NAME
                        });

                    }

                    //ASOCIAR LOS SERVICIOS SOLICITADOS
                    foreach (int ID_SERV in JSON_ARCHIVOS.SERVICIO_NUEVO)
                    {
                        //CREAR EL REGISTRO EN TABLA INTERMEDIA
                        SERVICIO_SOLICITUD_R.CREATE(new SERVICIO_SOLICITUD
                        {
                            ID_SERVICIO = ID_SERV,
                            ID_SOLICITUD = SOLICITUD_CREADA.ID_SOLICITUD
                        });
                    }

                    //CREAR LA HISTORIA DE LA SOLICITUD
                    HISTORIA_SOLICITUD_R.CREATE(new HISTORIA_SOLICITUD
                    {
                        ID_SOLICITUD = SOLICITUD_CREADA.ID_SOLICITUD,
                        ID_ESTADO = 1,
                        COMENTARIO = "SOLICITUD SPIEV, INGRESADA",
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        FECHA = DateTime.Now
                    });

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    //SE SETEA LA SALIDA
                    salida.status = true;
                    salida.message = "Solicitud generada correctamente";
                }
                catch (Exception ex)
                {
                    //EN CASO DE ERROR BORRAR LOS ARCHIVOS SUBIDOS
                    Parallel.ForEach(PATH_UPLOAD_FILES, FILE =>
                    {
                        System.IO.File.Delete(FILE);
                    });

                    //SE ENVIA EL ERROR
                    salida.message = ex.Message;
                }

            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("mis-detalle-ajax")]
        public string MIS_DETALLE_AJAX(int ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                SOLICITUD_SPIEV SOLICITUD = SOLICITUD_SPIEV_R.READ(ID_SOLICITUD);

                List<ARCHIVO_SOLICITUD_SPIEV> ARCHIVOS = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(ID_SOLICITUD).ToList();
                List<HISTORIA_SOLICITUD> HISTORIA_SOLICITUD = HISTORIA_SOLICITUD_R.HISTORIA_X_SOLICITUD(ID_SOLICITUD).ToList();
                List<HISTORIA_INSCRIPCION> HISTORIA_INSCRIPCION = new List<HISTORIA_INSCRIPCION>();

                //SI TIENE PRIMERA INSCRIPCIÓN ASOCIADA...
                if (SOLICITUD.ID_PRIMERA_INSCRIPCION.HasValue)
                {
                    HISTORIA_INSCRIPCION = HISTORIA_INSCRIPCION_R.HISTORIA_X_INSCRIPCION(SOLICITUD.ID_PRIMERA_INSCRIPCION.Value).ToList();
                }

                salida.status = true;
                salida.data = new
                {
                    ARCHIVOS,
                    HISTORIA_SOLICITUD,
                    HISTORIA_INSCRIPCION
                };
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("estados-solicitud-ajax")]
        public string ESTADOS_SOLICITUD_AJAX()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                salida.status = true;
                salida.data = ESTADO_SOLICITUD_SPIEV_R.TODOS_ESTADOS_SOLICITUD();
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("cambio-estado-ajax")]
        public string CAMBIO_ESTADO_AJAX(HISTORIA_SOLICITUD Historia)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //VALORES FALTANTES
                Historia.FECHA = DateTime.Now;
                Historia.ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO;

                //GENERAR EL CAMBIO DE ESTADO
                SOLICITUD_SPIEV_R.UPDATE(new SOLICITUD_SPIEV_R.UPDATE_SOLICITUD
                {
                    ID_SOLICITUD = Historia.ID_SOLICITUD,
                    ID_ESTADO = Historia.ID_ESTADO,
                });

                //GENERAR LA HISTORIA
                HISTORIA_SOLICITUD_R.CREATE(Historia);

                //LA RESPUESTA
                salida.status = true;
                salida.message = "Cambio de estado, aplicado correctamente";
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("detalle-solicitud-ajax")]
        public string DETALLE_SOLICITUD_AJAX(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                salida.data = SOLICITUD_SPIEV_R.READ(ID_SOLICITUD);
                salida.status = true;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("archivos-solicitud-ajax")]
        public string ARCHIVOS_SOLICITUD_AJAX(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                List<ARCHIVO_SOLICITUD_SPIEV> ARCHIVOS = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(ID_SOLICITUD).ToList();

                salida.status = true;
                salida.data = ARCHIVOS;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("servicios-solicitud-ajax")]
        public string SERVICIOS_SOLICITUD_AJAX(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                List<SERVICIO_SOLICITUD> SERVICIOS = SERVICIO_SOLICITUD_R.READ_X_SOLICITUD(ID_SOLICITUD).ToList();

                salida.status = true;
                salida.data = SERVICIOS;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpGet]
        [Route("mis-archivos-solicitud/{Id_Archivo:int}")]
        public FileResult MIS_ARCHIVOS_DESCARGAR(int Id_Archivo)
        {
            ARCHIVO_SOLICITUD_SPIEV archivo = ARCHIVO_SOLICITUD_SPIEV_R.READ(Id_Archivo);
            String MULTIMEDIA = Server.MapPath("~/Multimedia");
            String ARCHIVO = $"{MULTIMEDIA}/Solicitudes/{archivo.ARCHIVO}";
            
            //SI EL ARCHIVO NO EXISTE...
            ARCHIVO = (System.IO.File.Exists(ARCHIVO)) ? ARCHIVO : $"{MULTIMEDIA}/ARCHIVO_NO_ENCONTRADO.txt";

            byte[] fileBytes = System.IO.File.ReadAllBytes(ARCHIVO);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, archivo.ARCHIVO);
        }

        [HttpPost]
        [Route("editar-ajax")]
        public string EDITAR_SOLICITUD_AJAX(HttpPostedFileBase[] FILES, string JSON_PARAMETROS)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //DEFINICION DE VARIABLES
            List<string> PATH_UPLOAD_FILES = new List<string>();
            JSON_ARCHIVO_SOLICITUD JSON_ARCHIVOS = new JSON_ARCHIVO_SOLICITUD();

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO SOBRE LAS TRANSACCIONES CONTENIDAS
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SE CONVIERTE EL JSON RECIBIDO
                    JSON_ARCHIVOS = JsonConvert.DeserializeObject<JSON_ARCHIVO_SOLICITUD>(JSON_PARAMETROS);

                    //VALIDAR LOS DATOS
                    TryValidateModel(JSON_ARCHIVOS);

                    //VALIDAR EL RUT DEL DV
                    if (!Rut_Validar.ValidaRut(JSON_ARCHIVOS.RUT_ADQUIRIENTE))
                        ModelState.AddModelError("", "Rut ingresado no valido");

                    //SERVICIOS NULOS
                    if (JSON_ARCHIVOS.SERVICIO_NUEVO == null || JSON_ARCHIVOS.SERVICIO_NUEVO.Any() == false)
                        ModelState.AddModelError("", "Seleccione al menos un servicio");

                    //ENVIAR VALIDACIÓN
                    if (!ModelState.IsValid)
                        throw new Exception(ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).First());

                    //LA RUTA DE DESTINO
                    String DESTINO = Server.MapPath("~/Multimedia/Solicitudes");

                    int RUT_INT = Rut_Validar.ParteNumerica(JSON_ARCHIVOS.RUT_ADQUIRIENTE);

                    //OBJETO A ACTUALIZAR...
                    SOLICITUD_SPIEV_R.UPDATE_SOLICITUD SOLICITUD_UPDATE = new SOLICITUD_SPIEV_R.UPDATE_SOLICITUD()
                    {
                        ID_SOLICITUD = JSON_ARCHIVOS.ID_SOLICITUD,
                        ID_SUCURSAL = JSON_ARCHIVOS.ID_SUCURSAL,
                        ID_FINANCIERA = JSON_ARCHIVOS.ID_FINANCIERA,
                        RUT_ADQUIRIENTE = RUT_INT,
                        DV_ADQUIRIENTE = Rut_Validar.Digito(RUT_INT)[0],
                        CORREO_ADQUIRIENTE = JSON_ARCHIVOS.CORREO_ADQUIRIENTE,
                        COMENTARIO = JSON_ARCHIVOS.COMENTARIO,
                        ID_ESTADO = 1, //SOLICITUD INGRESADA,
                        TERMINACION_OPC1 = JSON_ARCHIVOS.TERMINACION_OPC1,
                        TERMINACION_OPC2 = JSON_ARCHIVOS.TERMINACION_OPC2
                    };

                    //CREAR LA HISTORIA DE LA SOLICITUD
                    HISTORIA_SOLICITUD_R.CREATE(new HISTORIA_SOLICITUD
                    {
                        ID_SOLICITUD = JSON_ARCHIVOS.ID_SOLICITUD,
                        ID_ESTADO = 1,
                        COMENTARIO = "SOLICITUD SPIEV, ACTUALIZADA",
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        FECHA = DateTime.Now
                    });

                    //ACTULIZACION SOLICITUD SPIEV
                    SOLICITUD_SPIEV_R.UPDATE(SOLICITUD_UPDATE);

                    //LISTA DE ARCHIVOS A BORRAR
                    List<ARCHIVO_SOLICITUD_SPIEV> ARCHIVOS_BORRAR = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(JSON_ARCHIVOS.ID_SOLICITUD).ToList();

                    //PROCESAR LOS ARCHIVOS (NUEVOS), SI EXISTEN
                    if (FILES != null)
                    {
                        //TOTAL DE ARCHIVOS PDFs
                        var ARCHIVOS_PDF = FILES.Where(n => Path.GetExtension(n.FileName).ToUpper() == ".PDF");

                        //NO TODOS LOS ARCHIVOS SON PDFs
                        if (ARCHIVOS_PDF.Count() != FILES.Count()) throw new Exception("Solo se permiten archivos PDF");

                        //CICLO SOBRE LOS ARCHIVOS...
                        for (int i = 0; i < FILES.Count(); i++)
                        {
                            //EL ARCHIVO
                            HttpPostedFileBase ARCHIVO = FILES[i];

                            //RUTA A GUARDAR
                            String FILE_NAME = $"{Guid.NewGuid()}{Path.GetExtension(ARCHIVO.FileName)}";

                            //EL ID DEL TIPO
                            int ID_TIPO_ARCHIVO = JSON_ARCHIVOS.TIPO_ARCHIVO_NUEVO[i];

                            //GUARDAR EL ARCHIVO
                            ARCHIVO.SaveAs($"{DESTINO}/{FILE_NAME}");

                            //SE REGISTRA EL ARCHIVO SUBIDO
                            PATH_UPLOAD_FILES.Add($"{DESTINO}/{FILE_NAME}");

                            //CREAR EL ARCHIVO ASOCIADO A LA SOLICITUD
                            ARCHIVO_SOLICITUD_SPIEV_R.CREATE(new ARCHIVO_SOLICITUD_SPIEV {
                                ID_SOLICITUD = JSON_ARCHIVOS.ID_SOLICITUD,
                                ID_TIPO_ARCHIVO_SOLICITUD = ID_TIPO_ARCHIVO,
                                ARCHIVO = FILE_NAME
                            });

                        }
                    }

                    //PROCESAR LOS ARCHIVOS (ACTUALIZAR), SI EXISTEN
                    for (int i = 0; i < JSON_ARCHIVOS.ARCHIVOS_ACTUALIZAR.Count(); i++)
                    {
                        //ID DE ARCHIVO...
                        int ID_ARCHIVO_SOLICITUD = JSON_ARCHIVOS.ARCHIVOS_ACTUALIZAR[i];

                        //ID DE TIPO DE ARCHIVO...
                        int ID_TIPO_ARCHIVO = JSON_ARCHIVOS.TIPO_ARCHIVO_ACTUALIZAR[i];

                        //ACTUALIZAR EL ARCHIVO
                        ARCHIVO_SOLICITUD_SPIEV_R.UPDATE(new ARCHIVO_SOLICITUD_SPIEV_R.UPDATE_ARCHIVO {
                            ID_ARCHIVO = ID_ARCHIVO_SOLICITUD,
                            ID_TIPO_ARCHIVO_SOLICITUD = ID_TIPO_ARCHIVO,
                        });

                        //SE ELIMINA EL ARCHIVO, SI ESTA EN LA LISTA DE ACTUALIZACIÓN
                        ARCHIVO_SOLICITUD_SPIEV ARCHIVO_EXCLUIR = ARCHIVOS_BORRAR.SingleOrDefault(x => x.ID_ARCHIVO == ID_ARCHIVO_SOLICITUD);
                        ARCHIVOS_BORRAR.Remove(ARCHIVO_EXCLUIR);
                    }

                    //BORRAMOS LOS ARCHIVOS QUE SE "BORRARON"
                    foreach (var ARCHIVO in ARCHIVOS_BORRAR)
                    {
                        //ACTUALIZAR EL ARCHIVO
                        ARCHIVO_SOLICITUD_SPIEV_R.UPDATE(new ARCHIVO_SOLICITUD_SPIEV_R.UPDATE_ARCHIVO
                        {
                            ID_ARCHIVO = ARCHIVO.ID_ARCHIVO,
                            BORRADO = false //AL DEFINIR FALSE SE BORRA, 0 = BORRADO, 1 = NO BORRADO
                        });
                    }

                    //PROCESAR...
                    List<SERVICIO_SOLICITUD> SERVICIO_SOLICITUD  = SERVICIO_SOLICITUD_R.READ_X_SOLICITUD(JSON_ARCHIVOS.ID_SOLICITUD).ToList();

                    //SERVICIOS...
                    List<SERVICIO_SOLICITUD> BORRAR_SERVICIOS = SERVICIO_SOLICITUD.Where(p => !JSON_ARCHIVOS.SERVICIO_NUEVO.Contains(p.ID_SERVICIO)).ToList();
                    List<long> NUEVOS_SERVICIOS = JSON_ARCHIVOS.SERVICIO_NUEVO.Where(p => !SERVICIO_SOLICITUD.Any(p2 => p2.ID_SERVICIO == p)).ToList();

                    //BORRAR
                    foreach (var BORRAR in BORRAR_SERVICIOS)
                    {
                        SERVICIO_SOLICITUD_R.DELETE(BORRAR.ID_SERVICIO_SOLICITUD);
                    }

                    //NUEVOS
                    foreach (var SERVICIO_ID in NUEVOS_SERVICIOS)
                    {
                        SERVICIO_SOLICITUD_R.CREATE(new LIB_WEB.SERVICIO_SOLICITUD
                        {
                            ID_SOLICITUD = JSON_ARCHIVOS.ID_SOLICITUD,
                            ID_SERVICIO = SERVICIO_ID
                        });
                    }

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    //SE SETEA LA SALIDA
                    salida.status = true;
                    salida.message = "Solicitud actualizada correctamente";
                }
                catch (Exception ex)
                {
                    //EN CASO DE ERROR BORRAR LOS ARCHIVOS SUBIDOS
                    Parallel.ForEach(PATH_UPLOAD_FILES, FILE =>
                    {
                        System.IO.File.Delete(FILE);
                    });

                    //SE ENVIA EL ERROR
                    salida.message = ex.Message;
                }

            }


            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("merge-documentos")]
        public string COMBINAR_PDF(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            GeneratePDF PDF_UTIL = new GeneratePDF();

            try
            {
                //LISTA DE ARCHIVOS
                var ARCHIVOS = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(ID_SOLICITUD);

                //LA RUTA DE DESTINO
                String CARPETA_ARCHIVOS = Server.MapPath("~/Multimedia/Solicitudes");

                //DONDE EL ARCHIVO EXISTA
                var ARCHIVOS_PROCESAR = ARCHIVOS.Where(n => System.IO.File.Exists($"{CARPETA_ARCHIVOS}\\{n.ARCHIVO}"));

                //SI NO EXISTEN ARCHIVOS...
                if (ARCHIVOS_PROCESAR.Any() == false) throw new Exception("Archivos de solicitud, no encontrados en el servidor");

                //SOLO LOS PDFs
                ARCHIVOS_PROCESAR = ARCHIVOS_PROCESAR.Where(n => Path.GetExtension(n.ARCHIVO).ToUpper() == ".PDF");

                //SI NO EXISTEN ARCHIVOS...
                if (ARCHIVOS_PROCESAR.Any() == false) throw new Exception("Archivos de solicitud, no existen documentos en PDF");

                //SOLO LOS NOMBRES
                var ARCHIVOS_NOMBRES = ARCHIVOS_PROCESAR.Select(n => $"{CARPETA_ARCHIVOS}\\{n.ARCHIVO}").ToArray();

                //RUTA DESTINO
                String RUTA_SALIDA = Server.MapPath("~/Multimedia/Merge");

                //FUSIONAR DOC
                PDF_UTIL.CombineMultiplePDFs(ARCHIVOS_NOMBRES, $"{RUTA_SALIDA}\\{ID_SOLICITUD}.PDF");

                //SI SE GENERO CORRECTAMENTE ENVIO PARTE DE LA RUTA
                salida.status = true;
                salida.data = $"/Multimedia/Merge/{ID_SOLICITUD}.PDF";

            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("tipos-requeridos-solicitud")]
        [HttpGet]
        public string VALIDAR_TIPOS_SOLICITUD()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SALIDA STATUS
                salida.status = true;
                salida.data = TIPO_ARCHIVO_SOLICITUD_R.VALIDACION_SOLICITUD();

            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

    }
}