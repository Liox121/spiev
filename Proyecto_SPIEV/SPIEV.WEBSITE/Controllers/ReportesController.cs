﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.WEBSITE.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;

namespace SPIEV.WEBSITE.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("reportes")]
    public class ReportesController : Controller
    {
        // GET: Reportes
        public ActionResult Index()
        {
            return View();
        }

        // GET: Reportes
        [HttpGet]
        [Route("mis-solicitudes")]
        public ActionResult MIS_SOLICITUDES()
        {
            return View();
        }

        [HttpPost]
        [Route("mis-estadisticas-inicio-ajax")]
        public string SOLICITUD_ESTADISTICA_AJAX_BY_USER()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SOLO MIS SOLICITUDES
                List<SOLICITUD_SPIEV> MIS_SOLICITUDES = SOLICITUD_SPIEV_R.TODOS_SOLICITUD_X_USUARIO(SESSION_WEB.SESION_USER.ID_USUARIO).ToList();

                //AGRUPAR SEGÚN ESTADO
                var GRUPO_POR_ESTADO = MIS_SOLICITUDES.GroupBy(
                        p => p.ESTADO.NOMBRE_ESTADO,
                        p => p,
                        (key, group) => new {  ESTADO = key, TOTAL = group.Count() });

                salida.status = true;
                salida.data = GRUPO_POR_ESTADO;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("registro-estadistica-ajax")]
        public string REGISTRO_ESTADISTICA_AJAX()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //TODAS LAS INSCRIPCIONES
                List<PRIMERA_INSCRIPCION> TODO_INSCRIPCIONES = PRIMERA_INSCRIPCION_R.TODO_INSCRIPCION().ToList();

                //SOLO SOLICITUDES INGRESADAS
                List<SOLICITUD_SPIEV> SOLICITUDES_INGRESO = SOLICITUD_SPIEV_R.TODOS_SOLICITUD_X_ESTADO(1).ToList();

                //AGRUPAR SEGÚN ESTADO
                var GRUPO_POR_ESTADO_INS = TODO_INSCRIPCIONES.GroupBy(
                        p => p.ESTADO.NOMBRE_ESTADO,
                        p => p,
                        (key, group) => new { ESTADO = key, TOTAL = group.Count() });

                //AGRUPAR SEGÚN ESTADO (SOLO UNO "Solicitud Ingresada")
                var GRUPO_POR_ESTADO_SOL = SOLICITUDES_INGRESO.GroupBy(
                        p => p.ID_ESTADO,
                        p => p,
                        (key, group) => new { ESTADO = "Solicitud Ingresada", TOTAL = group.Count() });

                //SE CONCATENAN AMBOS RESULTADOS
                var GRUPO_POR_ESTADO = GRUPO_POR_ESTADO_INS.Concat(GRUPO_POR_ESTADO_SOL);

                salida.status = true;
                salida.data = GRUPO_POR_ESTADO;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }
    }
}