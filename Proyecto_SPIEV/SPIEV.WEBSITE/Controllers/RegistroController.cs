﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.WEBSITE.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;

namespace SPIEV.WEBSITE.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("registro")]
    public class RegistroController : Controller
    {

        // GET: Registro
        [Route]
        public ActionResult INDEX()
        {
            //DATOS PARA LA VISTA...
            SPIEV_VM MODEL = new SPIEV_VM()
            {
                LISTA_COMUNAS = COMUNA_R.TODAS_COMUNA().ToList(),

                VEHICULO = new VEHICULO_VM()
                {
                    LISTA_TIPOS_COMBUSTIBLE = TIPO_COMBUSTIBLE_VEHICULO_R.TODOS_COMBUSTIBLE().ToList(),
                    LISTA_TIPOS_VEHICULO = TIPO_VEHICULO_R.TODOS_WEB().ToList(),
                    LISTA_TIPOS_MEDIDA = TIPO_MEDIDA_VEHICULO_R.TODOS_TIPOS_MEDIDA().ToList(),
                    LISTA_COLORES_VEHICULO = COLOR_VEHICULO_R.TODOS_COLOR_VEHICULO().ToList(),
                    LISTA_TIPOS_POTENCIA = TIPO_POTENCIA_R.TODOS_TIPO_POTENCIA().ToList(),
                    LISTA_TIPOS_CARROCERIA = TIPO_CARROCERIA_R.TODOS_TIPOS_CARROCERIA().ToList()
                },

                COMUNIDAD = new COMUNIDAD_VM()
                {
                    LISTA_OPCIONES_COMUNIDAD = OPCION_COMUNIDAD_R.TODOS_OPCION_COMUNIDAD().ToList()
                },

                FACTURA = new FACTURA_VM()
                {
                    LISTA_TIPOS_MONEDAS = TIPO_MONEDA_R.TODOS_TIPO_MONEDA().ToList()
                },

                OPERADOR = new OPERADOR_VM()
                {
                    LISTA_REGIONES = REGION_R.TODOS_REGION().ToList()
                },

                SOLICITANTE = new SOLICITANTE_VM()
                {
                    LISTA_CALIDAD_SOLICITANTE = CALIDAD_SOLICITANTE_R.TODOS_CALIDAD_SOLICITANTE().ToList()
                },

                ADQUIRIENTE = new ADQUIRIENTE_VM()
                {
                    LISTA_CALIDAD_ADQUIRIENTE = CALIDAD_ADQUIRIENTE_R.TODOS_CALIDAD_ADQUIRIENTE().ToList()
                },

                ESTIPULANTE = new ESTIPULANTE_VM()
                {
                    LISTA_CALIDAD_ESTIPULANTE = CALIDAD_ESTIPULANTE_R.TODOS_CALIDAD_ESTIPULANTE().ToList()
                }

            };

            //AÑADIR OPCIONES POR DEFECTO...
            //MODEL.VEHICULO.DEFAULT_OPCION();

            return View(MODEL);
        }

        [Route("pagina-inicio")]
        public ActionResult PAGINA_INICIO()
        {
            return View();
        }


        // GET: Registro
        [Route("desde-solicitud/{ID_SOLICITUD:long}")]
        [HttpGet]
        public ActionResult INSCRIPCION_SOLICITUD(long? ID_SOLICITUD)
        {
            //DATOS PARA LA VISTA...
            SPIEV_VM MODEL = new SPIEV_VM()
            {
                ID_SOLICITUD = ID_SOLICITUD,
                LISTA_COMUNAS = COMUNA_R.TODAS_COMUNA().ToList(),

                VEHICULO = new VEHICULO_VM()
                {
                    LISTA_TIPOS_COMBUSTIBLE = TIPO_COMBUSTIBLE_VEHICULO_R.TODOS_COMBUSTIBLE().ToList(),
                    LISTA_TIPOS_VEHICULO = TIPO_VEHICULO_R.TODOS_WEB().ToList(),
                    LISTA_TIPOS_MEDIDA = TIPO_MEDIDA_VEHICULO_R.TODOS_TIPOS_MEDIDA().ToList(),
                    LISTA_COLORES_VEHICULO = COLOR_VEHICULO_R.TODOS_COLOR_VEHICULO().ToList(),
                    LISTA_TIPOS_POTENCIA = TIPO_POTENCIA_R.TODOS_TIPO_POTENCIA().ToList(),
                    LISTA_TIPOS_CARROCERIA = TIPO_CARROCERIA_R.TODOS_TIPOS_CARROCERIA().ToList()
                },

                COMUNIDAD = new COMUNIDAD_VM()
                {
                    LISTA_OPCIONES_COMUNIDAD = OPCION_COMUNIDAD_R.TODOS_OPCION_COMUNIDAD().ToList()
                },

                FACTURA = new FACTURA_VM()
                {
                    LISTA_TIPOS_MONEDAS = TIPO_MONEDA_R.TODOS_TIPO_MONEDA().ToList()
                },

                OPERADOR = new OPERADOR_VM()
                {
                    LISTA_REGIONES = REGION_R.TODOS_REGION().ToList()
                },

                SOLICITANTE = new SOLICITANTE_VM()
                {
                    LISTA_CALIDAD_SOLICITANTE = CALIDAD_SOLICITANTE_R.TODOS_CALIDAD_SOLICITANTE().ToList()
                },

                ADQUIRIENTE = new ADQUIRIENTE_VM()
                {
                    LISTA_CALIDAD_ADQUIRIENTE = CALIDAD_ADQUIRIENTE_R.TODOS_CALIDAD_ADQUIRIENTE().ToList()
                },

                ESTIPULANTE = new ESTIPULANTE_VM()
                {
                    LISTA_CALIDAD_ESTIPULANTE = CALIDAD_ESTIPULANTE_R.TODOS_CALIDAD_ESTIPULANTE().ToList()
                }
            };

            return View(MODEL);
        }

        [HttpPost]
        [Route("guardar-spiev-ajax")]
        public string NUEVO_SPIEV_AJAX(SPIEV_VM MODEL)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO (EN CASO DE EXCEPCION)
            using (System.Transactions.TransactionScope SCOPE = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SI EL MODELO NO ES VALIDO DEVUELVE LA LISTA DE ERRORES
                    if (!ModelState.IsValid) throw new Exception(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).First());

                    //CREAR VEHICULO
                    VEHICULO VEHICULO_CREADO = VEHICULO_R.CREATE(MODEL.VEHICULO.OBJETO_CREAR());

                    //CREAR COMUNIDAD
                    COMUNIDAD COMUNIDAD_CREADA = COMUNIDAD_R.CREATE(MODEL.COMUNIDAD.OBJETO_CREAR());

                    //CREAR ADQUIRIENTE
                    ADQUIRIENTE ADQUIRIENTE_CREADO = ADQUIRIENTE_R.CREATE(MODEL.ADQUIRIENTE.OBJETO_CREAR());

                    //CREAR SOLICITANTE
                    SOLICITANTE SOLICITANTE_CREADO = SOLICITANTE_R.CREATE(MODEL.SOLICITANTE.OBJETO_CREAR());

                    //CREAR ESTIPULANTE
                    ESTIPULANTE ESTIPULANTE_CREADO = ESTIPULANTE_R.CREATE(MODEL.ESTIPULANTE.OBJETO_CREAR());

                    //CREAR IMPUESTO
                    IMPUESTO_ADICIONAL IMPUESTO_CREADO = IMPUESTO_ADICIONAL_R.CREATE(MODEL.IMPUESTO.OBJETO_CREAR());

                    //CREAR FACTURA
                    FACTURA FACTURA_CREADA = FACTURA_R.CREATE(MODEL.FACTURA.OBJETO_CREAR());

                    //CREAR OBSERVACION
                    OBSERVACION OBSERVACION_CREADA = OBSERVACION_R.CREATE(MODEL.OBSERVACION.OBJETO_CREAR());

                    //CREAR OPERADOR
                    OPERADOR OPERADOR_CREADO = OPERADOR_R.CREATE(MODEL.OPERADOR.OBJETO_CREAR());

                    LIB_WEB.SPIEV SPIEV = new LIB_WEB.SPIEV
                    {
                        ID_VEHICULO = VEHICULO_CREADO.ID_VEHICULO,
                        ID_COMUNIDAD = COMUNIDAD_CREADA.ID_COMUNIDAD,
                        ID_SOLICITANTE = SOLICITANTE_CREADO.ID_SOLICITANTE,
                        ID_OBSERVACION = OBSERVACION_CREADA.ID_OBSERVACION,
                        ID_OPERADOR = OPERADOR_CREADO.ID_OPERADOR,
                        FECHA = DateTime.Now
                    };

                    //CREAR SPIEV
                    LIB_WEB.SPIEV SPIEV_CREADO = SPIEV_R.CREATE(SPIEV);

                    //CREAR PRIMERA INSCRIPCION
                    PRIMERA_INSCRIPCION INSCRIPCION_CREADA = PRIMERA_INSCRIPCION_R.CREATE(new PRIMERA_INSCRIPCION
                    {
                        ID_SPIEV = SPIEV_CREADO.ID_SPIEV,
                        ID_ESTADO = 1,
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        ID_ESTIPULANTE = ESTIPULANTE_CREADO.ID_ESTIPULANTE,
                        ID_ADQUIRIENTE = ADQUIRIENTE_CREADO.ID_ADQUIRIENTE,
                        ID_IMPUESTO_ADICIONAL = IMPUESTO_CREADO.ID_IMPUESTO_ADICIONAL,
                        ID_FACTURA = FACTURA_CREADA.ID_FACTURA,
                    });

                    //SI VIENE DESDE UNA SOLICITUD
                    if (MODEL.ID_SOLICITUD.HasValue)
                    {
                        //ACTUALIZA EL ID DE PRIMERA INSCRIPCION EN LA SOLICITUD (ASOCIA INSCRIPCION CON SOLICITUD)
                        SOLICITUD_SPIEV_R.UPDATE(new SOLICITUD_SPIEV_R.UPDATE_SOLICITUD
                        {
                            ID_SOLICITUD = MODEL.ID_SOLICITUD.Value,
                            ID_ESTADO = 2,
                            ID_PRIMERA_INSCRIPCION = INSCRIPCION_CREADA.ID_PRIMERA_INSCRIPCION,
                        });

                        //GENERA LA HISTORIA SOBRE LA SOLICITUD AL CAMBIAR ESTADO
                        HISTORIA_SOLICITUD_R.CREATE(new HISTORIA_SOLICITUD
                        {
                            ID_SOLICITUD = MODEL.ID_SOLICITUD.Value,
                            ID_ESTADO = 2,
                            ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                            COMENTARIO = "SOLICITUD SPIEV, CON DOCUMENTACIÓN OK",
                            FECHA = DateTime.Now
                        });
                    }

                    //GENERA LA HISTORIA SOBRE LA PRIMERA INSCRIPCIÓN
                    HISTORIA_INSCRIPCION_R.CREATE(new HISTORIA_INSCRIPCION
                    {
                        ID_PRIMERA_INSCRIPCION = INSCRIPCION_CREADA.ID_PRIMERA_INSCRIPCION,
                        ID_ESTADO = 1,
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        COMENTARIO = "PRIMERA INSCRIPCIÓN, INGRESADA",
                        FECHA = DateTime.Now
                    });

                    //TERMINA LA EJECUCIÓN
                    SCOPE.Complete();

                    //SI TODO SALIO CORRECTO SE ENVIA OK
                    salida.status = true;
                    salida.data = new { ID_INSCRIPCION = INSCRIPCION_CREADA.ID_PRIMERA_INSCRIPCION };
                    salida.message = "Datos guardados exitosamente!";
                }
                catch (Exception ex)
                {
                    salida.message = ex.Message;
                }
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("tipos-vehiculos-con-carga")]
        [HttpGet]
        public string VALIDAR_TIPOS_VEHICULOS_CARGA()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SALIDA STATUS
                salida.data = VALIDACION_JQUERY_MULTIPLE_R.VALIDACION_CARGA_TIPO_VEHICULO();
                salida.status = true;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("tipos-vehiculos-con-cit")]
        [HttpGet]
        public string VALIDAR_TIPOS_VEHICULOS_CIT()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SALIDA STATUS
                salida.data = VALIDACION_JQUERY_MULTIPLE_R.VALIDACION_CIT_TIPO_VEHICULO();
                salida.status = true;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("tipos-vehiculos-con-ejes")]
        [HttpGet]
        public string VALIDAR_TIPOS_VEHICULOS_EJES()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SALIDA STATUS
                salida.data = VALIDACION_JQUERY_MULTIPLE_R.VALIDACION_EJES_TIPO_VEHICULO();
                salida.status = true;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("tipos-vehiculos-con-otra-carroceria")]
        [HttpGet]
        public string VALIDAR_TIPO_VEHICULOS_OTRA_CARROCERIA()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SALIDA STATUS
                salida.data = VALIDACION_JQUERY_MULTIPLE_R.VALIDACION_OTRA_CARROCERIA_TIPO_VEHICULO();
                salida.status = true;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("tipos-vehiculos-con-potencia")]
        [HttpGet]
        public string VALIDAR_TIPO_VEHICULOS_POTENCIA()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SALIDA STATUS
                salida.data = VALIDACION_JQUERY_MULTIPLE_R.VALIDACION_POTENCIA_TIPO_VEHICULO();
                salida.status = true;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("tipos-vehiculos-con-traccion")]
        [HttpGet]
        public string VALIDAR_TIPO_VEHICULOS_TRACCION()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SALIDA STATUS
                salida.data = VALIDACION_JQUERY_MULTIPLE_R.VALIDACION_TRACCION_TIPO_VEHICULO();
                salida.status = true;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpGet]
        [Route("carga-vehiculo-ajax")]
        public string CARGA_VEHICULO_AJAX(int ID_TIPO_VEHICULO)
        {
            dynamic salida = new ExpandoObject();

            IEnumerable<CARGA_VEHICULO> lista_carga = CARGA_VEHICULO_R.TODOS_CARGA_VEHICULO().Where(n => n.ID_TIPO_VEHICULO == ID_TIPO_VEHICULO);

            salida.valor = lista_carga.Any() ? lista_carga.First().VALOR_POSIBLE : (float?)null;

            return JsonConvert.SerializeObject(salida);

        }

        [HttpGet]
        [Route("cantidad-comunidad-ajax")]
        public string CANTIDAD_COMUNIDAD_AJAX(int ID_OPCION_COMUNIDAD)
        {
            dynamic salida = new ExpandoObject();
            
            IEnumerable<CANTIDAD_COMUNIDAD> lista_cantidad = CANTIDAD_COMUNIDAD_R.TODOS_CANTIDAD_COMUNIDAD().Where(n => n.ID_OPCION_COMUNIDAD == ID_OPCION_COMUNIDAD);
            
            //SI EXISTE ALGUN REGISTRO CON EL ID TIPO VEHICULO
            salida.valor = lista_cantidad.Any() ? lista_cantidad.First().VALOR_POSIBLE : (long?)null;

            return JsonConvert.SerializeObject(salida);

        }

        [Route("solicitudes-ingresadas")]
        [HttpGet]
        public ActionResult SOLICITUD_INGRESO()
        {
            return View();
        }
        

        [Route("solicitudes-ingresadas-ajax")]
        [HttpPost]
        public string LISTA_SOLICITUD_INGRESO_AJAX()
        {
            return JsonConvert.SerializeObject(SOLICITUD_SPIEV_R.TODOS_SOLICITUD_X_ESTADO(1) /* 1 = ESTADO "Solicitud Ingresada" */ );
        }

        [Route("borrar-solicitud-ajax")]
        [HttpPost]
        public string BORRAR_SOLICITUD_AJAX(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //LEER DATOS DE LA SOLICITUD
                SOLICITUD_SPIEV SOLICITUD_SPIEV = SOLICITUD_SPIEV_R.READ(ID_SOLICITUD);

                bool TIENE_INSCRIPCION = SOLICITUD_SPIEV.ID_PRIMERA_INSCRIPCION.HasValue;
                bool TIENE_LIMITACION = SOLICITUD_SPIEV.ID_LIMITACION.HasValue;

                //SI TIENE UNA INSCRIPCIÓN ASOCIADA
                if (TIENE_INSCRIPCION) throw new Exception($"La solicitud {ID_SOLICITUD}, tiene un inscripción asociada");

                //SI TIENE UNA LIMITACION ASOCIADA
                if (TIENE_LIMITACION) throw new Exception($"La solicitud {ID_SOLICITUD}, tiene una limitación asociada");

                //GENERAR EL BORRADO LOGICO
                SOLICITUD_SPIEV_R.UPDATE(new SOLICITUD_SPIEV_R.UPDATE_SOLICITUD
                {
                    ID_SOLICITUD = ID_SOLICITUD,
                    BORRADO = false
                });

                //LA RESPUESTA
                salida.status = true;
                salida.message = "Solicitud borrada, correctamente";
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

    }
}