﻿$(document).ready(function () {

    //VALIDACION DE PASOS
    $('#nav-tab a').on('click', function (e) {
        e.preventDefault();

        var ELEMENTO_ACTIVO = $("#nav-tab").find("a.active:first");

        var TAB_ACTIVO = $(ELEMENTO_ACTIVO).attr('href');

        var FORM_ACTIVO = $(TAB_ACTIVO).find('form:first');

        var VALIDACION = $(FORM_ACTIVO).valid();

        return VALIDACION;
    });

    $('#btn-generar-spiev').click(function () {

        var OBJETO = {};

        $('.form-spiev').each(function () {

            var FORM = $(this);
            var OBJETO_FORM = {};

            $(FORM).find('[data-propiedad]').each(function () {
                //VALOR EN STRING DEL CAMPO...
                var VALOR_CAMPO = $(this).val();

                //SI ES ENTERO
                if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

                //SI ES DECIMAL
                if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

                //SE CREA EL CAMPO CON EL VALOR
                OBJETO_FORM[$(this).data('propiedad')] = VALOR_CAMPO;
            });

            OBJETO[FORM.attr('name')] = OBJETO_FORM;

        });

        //SE AGREGA EL ID DE SOLICITUD
        OBJETO['ID_SOLICITUD'] = parseInt($('#id-solicitud').text());

        //SE ENVIAN A GUARDAR
        GUARDAR_SPIEV(OBJETO);
    });

    //CUANDO CARGA LA PAGINA SETEA LA VALIDACION SEGÚN OPCION
    var ID_TIPO_VEHICULO = $('#VEHICULO_ID_TIPO_VEHICULO').val();

    //LLAMAR LAS VALIDACIONES VIA AJAX
    CAMBIAR_POR_TIPO_VEHICULO(ID_TIPO_VEHICULO);

    //CUANDO CAMBIA EL TIPO DE VEHICULO SE CAMBIA LA CAPACIDAD DE CARGA SI ES REQUERIDA
    $('#VEHICULO_ID_TIPO_VEHICULO').change(function () {
        CAMBIAR_POR_TIPO_VEHICULO($(this).val());
    });

    //ARREGLO CON ID DE INPUTS PARA BUSCAR
    var ARRAY_ELEMENT =
    [
        { INPUT_ID: "ADQUIRIENTE_RUN", FUNCION: GET_ADQUIRIENTE_BY_RUT },
        { INPUT_ID: "SOLICITANTE_RUN", FUNCION: GET_SOLICITANTE_BY_RUT },
        { INPUT_ID: "ESTIPULANTE_RUN", FUNCION: GET_ESTIPULANTE_BY_RUT }
    ];

    $(ARRAY_ELEMENT).each(function (i, element) {

        SEARCH_LIVE_BIND(element.INPUT_ID, element.FUNCION);
               
    });

    $(document).mouseup(function (e)
    {
        var container = $(".search-div");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('.search-input').html("");
            $('.search-input').removeClass("live-search-input");
        }
    });

});

function COMPLETAR_ADQUIRIENTE_POR_ID(ID)
{
    if (ID === null) return;

    $.when(GET_ADQUIRIENTE_POR_ID(ID)).then(function (JSON)
    {
        $.each(JSON.data, function (k, v) {

            if (isArray(v)) return;

            var NOMBRE_INPUT = "#ADQUIRIENTE_" + k;
            $(NOMBRE_INPUT).val(v);
        });



        $('.search-input').html("");
        $('.search-input').removeClass("live-search-input");
    });
}

function isArray(what)
{
    return Object.prototype.toString.call(what) === '[object Array]';
}

function GET_ADQUIRIENTE_POR_ID(ID_ADQ)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/search-adquiriente/por-id",
        data: JSON.stringify({ ID: ID_ADQ }),
        dataType: "json"
    });
}

function GET_SOLICITANTE_BY_RUT(TEXTO_BUSQUEDA)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/coincidencia-solicitante/por-rut",
        data: JSON.stringify({ VALOR: TEXTO_BUSQUEDA }),
        dataType: "html"
    });
}

function SEARCH_LIVE_BIND(ID_ELEMENT, FUNCION)
{
    var INPUT_SEARCH = $('#' + ID_ELEMENT);
    var DIV_SEARCH = $(INPUT_SEARCH).siblings('.search-input');

    $(INPUT_SEARCH).keyup(function ()
    {
        var TEXTO_BUSCAR = $(this).val();

        if (TEXTO_BUSCAR.length === 0)
        {
            $(DIV_SEARCH).html("");
            $(DIV_SEARCH).removeClass("live-search-input");
            return;
        }

        $.when(FUNCION(TEXTO_BUSCAR)).then(function (HTML)
        {
            $(DIV_SEARCH).html(HTML);
            $(DIV_SEARCH).addClass("live-search-input");
        });

    });
}

function GET_ADQUIRIENTE_BY_RUT(TEXTO_BUSQUEDA)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/coincidencia-adquiriente/por-rut",
        data: JSON.stringify({ VALOR: TEXTO_BUSQUEDA }),
        dataType: "html"
    });
}

function COMPLETAR_SOLICITANTE_POR_ID(ID)
{
    if (ID === null) return;

    $.when(GET_SOLICITANTE_POR_ID(ID)).then(function (JSON) {
        $.each(JSON.data, function (k, v) {

            if (isArray(v)) return;

            var NOMBRE_INPUT = "#SOLICITANTE_" + k;
            $(NOMBRE_INPUT).val(v);

            if ($(NOMBRE_INPUT).data('select2')) $(NOMBRE_INPUT).trigger('change');
        });

        $('.search-input').html("");
        $('.search-input').removeClass("live-search-input");
    });
}

function GET_SOLICITANTE_POR_ID(ID_ADQ) {
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/search-solicitante/por-id",
        data: JSON.stringify({ ID: ID_ADQ }),
        dataType: "json"
    });
}

function GET_ESTIPULANTE_BY_RUT(TEXTO_BUSQUEDA) {
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/coincidencia-estipulante/por-rut",
        data: JSON.stringify({ VALOR: TEXTO_BUSQUEDA }),
        dataType: "html"
    });
}

function COMPLETAR_ESTIPULANTE_POR_ID(ID) {
    if (ID === null) return;

    $.when(GET_ESTIPULANTE_POR_ID(ID)).then(function (JSON) {
        $.each(JSON.data, function (k, v) {

            if (isArray(v)) return;

            var NOMBRE_INPUT = "#ESTIPULANTE_" + k;
            $(NOMBRE_INPUT).val(v);

            if ($(NOMBRE_INPUT).data('select2')) $(NOMBRE_INPUT).trigger('change');
        });

        $('.search-input').html("");
        $('.search-input').removeClass("live-search-input");
    });
}

function GET_ESTIPULANTE_POR_ID(ID_ADQ) {
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/search-estipulante/por-id",
        data: JSON.stringify({ ID: ID_ADQ }),
        dataType: "json"
    });
}

function CAMBIAR_POR_TIPO_VEHICULO(ID_TIPO_VEHICULO)
{
    var ID_VALIDAR = parseInt(ID_TIPO_VEHICULO);
    var ARRAY_AJAX = new Array();

    //ELEMENTO HTML, INSERTA ICONO DE CARGANDO
    var SPAN_VEHICULO_LOAD = $("#vehiculo-load");
    $(SPAN_VEHICULO_LOAD).html('<i class="fas fa-spinner fa-spin"></i>');

    //TABS DEL NAV-BAR (SE DESHABILITAN MIENTRAS CARGA)
    var NAVS = $("#nav-tab.nav-tabs > a");
    $.each(NAVS, function (INDEX, ELEMENTO) {
        $(ELEMENTO).removeAttr('data-toggle');
    });

    //VALIDACION POR TIPO DE VEHICULO (DEBE SER EL MISMO OBJETO)
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-carga"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-cit"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-ejes"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-otra-carroceria"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-potencia"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-traccion"));

    $.when.apply($, ARRAY_AJAX).then(function () {
        var ARRAY_CALLBACKS = arguments;

        //RECORRER LAS RESPUESTAS...
        $.each(ARRAY_CALLBACKS, function (INDEX, ELEMENTO) {
            var JSON = ELEMENTO[0];
            VALIDAR_MULTIPLE_DESDE_JSON(JSON, ID_VALIDAR);
        });

        //ELEMENTO HTML, REMUEVE ICONO DE CARGANDO
        $(SPAN_VEHICULO_LOAD).html('');

        //VUELVE A HABILITAR LOS TABS
        $.each(NAVS, function (INDEX, ELEMENTO) {
            $(ELEMENTO).attr('data-toggle', 'tab');
        });
    });

}

function GET_VALIDAR_TIPO(ACCION_CONTROLADOR)
{
    return $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/registro/" + ACCION_CONTROLADOR,
        dataType: "json"
    });
}

function VER_PDF_MERGE(BTN)
{
    var SRC = $('#frame-pdf-merge').attr('src');

    //SI TIENE UNA URL VALIDA
    if (SRC !== "")
    {
        //MOSTRAR
        $('#collapse-view-pdf').collapse('toggle');
        return;
    }

    //ID DE LA SOLICITUD
    var ID_SOL = parseInt($('#id-solicitud').text());

    //SE GENERA EL DOCUMENTO FUSIONADO
    $.when(PDF_MERGE_SOLICITUD(ID_SOL)).then(function (JSON)
    {
        //EN CASO DE ERROR...
        if (!JSON.status)
        {
            HTML_ALERTA("alert alert-dismissible fade show alert-danger", JSON.message);
            return;
        }

        //RUTA DEL PDF
        var RUTA_PDF = JSON.data;

        var URL_HOST = window.location.protocol + "//" + window.location.hostname;
        URL_HOST += window.location.port === "" ? "" : ":" + window.location.port;

        //RUTA DEL PDF
        var SRC_FRAME = $('#frame-pdf-merge').data('src');
        SRC_FRAME = SRC_FRAME.replace("{RUTA_PDF}", URL_HOST + RUTA_PDF);

        //SETEAR LA  URL DEL FRAME
        $('#frame-pdf-merge').attr('src', SRC_FRAME);
    
        //MINIZAR MENU SI CORRESPONDE
        if (!$('#accordionSidebar').hasClass('toggled') && !$('#collapse-view-pdf').hasClass('show')) $('#sidebarToggle').click();

        //MOSTRAR
        $('#collapse-view-pdf').collapse('toggle');

    });
}

function PDF_MERGE_SOLICITUD(ID)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/merge-documentos",
        data: JSON.stringify({ ID_SOLICITUD: ID }),
        dataType: "json"
    });
}


function GUARDAR_SPIEV(OBJETO)
{
    //BOTON EN CARGANDO...
    $('#btn-generar-spiev').html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/registro/guardar-spiev-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //PINTAR ALERTA CON OPCIONES
            if (RESULT.status)
            {
                HTML_ALERTA_INSCR_OK(RESULT.data.ID_INSCRIPCION);
                return;
            }

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message);

        },
        error: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos");
        }
    });
}

function HTML_ALERTA(CLASS, MENS)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PARTIAL_ALERT",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');

            //BOTON NORMAL...
            $('#btn-generar-spiev').html('Generar 1era Inscripción');
        }
    });
}

function HTML_ALERTA_INSCR_OK(ID_INSCRIPCION)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PARTIAL_ALERT_INSCRIPCION_OK",
        data: JSON.stringify({ ID_INSCRIPCION: ID_INSCRIPCION }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');

            //BOTON NORMAL...
            $('#btn-generar-spiev').html('Generar 1era Inscripción');
        }
    });
}

function VALIDAR_MULTIPLE_DESDE_JSON(JSON, ID_VALIDAR) {
    //SI EXISTIO ALGUN ERROR
    if (JSON.status === false) {
        console.error(JSON.message);
        return;
    }

    //SI NO HAY DATA
    if (JSON.data === null) {
        console.error("NO EXISTE REGISTRO PARA VALIDACION");
        return;
    }

    //EL JSON DESDE EL SERVIDOR
    var JSON_DATA = JSON.data;

    //LA LISTA DE VALIDACION
    var JSON_FILTRO = JSON_DATA.VALIDACIONES.filter(function (item) {
        return item.ID === ID_VALIDAR;
    });

    //REMOVER LAS VALIDACIONES
    QUIT_ALL_RULE(JSON_DATA.ID_INPUT);

    //INPUT HABILITADO PARA ESCRIBIR
    INPUT_READONLY(JSON_DATA.ID_INPUT, false);

    //APLICAR VALIDACIONES
    APPLICAR_VALIDACIONES_ARRAY(JSON_DATA, JSON_FILTRO);

    //SI SE APLICO ALGUNA VALIDACION ANTERIOR...
    if (JSON_FILTRO.length > 0) return;

    //LA LISTA DE VALIDACION (A TODOS)
    var JSON_FILTRO_ALL = JSON_DATA.VALIDACIONES.filter(function (item) {
        return item.FOR_ALL === true;
    });

    //APLICAR VALIDACIONES
    APPLICAR_VALIDACIONES_ARRAY(JSON_DATA, JSON_FILTRO_ALL);
}

function APPLICAR_VALIDACIONES_ARRAY(JSON_DATA, JSON_ARRAY) {
    $.each(JSON_ARRAY, function (INDEX, ELEMENTO) {

        //SI DEBE IR VACIO SE PONE EL INPUT EN READONLY Y SETEA A ""
        if (ELEMENTO.DEFECTO !== null) {
            INPUT_READONLY(JSON_DATA.ID_INPUT, true);
            $("input[id*=" + JSON_DATA.ID_INPUT + "]").val(ELEMENTO.DEFECTO);
            return;
        }

        //SI ES UN CAMPO REQUERIDO
        if (ELEMENTO.REQUERIDO === true) {
            ADD_RULE_REQUERIDO(JSON_DATA.ID_INPUT, JSON_DATA.MENSAJE_REQUERIDO);
        }

        //SI TIENE MIN
        if (ELEMENTO.MIN !== null) {
            var MENSAJE_MIN = JSON_DATA.MENSAJE_MINIMO.replace("[MIN]", ELEMENTO.MIN);
            ADD_RULE_MIN(JSON_DATA.ID_INPUT, MENSAJE_MIN, ELEMENTO.MIN);
        }

    });
}

function QUIT_ALL_RULE(ID_INPUT) {
    //REMOVER LAS VALIDACIONES
    $("#" + ID_INPUT).rules("remove", 'required');
    $("#" + ID_INPUT).rules("remove", 'min');
}

function ADD_RULE_REQUERIDO(ID_INPUT, MENSAJE) {
    $("input[id*=" + ID_INPUT + "]").rules("add", {
        required: true,
        messages: { required: MENSAJE }
    });
}

function ADD_RULE_MIN(ID_INPUT, MENSAJE, MIN_NUMBER) {
    $("input[id*=" + ID_INPUT + "]").rules("add", {
        min: MIN_NUMBER,
        messages: { min: MENSAJE }
    });
}

function INPUT_READONLY(ID_INPUT, EN_LECTURA) {
    $("input[id*=" + ID_INPUT + "]").attr('readonly', EN_LECTURA);
}
