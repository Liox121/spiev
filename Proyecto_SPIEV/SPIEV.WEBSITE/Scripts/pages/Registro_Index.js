﻿$(document).ready(function () {

    //VALIDACION DE PASOS
    $('#nav-tab a').on('click', function (e) {
        e.preventDefault();

        var ELEMENTO_ACTIVO = $("#nav-tab").find("a.active:first");

        var TAB_ACTIVO = $(ELEMENTO_ACTIVO).attr('href');

        var FORM_ACTIVO = $(TAB_ACTIVO).find('form:first');

        var VALIDACION = $(FORM_ACTIVO).valid();

        return VALIDACION;
    });

    $('#btn-generar-spiev').click(function () {

        var objeto_spiev = {};

        $('.form-spiev').each(function () {

            var formulario = $(this);
            var objeto_form = {};

            $(formulario).find('[data-propiedad]').each(function () {
                //VALOR EN STRING DEL CAMPO...
                var valor_campo = $(this).val();

                //SI ES ENTERO
                if ($(this).data('entero')) valor_campo = parseInt(valor_campo);

                //SI ES DECIMAL
                if ($(this).data('decimal')) valor_campo = parseFloat(valor_campo);

                //SE CREA EL CAMPO CON EL VALOR
                objeto_form[$(this).data('propiedad')] = valor_campo;
            });

            objeto_spiev[formulario.attr('name')] = objeto_form;

        });

        CREAR_SPIEV(objeto_spiev);
    });

    //CUANDO CARGA LA PAGINA SETEA LA VALIDACION SEGÚN OPCION
    var ID_TIPO_VEHICULO = $('#VEHICULO_ID_TIPO_VEHICULO').val();

    //LLAMAR LAS VALIDACIONES VIA AJAX
    CAMBIAR_POR_TIPO_VEHICULO(ID_TIPO_VEHICULO);

    //CUANDO CAMBIA EL TIPO DE VEHICULO SE CAMBIA LA CAPACIDAD DE CARGA SI ES REQUERIDA
    $('#VEHICULO_ID_TIPO_VEHICULO').change(function () {
        CAMBIAR_POR_TIPO_VEHICULO($(this).val());
    });


});

function CAMBIAR_POR_TIPO_VEHICULO(ID_TIPO_VEHICULO)
{
    var ID_VALIDAR = parseInt(ID_TIPO_VEHICULO);
    var ARRAY_AJAX = new Array();

    //ELEMENTO HTML, INSERTA ICONO DE CARGANDO
    var SPAN_VEHICULO_LOAD = $("#vehiculo-load");
    $(SPAN_VEHICULO_LOAD).html('<i class="fas fa-spinner fa-spin"></i>');

    //TABS DEL NAV-BAR (SE DESHABILITAN MIENTRAS CARGA)
    var NAVS = $("#nav-tab.nav-tabs > a");
    $.each(NAVS, function (INDEX, ELEMENTO) {
        $(ELEMENTO).removeAttr('data-toggle');
    });

    //VALIDACION POR TIPO DE VEHICULO (DEBE SER EL MISMO OBJETO)
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-carga"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-cit"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-ejes"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-otra-carroceria"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-potencia"));
    ARRAY_AJAX.push(GET_VALIDAR_TIPO("tipos-vehiculos-con-traccion"));

    $.when.apply($, ARRAY_AJAX).then(function () {
        var ARRAY_CALLBACKS = arguments;

        //RECORRER LAS RESPUESTAS...
        $.each(ARRAY_CALLBACKS, function (INDEX, ELEMENTO) {
            var JSON = ELEMENTO[0];
            VALIDAR_MULTIPLE_DESDE_JSON(JSON, ID_VALIDAR);
        });

        //ELEMENTO HTML, REMUEVE ICONO DE CARGANDO
        $(SPAN_VEHICULO_LOAD).html('');

        //VUELVE A HABILITAR LOS TABS
        $.each(NAVS, function (INDEX, ELEMENTO) {
            $(ELEMENTO).attr('data-toggle', 'tab');
        });
    });

}

function GET_VALIDAR_TIPO(ACCION_CONTROLADOR)
{
    return $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/registro/" + ACCION_CONTROLADOR,
        dataType: "json"
    });
}

function CREAR_SPIEV(datos_objeto)
{
    //BOTON EN CARGANDO...
    $('#btn-generar-spiev').html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //CLASE PARA ALERTA
    var clase_base = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/registro/guardar-spiev-ajax",
        data: JSON.stringify(datos_objeto),
        dataType: "json",
        success: function (result)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            clase_base += result.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(clase_base, result.message);

        },
        error: function (result)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            clase_base += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(clase_base, "Hay un problema al guardar los datos");
        }
    });
}

function HTML_ALERTA(clase, mensaje)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PARTIAL_ALERT",
        data: JSON.stringify({ Clase: clase, Mensaje: mensaje }),
        dataType: "html",
        success: function (result)
        {
            $('#alerta-spiev').hide().html(result).fadeIn('slow');

            $('#btn-generar-spiev').html('Generar 1era Inscripción');
        }
    });
}

function VALIDAR_MULTIPLE_DESDE_JSON(JSON, ID_VALIDAR)
{
    //SI EXISTIO ALGUN ERROR
    if (JSON.status === false) {
        console.error(JSON.message);
        return;
    }

    //SI NO HAY DATA
    if (JSON.data === null) {
        console.error("NO EXISTE REGISTRO PARA VALIDACION");
        return;
    }

    //EL JSON DESDE EL SERVIDOR
    var JSON_DATA = JSON.data;

    //LA LISTA DE VALIDACION
    var JSON_FILTRO = JSON_DATA.VALIDACIONES.filter(function (item) {
        return item.ID === ID_VALIDAR;
    });

    //REMOVER LAS VALIDACIONES
    QUIT_ALL_RULE(JSON_DATA.ID_INPUT);

    //INPUT HABILITADO PARA ESCRIBIR
    INPUT_READONLY(JSON_DATA.ID_INPUT, false);

    //APLICAR VALIDACIONES
    APPLICAR_VALIDACIONES_ARRAY(JSON_DATA, JSON_FILTRO);

    //SI SE APLICO ALGUNA VALIDACION ANTERIOR...
    if (JSON_FILTRO.length > 0) return;
    
    //LA LISTA DE VALIDACION (A TODOS)
    var JSON_FILTRO_ALL = JSON_DATA.VALIDACIONES.filter(function (item) {
        return item.FOR_ALL === true;
    });

    //APLICAR VALIDACIONES
    APPLICAR_VALIDACIONES_ARRAY(JSON_DATA, JSON_FILTRO_ALL);
}

function APPLICAR_VALIDACIONES_ARRAY(JSON_DATA, JSON_ARRAY)
{
    $.each(JSON_ARRAY, function (INDEX, ELEMENTO)
    {

        //SI DEBE IR VACIO SE PONE EL INPUT EN READONLY Y SETEA A ""
        if (ELEMENTO.DEFECTO !== null)
        {
            INPUT_READONLY(JSON_DATA.ID_INPUT, true);
            $("input[id*=" + JSON_DATA.ID_INPUT + "]").val(ELEMENTO.DEFECTO);
            return;
        }

        //SI ES UN CAMPO REQUERIDO
        if (ELEMENTO.REQUERIDO === true)
        {
            ADD_RULE_REQUERIDO(JSON_DATA.ID_INPUT, JSON_DATA.MENSAJE_REQUERIDO);
        }

        //SI TIENE MIN
        if (ELEMENTO.MIN !== null)
        {
            var MENSAJE_MIN = JSON_DATA.MENSAJE_MINIMO.replace("[MIN]", ELEMENTO.MIN);
            ADD_RULE_MIN(JSON_DATA.ID_INPUT, MENSAJE_MIN, ELEMENTO.MIN);
        }

    });
}

function QUIT_ALL_RULE(ID_INPUT)
{
    //REMOVER LAS VALIDACIONES
    $("#" + ID_INPUT).rules("remove", 'required');
    $("#" + ID_INPUT).rules("remove", 'min');
}

function ADD_RULE_REQUERIDO(ID_INPUT, MENSAJE)
{
    $("input[id*=" + ID_INPUT + "]").rules("add", {
        required: true,
        messages: { required: MENSAJE }
    });
}

function ADD_RULE_MIN(ID_INPUT, MENSAJE, MIN_NUMBER) {
    $("input[id*=" + ID_INPUT + "]").rules("add", {
        min: MIN_NUMBER,
        messages: { min: MENSAJE }
    });
}

function INPUT_READONLY(ID_INPUT, EN_LECTURA)
{
    $("input[id*=" + ID_INPUT + "]").attr('readonly', EN_LECTURA);
}