﻿$(function () {

    $("#logout-link").click(function (e) {
        e.preventDefault();
        $.post("/logout", function (res) {
            if (res.status === "done") {
                //close the window now.
                window.location = "/";
            }
        });
    });

    //RECARGAR LA SESION DEL USUARIO CADA X CANTIDAD DE MILISEGUNDOS
    setInterval(RECARGAR_SESION, 30000);
});

var RECARGAR_SESION = function ()
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/home/UPDATE_SESSION_AJAX",
        dataType: "json",
        success: function (JSON_RES)
        {
            console.log(JSON_RES);
        },
        error: function (RESULT)
        {
            console.log(RESULT);
        }
    });
};