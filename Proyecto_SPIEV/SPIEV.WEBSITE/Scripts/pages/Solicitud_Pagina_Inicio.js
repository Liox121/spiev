﻿$(document).ready(function () {

    $.when(ResumenMisSolicitudes()).then(function (json, textStatus, jqXHR) {
        console.log(jqXHR.status); // Alerts 200

        //SI EXISTIO ALGÚN ERROR
        if (!json.status) {
            return;
        }

        console.log(json);

        CompletarResumenEstado(json.data);
    });
});


function getSum(total, num)
{
    return total + num;
}


function CompletarResumenEstado(data)
{
    //VARIABLES
    var SOLICITUD_INGRESADA = data.filter(function (data) {

        //SE FILTRA POR ESTADO
        return data.ESTADO === 'Solicitud Ingresada';
    }).map(function (x) {

        //SE OBTIENE SOLO EL TOTAL
        return x.TOTAL;

        //SE CALCULA EL TOTAL , VALOR INICIAL
    }).reduce(getSum, 0);

    var SOLICITUD_OK = data.filter(function (data) {
        return data.ESTADO === 'Documentación OK';
    }).map(function (x) {
        return x.TOTAL;
    }).reduce(getSum, 0);

    var SOLICITUD_NOK = data.filter(function (data) {
        return data.ESTADO === 'Documentación Incorrecta';
    }).map(function (x) {
        return x.TOTAL;
    }).reduce(getSum, 0);


    //SUMA DE LOS ANTERIORES 
    var SOLICITUD_TOTAL = SOLICITUD_INGRESADA + SOLICITUD_OK + SOLICITUD_NOK;

    $('#total-solicitud-ingresada').text(SOLICITUD_INGRESADA);
    $('#total-solicitud-ok').text(SOLICITUD_OK);
    $('#total-solicitud-nok').text(SOLICITUD_NOK);
    $('#total-solicitud').text(SOLICITUD_TOTAL);

    //EFECTO DE CONTEO SOBRE LOS CUADROS DE RESUMEN
    $('span.integers').counterUp({
        delay: 10, // the delay time in ms
        time: 1000 // the speed time in ms
    });

    var JSON_GRAPHS = [];

    //SI LOS VALORES SON MAYORES A 0
    if (SOLICITUD_INGRESADA > 0) JSON_GRAPHS.push({ "estado": "Ingresadas", "total": SOLICITUD_INGRESADA, "color": "#36b9cc" });
    if (SOLICITUD_OK > 0) JSON_GRAPHS.push({ "estado": "Documentacion OK", "total": SOLICITUD_OK, "color": "#1cc88a" });
    if (SOLICITUD_NOK > 0) JSON_GRAPHS.push({ "estado": "Documentacion Incorrecta", "total": SOLICITUD_NOK, "color": "#e74a3b" });

    //LOS COLORCITOS...
    var COLORES = JSON_GRAPHS.map(function (graph) {
        return graph.color;
    });

    //CREAR EL GRAFICO
    CompletarResumenGrafico(JSON_GRAPHS, COLORES);
}

function CompletarResumenGrafico(json_graphs, colors)
{
    AmCharts.makeChart("chartdiv",
        {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "titleField": "estado",
            "valueField": "total",
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [],
            "colors": colors,
            "dataProvider": json_graphs
        }
    );
}

function ResumenMisSolicitudes()
{
    return $.ajax({
        type: "POST",
        cache: false,
        contentType: "application/json; charset=utf-8",
        url: "/reportes/mis-estadisticas-inicio-ajax",
        dataType: "json"
    });
}