﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SPIEV.WEBSITE.Models
{
    public class SUCURSAL_VM : SUCURSAL
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "(*) Campo nombre sucursal, requerido")]
        public override string NOMBRE_SUCURSAL { get; set; }
        public List<EMPRESA> EMPRESAS { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void SUCURSAL_VM_DESDE_SUCURSAL(SUCURSAL SUCURSAL_BASE)
        {
            this.ID_SUCURSAL = SUCURSAL_BASE.ID_SUCURSAL;
            this.NOMBRE_SUCURSAL = SUCURSAL_BASE.NOMBRE_SUCURSAL;
            this.ID_EMPRESA = SUCURSAL_BASE.ID_EMPRESA;
        }
    }
}