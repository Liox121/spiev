﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SPIEV.WEBSITE.Models
{
    public class PERFIL_VM : PERFIL
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "(*) Campo nombre, requerido")]
        public override string NOMBRE { get; set; }

        public List<PAGINA_PERFIL> PAGINAS { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void PERFIL_VM_DESDE_PERFIL(PERFIL PERFIL_BASE)
        {
            this.ID_PERFIL = PERFIL_BASE.ID_PERFIL;
            this.NOMBRE = PERFIL_BASE.NOMBRE;
            this.DESCRIPCION = PERFIL_BASE.DESCRIPCION;
            this.ICONO = PERFIL_BASE.ICONO;
        }
    }
}