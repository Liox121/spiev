﻿using System.Web;
using System.Web.Mvc;

namespace SPIEV.WEBSITE.Models
{
    /// <summary>
    /// VALIDA SESION ACTIVA DEL USUARIO LOGEADO
    /// </summary>
    public class SESION_VALIDA : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Session["UserWeb"] != null;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult("/");
        }
    }
}