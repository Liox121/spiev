﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEBSITE.Models
{
    public class EMPRESA_VM : EMPRESA
    {
        [Required( AllowEmptyStrings = false, ErrorMessage = "(*) Campo nombre empresa, requerido")]
        public override string NOMBRE_EMPRESA { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void EMPRESA_VM_DESDE_EMPRESA(EMPRESA EMPRESA_BASE)
        {
            this.ID_EMPRESA = EMPRESA_BASE.ID_EMPRESA;
            this.NOMBRE_EMPRESA = EMPRESA_BASE.NOMBRE_EMPRESA;
        }
    }
}