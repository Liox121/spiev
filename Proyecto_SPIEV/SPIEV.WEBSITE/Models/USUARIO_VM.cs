﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEBSITE.Models
{
    public class USUARIO_VM : USUARIO
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "(*) Campo Nombre Usuario, requerido")]
        public override string NOMBRE_USUARIO { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "(*) Campo Nombre Completo, requerido")]
        public override string NOMBRE_COMPLETO { get; set; }

        [EmailAddress(ErrorMessage = "(*) Campo Correo Electronico, requerido")]
        public override string CORREO_ELECTRONICO { get; set; }

        public string REPERTIR_CLAVE { get; set; }

        [Required(ErrorMessage = "(*) Seleccione un perfil por defecto")]
        public long PERFIL_POR_DEFECTO { get; set; }

        [ListNotEmptyValidation(ErrorMessage = "(*) Seleccione al menos un perfil")]
        public List<USUARIO_PERFIL> MIS_PERFILES { get; set; }

        public List<PERFIL> PERFILES { get; set; }
        public List<SUCURSAL> MIS_SUCURSALES { get; set; }
        public List<SUCURSAL> SUCURSALES { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void USUARIO_VM_DESDE_USUARIO(USUARIO USUARIO_BASE)
        {
            this.ID_USUARIO = USUARIO_BASE.ID_USUARIO;
            this.NOMBRE_USUARIO = USUARIO_BASE.NOMBRE_USUARIO;
            this.NOMBRE_COMPLETO = USUARIO_BASE.NOMBRE_COMPLETO;
            this.CLAVE = USUARIO_BASE.CLAVE;
            this.CORREO_ELECTRONICO = USUARIO_BASE.CORREO_ELECTRONICO;
            this.ACTIVO = USUARIO_BASE.ACTIVO;
        }

        /// <summary>
        /// VALIDAR CLAVES NO NULAS O VACIAS
        /// </summary>
        /// <returns></returns>
        public bool CLAVES_NO_NULAS()
        {
            return (!String.IsNullOrEmpty(CLAVE) && !String.IsNullOrEmpty(REPERTIR_CLAVE)) ? true : false;
        }

        /// <summary>
        /// VALIDAR CLAVES IGUALES
        /// </summary>
        /// <returns></returns>
        public bool CLAVES_IGUALES()
        {
            return (CLAVE == REPERTIR_CLAVE) ? true : false;
        }

    }
}