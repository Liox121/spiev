﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SPIEV.WEBSITE.Models
{
    public class SOLICITUD_VM
    {
        public SOLICITUD_VM()
        {
            FINANCIERAS = new List<FINANCIERA>();
            TERMINACIONES = new List<SelectListItem>();
        }

        public void DEFAULT_OPCION_FINANCIERA()
        {
            FINANCIERAS.Add(new FINANCIERA { ID_FINANCIERA = 0, NOMBRE_FINANCIERA = "SIN FINANCIERA" });
            FINANCIERAS = FINANCIERAS.OrderBy(n => n.ID_FINANCIERA).ToList();
        }

        public void DEFAULT_OPCION_TERMINACION()
        {
            TERMINACIONES.Add(new SelectListItem { Text = "-", Value = "NULL", Selected = true });
            TERMINACIONES = TERMINACIONES.OrderByDescending(n => n.Selected).ToList();
        }

        public List<SUCURSAL_USUARIO> SUCURSALES { get; set; }
        public List<FINANCIERA> FINANCIERAS { get; set; }
        public List<SERVICIO> SERVICIOS { get; set; }
        public List<SelectListItem> TERMINACIONES { get; set; }
    }
}