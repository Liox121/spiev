﻿using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using System;
using System.Linq;
using System.Web;

namespace SPIEV.WEBSITE.Models
{
    public static class SESSION_WEB
    {
        public static void CREATE_SESSION(string NOMBRE_USUARIO, string CLAVE)
        {
            //DTOS DEL USUARIO LOGEADO...
            USUARIO_SESSION SESSION = new USUARIO_SESSION();
            USUARIO USER = USUARIO_R.TODOS().Where(a => a.NOMBRE_USUARIO == NOMBRE_USUARIO && a.CLAVE == CLAVE).First();

            //SI ESTA INACTIVO
            if (USER.ACTIVO == false)
            {
                SESION_USER = null;
                return;
            }

            SESSION.USUARIO_SESSION_DESDE_USUARIO(USER);

            //LOS PERFILES ASOCIADOS (TRAE EL PERFIL COMO OBJETO)...
            SESSION.MIS_PERFILES = USUARIO_PERFIL_R.TODOS_X_USUARIO_SESSION(USER.ID_USUARIO).ToList();

            //LAS SUCURSALES ASOCIADAS (TRAE LA SUCURSAL COMO OBJETO)
            SESSION.MIS_SUCURSALES = SUCURSAL_USUARIO_R.TODOS_X_USUARIO_NO_BORRADO(USER.ID_USUARIO).ToList();

            //CREAMOS LA SESSION DEL USUARIO
            SESION_USER = SESSION;
        }

        public static USUARIO_SESSION SESION_USER
        {
            get { return (USUARIO_SESSION)HttpContext.Current.Session["UserWeb"]; }
            set { HttpContext.Current.Session["UserWeb"] = value; }
        }

    }
}