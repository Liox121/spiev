﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEBSITE.Models
{
    public class OPERADOR_VM : OPERADOR
    {
        [Display(Name = "Region")]
        public override int ID_REGION { get; set; }

        [Display(Name = "Run Usuario")]
        [Required(ErrorMessage = "(*) Run Usuario requerido")]
        [Range(0, 99999999)]
        public override int RUN_USUARIO { get; set; }

        [Display(Name = "Rut Empresa")]
        [Required(ErrorMessage = "(*) Rut Empresa requerido")]
        [Range(0, 99999999)]
        public override int RUN_EMPRESA { get; set; }

        public List<REGION> LISTA_REGIONES { get; set; }

        /// <summary>
        /// RETORNA OBJETO PARA CREAR EN REPOSITORIO
        /// </summary>
        /// <returns></returns>
        public OPERADOR OBJETO_CREAR()
        {
            return new OPERADOR
            {
                ID_REGION = this.ID_REGION,
                RUN_USUARIO = this.RUN_USUARIO,
                RUN_EMPRESA = this.RUN_EMPRESA
            };
        }
    }
}