﻿using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

namespace SPIEV.WEBSITE.Models
{
    /// <summary>
    /// VALIDA PERMISOS SEGÚN PERFILES Y PAGINAS ASOCIADAS
    /// </summary>
    public class PERMISO_ACCION : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            //OBTENEMOS EL NOMBRE DE LA ACCION Y EL CONTROLADOR ASOCIADO
            string ACTION_NAME = filterContext.ActionDescriptor.ActionName;
            string CONTROLLER_NAME = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            ////VALIDAR PAGINA POR USUARIO, SI NO TIENE PERMISOS REDIRECCIONA A OTRA PAGINA
            //if (!SESSION_WEB.ValidarUsuarioPagina(CONTROLLER_NAME, ACTION_NAME))
            //{
            //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            //    {
            //        controller = "Home",
            //        action = "PermisosInsuficientes"
            //    }));
            //}
        }
    }
}