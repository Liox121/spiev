﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEBSITE.Models
{
    public class FINANCIERA_VM : FINANCIERA
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "(*) Campo nombre financiera, requerido")]
        public override string NOMBRE_FINANCIERA { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void FINANCIERA_VM_DESDE_FINANCIERA(FINANCIERA FINANCIERA_BASE)
        {
            this.ID_FINANCIERA = FINANCIERA_BASE.ID_FINANCIERA;
            this.NOMBRE_FINANCIERA = FINANCIERA_BASE.NOMBRE_FINANCIERA;
        }
    }
}