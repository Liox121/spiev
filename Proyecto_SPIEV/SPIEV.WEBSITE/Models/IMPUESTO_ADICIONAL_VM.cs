﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEBSITE.Models
{
    public class IMPUESTO_ADICIONAL_VM : IMPUESTO_ADICIONAL
    {
        [Display(Name = "Codigo Identificación de pago")]
        [Required(ErrorMessage = "(*) Codigo Identificación de pago requerido")]
        [StringLength(30)]
        public override string CODIGO_IDENTIFICACION { get; set; }

        [Display(Name = "Monto Impuesto Pagado")]
        [Required(ErrorMessage = "(*) Monto Impuesto Pagado requerido")]
        public override long MONTO_IMPUESTO { get; set; }

        [Display(Name = "Monto Total Factura")]
        [Required(ErrorMessage = "(*) Monto Total Factura requerida")]
        public override long TOTAL_FACTURA { get; set; }

        [Display(Name = "Codigo Informe Tecnico")]
        [Required(ErrorMessage = "(*) Codigo Informe Tecnico requerido")]
        [StringLength(26)]
        public override string CODIGO_INFORME_TECNICO { get; set; }

        /// <summary>
        /// RETORNA OBJETO PARA CREAR EN REPOSITORIO
        /// </summary>
        /// <returns></returns>
        public IMPUESTO_ADICIONAL OBJETO_CREAR()
        {
            return new IMPUESTO_ADICIONAL
            {
                CODIGO_IDENTIFICACION   = this.CODIGO_IDENTIFICACION,
                CODIGO_INFORME_TECNICO  = this.CODIGO_INFORME_TECNICO,
                MONTO_IMPUESTO          = this.MONTO_IMPUESTO,         
                TOTAL_FACTURA           = this.TOTAL_FACTURA         
            };
        }
    }
}