﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SPIEV.WEBSITE.Models
{
    public class JSON_ARCHIVO_SOLICITUD
    {
        public JSON_ARCHIVO_SOLICITUD()
        {
            ARCHIVOS_ACTUALIZAR = new List<int>();
            TIPO_ARCHIVO_ACTUALIZAR = new List<int>();
            TIPO_ARCHIVO_NUEVO = new List<int>();
            SERVICIO_NUEVO = new List<long>();
        }

        public int ID_SOLICITUD { get; set; }
        public int ID_SUCURSAL { get; set; }
        public int? ID_FINANCIERA { get; set; }
        public int? TERMINACION_OPC1 { get; set; }
        public int? TERMINACION_OPC2 { get; set; }

        [RutValidation(ErrorMessage = "(*) Campo rut invalido")]
        public string RUT_ADQUIRIENTE { get; set; }

        [EmailAddress(ErrorMessage = "(*) Campo correo invalido")]
        public String CORREO_ADQUIRIENTE { get; set; }

        [StringLength(500, ErrorMessage = "(*) Campo comentario no puede contener mas de 500 caracteres")]
        public String COMENTARIO { get; set; }
        public List<int> ARCHIVOS_ACTUALIZAR { get; set; }
        public List<int> TIPO_ARCHIVO_ACTUALIZAR { get; set; }
        public List<int> TIPO_ARCHIVO_NUEVO { get; set; }
        public List<long> SERVICIO_NUEVO { get; set; }
    }
}