﻿using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEBSITE.Models
{
    public class VEHICULO_VM : VEHICULO
    {
        [Display(Name = "Termin. PPU")]
        [Required(ErrorMessage = "(*) Terminación PPU requerida")]       //CAMPO REQUERIDO
        [StringLength(1)] //LARGO STRING 1
        public override string TERMINACION_PPU { get; set; }

        [Display(Name = "Año Fabricación")]
        [Required(ErrorMessage = "(*) Año Fabricación requerida")]       //CAMPO REQUERIDO
        [Range(0, 9999)] //LARGO 9999
        public override int AGNO_FABRICACION { get; set; }

        [Display(Name = "Cantidad Asientos")]
        public override int? NUMERO_ASIENTOS { get; set; }

        [Display(Name = "Capacidad Carga")]
        public override decimal? CARGA { get; set; }

        [Display(Name = "Cod. Inf. Tecnico")]
        public override string CODIGO_INFORME_TECNICO { get; set; }

        [Display(Name = "Tipo Combustible")]
        public override int ID_TIPO_COMBUSTIBLE { get; set; }

        [Display(Name = "Marca")]
        [Required(ErrorMessage = "(*) Marca requerida")]       //CAMPO REQUERIDO
        [StringLength(26, MinimumLength = 1)] //LARGO  1 - 26
        public override string MARCA { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "(*) Modelo requerido")]       //CAMPO REQUERIDO
        [StringLength(35, MinimumLength = 1)] //LARGO  1 - 35
        public override string MODELO { get; set; }

        [Display(Name = "Numero Chasis")]
        [Required(ErrorMessage = "(*) Numero Chasis requerido")]       //CAMPO REQUERIDO
        [StringLength(20, MinimumLength = 1)] //LARGO  1 - 20
        public override string NUMERO_CHASIS { get; set; }

        [Display(Name = "Numero Motor")]
        [Required(ErrorMessage = "(*) Numero Motor requerido")]       //CAMPO REQUERIDO
        [StringLength(20, MinimumLength = 1)] //LARGO  1 - 20
        public override string NUMERO_MOTOR { get; set; }

        [Display(Name = "Numero Serie")]
        [Required(ErrorMessage = "(*) Numero Serie requerido")]       //CAMPO REQUERIDO
        [StringLength(20, MinimumLength = 1)] //LARGO  1 - 20
        public override string NUMERO_SERIE { get; set; }

        [Display(Name = "Numero VIN")]
        [Required(ErrorMessage = "(*) Numero VIN requerido")]       //CAMPO REQUERIDO
        [StringLength(20, MinimumLength = 1)] //LARGO  1 - 20
        public override string NUMERO_VIN { get; set; }

        [Display(Name = "PBV")]
        [Required(ErrorMessage = "(*) PBV requerido")]       //CAMPO REQUERIDO
        [Range(0, 5.2)] //LARGO 0 - 5.2
        public override decimal PESO_BRUTO_VEHICULAR { get; set; }

        [Display(Name = "Numero Puertas")]
        public override int? NUMERO_PUERTAS { get; set; }

        [Display(Name = "Tipo Vehiculo")]
        public override long ID_TIPO_VEHICULO { get; set; }

        [Display(Name = "Tipo Carga")]
        public override long ID_TIPO_CARGA { get; set; }

        [Display(Name = "Tipo PBV")]
        public override long ID_TIPO_PBV { get; set; }

        [Display(Name = "Tipo Carrocería")]
        public override long? ID_TIPO_CARROCERIA { get; set; }

        [Display(Name = "Tipo Potencia")]
        public override long? ID_TIPO_POTENCIA { get; set; }

        [Display(Name = "Nombre Color")]
        public string NOMBRE_COLOR { get; set; }

        [Display(Name = "Numero de ejes")]
        [RegularExpression("([0-9]+)", ErrorMessage = "(*) Numero de ejes no valido")]
        public override string NUMERO_EJES { get; set; }

        [Display(Name = "Otra Carroceria")]
        [StringLength(30, MinimumLength = 0)] //LARGO  0 - 30
        public override string OTRA_CARROCERIA { get; set; }

        [Display(Name = "Tipo Tracción")]
        [StringLength(4, MinimumLength = 0)] //LARGO  0 - 4
        public override string TIPO_TRACCION { get; set; }

        [Display(Name = "Potencia")]
        public override long? POTENCIA { get; set; }


        //LISTA DE DATOS PARA VISTA DE FORMULARIO
        public List<TIPO_COMBUSTIBLE_VEHICULO> LISTA_TIPOS_COMBUSTIBLE { get; set; }
        public List<TIPO_VEHICULO> LISTA_TIPOS_VEHICULO { get; set; }
        public List<TIPO_MEDIDA_VEHICULO> LISTA_TIPOS_MEDIDA { get; set; }
        public List<COLOR_VEHICULO> LISTA_COLORES_VEHICULO { get; set; }
        public List<TIPO_POTENCIA> LISTA_TIPOS_POTENCIA { get; set; }
        public List<TIPO_CARROCERIA> LISTA_TIPOS_CARROCERIA { get; set; }

        /// <summary>
        /// CONSTRUCTOR POR DEFECTO
        /// </summary>
        public VEHICULO_VM()
        {
            this.AGNO_FABRICACION = DateTime.Now.Year;
        }

        /// <summary>
        /// RETORNA OBJETO PARA CREAR EN REPOSITORIO
        /// </summary>
        /// <returns></returns>
        public VEHICULO OBJETO_CREAR()
        {
            //VALIDAR SEGÚN TIPO
            VALIDAR_TIPO_VEHICULO();

            return new VEHICULO
            {
                TERMINACION_PPU = this.TERMINACION_PPU,
                CODIGO_INFORME_TECNICO = this.CODIGO_INFORME_TECNICO,
                NUMERO_ASIENTOS = this.NUMERO_ASIENTOS,
                AGNO_FABRICACION = this.AGNO_FABRICACION,
                ID_TIPO_COMBUSTIBLE = this.ID_TIPO_COMBUSTIBLE,
                MARCA = this.MARCA,
                MODELO = this.MODELO,
                NUMERO_CHASIS = this.NUMERO_CHASIS,
                NUMERO_MOTOR = this.NUMERO_MOTOR,
                NUMERO_SERIE = this.NUMERO_SERIE,
                NUMERO_VIN = this.NUMERO_VIN,
                PESO_BRUTO_VEHICULAR = this.PESO_BRUTO_VEHICULAR,
                NUMERO_PUERTAS = this.NUMERO_PUERTAS,
                ID_TIPO_VEHICULO = this.ID_TIPO_VEHICULO,
                ID_TIPO_CARGA = this.ID_TIPO_CARGA,
                ID_TIPO_PBV = this.ID_TIPO_PBV,
                COLOR = this.COLOR,
                
                //DATOS VEHICULOS PESADOS
                ID_TIPO_POTENCIA = this.ID_TIPO_POTENCIA,
                ID_TIPO_CARROCERIA = this.ID_TIPO_CARROCERIA,
                NUMERO_EJES = this.NUMERO_EJES,
                OTRA_CARROCERIA = this.OTRA_CARROCERIA,
                TIPO_TRACCION = this.TIPO_TRACCION,
                POTENCIA = this.POTENCIA
            };
        }

        /// <summary>
        /// VALIDACIONES SEGÚN TIPO DE VEHICULO
        /// </summary>
        private void VALIDAR_TIPO_VEHICULO()
        {
            //TIPOS DE VEHICULOS PESADOS...
            List<TIPO_VEHICULO> TIPOS_VEHICULOS_CARGA = TIPO_VEHICULO_R.TIPOS_VEHICULOS_PESADOS().ToList();

            //ES VEHICULO PESADO...
            bool ES_CARGA = TIPOS_VEHICULOS_CARGA.Any(X => X.ID_TIPO_VEHICULO == this.ID_TIPO_VEHICULO);

            //SI EL TIPO DE VEHICULO NO ES CARGA...
            if (ES_CARGA == false)
            {
                this.ID_TIPO_POTENCIA = null;
                this.ID_TIPO_CARROCERIA = null;
                this.NUMERO_EJES = null;
                this.OTRA_CARROCERIA = null;
                this.TIPO_TRACCION = null;
                this.POTENCIA = null;
            };
        }

    }
}