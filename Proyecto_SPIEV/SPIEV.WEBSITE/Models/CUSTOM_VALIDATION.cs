﻿using SPIEV.UTIL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEBSITE.Models
{
    /// <summary>
    /// VALIDAR RUT CON FORMATO XXXXXXXX-X (EJ. 19570929-7)
    /// </summary>
    public class RutValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string RUT = value.ToString();

            //SI NO ES VACIO O NULO
            if (String.IsNullOrEmpty(RUT) == false)
            {
                bool RUT_VALIDO = Rut_Validar.ValidaRut(RUT);

                return (RUT_VALIDO) ? ValidationResult.Success : new ValidationResult(this.ErrorMessage);
            }

            return new ValidationResult(this.ErrorMessage);
        }
    }

    /// <summary>
    /// VALIDAR ARRAY DE NUMEROS (LONG) EN CASO DE NULO O VACIOS
    /// </summary>
    public class ListNotEmptyValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var list = value as IList;

            if (list != null)
            {
                return (list.Count > 0) ? ValidationResult.Success : new ValidationResult(this.ErrorMessage);
            }

            return new ValidationResult(this.ErrorMessage);
        }
    }
}