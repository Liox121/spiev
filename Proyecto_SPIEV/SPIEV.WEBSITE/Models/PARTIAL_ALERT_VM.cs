﻿namespace SPIEV.WEBSITE.Models
{
    public class PARTIAL_ALERT_VM
    {
        /// <summary>
        /// Clases para div de alerta 
        /// </summary>
        public string Clase { get; set; }

        /// <summary>
        /// Mensaje para div de alerta
        /// </summary>
        public string Mensaje { get; set; }
    }
}