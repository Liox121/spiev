﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;

namespace SPIEV.WEBSITE.Models
{
    public class SPIEV_VM
    {
        public VEHICULO_VM VEHICULO { get; set; }
        public COMUNIDAD_VM COMUNIDAD { get; set; }
        public ADQUIRIENTE_VM ADQUIRIENTE { get; set; }
        public SOLICITANTE_VM SOLICITANTE { get; set; }
        public ESTIPULANTE_VM ESTIPULANTE { get; set; }
        public IMPUESTO_ADICIONAL_VM IMPUESTO { get; set; }
        public FACTURA_VM FACTURA { get; set; }
        public OBSERVACION_VM OBSERVACION { get; set; }
        public OPERADOR_VM OPERADOR { get; set; }

        //LISTA PARA LAS VISTAS
        public List<COMUNA> LISTA_COMUNAS { get; set; }

        //DESDE SOLICITUD
        public long? ID_SOLICITUD { get; set; }
    }
}