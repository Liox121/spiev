﻿using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.UTIL
{
    public class GenerateExcel
    {
        public void GENERAR_DESDE_PLANTILLA(long FILA_INI,  string RUTA_PLANTILLA, DataTable CELDAS_VALORES, MemoryStream SALIDA_STREAM)
        {
            //SI LA PLANTILLA NO EXISTE
            if (!File.Exists(RUTA_PLANTILLA)) return;

            //ABRIMOS EL EXCEL
            SLDocument EXCEL = new SLDocument(RUTA_PLANTILLA);

            //CELDA INICIAL...
            long CELDA_INICIAL = FILA_INI;

            //RECORRIMOS TODA LA DATA...
            foreach (DataRow FILA in CELDAS_VALORES.Rows)
            {
                foreach (DataColumn COLUMNA in CELDAS_VALORES.Columns)
                {
                    var VALOR = FILA[COLUMNA].ToString();
                    var CELDA = $"{COLUMNA.ColumnName}{CELDA_INICIAL}";

                    //SETEAR EL VALOR DEL LA CELDA CORRESPONDIENTE
                    EXCEL.SetCellValue(CELDA, VALOR);

                }
                CELDA_INICIAL++;
            }

            //PRIMERA Y ULTIMA CELDA...
            var PRIMERA_CELDA = $"{CELDAS_VALORES.Columns[0].ColumnName}{FILA_INI}";
            var ULTIMA_CELDA = $"{CELDAS_VALORES.Columns[CELDAS_VALORES.Columns.Count - 1].ColumnName}{FILA_INI}";

            //AJUSTAR TEXTO EN LA CELDA
            EXCEL.AutoFitColumn(PRIMERA_CELDA, ULTIMA_CELDA);

            //GUARDAMOS
            EXCEL.SaveAs(SALIDA_STREAM);
        }

        public void GENERAR_DESDE_STREAM(long FILA_INI, MemoryStream ENTRADA_STREAM, DataTable CELDAS_VALORES, MemoryStream SALIDA_STREAM)
        {
            //SI LA PLANTILLA NO EXISTE
            if (ENTRADA_STREAM == null) return;

            //ABRIMOS EL EXCEL
            SLDocument EXCEL = new SLDocument(ENTRADA_STREAM);

            //CELDA INICIAL...
            long CELDA_INICIAL = FILA_INI;

            //RECORRIMOS TODA LA DATA...
            foreach (DataRow FILA in CELDAS_VALORES.Rows)
            {
                foreach (DataColumn COLUMNA in CELDAS_VALORES.Columns)
                {
                    var VALOR = FILA[COLUMNA].ToString();
                    var CELDA = $"{COLUMNA.ColumnName}{CELDA_INICIAL}";

                    //SETEAR EL VALOR DEL LA CELDA CORRESPONDIENTE
                    EXCEL.SetCellValue(CELDA, VALOR);

                }
                CELDA_INICIAL++;
            }

            //PRIMERA Y ULTIMA CELDA...
            var PRIMERA_CELDA = $"{CELDAS_VALORES.Columns[0].ColumnName}{FILA_INI}";
            var ULTIMA_CELDA = $"{CELDAS_VALORES.Columns[CELDAS_VALORES.Columns.Count - 1].ColumnName}{FILA_INI}";

            //AJUSTAR TEXTO EN LA CELDA
            EXCEL.AutoFitColumn(PRIMERA_CELDA, ULTIMA_CELDA);

            //GUARDAMOS
            EXCEL.SaveAs(SALIDA_STREAM);
        }
    }
}
