﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.UTIL.Export_Excel
{
    public class Export_Base_A : Export_Base
    {
        public string AA { get; set; }
        public string AB { get; set; }
        public string AC { get; set; }
        public string AD { get; set; }
        public string AE { get; set; }
        public string AF { get; set; }
        public string AG { get; set; }
        public string AH { get; set; }
        public string AI { get; set; }
        public string AJ { get; set; }
        public string AK { get; set; }
        public string AL { get; set; }
        public string AM { get; set; }
        public string AN { get; set; }
        public string AO { get; set; }
        public string AP { get; set; }
        public string AQ { get; set; }
        public string AR { get; set; }
        public string AS { get; set; }
        public string AT { get; set; }
        public string AU { get; set; }
        public string AV { get; set; }
        public string AW { get; set; }
        public string AX { get; set; }
        public string AY { get; set; }
        public string AZ { get; set; }
    }
}
