﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.UTIL.Export_Excel
{
    public class Export_Base_C : Export_Base_B
    {
        public string CA { get; set; }
        public string CB { get; set; }
        public string CC { get; set; }
        public string CD { get; set; }
        public string CE { get; set; }
        public string CF { get; set; }
        public string CG { get; set; }
        public string CH { get; set; }
        public string CI { get; set; }
        public string CJ { get; set; }
        public string CK { get; set; }
        public string CL { get; set; }
        public string CM { get; set; }
        public string CN { get; set; }
        public string CO { get; set; }
        public string CP { get; set; }
        public string CQ { get; set; }
        public string CR { get; set; }
        public string CS { get; set; }
        public string CT { get; set; }
        public string CU { get; set; }
        public string CV { get; set; }
        public string CW { get; set; }
        public string CX { get; set; }
        public string CY { get; set; }
        public string CZ { get; set; }
    }
}
