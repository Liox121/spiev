﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.UTIL.Export_Excel
{
    public class Export_Base_B : Export_Base_A
    {
        public string BA { get; set; }
        public string BB { get; set; }
        public string BC { get; set; }
        public string BD { get; set; }
        public string BE { get; set; }
        public string BF { get; set; }
        public string BG { get; set; }
        public string BH { get; set; }
        public string BI { get; set; }
        public string BJ { get; set; }
        public string BK { get; set; }
        public string BL { get; set; }
        public string BM { get; set; }
        public string BN { get; set; }
        public string BO { get; set; }
        public string BP { get; set; }
        public string BQ { get; set; }
        public string BR { get; set; }
        public string BS { get; set; }
        public string BT { get; set; }
        public string BU { get; set; }
        public string BV { get; set; }
        public string BW { get; set; }
        public string BX { get; set; }
        public string BY { get; set; }
        public string BZ { get; set; }
    }
}
