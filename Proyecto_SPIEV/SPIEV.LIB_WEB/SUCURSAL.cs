﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class SUCURSAL : SUCURSAL_DTO
    {
        public override string NOMBRE_SUCURSAL { get; set; }
        public EMPRESA EMPRESA { get; set; }
        
    }
}
