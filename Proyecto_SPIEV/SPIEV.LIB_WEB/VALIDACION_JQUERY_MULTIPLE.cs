﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB_WEB
{
    public class VALIDACION_JQUERY_MULTIPLE
    {
        public String ID_INPUT { get; set; }
        public String MENSAJE_REQUERIDO { get; set; }
        public String MENSAJE_MINIMO { get; set; }
        public List<VALIDACION_JQUERY> VALIDACIONES { get; set; }
    }
}
