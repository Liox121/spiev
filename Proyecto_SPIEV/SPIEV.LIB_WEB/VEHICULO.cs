﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class VEHICULO : VEHICULO_DTO
    {
        public override string TERMINACION_PPU { get; set; }
        public override int AGNO_FABRICACION { get; set; }
        public override int? NUMERO_ASIENTOS { get; set; }
        public override decimal? CARGA { get; set; }
        public override string CODIGO_INFORME_TECNICO { get; set; }
        public override int ID_TIPO_COMBUSTIBLE { get; set; }
        public override string MARCA { get; set; }
        public override string MODELO { get; set; }
        public override string NUMERO_CHASIS { get; set; }
        public override string NUMERO_MOTOR { get; set; }
        public override string NUMERO_SERIE { get; set; }
        public override string NUMERO_VIN { get; set; }
        public override string NUMERO_EJES { get; set; }
        public override decimal PESO_BRUTO_VEHICULAR { get; set; }
        public override int? NUMERO_PUERTAS { get; set; }
        public override long? POTENCIA { get; set; }
        public override string COLOR { get; set; }
        public override string OTRA_CARROCERIA { get; set; }
        public override long ID_TIPO_VEHICULO { get; set; }
        public override long ID_TIPO_CARGA { get; set; }
        public override long ID_TIPO_PBV { get; set; }
        public override long? ID_TIPO_CARROCERIA { get; set; }
        public override long? ID_TIPO_POTENCIA { get; set; }
        public override string TIPO_TRACCION { get; set; }


        //SUB-OBJETOS
        public TIPO_COMBUSTIBLE_VEHICULO TIPO_COMBUSTIBLE { get; set; }
        public TIPO_VEHICULO TIPO_VEHICULO { get; set; }
        public TIPO_MEDIDA_VEHICULO TIPO_CARGA { get; set; }
        public TIPO_MEDIDA_VEHICULO TIPO_PBV { get; set; }
        public TIPO_POTENCIA TIPO_POTENCIA { get; set; }
        public TIPO_CARROCERIA TIPO_CARROCERIA { get; set; }
        //FIN SUB-OBJETOS

        public VEHICULO()
        {
            this.TIPO_COMBUSTIBLE = new TIPO_COMBUSTIBLE_VEHICULO();
            this.TIPO_VEHICULO = new TIPO_VEHICULO();
            this.TIPO_CARGA = new TIPO_MEDIDA_VEHICULO();
            this.TIPO_PBV = new TIPO_MEDIDA_VEHICULO();
            this.TIPO_POTENCIA = new TIPO_POTENCIA();
            this.TIPO_CARROCERIA = new TIPO_CARROCERIA();
        }
    }
}
