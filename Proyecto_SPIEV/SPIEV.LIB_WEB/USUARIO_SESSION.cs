﻿using SPIEV.LIB;
using System.Collections.Generic;

namespace SPIEV.LIB_WEB
{
    public class USUARIO_SESSION : USUARIO_DTO
    {
        public override string NOMBRE_USUARIO { get; set; }
        public override string NOMBRE_COMPLETO { get; set; }
        public override string CORREO_ELECTRONICO { get; set; }
        public List<SUCURSAL_USUARIO> MIS_SUCURSALES { get; set; }
        public List<USUARIO_PERFIL_SESSION> MIS_PERFILES { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void USUARIO_SESSION_DESDE_USUARIO(USUARIO USUARIO_BASE)
        {
            this.ID_USUARIO = USUARIO_BASE.ID_USUARIO;
            this.NOMBRE_USUARIO = USUARIO_BASE.NOMBRE_USUARIO;
            this.NOMBRE_COMPLETO = USUARIO_BASE.NOMBRE_COMPLETO;
            this.CLAVE = USUARIO_BASE.CLAVE;
            this.CORREO_ELECTRONICO = USUARIO_BASE.CORREO_ELECTRONICO;
            this.ACTIVO = USUARIO_BASE.ACTIVO;
        }
    }
}