﻿using SPIEV.LIB;
using System.Collections.Generic;

namespace SPIEV.LIB_WEB
{
    public class PERFIL_SESSION : PERFIL_DTO
    {
        public List<PAGINA_PERFIL> PAGINAS { get; set; }
        public override string NOMBRE { get; set; }
    }
}