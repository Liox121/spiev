﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class OPERADOR : OPERADOR_DTO
    {

        public override int ID_REGION { get; set; }
        public override int RUN_USUARIO { get; set; }
        public override int RUN_EMPRESA { get; set; }

        public REGION REGION { get; set; }

        public OPERADOR()
        {
            this.REGION = new REGION();
        }
    }
}