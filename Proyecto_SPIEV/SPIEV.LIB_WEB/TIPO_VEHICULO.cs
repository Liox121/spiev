﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class TIPO_VEHICULO : TIPO_VEHICULO_DTO
    {
        public CATEGORIA_TIPO_VEHICULO CATEGORIA_TIPO_VEHICULO { get; set; }

        public TIPO_VEHICULO()
        {
            this.CATEGORIA_TIPO_VEHICULO = new CATEGORIA_TIPO_VEHICULO();
        }
    }
}
