﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB_WEB
{
    public class VALIDACION_JQUERY
    {
        public long? ID { get; set; }
        public bool? FOR_ALL { get; set; }
        public bool? REQUERIDO { get; set; }
        public float? MIN { get; set; }
        public string DEFECTO { get; set; }
    }
}
