﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class TIPO_ARCHIVO_SOLICITUD : TIPO_ARCHIVO_SOLICITUD_DTO
    {
        public TIPO_ARCHIVO TIPO_ARCHIVO { get; set; }
    }
}