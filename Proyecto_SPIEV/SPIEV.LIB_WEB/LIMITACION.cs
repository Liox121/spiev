﻿using SPIEV.LIB;


namespace SPIEV.LIB_WEB
{
    public class LIMITACION : LIMITACION_DTO
    {
        public SPIEV SPIEV { get; set; }
        public USUARIO USUARIO { get; set; }
        public ESTADO_LIMITACION ESTADO { get; set; }
        public ACREEDOR ACREEDOR { get; set; }
        public PROPIETARIO PROPIETARIO { get; set; }
        public DOCUMENTACION_LIMITACION ID_DOCUMENTACION_LIMITACION{ get; set; }

        public LIMITACION()
        {
            SPIEV                       = new SPIEV();
            ESTADO                      = new ESTADO_LIMITACION();
            USUARIO                     = new USUARIO();
            ACREEDOR                    = new ACREEDOR();
            PROPIETARIO                 = new PROPIETARIO();
            ID_DOCUMENTACION_LIMITACION = new DOCUMENTACION_LIMITACION();
        }                 


    }


}
