﻿using SPIEV.LIB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB_WEB
{
    public class SERVICIO_SOLICITUD : SERVICIO_SOLICITUD_DTO
    {
        public SERVICIO SERVICIO { get; set; }
        public SOLICITUD_SPIEV SOLICITUD { get; set; }
    }
}
