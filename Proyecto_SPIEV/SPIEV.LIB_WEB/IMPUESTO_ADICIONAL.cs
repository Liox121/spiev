﻿using SPIEV.LIB;
using System.ComponentModel.DataAnnotations;

namespace SPIEV.LIB_WEB
{
    public class IMPUESTO_ADICIONAL : IMPUESTO_ADICIONAL_DTO
    {
        [Display(Name = "Codigo Identificación de pago")]
        [Required(ErrorMessage = "(*) Codigo Identificación de pago requerido")]
        [StringLength(30)]
        public override string CODIGO_IDENTIFICACION { get; set; }

        [Display(Name = "Monto Impuesto Pagado")]
        [Required(ErrorMessage = "(*) Monto Impuesto Pagado requerido")]
        public override long MONTO_IMPUESTO { get; set; }

        [Display(Name = "Monto Total Factura")]
        [Required(ErrorMessage = "(*) Monto Total Factura requerida")]
        public override long TOTAL_FACTURA { get; set; }

        [Display(Name = "Codigo Informe Tecnico")]
        [Required(ErrorMessage = "(*) Codigo Informe Tecnico requerido")]
        [StringLength(26)]
        public override string CODIGO_INFORME_TECNICO { get; set; }
    }
}