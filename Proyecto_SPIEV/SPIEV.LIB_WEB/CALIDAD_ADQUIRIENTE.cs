﻿using SPIEV.LIB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB_WEB
{
    public class CALIDAD_ADQUIRIENTE : CALIDAD_ADQUIRIENTE_DTO
    {
        public CALIDAD_PERSONA CALIDAD_PERSONA { get; set; }

        public CALIDAD_ADQUIRIENTE()
        {
            this.CALIDAD_PERSONA = new CALIDAD_PERSONA();
        }

    }
}
