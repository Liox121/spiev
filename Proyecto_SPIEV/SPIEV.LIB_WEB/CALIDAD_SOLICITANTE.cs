﻿using SPIEV.LIB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB_WEB
{
    public class CALIDAD_SOLICITANTE : CALIDAD_SOLICITANTE_DTO
    {
        public CALIDAD_PERSONA CALIDAD_PERSONA { get; set; }

        public CALIDAD_SOLICITANTE()
        {
            this.CALIDAD_PERSONA = new CALIDAD_PERSONA();
        }
    }
}
