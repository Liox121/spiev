﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class USUARIO_PERFIL_SESSION : USUARIO_PERFIL_DTO
    {
        public PERFIL_SESSION PERFIL { get; set; }
    }
}