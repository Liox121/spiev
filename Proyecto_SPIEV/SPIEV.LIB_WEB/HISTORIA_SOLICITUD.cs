﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class HISTORIA_SOLICITUD : HISTORIA_SOLICITUD_DTO
    {
        public USUARIO USUARIO { get; set; }
        public ESTADO_SOLICITUD_SPIEV ESTADO { get; set; }
    }
}