﻿using SPIEV.LIB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB_WEB
{
    public class SPIEV : SPIEV_DTO
    {
        public VEHICULO VEHICULO { get; set; }
        public COMUNIDAD COMUNIDAD { get; set; }
        public SOLICITANTE SOLICITANTE { get; set; }
        public OBSERVACION OBSERVACION { get; set; }
        public OPERADOR OPERADOR { get; set; }

        public SPIEV()
        {
            VEHICULO = new VEHICULO();
            COMUNIDAD = new COMUNIDAD();
            SOLICITANTE = new SOLICITANTE();
            OBSERVACION = new OBSERVACION();
            OPERADOR = new OPERADOR();
        }
    }
}
