﻿using SPIEV.LIB;
using SPIEV.UTIL.Export_Excel;

namespace SPIEV.LIB_WEB
{
    public class PRIMERA_INSCRIPCION : PRIMERA_INSCRIPCION_DTO
    {
        public SPIEV SPIEV { get; set; }
        public ESTADO_PRIMERA_INSCRIPCION ESTADO { get; set; }
        public USUARIO USUARIO { get; set; }
        public ESTIPULANTE ESTIPULANTE { get; set; }
        public ADQUIRIENTE ADQUIRIENTE { get; set; }
        public IMPUESTO_ADICIONAL IMPUESTO { get; set; }
        public FACTURA FACTURA { get; set; }

        public PRIMERA_INSCRIPCION()
        {
            SPIEV = new SPIEV();
            ESTADO = new ESTADO_PRIMERA_INSCRIPCION();
            USUARIO = new USUARIO();
            ESTIPULANTE = new ESTIPULANTE();
            IMPUESTO = new IMPUESTO_ADICIONAL();
            FACTURA = new FACTURA();
            ADQUIRIENTE = new ADQUIRIENTE();
        }

        public Export_Base_C EXPORTAR_PRIMERA_INSCRIPCION()
        {
            return new Export_Base_C
            {
                A = this.ID_PRIMERA_INSCRIPCION.ToString(),
                B = this.SPIEV.FECHA.ToString(),
                C = this.ESTADO.NOMBRE_ESTADO,
                D = this.SPIEV.VEHICULO.ID_VEHICULO.ToString(),
                E = this.SPIEV.VEHICULO.CODIGO_INFORME_TECNICO,
                F = this.SPIEV.VEHICULO.NUMERO_ASIENTOS.ToString(),
                G = this.SPIEV.VEHICULO.AGNO_FABRICACION.ToString(),
                H = this.SPIEV.VEHICULO.TIPO_COMBUSTIBLE.NOMBRE_COMBUSTIBLE,
                I = this.SPIEV.VEHICULO.MARCA,
                J = this.SPIEV.VEHICULO.MODELO,
                K = this.SPIEV.VEHICULO.NUMERO_CHASIS,
                L = this.SPIEV.VEHICULO.NUMERO_MOTOR,
                M = this.SPIEV.VEHICULO.NUMERO_SERIE,
                N = this.SPIEV.VEHICULO.NUMERO_VIN,
                O = this.SPIEV.VEHICULO.PESO_BRUTO_VEHICULAR.ToString(),
                P = this.SPIEV.VEHICULO.NUMERO_PUERTAS.ToString(),
                Q = this.SPIEV.VEHICULO.TIPO_VEHICULO.NOMBRE_TIPO,
                R = this.SPIEV.VEHICULO.TIPO_CARGA.TIPO_CARGA_DESCRIPCION,
                S = this.SPIEV.VEHICULO.TIPO_PBV.TIPO_CARGA_DESCRIPCION,

                T = this.SPIEV.COMUNIDAD.ID_COMUNIDAD.ToString(),
                U = this.SPIEV.COMUNIDAD.OPCION_COMUNIDAD.OPCION,
                V = this.SPIEV.COMUNIDAD.CANTIDAD.ToString(),

                W = this.ADQUIRIENTE.ID_ADQUIRIENTE.ToString(),
                X = this.ADQUIRIENTE.APELLIDO_PATERNO,
                Y = this.ADQUIRIENTE.APELLIDO_MATERNO,
                Z = this.ADQUIRIENTE.RAZON_SOCIAL,
                AA = this.ADQUIRIENTE.RUN.ToString(),
                AB = this.ADQUIRIENTE.CALLE,
                AC = this.ADQUIRIENTE.NUMERO_DOMICILIO,
                AD = this.ADQUIRIENTE.LETRA_DOMICILIO,
                AE = this.ADQUIRIENTE.RESTO_DOMICILIO,
                AF = this.ADQUIRIENTE.TELEFONO,
                AG = this.ADQUIRIENTE.CODIGO_POSTAL,
                AH = this.ADQUIRIENTE.CORREO_ELECTRONICO,
                AI = this.ADQUIRIENTE.COMUNA.NOMBRE,
                AJ = this.ADQUIRIENTE.CALIDAD_PERSONA.TIPO_PERSONA_DESCRIPCION,

                AK = this.SPIEV.SOLICITANTE.ID_SOLICITANTE.ToString(),
                AL = this.SPIEV.SOLICITANTE.APELLIDO_PATERNO,
                AM = this.SPIEV.SOLICITANTE.APELLIDO_MATERNO,
                AN = this.SPIEV.SOLICITANTE.RAZON_SOCIAL,
                AO = this.SPIEV.SOLICITANTE.RUN.ToString(),
                AP = this.SPIEV.SOLICITANTE.CALLE,
                AQ = this.SPIEV.SOLICITANTE.NUMERO_DOMICILIO,
                AR = this.SPIEV.SOLICITANTE.LETRA_DOMICILIO,
                AS = this.SPIEV.SOLICITANTE.RESTO_DOMICILIO,
                AT = this.SPIEV.SOLICITANTE.TELEFONO,
                AU = this.SPIEV.SOLICITANTE.CODIGO_POSTAL,
                AV = this.SPIEV.SOLICITANTE.CORREO_ELECTRONICO,
                AW = this.SPIEV.SOLICITANTE.COMUNA.NOMBRE,

                AX = this.ESTIPULANTE.ID_ESTIPULANTE.ToString(),
                AY = this.ESTIPULANTE.APELLIDO_PATERNO,
                AZ = this.ESTIPULANTE.APELLIDO_MATERNO,
                BA = this.ESTIPULANTE.RAZON_SOCIAL,
                BB = this.ESTIPULANTE.RUN.ToString(),
                BC = this.ESTIPULANTE.CALLE,
                BD = this.ESTIPULANTE.NUMERO_DOMICILIO,
                BE = this.ESTIPULANTE.LETRA_DOMICILIO,
                BF = this.ESTIPULANTE.RESTO_DOMICILIO,
                BG = this.ESTIPULANTE.TELEFONO,
                BH = this.ESTIPULANTE.CODIGO_POSTAL,
                BI = this.ESTIPULANTE.CORREO_ELECTRONICO,
                BJ = this.ESTIPULANTE.COMUNA.NOMBRE,
                BK = this.ESTIPULANTE.CALIDAD_ESTIPULANTE.CALIDAD_PERSONA.TIPO_PERSONA_DESCRIPCION,

                BL = this.IMPUESTO.ID_IMPUESTO_ADICIONAL.ToString(),
                BM = this.IMPUESTO.CODIGO_IDENTIFICACION,
                BN = this.IMPUESTO.CODIGO_INFORME_TECNICO,
                BO = this.IMPUESTO.MONTO_IMPUESTO.ToString(),
                BP = this.IMPUESTO.TOTAL_FACTURA.ToString(),

                BQ = this.FACTURA.ID_FACTURA.ToString(),
                BR = this.FACTURA.NUMERO_FACTURA.ToString(),
                BS = this.FACTURA.FECHA.ToString(),
                BT = this.FACTURA.COMUNA.NOMBRE,
                BU = this.FACTURA.RUT_EMISOR.ToString(),
                BV = this.FACTURA.NOMBRE_EMISOR,
                BW = this.FACTURA.MONTO_TOTAL_FACTURA.ToString(),
                BX = this.FACTURA.TIPO_MONEDA.TIPO_MONEDA_DESCRIPCION,

                BY = this.SPIEV.OBSERVACION.ID_OBSERVACION.ToString(),
                BZ = this.SPIEV.OBSERVACION.COMENTARIO,

                CA = this.SPIEV.OPERADOR.ID_OPERADOR.ToString(),
                CB = this.SPIEV.OPERADOR.REGION.NOMBRE,
                CC = this.SPIEV.OPERADOR.RUN_USUARIO.ToString(),
                CD = this.SPIEV.OPERADOR.RUN_EMPRESA.ToString()
            };
        }
    }
}