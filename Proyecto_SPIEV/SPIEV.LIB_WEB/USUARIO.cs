﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class USUARIO : USUARIO_DTO
    {
        public override string NOMBRE_USUARIO { get; set; }
        public override string NOMBRE_COMPLETO { get; set; }
        public override string CORREO_ELECTRONICO { get; set; }

    }
}