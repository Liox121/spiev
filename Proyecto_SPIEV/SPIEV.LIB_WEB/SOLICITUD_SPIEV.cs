﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class SOLICITUD_SPIEV : SOLICITUD_SPIEV_DTO
    {
        public USUARIO USUARIO { get; set; }
        public ESTADO_SOLICITUD_SPIEV ESTADO { get; set; }
        public SUCURSAL SUCURSAL { get; set; }
        public PRIMERA_INSCRIPCION PRIMERA_INSCRIPCION { get; set; }
    }
}