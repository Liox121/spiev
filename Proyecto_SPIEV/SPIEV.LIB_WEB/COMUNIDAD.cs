﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class COMUNIDAD : COMUNIDAD_DTO
    {
        public override long CANTIDAD { get; set; }
        public override long ID_OPCION_COMUNIDAD { get; set; }
        public OPCION_COMUNIDAD OPCION_COMUNIDAD { get; set; }

        public COMUNIDAD()
        {
            this.OPCION_COMUNIDAD = new OPCION_COMUNIDAD();
        }
    }
}