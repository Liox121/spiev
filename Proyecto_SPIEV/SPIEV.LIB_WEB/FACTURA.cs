﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class FACTURA : FACTURA_DTO
    {
        public override long NUMERO_FACTURA { get; set; }
        public override int FECHA { get; set; }
        public override long ID_COMUNA { get; set; }
        public override int RUT_EMISOR { get; set; }
        public override string NOMBRE_EMISOR { get; set; }
        public override int MONTO_TOTAL_FACTURA { get; set; }
        public override long ID_TIPO_MONEDA { get; set; }

        //SUB-OBJETO
        public COMUNA COMUNA { get; set; } 
        public TIPO_MONEDA TIPO_MONEDA { get; set; }

        public FACTURA()
        {
            this.COMUNA = new COMUNA();
            this.TIPO_MONEDA = new TIPO_MONEDA();
        }
    }
}