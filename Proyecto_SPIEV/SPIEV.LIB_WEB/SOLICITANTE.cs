﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class SOLICITANTE : SOLICITANTE_DTO
    {
        public override string RAZON_SOCIAL { get; set; }
        public override string APELLIDO_PATERNO { get; set; }
        public override string APELLIDO_MATERNO { get; set; }
        public override long RUN { get; set; }
        public override long ID_CALIDAD_SOLICITANTE { get; set; }
        public override string CALLE { get; set; }
        public override long ID_COMUNA { get; set; }
        public override string NUMERO_DOMICILIO { get; set; }
        public override string LETRA_DOMICILIO { get; set; }
        public override string RESTO_DOMICILIO { get; set; }
        public override string TELEFONO { get; set; }
        public override string CODIGO_POSTAL { get; set; }
        public override string CORREO_ELECTRONICO { get; set; }

        //SUB-OBJETOS
        public COMUNA COMUNA { get; set; }

        public SOLICITANTE()
        {
            this.COMUNA = new COMUNA();
        }
    }
}