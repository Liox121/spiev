﻿using SPIEV.LIB;

namespace SPIEV.LIB_WEB
{
    public class HISTORIA_INSCRIPCION : HISTORIA_INSCRIPCION_DTO
    {
        public USUARIO_DTO USUARIO { get; set; }
        public ESTADO_PRIMERA_INSCRIPCION ESTADO { get; set; }

    }
}