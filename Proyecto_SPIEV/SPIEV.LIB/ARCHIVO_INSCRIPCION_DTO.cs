﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB
{
    public class ARCHIVO_INSCRIPCION_DTO
    {
        public long ID_ARCHIVO { get; set; }
        public long ID_PRIMERA_INSCRIPCION { get; set; }
        public long ID_TIPO_ARCHIVO_INSCRIPCION { get; set; }
        public string ARCHIVO { get; set; }
        public bool BORRADO { get; set; }
    }
}
