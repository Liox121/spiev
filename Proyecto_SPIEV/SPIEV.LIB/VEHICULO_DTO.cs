﻿namespace SPIEV.LIB
{
    public abstract class VEHICULO_DTO
    {
        public long ID_VEHICULO { get; set; }
        public abstract string TERMINACION_PPU { get; set; }
        public abstract int AGNO_FABRICACION { get; set; }
        public abstract int? NUMERO_ASIENTOS { get; set; }
        public abstract decimal? CARGA { get; set; }
        public abstract string CODIGO_INFORME_TECNICO { get; set; }
        public abstract int ID_TIPO_COMBUSTIBLE { get; set; }
        public abstract long? POTENCIA { get; set; }
        public abstract string MARCA { get; set; }
        public abstract string MODELO { get; set; }
        public abstract string NUMERO_CHASIS { get; set; }
        public abstract string NUMERO_MOTOR { get; set; }
        public abstract string NUMERO_SERIE { get; set; }
        public abstract string NUMERO_VIN { get; set; }
        public abstract string NUMERO_EJES { get; set; }
        public abstract string TIPO_TRACCION { get; set; }
        public abstract string COLOR { get; set; }
        public abstract string OTRA_CARROCERIA { get; set; }
        public abstract decimal PESO_BRUTO_VEHICULAR { get; set; }
        public abstract int? NUMERO_PUERTAS { get; set; }
        public abstract long ID_TIPO_VEHICULO { get; set; }
        public abstract long ID_TIPO_CARGA { get; set; }
        public abstract long ID_TIPO_PBV { get; set; }
        public abstract long? ID_TIPO_CARROCERIA { get; set; }
        public abstract long? ID_TIPO_POTENCIA { get; set; }
    }
}
