﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB
{
    public abstract class SPIEV_DTO
    {
        public long ID_SPIEV { get; set; }
        public long ID_VEHICULO { get; set; }
        public long ID_COMUNIDAD { get; set; }
        public long ID_SOLICITANTE { get; set; }
        public long ID_OBSERVACION { get; set; }
        public long ID_OPERADOR { get; set; }
        public DateTime FECHA { get; set; }
    }
}
