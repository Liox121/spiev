﻿namespace SPIEV.LIB
{
    public class TIPO_MONEDA_DTO
    {
        public long ID_TIPO_MONEDA { get; set; }
        public string TIPO_MONEDA { get; set; }
        public string TIPO_MONEDA_DESCRIPCION { get; set; }
    }
}
