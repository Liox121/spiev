﻿namespace SPIEV.LIB
{
    public abstract class EMPRESA_DTO
    {
        public long ID_EMPRESA { get; set; }
        public abstract string NOMBRE_EMPRESA { get; set; }
        public bool BORRADO { get; set; }
    }
}
