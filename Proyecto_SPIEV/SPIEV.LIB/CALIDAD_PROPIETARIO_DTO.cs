﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB
{
    public class CALIDAD_PROPIETARIO_DTO
    {
        public long ID_CALIDAD_PROPIETARIO { get; set; }
        public long ID_CALIDAD { get; set; }
    }
}
