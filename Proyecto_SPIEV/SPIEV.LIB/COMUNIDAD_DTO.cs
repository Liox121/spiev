﻿namespace SPIEV.LIB
{
    public abstract class COMUNIDAD_DTO
    {
        public long ID_COMUNIDAD { get; set; }
        public abstract long ID_OPCION_COMUNIDAD { get; set; }
        public abstract long CANTIDAD { get; set; }
    }
}
