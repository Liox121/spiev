﻿namespace SPIEV.LIB
{
    public abstract class CARGA_VEHICULO_DTO
    {
        public long ID_CARGA_VEHICULO { get; set; }
        public long ID_TIPO_VEHICULO { get; set; }
        public float VALOR_POSIBLE { get; set; }
    }
}
