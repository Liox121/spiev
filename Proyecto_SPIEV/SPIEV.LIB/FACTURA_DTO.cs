﻿namespace SPIEV.LIB
{
    public abstract class FACTURA_DTO
    {
        public long ID_FACTURA { get; set; }
        public abstract long NUMERO_FACTURA { get; set; }
        public abstract int FECHA { get; set; }
        public abstract long ID_COMUNA { get; set; }
        public abstract int RUT_EMISOR { get; set; }
        public abstract string NOMBRE_EMISOR { get; set; }
        public abstract int MONTO_TOTAL_FACTURA { get; set; }
        public abstract long ID_TIPO_MONEDA { get; set; }
    }
}
