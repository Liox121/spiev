﻿namespace SPIEV.LIB
{
    public abstract class SUCURSAL_DTO
    {
        public long ID_SUCURSAL { get; set; }
        public abstract string NOMBRE_SUCURSAL { get; set; }
        public long ID_EMPRESA { get; set; }
        public bool BORRADO { get; set; }
    }
}
