﻿namespace SPIEV.LIB
{
    public class TIPO_VEHICULO_DTO
    {
        public long ID_TIPO_VEHICULO { get; set; }
        public string NOMBRE_TIPO { get; set; }
        public long ID_CATEGORIA { get; set; }
    }
}
