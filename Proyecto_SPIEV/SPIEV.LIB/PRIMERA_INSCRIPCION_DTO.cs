﻿using System;

namespace SPIEV.LIB
{
    public abstract class PRIMERA_INSCRIPCION_DTO
    {
        public long ID_PRIMERA_INSCRIPCION { get; set; }
        public long ID_SPIEV { get; set; }
        public long ID_ESTADO { get; set; }
        public long ID_USUARIO { get; set; }
        public long ID_ESTIPULANTE { get; set; }
        public long ID_ADQUIRIENTE { get; set; }
        public long ID_IMPUESTO_ADICIONAL { get; set; }
        public long ID_FACTURA { get; set; }
    }
}
