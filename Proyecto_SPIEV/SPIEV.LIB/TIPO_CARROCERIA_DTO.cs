﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB
{
    public class TIPO_CARROCERIA_DTO
    {
        public long ID_TIPO_CARROCERIA { get; set; }
        public string NOMBRE_TIPO { get; set; }
    }
}
