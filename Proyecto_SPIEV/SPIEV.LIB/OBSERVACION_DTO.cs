﻿namespace SPIEV.LIB
{
    public abstract class OBSERVACION_DTO
    {
        public long ID_OBSERVACION { get; set; }
        public abstract string COMENTARIO { get; set; }
    }
}
