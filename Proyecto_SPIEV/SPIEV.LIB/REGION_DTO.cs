﻿namespace SPIEV.LIB
{
    public class REGION_DTO
    {
        public long ID_REGION { get; set; }
        public int CODIGO_REGION { get; set; }
        public string NOMBRE { get; set; }
    }
}
