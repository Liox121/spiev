﻿namespace SPIEV.LIB
{
    public class COLOR_VEHICULO_DTO
    {
        public long ID_COLOR { get; set; }
        public string COLOR_NOMBRE { get; set; }
        public string RESTO_COLOR { get; set; }
    }
}
