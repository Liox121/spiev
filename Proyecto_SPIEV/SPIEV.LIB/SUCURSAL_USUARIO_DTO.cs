﻿namespace SPIEV.LIB
{
    public abstract class SUCURSAL_USUARIO_DTO
    {
        public long ID_SUCURSAL_USUARIO { get; set; }
        public long ID_SUCURSAL { get; set; }
        public long ID_USUARIO { get; set; }
    }
}
