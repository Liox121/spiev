﻿namespace SPIEV.LIB
{
    public class TIPO_ARCHIVO_INSCRIPCION_DTO
    {
        public long ID_TIPO_ARCHIVO_INSCRIPCION { get; set; }
        public long ID_TIPO_ARCHIVO { get; set; }
        public int? TIPO_WS_SPIEV { get; set; }
    }
}
