﻿namespace SPIEV.LIB
{
    public abstract class PAGINA_PERFIL_DTO
    {
        public long ID_PAGINA_PERFIL { get; set; }
        public long ID_PAGINA { get; set; }
        public long ID_PERFIL { get; set; }
        public bool POR_DEFECTO { get; set; }
    }
}
