﻿namespace SPIEV.LIB
{
    public class USUARIO_PERFIL_DTO
    {
        public long ID_USUARIO_PERFIL { get; set; }
        public long ID_USUARIO { get; set; }
        public long ID_PERFIL { get; set; }
        public bool POR_DEFECTO { get; set; }
    }
}
