﻿using System;

namespace SPIEV.LIB
{
    public abstract class DOCUMENTACION_LIMITACION_DTO
    {

        public long ID_DOCUMENTACION_LIMITACION { get; set; }

        public long ID_TIPO_ARCHIVO_LIMITACION { get; set; }
        
        public long NUMERO_DOCUMENTO { get; set; }
        
        public long ID_COMUNA { get; set; }
        
        public DateTime FECHA_AUTORIZANTE { get; set; }
        
    }
}




