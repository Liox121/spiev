﻿namespace SPIEV.LIB
{
    public abstract class ESTADO_SOLICITUD_SPIEV_DTO
    {
        public long ID_ESTADO { get; set; }
        public string NOMBRE_ESTADO { get; set; }
    }
}
