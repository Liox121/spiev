﻿using System;

namespace SPIEV.LIB
{
    public class SOLICITUD_SPIEV_DTO
    {
        public long ID_SOLICITUD { get; set; }
        public long ID_USUARIO { get; set; }
        public long ID_SUCURSAL { get; set; }
        public long ID_ESTADO { get; set; }
        public long? ID_PRIMERA_INSCRIPCION { get; set; }
        public long? ID_LIMITACION { get; set; }
        public long? ID_FINANCIERA { get; set; }
        public int? TERMINACION_OPC1 { get; set; }
        public int? TERMINACION_OPC2 { get; set; }
        public long RUT_ADQUIRIENTE { get; set; }
        public char DV_ADQUIRIENTE { get; set; }
        public string CORREO_ADQUIRIENTE { get; set; }
        public string COMENTARIO { get; set; }
        public DateTime FECHA_SOLICITUD { get; set; }
        public bool BORRADO { get; set; }
    }
}
