﻿namespace SPIEV.LIB
{
    public class JSON_CONFIG_DTO
    {
        public string CLAVE_JSON { get; set; }
        public string JSON_OBJETO { get; set; }
        public string DESCRIPCION { get; set; }
    }
}
