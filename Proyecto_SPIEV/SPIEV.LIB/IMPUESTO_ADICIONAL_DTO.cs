﻿namespace SPIEV.LIB
{
    public abstract class IMPUESTO_ADICIONAL_DTO
    {
        public long ID_IMPUESTO_ADICIONAL { get; set; }
        public abstract string CODIGO_IDENTIFICACION { get; set; }
        public abstract string CODIGO_INFORME_TECNICO { get; set; }
        public abstract long MONTO_IMPUESTO { get; set; } 
        public abstract long TOTAL_FACTURA { get; set; }
    }
}
