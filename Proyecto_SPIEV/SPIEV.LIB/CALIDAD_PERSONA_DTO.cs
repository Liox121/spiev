﻿namespace SPIEV.LIB
{
    public abstract class CALIDAD_PERSONA_DTO
    {
        public long ID_CALIDAD { get; set; }
        public string TIPO_PERSONA { get; set; }
        public string TIPO_PERSONA_DESCRIPCION { get; set; }
    }
}
