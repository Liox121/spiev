﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB
{
    public class CATEGORIA_TIPO_VEHICULO_DTO
    {
        public long ID_CATEGORIA { get; set; }
        public string NOMBRE_CATEGORIA { get; set; }
    }
}
