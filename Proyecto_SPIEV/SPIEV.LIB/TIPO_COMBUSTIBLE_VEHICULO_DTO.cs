﻿namespace SPIEV.LIB
{
    public class TIPO_COMBUSTIBLE_VEHICULO_DTO
    {
        public long ID_TIPO_COMBUSTIBLE { get; set; }
        public string NOMBRE_COMBUSTIBLE { get; set; }
    }
}
