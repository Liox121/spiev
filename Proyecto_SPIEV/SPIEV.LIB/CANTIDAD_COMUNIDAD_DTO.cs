﻿namespace SPIEV.LIB
{
    public abstract class CANTIDAD_COMUNIDAD_DTO
    {
        public long ID_CANTIDAD_COMUNIDAD { get; set; }
        public long ID_OPCION_COMUNIDAD { get; set; }
        public long VALOR_POSIBLE { get; set; }
    }
}
