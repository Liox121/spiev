﻿namespace SPIEV.LIB
{
    public abstract class PROPIETARIO_DTO
    {
        public abstract long ID_PROPIETARIO { get; set; }
        public abstract string APELLIDO_MATERNO { get; set; }
        public abstract string APELLIDO_PATERNO { get; set; }
        public abstract string RAZON_SOCIAL { get; set; }
        public abstract long RUN { get; set; }
        public abstract string CALLE { get; set; }
        public abstract string NUMERO_DOMICILIO { get; set; }
        public abstract string LETRA_DOMICILIO { get; set; }
        public abstract string RESTO_DOMICILIO { get; set; }
        public abstract string TELEFONO { get; set; }
        public abstract string CODIGO_POSTAL { get; set; }
        public abstract long ID_COMUNA { get; set; }
        public abstract long ID_CALIDAD_PROPIETARIO { get; set; }
    }
}
