﻿namespace SPIEV.LIB
{
    public class TIPO_MEDIDA_VEHICULO_DTO
    {
        public long ID_TIPO_CARGA { get; set; }
        public char TIPO_CARGA { get; set; }
        public string TIPO_CARGA_DESCRIPCION { get; set; }
    }
}
