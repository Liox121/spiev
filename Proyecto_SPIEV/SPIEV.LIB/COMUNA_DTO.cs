﻿namespace SPIEV.LIB
{
    public class COMUNA_DTO
    {
        public long ID_COMUNA { get; set; }
        public long CODIGO_COMUNA { get; set; }
        public string NOMBRE { get; set; }
    }
}
