﻿namespace SPIEV.LIB
{
    public abstract class PAGINA_DTO
    {
        public PAGINA_DTO()
        {
            CONTROLADOR = "Home";
            ACCION = "Usuario_Sin_Perfil_Pagina";
        }

        public long ID_PAGINA { get; set; }
        public string CONTROLADOR { get; set; }
        public string CONTROLADOR_FULL { get; set; }
        public string ACCION { get; set; }
        public string NOMBRE_MENU { get; set; }
        public string ICONO_MENU { get; set; }
        public bool ES_MENU { get; set; }
    }
}
