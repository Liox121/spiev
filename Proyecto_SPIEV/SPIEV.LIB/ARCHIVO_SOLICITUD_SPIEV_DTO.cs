﻿namespace SPIEV.LIB
{
    public abstract class ARCHIVO_SOLICITUD_SPIEV_DTO
    {
        public long ID_ARCHIVO { get; set; }
        public string ARCHIVO { get; set; }
        public long ID_TIPO_ARCHIVO_SOLICITUD { get; set; }
        public long ID_SOLICITUD { get; set; }
        public bool BORRADO { get; set; }
    }
}
