﻿
namespace SPIEV.LIB
{
    public abstract class LIMITACION_DTO
    {

        public long ID_LIMITACION { get; set; }
        public long ID_SPIEV { get; set; }
        public long ID_ESTADO { get; set; }
        public long ID_USUARIO { get; set; }
        public long ID_ACREEDOR { get; set; }
        public long ID_PROPIETARIO { get; set; }
        public long ID_DOCUMENTO_LIMITACION { get; set; }
    }
}
