﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB
{
    public class TIPO_POTENCIA_DTO
    {
        public long ID_TIPO_POTENCIA { get; set; }
        public string NOMBRE_TIPO { get; set; }
    }
}
