﻿namespace SPIEV.LIB
{
    public abstract class PERFIL_DTO
    {
        public long ID_PERFIL { get; set; }
        public abstract string NOMBRE { get; set; }
        public string DESCRIPCION { get; set; }
        public string ICONO { get; set; }
    }
}
