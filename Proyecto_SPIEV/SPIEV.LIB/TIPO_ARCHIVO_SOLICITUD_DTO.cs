﻿namespace SPIEV.LIB
{
    public class TIPO_ARCHIVO_SOLICITUD_DTO
    {
        public long ID_TIPO_ARCHIVO_SOLICITUD { get; set; }
        public long ID_TIPO_ARCHIVO { get; set; }
    }
}
