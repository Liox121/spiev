﻿namespace SPIEV.LIB
{
    public abstract class FINANCIERA_DTO
    {
        public long ID_FINANCIERA { get; set; }
        public abstract string NOMBRE_FINANCIERA { get; set; }
    }
}
