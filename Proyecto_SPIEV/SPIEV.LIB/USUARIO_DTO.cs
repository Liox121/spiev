﻿namespace SPIEV.LIB
{
    public abstract class USUARIO_DTO
    {
        public long ID_USUARIO { get; set; }
        abstract public string NOMBRE_USUARIO { get; set; }
        abstract public string NOMBRE_COMPLETO { get; set; }
        public string CLAVE { get; set; }
        abstract public string CORREO_ELECTRONICO { get; set; }
        public bool ACTIVO { get; set; }
    }
}
