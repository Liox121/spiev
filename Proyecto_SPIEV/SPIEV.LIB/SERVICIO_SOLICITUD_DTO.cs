﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.LIB
{
    public class SERVICIO_SOLICITUD_DTO
    {
        public long ID_SERVICIO_SOLICITUD { get; set; }
        public long ID_SERVICIO { get; set; }
        public long ID_SOLICITUD { get; set; }
    }
}
