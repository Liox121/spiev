﻿using System;

namespace SPIEV.LIB
{
    public abstract class HISTORIA_SOLICITUD_DTO
    {
        public long ID_HISTORIA { get; set; }
        public long ID_SOLICITUD { get; set; }
        public long ID_USUARIO { get; set; }
        public long ID_ESTADO { get; set; }
        public string COMENTARIO { get; set; }
        public DateTime FECHA { get; set; }
    }
}
