﻿namespace SPIEV.LIB
{
    public class TIPO_ARCHIVO_DTO
    {
        public long ID_TIPO_ARCHIVO { get; set; }
        public string NOMBRE { get; set; }
    }
}
