﻿namespace SPIEV.LIB
{
    public abstract class OPERADOR_DTO
    {
        public long ID_OPERADOR { get; set; }
        public abstract int ID_REGION { get; set; }
        public abstract int RUN_USUARIO { get; set; }
        public abstract int RUN_EMPRESA { get; set; }
    }
}
