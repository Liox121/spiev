﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class CALIDAD_PERSONA_R 
    {
        public static IEnumerable<CALIDAD_PERSONA> TODOS_CALIDAD_PERSONA()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<CALIDAD_PERSONA>(PROCS.SP_TODO_CALIDAD_PERSONA, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static CALIDAD_PERSONA READ(long ID_CALIDAD)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<CALIDAD_PERSONA>(PROCS.SP_READ_CALIDAD_PERSONA, new { ID_CALIDAD }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}