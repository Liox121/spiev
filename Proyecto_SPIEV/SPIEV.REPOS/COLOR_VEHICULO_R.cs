﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class COLOR_VEHICULO_R 
    {
        public static IEnumerable<COLOR_VEHICULO> TODOS_COLOR_VEHICULO()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<COLOR_VEHICULO>(PROCS.SP_TODO_COLOR, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}