﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Linq;

namespace SPIEV.REPOS
{
    public static class SUCURSAL_R
    {
        public static IEnumerable<SUCURSAL> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<SUCURSAL>(PROCS.SP_TODO_SUCURSAL, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<SUCURSAL> TODOS_WEB()
        {
            IEnumerable<SUCURSAL> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<SUCURSAL>(PROCS.SP_TODO_SUCURSAL, null, commandType: CommandType.StoredProcedure);
            }

            Parallel.ForEach(SALIDA, SUCURSAL =>
            {
                SUCURSAL.EMPRESA = EMPRESA_R.READ(SUCURSAL.ID_EMPRESA);
            });

            return SALIDA;
        }

        public static IEnumerable<SUCURSAL> TODOS_WEB_NO_BORRADO()
        {
            IEnumerable<SUCURSAL> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<SUCURSAL>(PROCS.SP_TODO_SUCURSAL, null, commandType: CommandType.StoredProcedure);
            }

            Parallel.ForEach(SALIDA, SUCURSAL =>
            {
                SUCURSAL.EMPRESA = EMPRESA_R.READ(SUCURSAL.ID_EMPRESA);
            });

            //FILTRADO POR EMPRESAS BORRADAS
            return SALIDA.Where(X => X.EMPRESA.BORRADO);
        }

        public static SUCURSAL CREATE(SUCURSAL SUCURSAL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SUCURSAL>(PROCS.SP_CREATE_SUCURSAL, new { SUCURSAL.NOMBRE_SUCURSAL, SUCURSAL.ID_EMPRESA }, commandType: CommandType.StoredProcedure);
            }
        }

        public static SUCURSAL READ(long ID_SUCURSAL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SUCURSAL>(PROCS.SP_READ_SUCURSAL, new { ID_SUCURSAL }, commandType: CommandType.StoredProcedure);
            }
        }

        public static SUCURSAL READ_WEB(long ID_SUCURSAL)
        {
            SUCURSAL SALIDA = new SUCURSAL();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<SUCURSAL>(PROCS.SP_READ_SUCURSAL, new { ID_SUCURSAL }, commandType: CommandType.StoredProcedure);
            }

            //LECTURA DEL SUB-OBJETO
            SALIDA.EMPRESA = EMPRESA_R.READ(SALIDA.ID_EMPRESA);

            return SALIDA;
        }

        public static void UPDATE(UPDATE_SUCURSAL SUCURSAL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_SUCURSAL, new { SUCURSAL.ID_SUCURSAL, SUCURSAL.NOMBRE_SUCURSAL, SUCURSAL.ID_EMPRESA, SUCURSAL.BORRADO } , commandType: CommandType.StoredProcedure);
            }
        }

        public class UPDATE_SUCURSAL
        {
            public long ID_SUCURSAL { get; set; }
            public string NOMBRE_SUCURSAL { get; set; }
            public long? ID_EMPRESA { get; set; }
            public bool? BORRADO { get; set; }
        }
    }
}