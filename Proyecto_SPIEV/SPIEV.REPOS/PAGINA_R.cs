﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class PAGINA_R
    {
        public static PAGINA READ(long ID_PAGINA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<PAGINA>(PROCS.SP_READ_PAGINA, new { ID_PAGINA }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}