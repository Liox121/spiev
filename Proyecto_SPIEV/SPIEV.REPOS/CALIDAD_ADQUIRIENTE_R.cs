﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class CALIDAD_ADQUIRIENTE_R
    {
        public static IEnumerable<CALIDAD_ADQUIRIENTE> TODOS_CALIDAD_ADQUIRIENTE()
        {
            IEnumerable<CALIDAD_ADQUIRIENTE> SALIDA = null;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<CALIDAD_ADQUIRIENTE>(PROCS.SP_TODO_CALIDAD_ADQUIRIENTE, null, commandType: CommandType.StoredProcedure);
            }

            foreach (var ADQ in SALIDA)
            {
                ADQ.CALIDAD_PERSONA = CALIDAD_PERSONA_R.READ(ADQ.ID_CALIDAD);
            }

            return SALIDA;
        }

        public static CALIDAD_ADQUIRIENTE READ(long ID_CALIDAD_ADQUIRIENTE)
        {
            CALIDAD_ADQUIRIENTE SALIDA = new CALIDAD_ADQUIRIENTE();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<CALIDAD_ADQUIRIENTE>(PROCS.SP_READ_CALIDAD_ADQUIRIENTE, new { ID_CALIDAD_ADQUIRIENTE }, commandType: CommandType.StoredProcedure);
            }

            SALIDA.CALIDAD_PERSONA = CALIDAD_PERSONA_R.READ(SALIDA.ID_CALIDAD);

            return SALIDA;
        }
    }
}
