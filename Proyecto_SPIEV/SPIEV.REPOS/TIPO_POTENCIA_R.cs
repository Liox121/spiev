﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public class TIPO_POTENCIA_R
    {
        public static IEnumerable<TIPO_POTENCIA> TODOS_TIPO_POTENCIA()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_POTENCIA>(PROCS.SP_TODO_TIPO_POTENCIA, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static TIPO_POTENCIA READ(long ID_TIPO_POTENCIA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<TIPO_POTENCIA>(PROCS.SP_READ_TIPO_POTENCIA, new { ID_TIPO_POTENCIA }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
