﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public class TIPO_CARROCERIA_R
    {
        public static IEnumerable<TIPO_CARROCERIA> TODOS_TIPOS_CARROCERIA()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_CARROCERIA>(PROCS.SP_TODO_TIPO_CARROCERIA, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static TIPO_CARROCERIA READ(long ID_TIPO_CARROCERIA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<TIPO_CARROCERIA>(PROCS.SP_READ_TIPO_CARROCERIA, new { ID_TIPO_CARROCERIA }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
