﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class HISTORIA_SOLICITUD_R
    {
        public static HISTORIA_SOLICITUD CREATE(HISTORIA_SOLICITUD HISTORIA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<HISTORIA_SOLICITUD>(PROCS.SP_CREATE_HISTORIA_SOLICITUD,
                    new
                    {
                        HISTORIA.ID_SOLICITUD,
                        HISTORIA.ID_USUARIO,
                        HISTORIA.ID_ESTADO,
                        HISTORIA.COMENTARIO,
                        HISTORIA.FECHA
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<HISTORIA_SOLICITUD> HISTORIA_X_SOLICITUD(long ID_SOLICITUD)
        {
            IEnumerable<HISTORIA_SOLICITUD> HISTORIAS = null;

            //CONSULTA DE DATOS
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                HISTORIAS = _db.Query<HISTORIA_SOLICITUD>(PROCS.SP_READ_HISTORIA_SOLICITUD_X_SOLICITUD,
                    new { ID_SOLICITUD }, commandType: CommandType.StoredProcedure);
            }

            //LAS TABLAS ASOCIADAS
            Parallel.ForEach(HISTORIAS, HISTORIA =>
            {
                HISTORIA.ESTADO = ESTADO_SOLICITUD_SPIEV_R.READ(HISTORIA.ID_ESTADO);
                HISTORIA.USUARIO = USUARIO_R.READ_WEB(HISTORIA.ID_USUARIO);

                //BORRAR LA CLAVE
                HISTORIA.USUARIO.CLAVE = null;
            });

            return HISTORIAS;
        }
    }
}