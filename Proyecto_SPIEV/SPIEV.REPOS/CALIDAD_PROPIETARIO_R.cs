﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class CALIDAD_PROPIETARIO_R
    {
        public static IEnumerable<CALIDAD_PROPIETARIO> TODOS_CALIDAD_PROPIETARIO()
        {
            IEnumerable<CALIDAD_PROPIETARIO> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<CALIDAD_PROPIETARIO>(PROCS.SP_TODO_CALIDAD_PROPIETARIO, null, commandType: CommandType.StoredProcedure);
            }

            foreach (var PRO in SALIDA)
            {
                PRO.CALIDAD_PERSONA = CALIDAD_PERSONA_R.READ(PRO.ID_CALIDAD);
            }

            return SALIDA;
        }

        public static CALIDAD_PROPIETARIO READ(long ID_CALIDAD_PROPIETARIO)
        {
            CALIDAD_PROPIETARIO SALIDA = new CALIDAD_PROPIETARIO();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<CALIDAD_PROPIETARIO>(PROCS.SP_READ_CALIDAD_PROPIETARIO, new { ID_CALIDAD_PROPIETARIO }, commandType: CommandType.StoredProcedure);
            }

            SALIDA.CALIDAD_PERSONA = CALIDAD_PERSONA_R.READ(SALIDA.ID_CALIDAD);

            return SALIDA;
        }
    }
}
