﻿using SPIEV.LIB_WEB;
using Dapper;
using System.Data;
using System.Collections.Generic;

namespace SPIEV.REPOS
{
    public class ACREEDOR_R
    {
        public static ACREEDOR READ(long ID_ACREEDOR)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ACREEDOR>(PROCS.SP_READ_ACREEDOR, new { ID_ACREEDOR }, commandType: CommandType.StoredProcedure);
            }
            
        }


        public static IEnumerable<ACREEDOR> READ_TODO_ACREEDOR()
        {
            IEnumerable<ACREEDOR> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<ACREEDOR>(PROCS.SP_READ_TODO_ACREEDOR, null, commandType: CommandType.StoredProcedure);
            }

            return SALIDA;
        }
    }
}
