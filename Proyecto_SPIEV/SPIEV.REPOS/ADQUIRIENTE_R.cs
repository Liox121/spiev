﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    /// <summary>
    /// CLASE DE ACCESO A DATOS SOBRE LA TABLA: 'ADQUIRIENTE'
    /// </summary>
    public static class ADQUIRIENTE_R
    {
        public static ADQUIRIENTE CREATE(ADQUIRIENTE ADQUIRIENTE)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ADQUIRIENTE>(PROCS.SP_CREATE_ADQUIRIENTE,
                    new
                    {
                        ADQUIRIENTE.APELLIDO_PATERNO,
                        ADQUIRIENTE.APELLIDO_MATERNO,
                        ADQUIRIENTE.RAZON_SOCIAL,
                        ADQUIRIENTE.RUN,
                        ADQUIRIENTE.CALLE,
                        ADQUIRIENTE.NUMERO_DOMICILIO,
                        ADQUIRIENTE.LETRA_DOMICILIO,
                        ADQUIRIENTE.RESTO_DOMICILIO,
                        ADQUIRIENTE.TELEFONO,
                        ADQUIRIENTE.CODIGO_POSTAL,
                        ADQUIRIENTE.CORREO_ELECTRONICO,
                        ADQUIRIENTE.ID_COMUNA,
                        ADQUIRIENTE.ID_CALIDAD_ADQUIRIENTE,
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// LISTA DE OBJETOS (ADQUIRIENTES)
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ADQUIRIENTE> TODOS_ADQUIRIENTE()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<ADQUIRIENTE>(PROCS.SP_TODO_ADQUIRIENTE, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static ADQUIRIENTE READ(long ID_ADQUIRIENTE)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ADQUIRIENTE>(PROCS.SP_READ_ADQUIRIENTE, new { ID_ADQUIRIENTE }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}