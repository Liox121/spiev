﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public class CALIDAD_SOLICITANTE_R
    {
        public static IEnumerable<CALIDAD_SOLICITANTE> TODOS_CALIDAD_SOLICITANTE()
        {
            IEnumerable<CALIDAD_SOLICITANTE> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<CALIDAD_SOLICITANTE>(PROCS.SP_TODO_CALIDAD_SOLICITANTE, null, commandType: CommandType.StoredProcedure);
            }

            foreach (var SOL in SALIDA)
            {
                SOL.CALIDAD_PERSONA = CALIDAD_PERSONA_R.READ(SOL.ID_CALIDAD);
            }

            return SALIDA;
        }

        public static CALIDAD_SOLICITANTE READ(long ID_CALIDAD_SOLICITANTE)
        {
            CALIDAD_SOLICITANTE SALIDA = new CALIDAD_SOLICITANTE();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<CALIDAD_SOLICITANTE>(PROCS.SP_READ_CALIDAD_SOLICITANTE, new { ID_CALIDAD_SOLICITANTE }, commandType: CommandType.StoredProcedure);
            }

            SALIDA.CALIDAD_PERSONA = CALIDAD_PERSONA_R.READ(SALIDA.ID_CALIDAD);

            return SALIDA;
        }
    }
}
