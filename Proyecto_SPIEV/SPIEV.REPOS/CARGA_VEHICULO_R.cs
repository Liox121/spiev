﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class CARGA_VEHICULO_R
    {
        public static IEnumerable<CARGA_VEHICULO> TODOS_CARGA_VEHICULO()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<CARGA_VEHICULO>(PROCS.SP_TODO_CARGA_VEHICULO, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}