﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class ESTIPULANTE_R
    {
        public static ESTIPULANTE CREATE(ESTIPULANTE ESTIPULANTE)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ESTIPULANTE>(PROCS.SP_CREATE_ESTIPULANTE,
                    new
                    {
                        ESTIPULANTE.APELLIDO_PATERNO,
                        ESTIPULANTE.APELLIDO_MATERNO,
                        ESTIPULANTE.RAZON_SOCIAL,
                        ESTIPULANTE.RUN,
                        ESTIPULANTE.CALLE,
                        ESTIPULANTE.NUMERO_DOMICILIO,
                        ESTIPULANTE.LETRA_DOMICILIO,
                        ESTIPULANTE.RESTO_DOMICILIO,
                        ESTIPULANTE.TELEFONO,
                        ESTIPULANTE.CODIGO_POSTAL,
                        ESTIPULANTE.CORREO_ELECTRONICO,
                        ESTIPULANTE.ID_COMUNA,
                        ESTIPULANTE.ID_CALIDAD_ESTIPULANTE,
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// LISTA DE OBJETOS (SOLICITANTES)
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ESTIPULANTE> TODOS_ESTIPULANTE()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<ESTIPULANTE>(PROCS.SP_TODO_ESTIPULANTE, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static ESTIPULANTE READ(long ID_ESTIPULANTE)
        {
            ESTIPULANTE SALIDA = new ESTIPULANTE();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<ESTIPULANTE>(PROCS.SP_READ_ESTIPULANTE, new { ID_ESTIPULANTE }, commandType: CommandType.StoredProcedure);
            }

            //LECTURA DE OBJETOS DERIVADOS...
            SALIDA.COMUNA = COMUNA_R.READ(SALIDA.ID_COMUNA);
            SALIDA.CALIDAD_ESTIPULANTE = CALIDAD_ESTIPULANTE_R.READ(SALIDA.ID_CALIDAD_ESTIPULANTE);

            return SALIDA;
        }
    }
}