﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class OPERADOR_R
    {
        public static OPERADOR CREATE(OPERADOR OPERADOR)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<OPERADOR>(PROCS.SP_CREATE_OPERADOR, new { OPERADOR.ID_REGION, OPERADOR.RUN_USUARIO , OPERADOR.RUN_EMPRESA }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}