﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public class CATEGORIA_TIPO_VEHICULO_R
    {
        public static IEnumerable<CATEGORIA_TIPO_VEHICULO> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<CATEGORIA_TIPO_VEHICULO>(PROCS.SP_TODO_CATEGORIA_TIPO_VEHICULO, null, commandType: CommandType.StoredProcedure);
            }
        }
        public static CATEGORIA_TIPO_VEHICULO READ(long ID_CATEGORIA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<CATEGORIA_TIPO_VEHICULO>(PROCS.SP_READ_CATEGORIA_TIPO_VEHICULO, new { ID_CATEGORIA }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
