﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class HISTORIA_INSCRIPCION_R
    {
        public static HISTORIA_INSCRIPCION CREATE(HISTORIA_INSCRIPCION HISTORIA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<HISTORIA_INSCRIPCION>(PROCS.SP_CREATE_HISTORIA_INSCRIPCION,
                    new
                    {
                        HISTORIA.ID_PRIMERA_INSCRIPCION,
                        HISTORIA.ID_USUARIO,
                        HISTORIA.ID_ESTADO,
                        HISTORIA.COMENTARIO,
                        HISTORIA.FECHA
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<HISTORIA_INSCRIPCION> HISTORIA_X_INSCRIPCION(long ID_PRIMERA_INSCRIPCION)
        {
            IEnumerable<HISTORIA_INSCRIPCION> HISTORIAS = null;

            //CONSULTA DE DATOS
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                HISTORIAS = _db.Query<HISTORIA_INSCRIPCION>(PROCS.SP_READ_HISTORIA_INSCRIPCION_X_INSCRIPCION,
                    new { ID_PRIMERA_INSCRIPCION }, commandType: CommandType.StoredProcedure);
            }

            //LAS TABLAS ASOCIADAS
            Parallel.ForEach(HISTORIAS, HISTORIA =>
            {
                HISTORIA.ESTADO = ESTADO_PRIMERA_INSCRIPCION_R.READ(HISTORIA.ID_ESTADO);
                HISTORIA.USUARIO = USUARIO_R.READ_WEB(HISTORIA.ID_USUARIO);

                //BORRAR LA CLAVE
                HISTORIA.USUARIO.CLAVE = null;
            });

            return HISTORIAS;
        }

    }
}