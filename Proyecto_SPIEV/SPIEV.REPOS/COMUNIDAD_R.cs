﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class COMUNIDAD_R
    {
        public static COMUNIDAD CREATE(COMUNIDAD COMUNIDAD)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<COMUNIDAD>(PROCS.SP_CREATE_COMUNIDAD, new { COMUNIDAD.ID_OPCION_COMUNIDAD, COMUNIDAD.CANTIDAD }, commandType: CommandType.StoredProcedure);
            }
        }

    }
}