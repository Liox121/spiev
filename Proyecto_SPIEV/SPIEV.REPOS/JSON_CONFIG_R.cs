﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class JSON_CONFIG_R
    {
        public static JSON_CONFIG READ(String CLAVE_JSON)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<JSON_CONFIG>(PROCS.SP_READ_JSON_CONFIG, new { CLAVE_JSON }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<JSON_CONFIG> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<JSON_CONFIG>(PROCS.SP_TODO_JSON_CONFIG, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
