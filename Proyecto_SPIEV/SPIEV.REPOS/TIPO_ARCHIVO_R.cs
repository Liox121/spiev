﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class TIPO_ARCHIVO_R
    {
        public static IEnumerable<TIPO_ARCHIVO> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_ARCHIVO>(PROCS.SP_TODO_TIPO_ARCHIVO, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static TIPO_ARCHIVO READ(long ID_TIPO_ARCHIVO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<TIPO_ARCHIVO>(PROCS.SP_READ_TIPO_ARCHIVO, new { ID_TIPO_ARCHIVO }, commandType: CommandType.StoredProcedure);
            }
        }

    }
}