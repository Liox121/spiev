﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using SPIEV.LIB_WEB;

namespace SPIEV.REPOS
{
    public static class LIMITACION_R
    {
        public class UPDATE_LIMITACION {
            public int ID_LIMITACION { get; set; }
            public long ID_SPIEV { get; set; }
            public long ID_ESTADO { get; set; }
            public long ID_USUARIO { get; set; }
            public long ID_ACREEDOR { get; set; }
            public long ID_PROPIETARIO { get; set; }
            public long ID_DOCUMENTO_LIMITACION { get; set; }
        }


        public static LIMITACION READ(long ID_LIMITACION)
        {

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<LIMITACION>(PROCS.SP_READ_LIMITACION, new { ID_LIMITACION }, commandType: CommandType.StoredProcedure);
            }

            //SALIDA.SPIEV = SPIEV_R.READ(SALIDA.ID_SPIEV);
            //SALIDA.USUARIO = USUARIO_R.READ(SALIDA.ID_USUARIO);
            //SALIDA.ESTADO = ESTADO_LIMITACION_R.READ(SALIDA.ID_ESTADO);
            //SALIDA.ACREEDOR = ACREEDOR_R.READ(SALIDA.ID_ACREEDOR);
            //SALIDA.PROPIETARIO = PROPIETARIO_R.READ(SALIDA.ID_PROPIETARIO);
            //SALIDA.ID_DOCUMENTACION_LIMITACION = DOCUMENTACION_LIMITACION_R.READ(SALIDA.ID_DOCUMENTO_LIMITACION);
        }

        public static IEnumerable<LIMITACION> TODO_LIMITACION()
        {
            IEnumerable<LIMITACION> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<LIMITACION>(PROCS.SP_READ_TODO_LIMITACION, null, commandType: CommandType.StoredProcedure);
            }
            foreach (LIMITACION INS in SALIDA)
            {
                INS.SPIEV = SPIEV_R.READ(INS.ID_SPIEV);
                INS.USUARIO = USUARIO_R.READ(INS.ID_USUARIO);
                INS.ESTADO = ESTADO_LIMITACION_R.READ(INS.ID_ESTADO);
                INS.ACREEDOR = ACREEDOR_R.READ(INS.ID_ACREEDOR);
                INS.PROPIETARIO = PROPIETARIO_R.READ(INS.ID_PROPIETARIO);
                INS.ID_DOCUMENTACION_LIMITACION = DOCUMENTACION_LIMITACION_R.READ(INS.ID_DOCUMENTO_LIMITACION);
            }
            return SALIDA;
        }

    }
}
