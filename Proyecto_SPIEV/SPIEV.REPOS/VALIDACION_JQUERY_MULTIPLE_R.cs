﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class VALIDACION_JQUERY_MULTIPLE_R
    {
        /// <summary>
        /// RETORNA LISTA DE TIPOS DE VEHICULOS, QUE DEBEN TENER VALOR EN CAMPO CARGA, PARA CREAR/EDITAR UNA LIMITACION/INSCRIPCION, DESDE TABLA JSON_CONFIG
        /// </summary>
        /// <returns></returns>
        public static VALIDACION_JQUERY_MULTIPLE VALIDACION_CARGA_TIPO_VEHICULO()
        {
            //JSON_LLAVE
            string LLAVE_JSON = "VALIDACION_CARGA_TIPO_VEHICULO";

            //RETORNO...
            return READ_POR_CLAVE(LLAVE_JSON);
        }

        /// <summary>
        /// RETORNA LISTA DE TIPOS DE VEHICULOS, QUE DEBEN TENER VALOR EN CIT, PARA CREAR/EDITAR UNA LIMITACION/INSCRIPCION, DESDE TABLA JSON_CONFIG
        /// </summary>
        /// <returns></returns>
        public static VALIDACION_JQUERY_MULTIPLE VALIDACION_CIT_TIPO_VEHICULO()
        {
            //JSON_LLAVE
            string LLAVE_JSON = "VALIDACION_CIT_TIPO_VEHICULO";

            //RETORNO...
            return READ_POR_CLAVE(LLAVE_JSON);
        }

        /// <summary>
        /// RETORNA LISTA DE TIPOS DE VEHICULOS, QUE DEBEN TENER VALOR EN NUMERO DE EJES, PARA CREAR/EDITAR UNA LIMITACION/INSCRIPCION, DESDE TABLA JSON_CONFIG
        /// </summary>
        /// <returns></returns>
        public static VALIDACION_JQUERY_MULTIPLE VALIDACION_EJES_TIPO_VEHICULO()
        {
            //JSON_LLAVE
            string LLAVE_JSON = "VALIDACION_EJES_TIPO_VEHICULO";

            //RETORNO...
            return READ_POR_CLAVE(LLAVE_JSON);
        }

        /// <summary>
        /// RETORNA LISTA DE TIPOS DE VEHICULOS, QUE PUEDEN TENER VALOR EN CAMPO "OTRA_CARROCERIA", CREAR/EDITAR UNA LIMITACION/INSCRIPCION, DESDE TABLA JSON_CONFIG
        /// </summary>
        /// <returns></returns>
        public static VALIDACION_JQUERY_MULTIPLE VALIDACION_OTRA_CARROCERIA_TIPO_VEHICULO()
        {
            //JSON_LLAVE
            string LLAVE_JSON = "VALIDACION_CARR_TIPO_VEHICULO";

            //RETORNO...
            return READ_POR_CLAVE(LLAVE_JSON);
        }

        /// <summary>
        /// RETORNA LISTA DE TIPOS DE VEHICULOS, QUE PUEDEN TENER VALOR EN CAMPO "POTENCIA", CREAR/EDITAR UNA LIMITACION/INSCRIPCION, DESDE TABLA JSON_CONFIG
        /// </summary>
        /// <returns></returns>
        public static VALIDACION_JQUERY_MULTIPLE VALIDACION_POTENCIA_TIPO_VEHICULO()
        {
            //JSON_LLAVE
            string LLAVE_JSON = "VALIDACION_POTENCIA_TIPO_VEHICULO";

            //RETORNO...
            return READ_POR_CLAVE(LLAVE_JSON);
        }

        /// <summary>
        /// RETORNA LISTA DE TIPOS DE VEHICULOS, QUE PUEDEN TENER VALOR EN CAMPO "TRACCION", CREAR/EDITAR UNA LIMITACION/INSCRIPCION, DESDE TABLA JSON_CONFIG
        /// </summary>
        /// <returns></returns>
        public static VALIDACION_JQUERY_MULTIPLE VALIDACION_TRACCION_TIPO_VEHICULO()
        {
            //JSON_LLAVE
            string LLAVE_JSON = "VALIDACION_TRACCION_TIPO_VEHICULO";

            //RETORNO...
            return READ_POR_CLAVE(LLAVE_JSON);
        }

        /// <summary>
        /// CONSULTA A TABLA JSON_CONFIG (POR CLAVE_JSON) Y SERIALIZA AL OBJETO DETERMINADO
        /// </summary>
        static VALIDACION_JQUERY_MULTIPLE READ_POR_CLAVE(string LLAVE_JSON)
        {
            try
            {
                //VALIDACION
                VALIDACION_JQUERY_MULTIPLE SALIDA = null;
                //SI EXISTE EL JSON
                if (JSON_CONFIG_R.TODOS().Any(n => n.CLAVE_JSON == LLAVE_JSON))
                {
                    //JSON COMO STRING
                    string JSON_STRING = JSON_CONFIG_R.READ(LLAVE_JSON).JSON_OBJETO;

                    //SE DESERIALIZA EL OBJETO A UNA LISTA
                    SALIDA = JsonConvert.DeserializeObject<VALIDACION_JQUERY_MULTIPLE>(JSON_STRING);
                }

                //SALIDA...
                return SALIDA;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}
