﻿using Dapper;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class PAGINA_PERFIL_R
    {
        public static IEnumerable<PAGINA_PERFIL> TODOS(long ID_PERFIL)
        {
            IEnumerable<PAGINA_PERFIL> salida;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                salida = _db.Query<PAGINA_PERFIL>(PROCS.SP_READ_PAGINA_PERFIL_X_PERFIL, new { ID_PERFIL }, commandType: CommandType.StoredProcedure);
            }

            //BUSCAMOS LA PAGINA ASOCIADA
            Parallel.ForEach(salida, PAGINA =>
            {
                PAGINA.PAGINA = PAGINA_R.READ(PAGINA.ID_PAGINA);
            });

            return salida;

        }

        public static void UPDATE(PAGINA_PERFIL PAGINA_PERFIL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_PAGINA_PERFIL, 
                    new
                    {
                        PAGINA_PERFIL.ID_PAGINA_PERFIL,
                        PAGINA_PERFIL.ID_PAGINA,
                        PAGINA_PERFIL.ID_PERFIL,
                        PAGINA_PERFIL.POR_DEFECTO
                    }, 
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}