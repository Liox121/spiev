﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    /// <summary>
    /// CLASE DE ACCESO A DATOS SOBRE LA TABLA: 'EMPRESA'
    /// </summary>
    public static class EMPRESA_R
    {
        public static IEnumerable<EMPRESA> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<EMPRESA>(PROCS.SP_TODO_EMPRESA, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<EMPRESA> TODOS_WEB()
        {
            IEnumerable<EMPRESA> SALIDA;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<EMPRESA>(PROCS.SP_TODO_EMPRESA, null, commandType: CommandType.StoredProcedure);
            }

            ////TODAS LAS SUCURSALES
            //IEnumerable<SUCURSAL> SUCURSALES = SUCURSAL_R.TODOS();

            ////SE FILTRAN LAS SUCURSALES POR EMPRESA
            //Parallel.ForEach(SALIDA, EMPRESA =>
            //{
            //    EMPRESA.SUCURSALES = SUCURSALES.Where(n => n.ID_EMPRESA == EMPRESA.ID_EMPRESA).ToList();
            //});

            return SALIDA;
        }

        public static EMPRESA CREATE(EMPRESA EMPRESA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<EMPRESA>(PROCS.SP_CREATE_EMPRESA, new { EMPRESA.NOMBRE_EMPRESA }, commandType: CommandType.StoredProcedure);
            }
        }

        public static EMPRESA READ(long ID_EMPRESA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<EMPRESA>(PROCS.SP_READ_EMPRESA, new { ID_EMPRESA }, commandType: CommandType.StoredProcedure);
            }
        }

        public static void UPDATE(UPDATE_EMPRESA EMPRESA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_EMPRESA, EMPRESA, commandType: CommandType.StoredProcedure);
            }
        }

        public class UPDATE_EMPRESA
        {
            public long ID_EMPRESA { get; set; }
            public string NOMBRE_EMPRESA { get; set; }
            public bool? BORRADO { get; set; }
        }
    }
}