﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class SOLICITANTE_R
    {
        public static SOLICITANTE CREATE(SOLICITANTE SOLICITANTE)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SOLICITANTE>(PROCS.SP_CREATE_SOLICITANTE,
                    new
                    {
                        SOLICITANTE.APELLIDO_PATERNO,
                        SOLICITANTE.APELLIDO_MATERNO,
                        SOLICITANTE.RAZON_SOCIAL,
                        SOLICITANTE.RUN,
                        SOLICITANTE.CALLE,
                        SOLICITANTE.NUMERO_DOMICILIO,
                        SOLICITANTE.LETRA_DOMICILIO,
                        SOLICITANTE.RESTO_DOMICILIO,
                        SOLICITANTE.TELEFONO,
                        SOLICITANTE.CODIGO_POSTAL,
                        SOLICITANTE.CORREO_ELECTRONICO,
                        SOLICITANTE.ID_COMUNA,
                        SOLICITANTE.ID_CALIDAD_SOLICITANTE
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// LISTA DE OBJETOS (SOLICITANTES)
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SOLICITANTE> TODOS_SOLICITANTE()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<SOLICITANTE>(PROCS.SP_TODO_SOLICITANTE, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static SOLICITANTE READ(long ID_SOLICITANTE)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SOLICITANTE>(PROCS.SP_READ_SOLICITANTE, new { ID_SOLICITANTE }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}