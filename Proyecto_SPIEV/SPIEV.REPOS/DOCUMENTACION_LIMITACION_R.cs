﻿using SPIEV.LIB_WEB;
using Dapper;
using System.Data;
namespace SPIEV.REPOS
{
    public class DOCUMENTACION_LIMITACION_R : DOCUMENTACION_LIMITACION
    {
        public static DOCUMENTACION_LIMITACION READ(long ID_DOCUMENTACION)
        {

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<DOCUMENTACION_LIMITACION>(PROCS.SP_READ_DOCUMENTACION, new { ID_DOCUMENTACION }, commandType: CommandType.StoredProcedure);
            }

        }
    }
}
