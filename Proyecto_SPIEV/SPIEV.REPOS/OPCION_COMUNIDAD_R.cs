﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class OPCION_COMUNIDAD_R
    {
        public static IEnumerable<OPCION_COMUNIDAD> TODOS_OPCION_COMUNIDAD()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<OPCION_COMUNIDAD>(PROCS.SP_TODO_OPCION_COMUNIDAD, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static OPCION_COMUNIDAD READ(long ID_OPCION_COMUNIDAD)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<OPCION_COMUNIDAD>(PROCS.SP_READ_OPCION_COMUNIDAD, new { ID_OPCION_COMUNIDAD }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}