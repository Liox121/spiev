﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class SOLICITUD_SPIEV_R
    {
        public class UPDATE_SOLICITUD
        {
            public long? ID_SOLICITUD { get; set; }
            public long? ID_USUARIO { get; set; }
            public long? ID_SUCURSAL { get; set; }
            public long? ID_FINANCIERA { get; set; }
            public long? RUT_ADQUIRIENTE { get; set; }
            public char? DV_ADQUIRIENTE { get; set; }
            public string CORREO_ADQUIRIENTE { get; set; }
            public long? ID_ESTADO { get; set; }
            public long? ID_PRIMERA_INSCRIPCION { get; set; }
            public long? ID_LIMITACION { get; set; }
            public int? TERMINACION_OPC1 { get; set; }
            public int? TERMINACION_OPC2 { get; set; }
            public string COMENTARIO { get; set; }
            public DateTime? FECHA_SOLICITUD { get; set; }
            public bool? BORRADO { get; set; }
        }

        public static SOLICITUD_SPIEV CREATE(SOLICITUD_SPIEV SOLICITUD)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SOLICITUD_SPIEV>(PROCS.SP_CREATE_SOLICITUD_SPIEV, 
                    new {
                        SOLICITUD.ID_USUARIO,
                        SOLICITUD.ID_SUCURSAL,
                        SOLICITUD.ID_ESTADO,
                        SOLICITUD.COMENTARIO,
                        SOLICITUD.FECHA_SOLICITUD,
                        SOLICITUD.RUT_ADQUIRIENTE,
                        SOLICITUD.DV_ADQUIRIENTE,
                        SOLICITUD.CORREO_ADQUIRIENTE,
                        SOLICITUD.ID_PRIMERA_INSCRIPCION,
                        SOLICITUD.ID_LIMITACION,
                        SOLICITUD.ID_FINANCIERA,
                        SOLICITUD.TERMINACION_OPC1,
                        SOLICITUD.TERMINACION_OPC2
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static void UPDATE(UPDATE_SOLICITUD SOLICITUD)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_SOLICITUD_SPIEV, SOLICITUD, commandType: CommandType.StoredProcedure);
            }
        }

        public static SOLICITUD_SPIEV READ(long ID_SOLICITUD)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SOLICITUD_SPIEV>(PROCS.SP_READ_SOLICITUD_SPIEV, new { ID_SOLICITUD }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<SOLICITUD_SPIEV> TODOS_SOLICITUD()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<SOLICITUD_SPIEV>(PROCS.SP_TODO_SOLICITUD_SPIEV, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<SOLICITUD_SPIEV> TODOS_SOLICITUD_X_ESTADO(int ID_ESTADO)
        {
            IEnumerable<SOLICITUD_SPIEV> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<SOLICITUD_SPIEV>(PROCS.SP_TODO_SOLICITUD_SPIEV, null, commandType: CommandType.StoredProcedure);
            }

            //SE FILTRAN POR ESTADO...
            SALIDA = SALIDA.Where(n => n.ID_ESTADO == ID_ESTADO);

            //SE HACEN LOS CRUCES...
            Parallel.ForEach(SALIDA, SOLICITUD =>
            {
                SOLICITUD.SUCURSAL = SUCURSAL_R.READ(SOLICITUD.ID_SUCURSAL);
                SOLICITUD.USUARIO = USUARIO_R.READ_WEB(SOLICITUD.ID_USUARIO);

                //ELIMINAMOS LA CLAVE
                SOLICITUD.USUARIO.CLAVE = null;

                //SI TIENE ASOCIADA UNA PRIMERA INSCRIPCION
                if (SOLICITUD.ID_PRIMERA_INSCRIPCION.HasValue) SOLICITUD.PRIMERA_INSCRIPCION = PRIMERA_INSCRIPCION_R.READ(SOLICITUD.ID_PRIMERA_INSCRIPCION.Value);

            });

            return SALIDA;
        }

        public static IEnumerable<SOLICITUD_SPIEV> TODOS_SOLICITUD_X_USUARIO(long ID_USUARIO)
        {
            IEnumerable<SOLICITUD_SPIEV> SALIDA = null;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<SOLICITUD_SPIEV>(PROCS.SP_TODO_SOLICITUD_SPIEV, null, commandType: CommandType.StoredProcedure);
            }

            //SOLO LO DEL USUARIO
            SALIDA = SALIDA.Where(n => n.ID_USUARIO == ID_USUARIO);

            //SE HACEN LOS CRUCES...
            Parallel.ForEach(SALIDA, SOLICITUD =>
            {
                SOLICITUD.SUCURSAL = SUCURSAL_R.READ(SOLICITUD.ID_SUCURSAL);
                SOLICITUD.ESTADO = ESTADO_SOLICITUD_SPIEV_R.READ(SOLICITUD.ID_ESTADO);

                //SI TIENE ASOCIADA UNA PRIMERA INSCRIPCION
                if (SOLICITUD.ID_PRIMERA_INSCRIPCION.HasValue)
                    SOLICITUD.PRIMERA_INSCRIPCION = PRIMERA_INSCRIPCION_R.READ(SOLICITUD.ID_PRIMERA_INSCRIPCION.Value);

            });

            return SALIDA;
        }

    }
}