﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class FACTURA_R
    {
        public static FACTURA CREATE(FACTURA FACTURA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<FACTURA>(PROCS.SP_CREATE_FACTURA,
                    new
                    {
                        FACTURA.NUMERO_FACTURA,
                        FACTURA.FECHA,
                        FACTURA.ID_COMUNA,
                        FACTURA.RUT_EMISOR,
                        FACTURA.NOMBRE_EMISOR,
                        FACTURA.MONTO_TOTAL_FACTURA,
                        FACTURA.ID_TIPO_MONEDA,
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static FACTURA READ(long ID_FACTURA)
        {
            FACTURA SALIDA = new FACTURA();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<FACTURA>(PROCS.SP_READ_FACTURA, new { ID_FACTURA }, commandType: CommandType.StoredProcedure);
            }

            SALIDA.COMUNA = COMUNA_R.READ(SALIDA.ID_COMUNA);
            SALIDA.TIPO_MONEDA = TIPO_MONEDA_R.READ(SALIDA.ID_TIPO_MONEDA);

            return SALIDA;
        }
    }
}