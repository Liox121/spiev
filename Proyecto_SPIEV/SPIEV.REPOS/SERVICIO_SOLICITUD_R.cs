﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class SERVICIO_SOLICITUD_R
    {
        public static IEnumerable<SERVICIO_SOLICITUD> READ_X_SOLICITUD(long ID_SOLICITUD)
        {
            IEnumerable<SERVICIO_SOLICITUD> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<SERVICIO_SOLICITUD>(PROCS.SP_READ_SERVICIO_SOLICITUD_X_SOLICITUD, new { ID_SOLICITUD }, commandType: CommandType.StoredProcedure);
            }

            foreach (SERVICIO_SOLICITUD ITEM in SALIDA)
            {
                ITEM.SERVICIO = SERVICIO_R.READ(ITEM.ID_SERVICIO);
                ITEM.SOLICITUD = SOLICITUD_SPIEV_R.READ(ITEM.ID_SOLICITUD);
            }

            return SALIDA;
        }

        public static SERVICIO_SOLICITUD CREATE(SERVICIO_SOLICITUD SERVICIO_SOL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SERVICIO_SOLICITUD>(PROCS.SP_CREATE_SERVICIO_SOLICITUD,
                    new { SERVICIO_SOL.ID_SERVICIO, SERVICIO_SOL.ID_SOLICITUD, }, 
                    commandType: CommandType.StoredProcedure);
            }
        }

        public static void DELETE(long ID_SERVICIO_SOLICITUD)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_DELETE_SERVICIO_SOLICITUD, new { ID_SERVICIO_SOLICITUD }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
