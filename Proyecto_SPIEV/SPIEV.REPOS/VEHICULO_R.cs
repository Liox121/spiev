﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class VEHICULO_R
    {
        public static VEHICULO CREATE(VEHICULO VEHICULO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<VEHICULO>(PROCS.SP_CREATE_VEHICULO,
                    new
                    {
                        VEHICULO.TERMINACION_PPU,
                        VEHICULO.CODIGO_INFORME_TECNICO,
                        VEHICULO.AGNO_FABRICACION,
                        VEHICULO.ID_TIPO_COMBUSTIBLE,
                        VEHICULO.MARCA,
                        VEHICULO.MODELO,
                        VEHICULO.POTENCIA,
                        VEHICULO.NUMERO_ASIENTOS,
                        VEHICULO.NUMERO_PUERTAS,
                        VEHICULO.NUMERO_CHASIS,
                        VEHICULO.NUMERO_MOTOR,
                        VEHICULO.NUMERO_SERIE,
                        VEHICULO.NUMERO_VIN,
                        VEHICULO.PESO_BRUTO_VEHICULAR,
                        VEHICULO.CARGA,
                        VEHICULO.COLOR,
                        VEHICULO.OTRA_CARROCERIA,
                        VEHICULO.ID_TIPO_VEHICULO,
                        VEHICULO.ID_TIPO_CARGA,
                        VEHICULO.ID_TIPO_PBV,
                        VEHICULO.ID_TIPO_CARROCERIA,
                        VEHICULO.ID_TIPO_POTENCIA,
                        VEHICULO.NUMERO_EJES,
                        VEHICULO.TIPO_TRACCION
                    }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}