﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class TIPO_MEDIDA_VEHICULO_R
    {
        public static IEnumerable<TIPO_MEDIDA_VEHICULO> TODOS_TIPOS_MEDIDA()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_MEDIDA_VEHICULO>(PROCS.SP_TODO_TIPO_MEDIDA, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static TIPO_MEDIDA_VEHICULO READ(long ID_TIPO_CARGA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<TIPO_MEDIDA_VEHICULO>(PROCS.SP_READ_TIPO_MEDIDA, new { ID_TIPO_CARGA }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}