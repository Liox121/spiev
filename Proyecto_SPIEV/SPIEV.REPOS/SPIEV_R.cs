﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using SPIEV.LIB_WEB;

namespace SPIEV.REPOS
{
    public class SPIEV_R
    {
        public class UPDATE_SPIEV
        {
            public long ID_PRIMERA_INSCRIPCION { get; set; }
            public long? ID_VEHICULO { get; set; }
            public long? ID_COMUNIDAD { get; set; }
            public long? ID_SOLICITANTE { get; set; }
            public long? ID_OBSERVACION { get; set; }
            public long? ID_OPERADOR { get; set; }
            public DateTime? FECHA { get; set; }
        }

        public static LIB_WEB.SPIEV CREATE(LIB_WEB.SPIEV SPIEV)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<LIB_WEB.SPIEV>(PROCS.SP_CREATE_SPIEV,
                    new
                    {
                        SPIEV.ID_VEHICULO,
                        SPIEV.ID_COMUNIDAD,
                        SPIEV.ID_SOLICITANTE,
                        SPIEV.ID_OBSERVACION,
                        SPIEV.ID_OPERADOR,
                        SPIEV.FECHA
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static LIB_WEB.SPIEV READ(long ID_SPIEV)
        {
            LIB_WEB.SPIEV SALIDA = new LIB_WEB.SPIEV();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query(PROCS.SP_READ_SPIEV,
                new[]
                {
                        typeof(LIB_WEB.SPIEV),
                        typeof(VEHICULO),
                        typeof(COMUNIDAD),
                        typeof(SOLICITANTE),
                        typeof(OBSERVACION),
                        typeof(OPERADOR)

                }, obj =>
                {
                    LIB_WEB.SPIEV SPIEV = (LIB_WEB.SPIEV)obj[0];
                    SPIEV.VEHICULO = (VEHICULO)obj[1];
                    SPIEV.COMUNIDAD = (COMUNIDAD)obj[2];
                    SPIEV.SOLICITANTE = (SOLICITANTE)obj[3];
                    SPIEV.OBSERVACION = (OBSERVACION)obj[4];
                    SPIEV.OPERADOR = (OPERADOR)obj[5];

                    return SPIEV;

                },
                new { ID_SPIEV },
                splitOn: "ID_VEHICULO,ID_COMUNIDAD,ID_SOLICITANTE,ID_OBSERVACION,ID_OPERADOR",
                commandType: CommandType.StoredProcedure).First();

            }

            return SALIDA;
        }

        /// <summary>
        /// COMPLETA TODOS LOS SUB-OBJETOS DE LA PRIMERA INSCRIPCION (VEHICULO, ADQUIRIENTE, ETC)
        /// </summary>
        /// <param name="OBJ"></param>
        /// <returns></returns>
        public static LIB_WEB.SPIEV COMPLETAR_OBJETO(LIB_WEB.SPIEV OBJ)
        {
            //VEHICULO
            OBJ.VEHICULO.TIPO_COMBUSTIBLE = TIPO_COMBUSTIBLE_VEHICULO_R.READ(OBJ.VEHICULO.ID_TIPO_COMBUSTIBLE);
            OBJ.VEHICULO.TIPO_VEHICULO = TIPO_VEHICULO_R.READ(OBJ.VEHICULO.ID_TIPO_VEHICULO);
            OBJ.VEHICULO.TIPO_CARGA = TIPO_MEDIDA_VEHICULO_R.READ(OBJ.VEHICULO.ID_TIPO_CARGA);
            OBJ.VEHICULO.TIPO_PBV = TIPO_MEDIDA_VEHICULO_R.READ(OBJ.VEHICULO.ID_TIPO_PBV);

            //COMUNIDAD 
            OBJ.COMUNIDAD.OPCION_COMUNIDAD = OPCION_COMUNIDAD_R.READ(OBJ.COMUNIDAD.ID_OPCION_COMUNIDAD);

            //SOLICITANTE
            OBJ.SOLICITANTE.COMUNA = COMUNA_R.READ(OBJ.SOLICITANTE.ID_COMUNA);

            //OPERADOR
            OBJ.OPERADOR.REGION = REGION_R.READ(OBJ.OPERADOR.ID_REGION);

            return OBJ;
        }



        public static IEnumerable<LIB_WEB.SPIEV> TODO_SPIEV()
        {
            IEnumerable<LIB_WEB.SPIEV> SPIEVS = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SPIEVS = _db.Query(PROCS.SP_TODO_SPIEV,
                new[]
                {
                        typeof(LIB_WEB.SPIEV),
                        typeof(VEHICULO),
                        typeof(COMUNIDAD),
                        typeof(SOLICITANTE),
                        typeof(OBSERVACION),
                        typeof(OPERADOR),

                }, obj =>
                {
                    LIB_WEB.SPIEV SPIEV = (LIB_WEB.SPIEV)obj[0];
                    SPIEV.VEHICULO = (VEHICULO)obj[1];
                    SPIEV.COMUNIDAD = (COMUNIDAD)obj[2];
                    SPIEV.SOLICITANTE = (SOLICITANTE)obj[4];
                    SPIEV.OBSERVACION = (OBSERVACION)obj[5];
                    SPIEV.OPERADOR = (OPERADOR)obj[6];

                    return SPIEV;
                },
                null,
                splitOn: "ID_VEHICULO,ID_COMUNIDAD,ID_ADQUIRIENTE,ID_SOLICITANTE,ID_OBSERVACION,ID_OPERADOR",
                commandType: CommandType.StoredProcedure);
            }

            return SPIEVS;
        }

        public static void UPDATE(UPDATE_SPIEV SPIEV)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_SPIEV, SPIEV, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
