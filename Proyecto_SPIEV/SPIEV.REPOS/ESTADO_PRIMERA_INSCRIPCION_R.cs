﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class ESTADO_PRIMERA_INSCRIPCION_R
    {
        public static ESTADO_PRIMERA_INSCRIPCION READ(long ID_ESTADO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ESTADO_PRIMERA_INSCRIPCION>(PROCS.SP_READ_ESTADO_PRIMERA_INSCRIPCION, new { ID_ESTADO }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}