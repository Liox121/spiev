﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace SPIEV.REPOS
{
    /// <summary>
    /// CLASE DE CONEXION A BASE DE DATOS SQL SERVER
    /// </summary>
    public static class CONEXION
    {
        /// <summary>
        /// CONEXION A BASE DE DATOS PRIMERAS INSCRIPCIONES
        /// </summary>
        /// <returns></returns>
        public static IDbConnection CONEXION_INSCRIPCION()
        {
            string connection = System.Configuration.ConfigurationManager.ConnectionStrings["Inscripcion_DB"].ConnectionString;
            return new SqlConnection(connection);
        }
    }
}
