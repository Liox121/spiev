﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SPIEV.REPOS
{
    public static class PERFIL_R
    {
        public static IEnumerable<PERFIL> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<PERFIL>(PROCS.SP_TODO_PERFIL, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static PERFIL READ(long ID_PERFIL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<PERFIL>(PROCS.SP_READ_PERFIL, new { ID_PERFIL }, commandType: CommandType.StoredProcedure);
            }
        }

        public static void UPDATE(PERFIL PERFIL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_PERFIL, new { PERFIL.ID_PERFIL, PERFIL.NOMBRE, PERFIL.DESCRIPCION, PERFIL.ICONO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<PERFIL> TODOS_LOGIN()
        {
            IEnumerable<PERFIL> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<PERFIL>(PROCS.SP_TODO_PERFIL, null, commandType: CommandType.StoredProcedure);
            }

            return SALIDA;
        }

        public static IEnumerable<PERFIL_SESSION> TODOS_SESSION()
        {
            IEnumerable<PERFIL_SESSION> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<PERFIL_SESSION>(PROCS.SP_TODO_PERFIL, null, commandType: CommandType.StoredProcedure);
            }

            foreach (PERFIL_SESSION PERFIL_SESION in SALIDA)
            {
                PERFIL_SESION.PAGINAS = PAGINA_PERFIL_R.TODOS(PERFIL_SESION.ID_PERFIL).ToList();
            }

            return SALIDA;
        }
    }
}
