﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class TIPO_ARCHIVO_INSCRIPCION_R
    {
        public static IEnumerable<TIPO_ARCHIVO_INSCRIPCION> TODOS_WEB()
        {
            IEnumerable<TIPO_ARCHIVO_INSCRIPCION> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<TIPO_ARCHIVO_INSCRIPCION>(PROCS.SP_TODO_TIPO_ARCHIVO_INSCRIPCION, null, commandType: CommandType.StoredProcedure);
            }

            foreach (TIPO_ARCHIVO_INSCRIPCION item in SALIDA)
            {
                item.TIPO_ARCHIVO = TIPO_ARCHIVO_R.READ(item.ID_TIPO_ARCHIVO);
            }

            return SALIDA;
        }

        public static IEnumerable<TIPO_ARCHIVO_INSCRIPCION> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_ARCHIVO_INSCRIPCION>(PROCS.SP_TODO_TIPO_ARCHIVO_INSCRIPCION, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}