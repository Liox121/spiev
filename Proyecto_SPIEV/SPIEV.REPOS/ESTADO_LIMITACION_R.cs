﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class ESTADO_LIMITACION_R
    {
        public static ESTADO_LIMITACION READ(long ID_ESTADO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ESTADO_LIMITACION>(PROCS.SP_READ_ESTADO_LIMITACION, new { ID_ESTADO }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
