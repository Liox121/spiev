﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public class ARCHIVO_INSCRIPCION_R
    {
        public class UPDATE_ARCHIVO
        {
            public long ID_ARCHIVO { get; set; }
            public long? ID_PRIMERA_INSCRIPCION { get; set; }
            public long? ID_TIPO_ARCHIVO_INSCRIPCION { get; set; }
            public string ARCHIVO { get; set; }
            public bool? BORRADO { get; set; }
           
        }

        public static ARCHIVO_INSCRIPCION CREATE(ARCHIVO_INSCRIPCION ARCHIVO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ARCHIVO_INSCRIPCION>(PROCS.SP_CREATE_ARCHIVO_INSCRIPCION, new { ARCHIVO.ID_PRIMERA_INSCRIPCION, ARCHIVO.ID_TIPO_ARCHIVO_INSCRIPCION, ARCHIVO.ARCHIVO }, commandType: CommandType.StoredProcedure);
            }
        }



        public static IEnumerable<ARCHIVO_INSCRIPCION> ARCHIVOS_X_INSCRIPCION(long ID_PRIMERA_INSCRIPCION)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<ARCHIVO_INSCRIPCION>(PROCS.SP_READ_ARCHIVO_INSCRIPCION_X_INSCRIPCION, new { ID_PRIMERA_INSCRIPCION }, commandType: CommandType.StoredProcedure);
            }
        }

        public static void UPDATE(UPDATE_ARCHIVO ARCHIVO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_ARCHIVO_INSCRIPCION, ARCHIVO, commandType: CommandType.StoredProcedure);
            }
        }

        public static ARCHIVO_INSCRIPCION READ(long ID_aRCHIVO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ARCHIVO_INSCRIPCION>(PROCS.SP_READ_ARCHIVO_INSCRIPCION_X_ARCHIVO, new { ID_aRCHIVO }, commandType: CommandType.StoredProcedure);
            }
        }


    }
}
