﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class SUCURSAL_USUARIO_R
    {
        public static SUCURSAL_USUARIO CREATE(SUCURSAL_USUARIO USUARIO_SUCURSAL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SUCURSAL_USUARIO>(PROCS.SP_CREATE_SUCURSAL_USUARIO, new { USUARIO_SUCURSAL.ID_USUARIO, USUARIO_SUCURSAL.ID_SUCURSAL }, commandType: CommandType.StoredProcedure);
            }
        }

        public static SUCURSAL_USUARIO CREATE(IDbConnection _db, SUCURSAL_USUARIO USUARIO_SUCURSAL)
        {
            return _db.QuerySingle<SUCURSAL_USUARIO>(PROCS.SP_CREATE_SUCURSAL_USUARIO, new { USUARIO_SUCURSAL.ID_USUARIO, USUARIO_SUCURSAL.ID_SUCURSAL }, commandType: CommandType.StoredProcedure);
        }

        public static void DELETE_X_USUARIO(long ID_USUARIO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_DELETE_SUCURSAL_USUARIO_X_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static void DELETE_X_USUARIO(IDbConnection _db, long ID_USUARIO)
        {
            _db.Execute(PROCS.SP_DELETE_SUCURSAL_USUARIO_X_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
        }

        public static IEnumerable<SUCURSAL_USUARIO> TODOS_X_USUARIO(long ID_USUARIO)
        {
            IEnumerable<SUCURSAL_USUARIO> SALIDA = null;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<SUCURSAL_USUARIO>(PROCS.SP_READ_SUCURSAL_USUARIO_X_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
            }

            foreach (SUCURSAL_USUARIO SUCURSAL_ITEM in SALIDA)
            {
                SUCURSAL_ITEM.SUCURSAL = SUCURSAL_R.READ_WEB(SUCURSAL_ITEM.ID_SUCURSAL);
            }

            return SALIDA;
        }

        public static IEnumerable<SUCURSAL_USUARIO> TODOS_X_USUARIO_NO_BORRADO(long ID_USUARIO)
        {
            IEnumerable<SUCURSAL_USUARIO> SALIDA = null;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<SUCURSAL_USUARIO>(PROCS.SP_READ_SUCURSAL_USUARIO_X_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
            }

            foreach (SUCURSAL_USUARIO SUCURSAL_ITEM in SALIDA)
            {
                SUCURSAL_ITEM.SUCURSAL = SUCURSAL_R.READ_WEB(SUCURSAL_ITEM.ID_SUCURSAL);
            }

            //TODO LO NO BORRADO (POR EMPRESA)
            SALIDA = SALIDA.Where(X => X.SUCURSAL.EMPRESA.BORRADO);

            //TODO LO NO BORRADO (POR SUCURSAL)
            SALIDA = SALIDA.Where(X => X.SUCURSAL.BORRADO);

            return SALIDA;
        }
    }
}