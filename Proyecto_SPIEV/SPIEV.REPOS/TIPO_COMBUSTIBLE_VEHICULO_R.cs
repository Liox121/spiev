﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class TIPO_COMBUSTIBLE_VEHICULO_R
    {
        public static IEnumerable<TIPO_COMBUSTIBLE_VEHICULO> TODOS_COMBUSTIBLE()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_COMBUSTIBLE_VEHICULO>(PROCS.SP_TODO_TIPO_COMBUSTIBLE, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static TIPO_COMBUSTIBLE_VEHICULO READ(long ID_TIPO_COMBUSTIBLE)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<TIPO_COMBUSTIBLE_VEHICULO>(PROCS.SP_READ_TIPO_COMBUSTIBLE, new { ID_TIPO_COMBUSTIBLE }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}