﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class CALIDAD_ESTIPULANTE_R
    {
        public static IEnumerable<CALIDAD_ESTIPULANTE> TODOS_CALIDAD_ESTIPULANTE()
        {
            IEnumerable<CALIDAD_ESTIPULANTE> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<CALIDAD_ESTIPULANTE>(PROCS.SP_TODO_CALIDAD_ESTIPULANTE, null, commandType: CommandType.StoredProcedure);
            }

            foreach (var EST in SALIDA)
            {
                EST.CALIDAD_PERSONA = CALIDAD_PERSONA_R.READ(EST.ID_CALIDAD);
            }
            return SALIDA;
        }

        public static CALIDAD_ESTIPULANTE READ(long ID_CALIDAD_ESTIPULANTE)
        {
            CALIDAD_ESTIPULANTE SALIDA = new CALIDAD_ESTIPULANTE();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<CALIDAD_ESTIPULANTE>(PROCS.SP_READ_CALIDAD_ESTIPULANTE, new { ID_CALIDAD_ESTIPULANTE }, commandType: CommandType.StoredProcedure);
            }

            SALIDA.CALIDAD_PERSONA = CALIDAD_PERSONA_R.READ(SALIDA.ID_CALIDAD);

            return SALIDA;
        }
    }
}
