﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class IMPUESTO_ADICIONAL_R
    {
        public static IMPUESTO_ADICIONAL CREATE(IMPUESTO_ADICIONAL IMPUESTO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<IMPUESTO_ADICIONAL>(PROCS.SP_CREATE_IMPUESTO,
                    new
                    {
                        IMPUESTO.CODIGO_IDENTIFICACION,
                        IMPUESTO.CODIGO_INFORME_TECNICO,
                        IMPUESTO.MONTO_IMPUESTO,
                        IMPUESTO.TOTAL_FACTURA
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IMPUESTO_ADICIONAL READ(long ID_IMPUESTO_ADICIONAL)
        {
            IMPUESTO_ADICIONAL SALIDA = new IMPUESTO_ADICIONAL();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<IMPUESTO_ADICIONAL>(PROCS.SP_READ_IMPUESTO_ADICIONAL, new { ID_IMPUESTO_ADICIONAL }, commandType: CommandType.StoredProcedure);
            }

            return SALIDA;
        }
    }
}