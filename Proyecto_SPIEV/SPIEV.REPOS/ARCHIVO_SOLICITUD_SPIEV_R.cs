﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class ARCHIVO_SOLICITUD_SPIEV_R
    {
        public static ARCHIVO_SOLICITUD_SPIEV CREATE(ARCHIVO_SOLICITUD_SPIEV ARCHIVO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ARCHIVO_SOLICITUD_SPIEV>(PROCS.SP_CREATE_ARCHIVO_SOLICITUD_SPIEV, new { ARCHIVO.ID_SOLICITUD, ARCHIVO.ID_TIPO_ARCHIVO_SOLICITUD, ARCHIVO.ARCHIVO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<ARCHIVO_SOLICITUD_SPIEV> TODOS_BY_SOLICITUD(long ID_SOLICITUD)
        {
            IEnumerable<ARCHIVO_SOLICITUD_SPIEV> SALIDA = null;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<ARCHIVO_SOLICITUD_SPIEV>(PROCS.SP_TODO_ARCHIVO_SOLICITUD_SPIEV,null, commandType: CommandType.StoredProcedure);
            }

            //SOLO LO DE LA SOLICITUD
            SALIDA = SALIDA.Where(n => n.ID_SOLICITUD == ID_SOLICITUD);

            //SE HACEN LOS CRUCES...
            Parallel.ForEach(SALIDA, ARCHIVO =>
            {
                ARCHIVO.TIPO_ARCHIVO_SOLICITUD = TIPO_ARCHIVO_SOLICITUD_R.READ(ARCHIVO.ID_TIPO_ARCHIVO_SOLICITUD);
            });

            return SALIDA;

        }

        public static ARCHIVO_SOLICITUD_SPIEV READ(long ID_ARCHIVO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ARCHIVO_SOLICITUD_SPIEV>(PROCS.SP_READ_ARCHIVO_SOLICITUD_SPIEV, new { ID_ARCHIVO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static void UPDATE(UPDATE_ARCHIVO ARCHIVO_SPIEV)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_ARCHIVO_SOLICITUD_SPIEV, ARCHIVO_SPIEV, commandType: CommandType.StoredProcedure);
            }
        }

        public  class UPDATE_ARCHIVO
        {
            public long ID_ARCHIVO { get; set; }
            public string ARCHIVO { get; set; }
            public long? ID_TIPO_ARCHIVO_SOLICITUD { get; set; }
            public long? ID_SOLICITUD { get; set; }
            public bool? BORRADO { get; set; }
        }
    }
}