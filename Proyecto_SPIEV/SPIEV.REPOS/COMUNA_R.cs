﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class COMUNA_R
    {
        public static IEnumerable<COMUNA> TODAS_COMUNA()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<COMUNA>(PROCS.SP_TODO_COMUNA, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static COMUNA READ(long ID_COMUNA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<COMUNA>(PROCS.SP_READ_COMUNA, new { ID_COMUNA }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}