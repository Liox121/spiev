﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{

    public static class USUARIO_PERFIL_R
    {
        public static USUARIO_PERFIL CREATE(USUARIO_PERFIL USUARIO_PERFIL)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<USUARIO_PERFIL>(PROCS.SP_CREATE_USUARIO_PERFIL, new { USUARIO_PERFIL.ID_USUARIO, USUARIO_PERFIL.ID_PERFIL, USUARIO_PERFIL.POR_DEFECTO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static USUARIO_PERFIL CREATE(IDbConnection _db, USUARIO_PERFIL USUARIO_PERFIL)
        {
            return _db.QuerySingle<USUARIO_PERFIL>(PROCS.SP_CREATE_USUARIO_PERFIL, new { USUARIO_PERFIL.ID_USUARIO, USUARIO_PERFIL.ID_PERFIL, USUARIO_PERFIL.POR_DEFECTO }, commandType: CommandType.StoredProcedure);
        }

        public static IEnumerable<USUARIO_PERFIL> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<USUARIO_PERFIL>(PROCS.SP_TODO_USUARIO_PERFIL, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<USUARIO_PERFIL> TODOS_X_USUARIO(long ID_USUARIO)
        {
            IEnumerable<USUARIO_PERFIL> SALIDA;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<USUARIO_PERFIL>(PROCS.SP_READ_USUARIO_PERFIL_X_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
            }

            //TODOS LOS PERFILES
            List<PERFIL> LISTA_PERFIL = PERFIL_R.TODOS_LOGIN().ToList();

            //LOS PERFILES X USUARIO
            Parallel.ForEach(SALIDA, USUARIO_PERFIL =>
            {
                USUARIO_PERFIL.PERFIL = LISTA_PERFIL.First(n => n.ID_PERFIL == USUARIO_PERFIL.ID_PERFIL);
            });

            return SALIDA;
        }

        public static IEnumerable<USUARIO_PERFIL_SESSION> TODOS_X_USUARIO_SESSION(long ID_USUARIO)
        {
            IEnumerable<USUARIO_PERFIL_SESSION> SALIDA;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<USUARIO_PERFIL_SESSION>(PROCS.SP_READ_USUARIO_PERFIL_X_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
            }

            //TODOS LOS PERFILES
            List<PERFIL_SESSION> LISTA_PERFIL = PERFIL_R.TODOS_SESSION().ToList();

            //LOS PERFILES X USUARIO
            Parallel.ForEach(SALIDA, USUARIO_PERFIL =>
            {
                USUARIO_PERFIL.PERFIL = LISTA_PERFIL.First(n => n.ID_PERFIL == USUARIO_PERFIL.ID_PERFIL);
            });

            return SALIDA;
        }

        public static void DELETE_X_USUARIO(long ID_USUARIO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_DELETE_USUARIO_PERFIL_X_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static void DELETE_X_USUARIO(IDbConnection _db, long ID_USUARIO)
        {
           _db.Execute(PROCS.SP_DELETE_USUARIO_PERFIL_X_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
        }

    }
}
