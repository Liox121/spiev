﻿using Dapper;
using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SPIEV.REPOS
{
    public static class TIPO_VEHICULO_R
    {
        public static IEnumerable<TIPO_VEHICULO> TODOS_TIPOS_VEHICULOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_VEHICULO>(PROCS.SP_TODO_TIPO_VEHICULO, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<TIPO_VEHICULO> TODOS_WEB()
        {
            IEnumerable<TIPO_VEHICULO> SALIDA = null;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<TIPO_VEHICULO>(PROCS.SP_TODO_TIPO_VEHICULO, null, commandType: CommandType.StoredProcedure);
            }

            foreach (TIPO_VEHICULO TIPO in SALIDA)
            {
                TIPO.CATEGORIA_TIPO_VEHICULO = CATEGORIA_TIPO_VEHICULO_R.READ(TIPO.ID_CATEGORIA);
            }

            return SALIDA;
        }



        public static TIPO_VEHICULO READ(long ID_TIPO_VEHICULO)
        {
            TIPO_VEHICULO SALIDA = new TIPO_VEHICULO();

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<TIPO_VEHICULO>(PROCS.SP_READ_TIPO_VEHICULO, new { ID_TIPO_VEHICULO }, commandType: CommandType.StoredProcedure);
            }

            //SUB-OBJETO
            SALIDA.CATEGORIA_TIPO_VEHICULO = CATEGORIA_TIPO_VEHICULO_R.READ(SALIDA.ID_CATEGORIA);

            return SALIDA;
        }

        /// <summary>
        /// TIPOS DE VEHICULOS, PESADOS DESDE JSON_CONFIG
        /// </summary>
        /// <param name="LLAVE_JSON"></param>
        /// <returns></returns>
        public static IEnumerable<TIPO_VEHICULO> TIPOS_VEHICULOS_PESADOS()
        {
            IEnumerable<TIPO_VEHICULO> SALIDA = null;
            string LLAVE_JSON = "TIPOS_VEHICULOS_PESADOS";

            //SI EXISTE EL JSON
            if (JSON_CONFIG_R.TODOS().Any(n => n.CLAVE_JSON == LLAVE_JSON))
            {
                //SE DESERIALIZA EL OBJETO A UNA LISTA
                string JSON_STRING = JSON_CONFIG_R.READ(LLAVE_JSON).JSON_OBJETO;
                SALIDA = JsonConvert.DeserializeObject<IEnumerable<TIPO_VEHICULO>>(JSON_STRING);
            }

            return SALIDA;
        }
    }
}