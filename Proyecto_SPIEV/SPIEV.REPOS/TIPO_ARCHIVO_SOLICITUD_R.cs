﻿using Dapper;
using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SPIEV.REPOS
{
    public class TIPO_ARCHIVO_SOLICITUD_R
    {
        public static TIPO_ARCHIVO_SOLICITUD READ(long ID_TIPO_ARCHIVO_SOLICITUD)
        {
            TIPO_ARCHIVO_SOLICITUD SALIDA = new TIPO_ARCHIVO_SOLICITUD();
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<TIPO_ARCHIVO_SOLICITUD>(PROCS.SP_READ_TIPO_ARCHIVO_SOLICITUD, new { ID_TIPO_ARCHIVO_SOLICITUD }, commandType: CommandType.StoredProcedure);
            }

            SALIDA.TIPO_ARCHIVO = TIPO_ARCHIVO_R.READ(SALIDA.ID_TIPO_ARCHIVO);

            return SALIDA;
        }

        public static IEnumerable<TIPO_ARCHIVO_SOLICITUD> TODOS_WEB()
        {
            IEnumerable<TIPO_ARCHIVO_SOLICITUD> SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<TIPO_ARCHIVO_SOLICITUD>(PROCS.SP_TODO_TIPO_ARCHIVO_SOLICITUD, null, commandType: CommandType.StoredProcedure);
            }

            //SUBCLASE
            foreach (TIPO_ARCHIVO_SOLICITUD item in SALIDA)
            {
                item.TIPO_ARCHIVO = TIPO_ARCHIVO_R.READ(item.ID_TIPO_ARCHIVO);
            }

            return SALIDA;
        }

        public static IEnumerable<TIPO_ARCHIVO_SOLICITUD> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_ARCHIVO_SOLICITUD>(PROCS.SP_TODO_TIPO_ARCHIVO_SOLICITUD, null, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// RETORNA LISTA DE TIPOS DE ARCHIVOS NECESARIOS, PARA CREAR/EDITAR UNA SOLICITUD, DESDE TABLA JSON_CONFIG
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<TIPO_ARCHIVO_SOLICITUD> VALIDACION_SOLICITUD()
        {
            //JSON_LLAVE
            string LLAVE_JSON = "CREAR_SOLICITUD_VALIDACION";

            //VALIDACION
            IEnumerable<TIPO_ARCHIVO_SOLICITUD> SALIDA = null;

            //SI EXISTE EL JSON
            if (JSON_CONFIG_R.TODOS().Any(n => n.CLAVE_JSON == LLAVE_JSON))
            {
                //SE DESERIALIZA EL OBJETO A UNA LISTA
                SALIDA = JsonConvert.DeserializeObject<IEnumerable<TIPO_ARCHIVO_SOLICITUD>>(JSON_CONFIG_R.READ(LLAVE_JSON).JSON_OBJETO);
            }

            return SALIDA;
        }

        /// <summary>
        /// RETORNA LISTA DE TIPOS DE ARCHIVOS, PARA COMBOBOX AL CREAR UNA SOLICITUD, DESDE TABLA JSON_CONFIG
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<TIPO_ARCHIVO_SOLICITUD> TODOS_WEB_CREAR()
        {
            //JSON_LLAVE
            string LLAVE_JSON = "TIPOS_ARCHIVOS_CREAR_SOLICITUD";
            //VALIDACION
            IEnumerable<TIPO_ARCHIVO_SOLICITUD> SALIDA = null;

            //SI EXISTE EL JSON
            if (JSON_CONFIG_R.TODOS().Any(n => n.CLAVE_JSON == LLAVE_JSON))
            {
                //SE DESERIALIZA EL OBJETO A UNA LISTA
                SALIDA = JsonConvert.DeserializeObject<IEnumerable<TIPO_ARCHIVO_SOLICITUD>>(JSON_CONFIG_R.READ(LLAVE_JSON).JSON_OBJETO);
            }

            return SALIDA;
        }


    }
}