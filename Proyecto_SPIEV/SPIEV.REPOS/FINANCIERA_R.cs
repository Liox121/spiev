﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class FINANCIERA_R
    {
        public static IEnumerable<FINANCIERA> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<FINANCIERA>(PROCS.SP_TODO_FINANCIERA, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static FINANCIERA READ(long ID_FINANCIERA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<FINANCIERA>(PROCS.SP_READ_FINANCIERA, new { ID_FINANCIERA }, commandType: CommandType.StoredProcedure);
            }
        }

        public static FINANCIERA CREATE(FINANCIERA FINANCIERA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<FINANCIERA>(PROCS.SP_CREATE_FINANCIERA, new { FINANCIERA.NOMBRE_FINANCIERA }, commandType: CommandType.StoredProcedure);
            }
        }

        public static void UPDATE(FINANCIERA FINANCIERA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_FINANCIERA, FINANCIERA, commandType: CommandType.StoredProcedure);
            }
        }
    }
}