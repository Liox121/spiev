﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class REGION_R
    {
        public static IEnumerable<REGION> TODOS_REGION()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<REGION>(PROCS.SP_TODO_REGION, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static REGION READ(long ID_REGION)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<REGION>(PROCS.SP_READ_REGION, new { ID_REGION }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}