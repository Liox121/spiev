﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;

namespace SPIEV.REPOS
{
    public static class USUARIO_R
    {
        public static USUARIO CREATE(USUARIO USUARIO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<USUARIO>(PROCS.SP_CREATE_USUARIO, new { USUARIO.NOMBRE_USUARIO, USUARIO.CLAVE, USUARIO.CORREO_ELECTRONICO, USUARIO.ACTIVO, USUARIO.NOMBRE_COMPLETO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static bool DELETE(long ID_USUARIO)
        {
            throw new NotImplementedException();
        }

        //public static bool LOGIN(USUARIO USUARIO)
        //{
        //    using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
        //    {
        //        return _db.QuerySingle<bool>(PROCS.SP_LOGIN_USUARIO,  new { USUARIO.NOMBRE_USUARIO, USUARIO.CLAVE },  commandType: CommandType.StoredProcedure);
        //    }
        //}

        public static bool LOGIN(string NOMBRE_USUARIO, string CLAVE)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<bool>(PROCS.SP_LOGIN_USUARIO, new { NOMBRE_USUARIO, CLAVE }, commandType: CommandType.StoredProcedure);
            }
        }

        public static USUARIO READ(long ID_USUARIO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<USUARIO>(PROCS.SP_READ_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static USUARIO READ_WEB(long ID_USUARIO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<USUARIO>(PROCS.SP_READ_USUARIO, new { ID_USUARIO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<USUARIO> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<USUARIO>(PROCS.SP_TODO_USUARIO, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static void UPDATE(USUARIO USUARIO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_USUARIO, USUARIO, commandType: CommandType.StoredProcedure);
            }
        }

        public static void UPDATE(IDbConnection _db, USUARIO USUARIO)
        {
            _db.Execute(PROCS.SP_UPDATE_USUARIO, USUARIO, commandType: CommandType.StoredProcedure);
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        public static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
