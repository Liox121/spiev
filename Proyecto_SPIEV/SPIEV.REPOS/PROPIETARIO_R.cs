﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using SPIEV.LIB_WEB;

namespace SPIEV.REPOS
{
    public static class PROPIETARIO_R
    {
        public static PROPIETARIO READ(long ID_PROPIETARIO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<PROPIETARIO>(PROCS.SP_READ_PROPIETARIO, new { ID_PROPIETARIO }, commandType: CommandType.StoredProcedure);
            }
        }


        public static IEnumerable<PROPIETARIO> READ_TODO_PROPIETARIO()
        {
            IEnumerable<PROPIETARIO> SALIDA = null;
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.Query<PROPIETARIO>(PROCS.SP_READ_PROPIETARIO,null , commandType: CommandType.StoredProcedure);
            }

            return SALIDA;
        }
    }
}



