﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPIEV.REPOS
{
    public static class SERVICIO_R
    {
        public static SERVICIO READ(long ID_SERVICIO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<SERVICIO>(PROCS.SP_READ_SERVICIO, new { ID_SERVICIO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<SERVICIO> TODOS()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<SERVICIO>(PROCS.SP_TODO_SERVICIO, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
