﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class CANTIDAD_COMUNIDAD_R
    {
        public static IEnumerable<CANTIDAD_COMUNIDAD> TODOS_CANTIDAD_COMUNIDAD()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<CANTIDAD_COMUNIDAD>(PROCS.SP_TODO_CANTIDAD_COMUNIDAD, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}