﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class TIPO_MONEDA_R
    {
        public static IEnumerable<TIPO_MONEDA> TODOS_TIPO_MONEDA()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<TIPO_MONEDA>(PROCS.SP_TODO_TIPO_MONEDA, null, commandType: CommandType.StoredProcedure);
            }
        }

        public static TIPO_MONEDA READ(long ID_TIPO_MONEDA)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<TIPO_MONEDA>(PROCS.SP_READ_TIPO_MONEDA, new { ID_TIPO_MONEDA }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}