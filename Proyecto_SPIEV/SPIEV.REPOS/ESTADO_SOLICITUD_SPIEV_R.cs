﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Data;

namespace SPIEV.REPOS
{
    public static class ESTADO_SOLICITUD_SPIEV_R
    {
        public static ESTADO_SOLICITUD_SPIEV READ(long ID_ESTADO)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<ESTADO_SOLICITUD_SPIEV>(PROCS.SP_READ_ESTADO_SOLICITUD_SPIEV, new { ID_ESTADO }, commandType: CommandType.StoredProcedure);
            }
        }

        public static IEnumerable<ESTADO_SOLICITUD_SPIEV> TODOS_ESTADOS_SOLICITUD()
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.Query<ESTADO_SOLICITUD_SPIEV>(PROCS.SP_TODO_ESTADO_SOLICITUD_SPIEV, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}