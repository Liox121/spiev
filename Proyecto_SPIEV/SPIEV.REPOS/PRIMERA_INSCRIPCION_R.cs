﻿using Dapper;
using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SPIEV.REPOS
{
    public static class PRIMERA_INSCRIPCION_R
    {
        public class UPDATE_INSCRIPCION
        {
            public long ID_PRIMERA_INSCRIPCION { get; set; }
            public long? ID_SPIEV { get; set; }
            public long? ID_ESTADO { get; set; }
            public long? ID_USUARIO { get; set; }
            public long? ID_ESTIPULANTE { get; set; }
            public long? ID_IMPUESTO_ADICIONAL { get; set; }
            public long? ID_FACTURA { get; set; }
            public long? ID_ADQUIRIENTE { get; set; }
        }

        public static PRIMERA_INSCRIPCION CREATE(PRIMERA_INSCRIPCION PRIMERA_INSCRIPCION)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<PRIMERA_INSCRIPCION>(PROCS.SP_CREATE_PRIMERA_INSCRIPCION, 
                    new
                    {
                        PRIMERA_INSCRIPCION.ID_SPIEV,
                        PRIMERA_INSCRIPCION.ID_ESTADO,
                        PRIMERA_INSCRIPCION.ID_USUARIO,
                        PRIMERA_INSCRIPCION.ID_ESTIPULANTE,
                        PRIMERA_INSCRIPCION.ID_ADQUIRIENTE,
                        PRIMERA_INSCRIPCION.ID_IMPUESTO_ADICIONAL,
                        PRIMERA_INSCRIPCION.ID_FACTURA
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static PRIMERA_INSCRIPCION READ(long ID_PRIMERA_INSCRIPCION)
        {
            PRIMERA_INSCRIPCION SALIDA = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                SALIDA = _db.QuerySingle<PRIMERA_INSCRIPCION>(PROCS.SP_READ_PRIMERA_INSCRIPCION, new { ID_PRIMERA_INSCRIPCION }, commandType: CommandType.StoredProcedure);
            }

            SALIDA.ESTADO = ESTADO_PRIMERA_INSCRIPCION_R.READ(SALIDA.ID_ESTADO);
            SALIDA.SPIEV = SPIEV_R.READ(SALIDA.ID_SPIEV);
            SALIDA.USUARIO = USUARIO_R.READ(SALIDA.ID_USUARIO);
            SALIDA.ESTIPULANTE = ESTIPULANTE_R.READ(SALIDA.ID_ESTIPULANTE);
            SALIDA.ADQUIRIENTE = ADQUIRIENTE_R.READ(SALIDA.ID_ADQUIRIENTE);
            SALIDA.IMPUESTO = IMPUESTO_ADICIONAL_R.READ(SALIDA.ID_IMPUESTO_ADICIONAL);
            SALIDA.FACTURA = FACTURA_R.READ(SALIDA.ID_FACTURA);

            return SALIDA;
        }

        public static IEnumerable<PRIMERA_INSCRIPCION> TODO_INSCRIPCION()
        {
            IEnumerable<PRIMERA_INSCRIPCION> PRIMERAS_INSCRIPCIONES = null;

            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                PRIMERAS_INSCRIPCIONES = _db.Query<PRIMERA_INSCRIPCION>(PROCS.SP_TODO_PRIMERA_INSCRIPCION, null, commandType: CommandType.StoredProcedure);
            }

            foreach (PRIMERA_INSCRIPCION INS in PRIMERAS_INSCRIPCIONES)
            {
                INS.ESTADO = ESTADO_PRIMERA_INSCRIPCION_R.READ(INS.ID_ESTADO);
                INS.SPIEV  = SPIEV_R.READ(INS.ID_SPIEV);
                INS.USUARIO = USUARIO_R.READ(INS.ID_USUARIO);
                INS.ESTIPULANTE = ESTIPULANTE_R.READ(INS.ID_ESTIPULANTE);
                INS.ADQUIRIENTE = ADQUIRIENTE_R.READ(INS.ID_ADQUIRIENTE);
                INS.IMPUESTO = IMPUESTO_ADICIONAL_R.READ(INS.ID_IMPUESTO_ADICIONAL);
                INS.FACTURA = FACTURA_R.READ(INS.ID_FACTURA);
            }

            return PRIMERAS_INSCRIPCIONES;
        }

        public static void UPDATE(UPDATE_INSCRIPCION INSCRIPCION)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                _db.Execute(PROCS.SP_UPDATE_PRIMERA_INSCRIPCION, INSCRIPCION, commandType: CommandType.StoredProcedure);
            }
        }
        //public static void DELETE(long id_archivo)
        //{
        //    using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
        //    {
        //        _db.Execute(PROCS.SP_DELETE_ARCHIVO_INSCRIPCION, id_archivo, commandType: CommandType.StoredProcedure);
        //    }

        //}


    }
}