﻿using Dapper;
using SPIEV.LIB_WEB;
using System.Data;

namespace SPIEV.REPOS
{
    public static class OBSERVACION_R
    {
        public static OBSERVACION CREATE(OBSERVACION OBSERVACION)
        {
            using (IDbConnection _db = CONEXION.CONEXION_INSCRIPCION())
            {
                return _db.QuerySingle<OBSERVACION>(PROCS.SP_CREATE_OBSERVACION, new { OBSERVACION.COMENTARIO }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}