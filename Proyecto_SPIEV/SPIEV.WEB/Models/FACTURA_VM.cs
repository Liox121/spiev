﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEB.Models
{
    public class FACTURA_VM : FACTURA
    {
        [Display(Name = "Numero Factura")]
        [Required(ErrorMessage = "(*) Numero Factura requerida")]
        [Range(1, 999999999999999999)]
        public override long NUMERO_FACTURA { get; set; }

        [Display(Name = "Fecha Emisión Factura")]
        [Required(ErrorMessage = "(*) Fecha Emisión Factura requerida")]
        public override int FECHA { get; set; }

        [Display(Name = "Comuna de emisión")]
        public override long ID_COMUNA { get; set; }

        [Display(Name = "Rut Emisor")]
        [Required(ErrorMessage = "(*) Run Emisor requerido")]
        [Range(0, 99999999)]
        public override int RUT_EMISOR { get; set; }

        [Display(Name = "Nombre Emisor")]
        [Required(ErrorMessage = "(*) Nombre Emisor requerido")]
        [StringLength(100)]
        public override string NOMBRE_EMISOR { get; set; }

        [Display(Name = "Monto Total Factura")]
        [Required(ErrorMessage = "(*) Monto Total Factura requerida")]
        public override int MONTO_TOTAL_FACTURA { get; set; }

        [Display(Name = "Tipo Moneda")]
        public override long ID_TIPO_MONEDA { get; set; }

        //LISTA DE DATOS PARA VISTA DE FORMULARIO
        public List<TIPO_MONEDA> LISTA_TIPOS_MONEDAS { get; set; }

        /// <summary>
        /// RETORNA OBJETO PARA CREAR EN REPOSITORIO
        /// </summary>
        /// <returns></returns>
        public FACTURA OBJETO_CREAR()
        {
            return new FACTURA
            {
                NUMERO_FACTURA      = this.NUMERO_FACTURA,
                FECHA               = this.FECHA,       
                ID_COMUNA           = this.ID_COMUNA,          
                RUT_EMISOR          = this.RUT_EMISOR,         
                NOMBRE_EMISOR       = this.NOMBRE_EMISOR,      
                MONTO_TOTAL_FACTURA = this.MONTO_TOTAL_FACTURA,
                ID_TIPO_MONEDA      = this.ID_TIPO_MONEDA     
            };
        }
    }
}