﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPIEV.WEB.Models
{
    public class USUARIO_VM : USUARIO
    {
        public string REPERTIR_CLAVE { get; set; }
        public long PERFIL_POR_DEFECTO { get; set; }
        public List<PERFIL> MIS_PERFILES { get; set; }
        public List<PERFIL> PERFILES { get; set; }
        public List<SUCURSAL> MIS_SUCURSALES { get; set; }
        public List<SUCURSAL> SUCURSALES { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void USUARIO_VM_DESDE_USUARIO(USUARIO USUARIO_BASE)
        {
            this.ID_USUARIO = USUARIO_BASE.ID_USUARIO;
            this.NOMBRE_USUARIO = USUARIO_BASE.NOMBRE_USUARIO;
            this.NOMBRE_COMPLETO = USUARIO_BASE.NOMBRE_COMPLETO;
            this.CLAVE = USUARIO_BASE.CLAVE;
            this.CORREO_ELECTRONICO = USUARIO_BASE.CORREO_ELECTRONICO;
            this.ACTIVO = USUARIO_BASE.ACTIVO;
        }

        /// <summary>
        /// VALIDAR CLAVES NO NULAS O VACIAS
        /// </summary>
        /// <returns></returns>
        public bool CLAVES_NO_NULAS()
        {
            return (!String.IsNullOrEmpty(CLAVE) && !String.IsNullOrEmpty(REPERTIR_CLAVE)) ? true : false;
        }

        /// <summary>
        /// VALIDAR CLAVES IGUALES
        /// </summary>
        /// <returns></returns>
        public bool CLAVES_IGUALES()
        {
            return (CLAVE == REPERTIR_CLAVE) ? true : false;
        }

    }
}