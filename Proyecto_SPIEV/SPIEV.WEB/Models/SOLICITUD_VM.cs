﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.Linq;

namespace SPIEV.WEB.Models
{
    public class SOLICITUD_VM
    {
        public SOLICITUD_VM()
        {
            FINANCIERAS = new List<FINANCIERA>();
        }

        public void DEFAULT_OPCION_FINANCIERA()
        {
            FINANCIERAS.Add(new FINANCIERA { ID_FINANCIERA = 0, NOMBRE_FINANCIERA = "SIN FINANCIERA" });

            FINANCIERAS = FINANCIERAS.OrderBy(n => n.ID_FINANCIERA).ToList();
        }

        public List<SUCURSAL_USUARIO> SUCURSALES { get; set; }
        public List<FINANCIERA> FINANCIERAS { get; set; }
    }
}