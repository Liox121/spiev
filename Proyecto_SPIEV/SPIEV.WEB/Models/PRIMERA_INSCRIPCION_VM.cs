﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;

namespace SPIEV.WEB.Models
{
    public class PRIMERA_INSCRIPCION_VM
    {
        public VEHICULO_VM VehiculoForm { get; set; }
        public COMUNIDAD_VM ComunidadForm { get; set; }
        public ADQUIRIENTE_VM AdquirienteForm { get; set; }
        public SOLICITANTE_VM SolicitanteForm { get; set; }
        public ESTIPULANTE_VM EstipulanteForm { get; set; }
        public IMPUESTO_ADICIONAL_VM ImpuestoForm { get; set; }
        public FACTURA_VM FacturaForm { get; set; }
        public OBSERVACION_VM ObservacionForm { get; set; }
        public OPERADOR_VM OperadorForm { get; set; }

        //LISTA PARA LAS VISTAS
        public List<COMUNA> LISTA_COMUNAS { get; set; }
        public List<CALIDAD_PERSONA> LISTA_CALIDAD_PERSONAS { get; set; }


        //DESDE SOLICITUD
        public long? ID_SOLICITUD { get; set; }
    }
}