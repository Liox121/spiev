﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;

namespace SPIEV.WEB.Models
{
    public class PERFIL_VM : PERFIL
    {
        public List<PAGINA_PERFIL> PAGINAS { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void PERFIL_VM_DESDE_PERFIL(PERFIL PERFIL_BASE)
        {
            this.ID_PERFIL = PERFIL_BASE.ID_PERFIL;
            this.NOMBRE = PERFIL_BASE.NOMBRE;
            this.DESCRIPCION = PERFIL_BASE.DESCRIPCION;
            this.ICONO = PERFIL_BASE.ICONO;
        }
    }
}