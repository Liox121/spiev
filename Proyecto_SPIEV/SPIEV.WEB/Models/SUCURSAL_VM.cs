﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;

namespace SPIEV.WEB.Models
{
    public class SUCURSAL_VM : SUCURSAL
    {
        public List<EMPRESA> EMPRESAS { get; set; }

        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void SUCURSAL_VM_DESDE_SUCURSAL(SUCURSAL SUCURSAL_BASE)
        {
            this.ID_SUCURSAL = SUCURSAL_BASE.ID_SUCURSAL;
            this.NOMBRE_SUCURSAL = SUCURSAL_BASE.NOMBRE_SUCURSAL;
            this.ID_EMPRESA = SUCURSAL_BASE.ID_EMPRESA;
        }
    }
}