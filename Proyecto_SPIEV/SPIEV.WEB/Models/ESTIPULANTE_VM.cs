﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEB.Models
{
    public class ESTIPULANTE_VM : ESTIPULANTE
    {
        [Display(Name = "Razon Social")]
        [Required(ErrorMessage = "(*) Razon Social requerida")]
        [StringLength(65)]
        public override string RAZON_SOCIAL { get; set; }

        [Display(Name = "Apellido Paterno")]
        [StringLength(20)]
        public override string APELLIDO_PATERNO { get; set; }

        [Display(Name = "Apellido Materno")]
        [StringLength(20)]
        public override string APELLIDO_MATERNO { get; set; }

        [Display(Name = "Run/Rut")]
        [Required(ErrorMessage = "(*) Run/Rut requerido")]
        [Range(0, 99999999)]
        public override long RUN { get; set; }

        [Display(Name = "Calidad Persona")]
        public override long ID_CALIDAD_PERSONA { get; set; }

        [Display(Name = "Calle Domicilio")]
        [Required(ErrorMessage = "(*) Calle Domicilio requerida")]
        [StringLength(45)]
        public override string CALLE { get; set; }

        [Display(Name = "Comuna")]
        public override long ID_COMUNA { get; set; }

        [Display(Name = "Numero Domicilio")]
        [Required(ErrorMessage = "(*) Numero Domicilio requerido")]
        [StringLength(9)]
        public override string NUMERO_DOMICILIO { get; set; }

        [Display(Name = "Letra Domicilio")]
        [StringLength(45)]
        public override string LETRA_DOMICILIO { get; set; }

        [Display(Name = "Resto Domicilio")]
        [StringLength(45)]
        public override string RESTO_DOMICILIO { get; set; }

        [Display(Name = "Telefono")]
        [Required(ErrorMessage = "(*) Telefono requerido")]
        [StringLength(10)]
        public override string TELEFONO { get; set; }

        [Display(Name = "Codigo Postal")]
        [StringLength(20)]
        public override string CODIGO_POSTAL { get; set; }

        [Display(Name = "Correo Electronico")]
        [Required(ErrorMessage = "(*) Correo Electronico requerido")]
        [EmailAddress(ErrorMessage = "(*) Correo Electronico invalido")]
        [StringLength(100)]
        public override string CORREO_ELECTRONICO { get; set; }

        /// <summary>
        /// RETORNA OBJETO PARA CREAR EN REPOSITORIO
        /// </summary>
        /// <returns></returns>
        public ESTIPULANTE OBJETO_CREAR()
        {
            return new ESTIPULANTE
            {
                APELLIDO_PATERNO = this.APELLIDO_PATERNO,
                APELLIDO_MATERNO = this.APELLIDO_MATERNO,
                RAZON_SOCIAL = this.RAZON_SOCIAL,
                RUN = this.RUN,
                CALLE = this.CALLE,
                NUMERO_DOMICILIO = this.NUMERO_DOMICILIO,
                LETRA_DOMICILIO = this.LETRA_DOMICILIO,
                RESTO_DOMICILIO = this.RESTO_DOMICILIO,
                TELEFONO = this.TELEFONO,
                CODIGO_POSTAL = this.CODIGO_POSTAL,
                CORREO_ELECTRONICO = this.CORREO_ELECTRONICO,
                ID_COMUNA = this.ID_COMUNA,
                ID_CALIDAD_PERSONA = this.ID_CALIDAD_PERSONA
            };
        }
    }
}