﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEB.Models
{
    public class OBSERVACION_VM : OBSERVACION
    {
        [Display(Name = "Observación")]
        [StringLength(100)]
        public override string COMENTARIO { get; set; }

        /// <summary>
        /// RETORNA OBJETO PARA CREAR EN REPOSITORIO
        /// </summary>
        /// <returns></returns>
        public OBSERVACION OBJETO_CREAR()
        {
            return new OBSERVACION
            {
                COMENTARIO = this.COMENTARIO
            };
        }
    }
}