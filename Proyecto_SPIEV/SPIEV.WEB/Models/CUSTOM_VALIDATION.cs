﻿using SPIEV.UTIL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPIEV.WEB.Models
{
    /// <summary>
    /// VALIDAR RUT CON FORMATO XXXXXXXX-X (EJ. 19570929-7)
    /// </summary>
    public class RutValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string RUT = value.ToString();

            //SI NO ES VACIO O NULO
            if (String.IsNullOrEmpty(RUT) == false)
            {
                bool RUT_VALIDO = Rut_Validar.ValidaRut(RUT);

                return (RUT_VALIDO) ? ValidationResult.Success : new ValidationResult(this.ErrorMessage);
            }

            return new ValidationResult(this.ErrorMessage);
        }
    }

    /// <summary>
    /// VALIDAR ARRAY DE NUMEROS (LONG) EN CASO DE NULO O VACIOS
    /// </summary>
    public class ArrayLongValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            List<long> LIST_LONG = (List<long>)value;

            //SI ES NULO O VACIO
            if (LIST_LONG == null || LIST_LONG.Any() == false) return new ValidationResult(this.ErrorMessage);

            return ValidationResult.Success;
        }
    }
}