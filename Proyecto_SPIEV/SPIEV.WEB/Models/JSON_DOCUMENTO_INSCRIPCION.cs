﻿using System.Collections.Generic;

namespace SPIEV.WEB.Models
{
    public class JSON_DOCUMENTO_INSCRIPCION
    {
        public JSON_DOCUMENTO_INSCRIPCION()
        {
            ARCHIVOS_ACTUALIZAR = new List<int>();
            TIPO_ARCHIVO_ACTUALIZAR = new List<int>();
            TIPO_ARCHIVO_NUEVO = new List<int>();
        }

        public int ID_INSCRIPCION { get; set; }
        public List<int> ARCHIVOS_ACTUALIZAR { get; set; }
        public List<int> TIPO_ARCHIVO_ACTUALIZAR { get; set; }
        public List<int> TIPO_ARCHIVO_NUEVO { get; set; }
    }
}