﻿using SPIEV.LIB_WEB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPIEV.WEB.Models
{
    public class FINANCIERA_VM : FINANCIERA
    {
        /// <summary>
        /// SETEA LOS VALORES CON OBJETO DESDE REPOSITORIO
        /// </summary>
        /// <param name="USUARIO_BASE"></param>
        public void FINANCIERA_VM_DESDE_FINANCIERA(FINANCIERA FINANCIERA_BASE)
        {
            this.ID_FINANCIERA = FINANCIERA_BASE.ID_FINANCIERA;
            this.NOMBRE_FINANCIERA = FINANCIERA_BASE.NOMBRE_FINANCIERA;
        }
    }
}