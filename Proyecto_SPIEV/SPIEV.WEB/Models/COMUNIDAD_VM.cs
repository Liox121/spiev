﻿using SPIEV.LIB_WEB;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SPIEV.WEB.Models
{
    public class COMUNIDAD_VM : COMUNIDAD
    {
        [Display(Name = "Cantidad")]
        [Required(ErrorMessage = "(*) Cantidad requerida")] //CAMPO REQUERIDO
        [Range(0, 99)] //LARGO 99
        public override long CANTIDAD { get; set; }

        [Display(Name = "Es Comunidad")]
        public override long ID_OPCION_COMUNIDAD { get; set; }

        //LISTA DE DATOS
        public List<OPCION_COMUNIDAD> LISTA_OPCIONES_COMUNIDAD { get; set; }

        /// <summary>
        /// RETORNA OBJETO PARA CREAR EN REPOSITORIO
        /// </summary>
        /// <returns></returns>
        public COMUNIDAD OBJETO_CREAR()
        {
            return new COMUNIDAD
            {
                ID_OPCION_COMUNIDAD = this.ID_OPCION_COMUNIDAD,
                CANTIDAD = this.CANTIDAD
            };
        }
    }
}