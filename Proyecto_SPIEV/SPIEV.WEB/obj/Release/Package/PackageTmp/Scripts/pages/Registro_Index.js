﻿$(document).ready(function () {

    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        //keyNavigation: true,
        theme: 'dots',
        transitionEffect: 'fade',
        showStepURLhash: true,
        lang: { next: 'Siguiente', previous: 'Anterior' },
        toolbarSettings: {
            toolbarPosition: 'botton',
            toolbarButtonPosition: 'end'
            //toolbarExtraButtons: [btnFinish, btnCancel]
        },
        anchorSettings: {
            markDoneStep: true, // add done css
            markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
            removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
        }
    });

    $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {

        try {
            var formularioValido = $('#form-step-' + stepNumber).valid();
            //return formularioValido;
            return true;
        } catch (e) {
            return true;
        }


        //var elmForm = $("#form-step-" + stepNumber);
        // stepDirection === 'forward' :- this condition allows to do the form validation
        // only on forward navigation, that makes easy navigation on backwards still do the validation when going next

        //if (stepDirection === 'forward' && elmForm) {
        //    elmForm.validator('validate');
        //    var elmErr = elmForm.children('.has-error');
        //    if (elmErr && elmErr.length > 0) {
        //        // Form validation failed
        //        return false;
        //    }
        //}
    });

    //HABILITA EL GUARDADO DE DATOS SOBRE LOS INPUTS...
    $('.form-spiev').each(function () {
        var formulario = $(this);

        $(formulario).find('[data-propiedad]').each(function ()
        {
            var input = $(this);

            //AL CAMBIAR EL VALOR GUARDA EL VALOR
            $(input).change(function () {
                saveValue(this);
            });

            //AL CARGAR LA PAGINA TRAE LOS DATOS...
            $(input).val(getSavedValue($(input).attr('id')));
        });
    });



    $('#btn-generar-spiev').click(function () {

        var objeto_spiev = {};

        $('.form-spiev').each(function () {

            var formulario = $(this);
            var objeto_form = {};

            $(formulario).find('[data-propiedad]').each(function () {
                //VALOR EN STRING DEL CAMPO...
                var valor_campo = $(this).val();

                //SI ES ENTERO
                if ($(this).data('entero')) valor_campo = parseInt(valor_campo);

                //SI ES DECIMAL
                if ($(this).data('decimal')) valor_campo = parseFloat(valor_campo);

                //SE CREA EL CAMPO CON EL VALOR
                objeto_form[$(this).data('propiedad')] = valor_campo;
            });

            objeto_spiev[formulario.attr('name')] = objeto_form;

        });

        console.log(objeto_spiev);

        GuardarSpiev(objeto_spiev);
    });

    //CUANDO CARGA LA PAGINA SETEA LA CAPACIDAD DE CARGA
    CargaVehiculoXTipo($('#VehiculoForm_ID_TIPO_VEHICULO').val());

    //CUANDO CAMBIA EL TIPO DE VEHICULO SE CAMBIA LA CAPACIDAD DE CARGA SI ES REQUERIDA
    $('#VehiculoForm_ID_TIPO_VEHICULO').change(function () {
        var ID = $(this).val();
        CargaVehiculoXTipo(ID);
    });

    //CUANDO CARGA LA PAGINA SETEA LA CANTIDAD EN COMUNIDAD SEGÚN OPCION
    CantidadComunidadXOpcion($('#ComunidadForm_ID_OPCION_COMUNIDAD').val());

    //CUANDO CAMBIA LA OPCION DE COMUNIDAD CAMBIA LA CANTIDAD ASOCIADA
    $('#ComunidadForm_ID_OPCION_COMUNIDAD').change(function () {
        var ID = $(this).val();
        CantidadComunidadXOpcion(ID);
    });

});

function CargaVehiculoXTipo(ID_TIPO)
{
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/registro/carga-vehiculo-ajax",
        data: { ID_TIPO_VEHICULO: ID_TIPO },
        dataType: "json",
        success: function (result)
        {
            //console.log(result);
            if (result.valor === null)
            {
                $('#VehiculoForm_CARGA').val('');
                $('#VehiculoForm_CARGA').attr('readonly', false);
            }
            else
            {
                $('#VehiculoForm_CARGA').val(result.valor);
                $('#VehiculoForm_CARGA').attr('readonly', true);
            }
        },
        error: function (result)
        {

        }
    });
}

function CantidadComunidadXOpcion(ID_OPCION) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/registro/cantidad-comunidad-ajax",
        data: { ID_OPCION_COMUNIDAD: ID_OPCION },
        dataType: "json",
        success: function (result)
        {
            if (result.valor === null)
            {
                $('#ComunidadForm_CANTIDAD').val('');
                $('#ComunidadForm_CANTIDAD').attr('readonly', false);
            }
            else
            {
                $('#ComunidadForm_CANTIDAD').val(result.valor);
                $('#ComunidadForm_CANTIDAD').attr('readonly', true);
            }
        },
        error: function (result)
        {

        }
    });
}

function GuardarSpiev(datos_objeto)
{
    //BOTON EN CARGANDO...
    LoadButton($('#btn-generar-spiev'), '<i class="fas fa-spinner fa-spin"></i> Cargando');

    //CLASE PARA ALERTA
    var clase_base = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/registro/guardar-spiev-ajax",
        data: JSON.stringify(datos_objeto),
        dataType: "json",
        success: function (result)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            clase_base += result.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HtmlAlerta(clase_base, result.message);

        },
        error: function (result)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            clase_base += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HtmlAlerta(clase_base, "Hay un problema al guardar los datos");
        }
    });
}

function HtmlAlerta(clase, mensaje)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PartialAlert",
        data: JSON.stringify({ Clase: clase, Mensaje: mensaje }),
        dataType: "html",
        success: function (result)
        {
            $('#alerta-spiev').hide().html(result).fadeIn('slow');

            //BOTON NORMAL...
            LoadButton($('#btn-generar-spiev'), 'Generar 1era Inscripción');
        }
    });
}

function LoadButton(button, html)
{
    $(button).html(html);
}