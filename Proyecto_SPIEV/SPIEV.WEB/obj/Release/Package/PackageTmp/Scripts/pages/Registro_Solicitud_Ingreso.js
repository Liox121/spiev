﻿$(document).ready(function () {

    $('#tabla-solicitudes-ingreso').DataTable({
        "ajax": {
            url: "/registro/solicitudes-ingresadas-ajax",
            dataSrc: '',
            type: 'POST'
        },
        "language": { "url": "/Scripts/datatables/Spanish.json" },
        "columns": [
            { "data": "ID_SOLICITUD" },
            { "data": "RUT_ADQUIRIENTE" },
            { "data": "USUARIO.NOMBRE_USUARIO" },
            { "data": "SUCURSAL.NOMBRE_SUCURSAL" },
            {
                "data": "FECHA_SOLICITUD",
                render: function (data, type, row)
                {
                    if(type === "sort" || type === "type")
                    {
                        return data;
                    }
                    return moment(data).format("DD-MM-YYYY");
                }
            },
            {
                "mRender": function (data, type, row) {
                    return DIV_GROUP_OPTIONS(row['ID_SOLICITUD']);
                },
                "searchable": false,
                "orderable": false
            }
        ]
    });
});

function DIV_GROUP_OPTIONS(ID_SOLICITUD)
{
    var DIV_CONTENEDOR = $('<div>');
    var DIV_BTN_GROUP = $('<div>').addClass('btn-group btn-group-sm');

    var BTN_VER = $('<button>').attr({ "type": "button", "onclick": "CLICK_MODAL_DETALLE(" + ID_SOLICITUD + ")" }).addClass('btn btn-outline-primary').html('<i class="fas fa-eye fa-sm"></i>');
    var BTN_EDIT = $('<a>').attr('href', '/solicitud/mis-solicitudes/' + ID_SOLICITUD).addClass('btn btn-outline-primary').html('<i class="fas fa-edit fa-sm"></i>');
    var BTN_DELETE = $('<button>').attr('type', 'button').addClass('btn btn-outline-danger').html('<i class="fas fa-trash fa-sm"></i>');

    $(DIV_BTN_GROUP).append(BTN_VER);
    $(DIV_BTN_GROUP).append(BTN_EDIT);
    $(DIV_BTN_GROUP).append(BTN_DELETE);

    $(DIV_CONTENEDOR).append(DIV_BTN_GROUP);

    return $(DIV_CONTENEDOR).html();

}

function CLICK_MODAL_DETALLE(ID_SOLICITUD)
{
    $('#span-id-solicitud').text(ID_SOLICITUD);

    var URL = $('#inscripcion-solicitud').data('url');

    //SETEAMOS LA RUTA DEL LINK
    $('#inscripcion-solicitud').attr('href', URL + ID_SOLICITUD);

    //SETEAMOS EL ID DEL BOTON PARA CAMBIO DE ESTADO
    $('#btn-modal-cambio-estado').attr("onclick", "CLICK_MODAL_ESTADO(" + ID_SOLICITUD + ")");

    //ABRIMOS EL MODAL
    $('#modal-detalle-solicitud').modal('show');

    $.when(GET_ARCHIVOS_SOLICITUD(ID_SOLICITUD)).then(function (JSON, textStatus, jqXHR)
    {
        console.log(jqXHR.status); // Alerts 200

        //SI EXISTIO ALGÚN ERROR
        if (!JSON.status) {
            return;
        }

        LOAD_ARCHIVOS(JSON.data.ARCHIVOS);
        LOAD_HISTORIA_SOLICITUD(JSON.data.HISTORIA_SOLICITUD);

    });
}

function CLICK_MODAL_ESTADO(ID_SOLICITUD)
{
    //CERRAR EL MODAL ANTERIOR
    $('#modal-detalle-solicitud').modal('hide');

    //DATOS VARIABLES
    $('#cambio-estado-id-solicitud').text(ID_SOLICITUD);

    //OBTENEMOS LOS ESTADOS DE LA SOLICITUD Y EL DETALLE DE LA MISMA 
    $.when(GET_ESTADOS_SOLICITUD(), GET_DETALLE_SOLICITUD(ID_SOLICITUD)).then(function (JSON_ESTADOS, JSON_DETALLE) {

        var ESTADOS = JSON_ESTADOS[0];
        var DETALLE = JSON_DETALLE[0];

        //SI EXISTIO ALGÚN ERROR
        if (!ESTADOS.status || !DETALLE.status) {
            return;
        }

        //OBTENEMOS EL SELECTOR
        var SELECT_ESTADO = $('#select-estados-solicitud');

        //SE LIMPIA EL COMBOBOX
        $(SELECT_ESTADO).empty();

        //SE RECORREN LOS ESTADOS DE LA SOLICITUD
        $.each(ESTADOS.data, function (ID, VALUE) {

            var OPCION = $('<option>').attr('value', VALUE.ID_ESTADO).html(VALUE.NOMBRE_ESTADO);

            $(SELECT_ESTADO).append(OPCION);

        });

        //PONER EL COMBOBOX EN POSICION
        $(SELECT_ESTADO).val(DETALLE.data.ID_ESTADO);

    });

    //ABRIR EL MODAL DE CAMBIO DE ESTADO
    $('#modal-cambio-estado').modal('show');
}

function CLICK_GUARDAR_ESTADO()
{
    var ID_SOLICITUD = parseInt($('#cambio-estado-id-solicitud').text());
    var ID_ESTADO = parseInt($('#select-estados-solicitud').val());
    var COMENTARIO = $('#comentario-cambio-estado').val();

    var NUEVA_HISTORIA =
    {
        ID_SOLICITUD: ID_SOLICITUD,
        ID_ESTADO: ID_ESTADO,
        COMENTARIO: COMENTARIO
    };

    $.when(CAMBIO_ESTADO(NUEVA_HISTORIA)).then(function (JSON_REQ, textStatus, jqXHR) {

        //CLASE PARA ALERTA
        var CLASE = "alert alert-dismissible fade show ";

        //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
        CLASE += JSON_REQ.status ? "alert-success" : "alert-danger";

        //SE INSERTA LA ALERTA EN PANTALLA
        HTML_ALERTA(CLASE, JSON_REQ.message);

    });

    console.log(NUEVA_HISTORIA);

}

function LOAD_ARCHIVOS(JSON_ARCHIVOS)
{
    var T_BODY = $('#tabla-detalle-archivos').find('tbody');
    $(T_BODY).empty();

    $.each(JSON_ARCHIVOS, function (ID, VALUE) {
        var TR = $('<tr>');

        $(TR).append($('<td>').html(VALUE.ID_ARCHIVO));
        $(TR).append($('<td>').html(VALUE.TIPO_ARCHIVO_SOLICITUD.TIPO_ARCHIVO.NOMBRE));

        var LINK = $('<a>').attr('href', '/solicitud/mis-archivos-solicitud/' + VALUE.ID_ARCHIVO).html('Descargar');

        $(TR).append($('<td>').html(LINK));

        $(T_BODY).append(TR);

    });

}

function LOAD_HISTORIA_SOLICITUD(JSON_HISTORIA)
{
    var T_BODY = $('#tabla-historia-solicitud').find('tbody');
    $(T_BODY).empty();

    $.each(JSON_HISTORIA, function (ID, VALUE) {
        var TR = $('<tr>');

        $(TR).append($('<td>').html(VALUE.ID_HISTORIA));
        $(TR).append($('<td>').html(VALUE.USUARIO.NOMBRE_USUARIO));
        $(TR).append($('<td>').html(VALUE.ESTADO.NOMBRE_ESTADO));
        $(TR).append($('<td>').html(VALUE.COMENTARIO));
        $(TR).append($('<td>').html(VALUE.FECHA));

        $(T_BODY).append(TR);
    });

    //SI NO EXISTE HISTORIA ALGUNA
    if (JSON_HISTORIA.length === 0) {
        var TR = $('<tr>');
        $(TR).append($('<td>').attr('colspan', 4).html('Sin historia para mostrar'));
        $(T_BODY).append(TR);
    }

}


function GET_ARCHIVOS_SOLICITUD(ID_SOLICITUD)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/mis-detalle-ajax",
        data: JSON.stringify({ ID_SOLICITUD: parseInt(ID_SOLICITUD) }),
        dataType: "json"
    });
}

function GET_ESTADOS_SOLICITUD()
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/estados-solicitud-ajax",
        dataType: "json"
    });
}

function GET_DETALLE_SOLICITUD(ID_SOLICITUD)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/detalle-solicitud-ajax",
        data: JSON.stringify({ ID_SOLICITUD: parseInt(ID_SOLICITUD) }),
        dataType: "json"
    });
}

function CAMBIO_ESTADO(OBJETO)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/cambio-estado-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json"
    });
}

function HTML_ALERTA(CLASS, MENSAJE)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PartialAlert",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENSAJE }),
        dataType: "html",
        success: function (result)
        {
            $('#alerta-spiev').hide().html(result).fadeIn('slow');
        }
    });
}