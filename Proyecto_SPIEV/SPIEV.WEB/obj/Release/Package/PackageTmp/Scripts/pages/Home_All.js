﻿$(function () {

    $("#logout-link").click(function (e) {
        e.preventDefault();
        $.post("/logout", function (res) {
            if (res.status === "done") {
                //close the window now.
                window.location = "/";
            }
        });
    });

});