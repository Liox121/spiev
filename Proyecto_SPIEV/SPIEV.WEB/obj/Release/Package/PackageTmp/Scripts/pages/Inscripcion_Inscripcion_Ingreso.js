﻿$(document).ready(function () {

    $('#tabla-inscripciones-ingreso').DataTable({
        "ajax": {
            url: "/inscripcion/inscripciones-ingresadas-ajax",
            dataSrc: '',
            type: 'POST'
        },
        "language": { "url": "/Scripts/datatables/Spanish.json" },
        "columns": [
            { "data": "ID_PRIMERA_INSCRIPCION" },
            { "data": "ADQUIRIENTE.RUN" },
            { "data": "USUARIO.NOMBRE_USUARIO" },
            {
                "data": "FECHA",
                render: function (data, type, row)
                {
                    if(type === "sort" || type === "type")
                    {
                        return data;
                    }
                    return moment(data).format("DD-MM-YYYY");
                }
            },
            {
                "mRender": function (data, type, row)
                {
                    return DIV_GROUP_OPTIONS(row['ID_PRIMERA_INSCRIPCION']);
                },
                "searchable": false,
                "orderable": false
            }
        ]
    });
});

function DIV_GROUP_OPTIONS(ID_PRIMERA_INSCRIPCION)
{
    var DIV = $('<div>');
    var BTN_GROUP = $('<div>').addClass('btn-group btn-group-sm');

    var BTN_VER = $('<button>').attr({ "type": "button", "onclick": "CLICK_MODAL_DETALLE(" + ID_PRIMERA_INSCRIPCION + ")" }).addClass('btn btn-outline-primary btn-sm').html('<i class="fas fa-eye"></i>');
    //var BTN_BORRAR = $('<button>').attr('type', 'button').addClass('btn btn-outline-danger').html('<i class="fas fa-trash"></i>');

    $(BTN_GROUP).append(BTN_VER);
    //$(BTN_GROUP).append(BTN_BORRAR);

    $(DIV).append(BTN_GROUP);

    return $(DIV).html();

}

function CLICK_MODAL_DETALLE(ID_PRIMERA_INSCRIPCION)
{
    $('#span-id-inscripcion').text(ID_PRIMERA_INSCRIPCION);

    var URL = $('#inscripcion-documentos').data('url');

    //SETEAMOS LA RUTA DEL LINK
    $('#inscripcion-documentos').attr('href', URL + ID_PRIMERA_INSCRIPCION);

    //ABRIMOS EL MODAL
    $('#modal-detalle-solicitud').modal('show');
}

function CLICK_MODAL_ESTADO(ID_SOLICITUD)
{
    //CERRAR EL MODAL ANTERIOR
    $('#modal-detalle-solicitud').modal('hide');

    //DATOS VARIABLES
    $('#cambio-estado-id-solicitud').text(ID_SOLICITUD);

    //OBTENEMOS LOS ESTADOS DE LA SOLICITUD Y EL DETALLE DE LA MISMA 
    $.when(GET_ESTADOS_SOLICITUD(), GET_DETALLE_SOLICITUD(ID_SOLICITUD)).then(function (JSON_ESTADOS, JSON_DETALLE) {

        var ESTADOS = JSON_ESTADOS[0];
        var DETALLE = JSON_DETALLE[0];

        //SI EXISTIO ALGÚN ERROR
        if (!ESTADOS.status || !DETALLE.status) {
            return;
        }

        //OBTENEMOS EL SELECTOR
        var SELECT_CAMBIO = $('#select-estados-solicitud');

        //SE LIMPIA EL COMBOBOX
        $(SELECT_CAMBIO).empty();

        //SE RECORREN LOS ESTADOS DE LA SOLICITUD
        $.each(ESTADOS.data, function (ID, VALUE) {

            var OPCION = $('<option>').attr('value', ID.ID_ESTADO).html(VALUE.NOMBRE_ESTADO);

            $(SELECT_CAMBIO).append(OPCION);

        });

        //PONER EL COMBOBOX EN POSICION
        $(SELECT_CAMBIO).val(DETALLE.data.ID_ESTADO);

    });

    //ABRIR EL MODAL DE CAMBIO DE ESTADO
    $('#modal-cambio-estado').modal('show');
}

function CLICK_CAMBIO_ESTADO()
{
    var ID_SOLICITUD = parseInt($('#cambio-estado-id-solicitud').text());

    var ID_ESTADO = parseInt($('#select-estados-solicitud').val());

    var COMENTARIO = $('#comentario-cambio-estado').val();

    var NUEVA_HISTORIA = {
        ID_SOLICITUD: ID_SOLICITUD,
        ID_ESTADO: ID_ESTADO,
        COMENTARIO: COMENTARIO
    };

    $.when(GUARDAR_CAMBIO_ESTADO(NUEVA_HISTORIA)).then(function (JSON_RES, textStatus, jqXHR) {

        //CLASE PARA ALERTA
        var CLASE = "alert alert-dismissible fade show ";

        //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
        CLASE += JSON_RES.status ? "alert-success" : "alert-danger";

        //SE INSERTA LA ALERTA EN PANTALLA
        HTML_ALERTA(CLASE, JSON_RES.message);
    });

    console.log(NUEVA_HISTORIA);

}

function GET_ESTADOS_SOLICITUD()
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/estados-solicitud-ajax",
        dataType: "json"
    });
}

function HTML_ALERTA(CLASS, MENS)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PartialAlert",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');
        }
    });
}