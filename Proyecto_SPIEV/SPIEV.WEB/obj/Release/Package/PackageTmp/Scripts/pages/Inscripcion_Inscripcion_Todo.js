﻿$(document).ready(function () {

   $('#tabla-inscripciones').DataTable({
        dom: 'Bfrtip',
        buttons: [
            { extend: 'selectAll', className: 'btn-sm', text: 'Seleccionar Todo' },
            { extend: 'selectNone', className: 'btn-sm', text: 'Desmarcar Todo' }
        ],
        ajax : {
            url: "/inscripcion/inscripciones-todas-ajax",
            dataSrc: '',
            type: 'POST'
        },
        language : { "url": "/Scripts/datatables/Spanish.json" },
        columns : [
            { "defaultContent": "" },
            { "data": "ID_PRIMERA_INSCRIPCION" },
            { "data": "ADQUIRIENTE.RUN" },
            { "data": "USUARIO.NOMBRE_USUARIO" },
            { "data": "ESTADO.NOMBRE_ESTADO" },
            {
                "data": "FECHA",
                render: function (data, type, row)
                {
                    if(type === "sort" || type === "type")
                    {
                        return data;
                    }
                    return moment(data).format("DD-MM-YYYY");
                }
            },
            {
                "mRender": function (data, type, row)
                {
                    return DIV_GROUP_OPTIONS(row['ID_PRIMERA_INSCRIPCION']);
                },
                "searchable": false,
                "orderable": false
            }
        ],
        columnDefs: [{ orderable: false, className: 'select-checkbox', targets: 0 }],
        select: { style: 'multi', selector: 'td:first-child' },
        order: [[1, 'desc']]
    });

});

function CLICK_EXCEL_SELECCION(BTN)
{
    //ESTADO CARGANDO EN EL BOTON
    $(BTN).attr('disabled', true);
    $(BTN).addClass('disabled');
    $(BTN).html("<i class='fas fa-spinner fa-spin'></i> Cargando");

    var DATATABLE = $('#tabla-inscripciones').DataTable();

    var SELECCIONADOS = DATATABLE.rows({ selected: true }).data().toArray();

    var ARRAY_ID = SELECCIONADOS.map(function (x) {
        return x.ID_PRIMERA_INSCRIPCION;
    });

    //OBJETO A JSON "STRING"
    var JSON_STRING = JSON.stringify(ARRAY_ID);

    //EXPORTAR A EXCEL
    $.when(GENERAR_EXCEL_INSCRIPCION(JSON_STRING)).then(function (JSON) {

        //EN CASO DE ERROR...
        if (!JSON.status) {
            return;
        }

        //DESCARGAR EL ARCHIVO...
        window.location = '/inscripcion/DescargarExcelInscripcion?FILE_GUID=' + JSON.data.FILE_GUID + '&FILE_NAME=' + JSON.data.FILE_NAME;

        //ESTADO NORMAL EN EL BOTON
        $(BTN).removeAttr('disabled');
        $(BTN).removeClass('disabled');
        $(BTN).html("<i class='far fa-file-excel'></i> Exportar Excel");
    });

}

function DIV_GROUP_OPTIONS(ID_PRIMERA_INSCRIPCION)
{
    var DIV = $('<div>');
    var DIV_BTN_GROUP = $('<div>').addClass('btn-group btn-group-sm');

    var BTN_VER = $('<button>').attr({ "type": "button", "onclick": "CLICK_MODAL_DETALLE(" + ID_PRIMERA_INSCRIPCION + ")" } ).addClass('btn btn-outline-primary btn-sm').html('<i class="fas fa-eye"></i>');

    $(DIV_BTN_GROUP).append(BTN_VER);
    $(DIV).append(DIV_BTN_GROUP);

    return $(DIV).html();

}

function CLICK_MODAL_DETALLE(ID_PRIMERA_INSCRIPCION)
{
    //EL ID DE PRIMERA INSCRIPCIÓN
    $('#span-id-inscripcion').text(ID_PRIMERA_INSCRIPCION);

    //SETEAMOS LA RUTA DEL LINK DE CARGA DE DOC. FUNDANTE
    var URL_CARGA_DOC = $('#inscripcion-documentos').data('url');
    $('#inscripcion-documentos').attr('href', URL_CARGA_DOC + ID_PRIMERA_INSCRIPCION);

     //SETEAMOS LA RUTA DEL LINK DE SOLICITUD DE LIMITACION
    var URL_LIMITACION = $('#inscripcion-limitacion').data('url');
    $('#inscripcion-limitacion').attr('href', URL_LIMITACION + ID_PRIMERA_INSCRIPCION);

    //ABRIMOS EL MODAL
    $('#modal-detalle-solicitud').modal('show');

    //SETEA LOS VALORES DEL DETALLE DE INSCRIPCION A CARGANDO
    $('.loading-table-ajax > td').each(function (i, element) {
        $(this).html("Cargando...");
    });

    //CARGAR LOS DETALLES...
    LOAD_DETALLE_INSCRIPCION(ID_PRIMERA_INSCRIPCION);
}

function LOAD_DETALLE_INSCRIPCION(ID_INSCRIPCION)
{
    //OBTENEMOS LOS ESTADOS DE LA SOLICITUD Y EL DETALLE DE LA MISMA 
    $.when(GET_DETALLE_INSCRIPCION(ID_INSCRIPCION)).then(function (JSON_DETALLE) {

        //EN CASO DE ERROR EN LA CONSULTA...
        if (!JSON_DETALLE.status) {
            return;
        }

        var JSON_INS = JSON_DETALLE.data;

        //CARGAR LOS DATOS...
        LOAD_VEHICULO(JSON_INS.VEHICULO);
        LOAD_COMUNIDAD(JSON_INS.COMUNIDAD);
        LOAD_ADQUIRIENTE(JSON_INS.ADQUIRIENTE);
        LOAD_SOLICITANTE(JSON_INS.SOLICITANTE);
        LOAD_ESTIPULANTE(JSON_INS.ESTIPULANTE);
        LOAD_IMPUESTO(JSON_INS.IMPUESTO);
        LOAD_FACTURA(JSON_INS.FACTURA);
        LOAD_OBSERVACION(JSON_INS.OBSERVACION);
        LOAD_OPERADOR(JSON_INS.OPERADOR);
    });
}

function LOAD_VEHICULO(OBJ_VEHICULO)
{
    $('#id-vehiculo').html(OBJ_VEHICULO.ID_VEHICULO);
    $('#codigo-informe-tecnico-vehiculo').html(OBJ_VEHICULO.CODIGO_INFORME_TECNICO);
    $('#num-asientos-vehiculo').html(OBJ_VEHICULO.NUMERO_ASIENTOS);
    $('#anho-fabricacion-vehiculo').html(OBJ_VEHICULO.AGNO_FABRICACION);
    $('#tipo-combustible-vehiculo').html(OBJ_VEHICULO.TIPO_COMBUSTIBLE.NOMBRE_COMBUSTIBLE);
    $('#marca-vehiculo').html(OBJ_VEHICULO.MARCA);
    $('#modelo-vehiculo').html(OBJ_VEHICULO.MODELO);
    $('#num-chasis-vehiculo').html(OBJ_VEHICULO.NUMERO_CHASIS);
    $('#num-motor-vehiculo').html(OBJ_VEHICULO.NUMERO_MOTOR);
    $('#num-serie-vehiculo').html(OBJ_VEHICULO.NUMERO_SERIE);
    $('#num-vin-vehiculo').html(OBJ_VEHICULO.NUMERO_VIN);
    $('#pbv-vehiculo').html(OBJ_VEHICULO.PESO_BRUTO_VEHICULAR);
    $('#num-puertas-vehiculo').html(OBJ_VEHICULO.NUMERO_PUERTAS);
    $('#tipo-vehiculo').html(OBJ_VEHICULO.TIPO_VEHICULO.NOMBRE_TIPO);
    $('#tipo-carga-vehiculo').html(OBJ_VEHICULO.TIPO_CARGA.TIPO_CARGA_DESCRIPCION);
    $('#tipo-pbv-vehiculo').html(OBJ_VEHICULO.TIPO_PBV.TIPO_CARGA_DESCRIPCION);
}

function LOAD_COMUNIDAD(OBJ_COMUNIDAD)
{
    $('#id-comunidad').html(OBJ_COMUNIDAD.ID_COMUNIDAD);
    $('#opcion-comunidad').html(OBJ_COMUNIDAD.OPCION_COMUNIDAD.OPCION);
    $('#cantidad-comunidad').html(OBJ_COMUNIDAD.CANTIDAD);
}

function LOAD_ADQUIRIENTE(OBJ_ADQUIRIENTE)
{
    $('#id-adquiriente').html(OBJ_ADQUIRIENTE.ID_ADQUIRIENTE);
    $('#ape-paterno-adquiriente').html(OBJ_ADQUIRIENTE.APELLIDO_PATERNO);
    $('#ape-materno-adquiriente').html(OBJ_ADQUIRIENTE.APELLIDO_MATERNO);
    $('#razon-social-adquiriente').html(OBJ_ADQUIRIENTE.RAZON_SOCIAL);
    $('#run-adquiriente').html(OBJ_ADQUIRIENTE.RUN);
    $('#calle-adquiriente').html(OBJ_ADQUIRIENTE.CALLE);
    $('#num-domicilio-adquiriente').html(OBJ_ADQUIRIENTE.NUMERO_DOMICILIO);
    $('#letra-domicilio-adquiriente').html(OBJ_ADQUIRIENTE.LETRA_DOMICILIO);
    $('#resto-domicilio-adquiriente').html(OBJ_ADQUIRIENTE.RESTO_DOMICILIO);
    $('#telefono-adquiriente').html(OBJ_ADQUIRIENTE.TELEFONO);
    $('#cod-postal-adquiriente').html(OBJ_ADQUIRIENTE.CODIGO_POSTAL);
    $('#correo-adquiriente').html(OBJ_ADQUIRIENTE.CORREO_ELECTRONICO);
    $('#comuna-adquiriente').html(OBJ_ADQUIRIENTE.COMUNA.NOMBRE);
    $('#calidad-adquiriente').html(OBJ_ADQUIRIENTE.CALIDAD_PERSONA.TIPO_PERSONA_DESCRIPCION);
}

function LOAD_SOLICITANTE(OBJ_SOLICITANTE)
{
    $('#id-solicitante').html(OBJ_SOLICITANTE.ID_SOLICITANTE);
    $('#ape-paterno-solicitante').html(OBJ_SOLICITANTE.APELLIDO_PATERNO);
    $('#ape-materno-solicitante').html(OBJ_SOLICITANTE.APELLIDO_MATERNO);
    $('#razon-social-solicitante').html(OBJ_SOLICITANTE.RAZON_SOCIAL);
    $('#run-solicitante').html(OBJ_SOLICITANTE.RUN);
    $('#calle-solicitante').html(OBJ_SOLICITANTE.CALLE);
    $('#num-domicilio-solicitante').html(OBJ_SOLICITANTE.NUMERO_DOMICILIO);
    $('#letra-domicilio-solicitante').html(OBJ_SOLICITANTE.LETRA_DOMICILIO);
    $('#resto-domicilio-solicitante').html(OBJ_SOLICITANTE.RESTO_DOMICILIO);
    $('#telefono-solicitante').html(OBJ_SOLICITANTE.TELEFONO);
    $('#cod-postal-solicitante').html(OBJ_SOLICITANTE.CODIGO_POSTAL);
    $('#correo-solicitante').html(OBJ_SOLICITANTE.CORREO_ELECTRONICO);
    $('#comuna-solicitante').html(OBJ_SOLICITANTE.COMUNA.NOMBRE);
}

function LOAD_ESTIPULANTE(OBJ_ESTIPULANTE) {
    $('#id-estipulante').html(OBJ_ESTIPULANTE.ID_ESTIPULANTE);
    $('#ape-paterno-estipulante').html(OBJ_ESTIPULANTE.APELLIDO_PATERNO);
    $('#ape-materno-estipulante').html(OBJ_ESTIPULANTE.APELLIDO_MATERNO);
    $('#razon-social-estipulante').html(OBJ_ESTIPULANTE.RAZON_SOCIAL);
    $('#run-estipulante').html(OBJ_ESTIPULANTE.RUN);
    $('#calle-estipulante').html(OBJ_ESTIPULANTE.CALLE);
    $('#num-domicilio-estipulante').html(OBJ_ESTIPULANTE.NUMERO_DOMICILIO);
    $('#letra-domicilio-estipulante').html(OBJ_ESTIPULANTE.LETRA_DOMICILIO);
    $('#resto-domicilio-estipulante').html(OBJ_ESTIPULANTE.RESTO_DOMICILIO);
    $('#telefono-estipulante').html(OBJ_ESTIPULANTE.TELEFONO);
    $('#cod-postal-estipulante').html(OBJ_ESTIPULANTE.CODIGO_POSTAL);
    $('#correo-estipulante').html(OBJ_ESTIPULANTE.CORREO_ELECTRONICO);
    $('#comuna-estipulante').html(OBJ_ESTIPULANTE.COMUNA.NOMBRE);
    $('#calidad-estipulante').html(OBJ_ESTIPULANTE.CALIDAD_PERSONA.TIPO_PERSONA_DESCRIPCION);
}

function LOAD_IMPUESTO(OBJ_IMPUESTO) {
    $('#id-impuesto').html(OBJ_IMPUESTO.ID_IMPUESTO_ADICIONAL);
    $('#cod-ide-impuesto').html(OBJ_IMPUESTO.CODIGO_IDENTIFICACION);
    $('#cod-inf-tecnico').html(OBJ_IMPUESTO.CODIGO_INFORME_TECNICO);
    $('#monto-impuesto').html(OBJ_IMPUESTO.MONTO_IMPUESTO);
    $('#total-factura-impuesto').html(OBJ_IMPUESTO.TOTAL_FACTURA);
}

function LOAD_FACTURA(OBJ_FACTURA) {
    $('#id-factura').html(OBJ_FACTURA.ID_FACTURA);
    $('#num-factura').html(OBJ_FACTURA.NUMERO_FACTURA);
    $('#fecha-factura').html(OBJ_FACTURA.FECHA);
    $('#comuna-factura').html(OBJ_FACTURA.COMUNA.NOMBRE);
    $('#rut-emisor-factura').html(OBJ_FACTURA.RUT_EMISOR);
    $('#nombre-emisor-factura').html(OBJ_FACTURA.NOMBRE_EMISOR);
    $('#total-factura').html(OBJ_FACTURA.MONTO_TOTAL_FACTURA);
    $('#tipo-moneda-factura').html(OBJ_FACTURA.TIPO_MONEDA.TIPO_MONEDA_DESCRIPCION);
}

function LOAD_OBSERVACION(OBJ_OBSERVACION){
    $('#id-observacion').html(OBJ_OBSERVACION.ID_OBSERVACION);
    $('#observacion').html(OBJ_OBSERVACION.OBSERVACION);
}

function LOAD_OPERADOR(OBJ_OPERADOR) {
    $('#id-operador').html(OBJ_OPERADOR.ID_OPERADOR);
    $('#region-operador').html(OBJ_OPERADOR.REGION.NOMBRE);
    $('#run-usuario-operador').html(OBJ_OPERADOR.RUN_USUARIO);
    $('#run-empresa-operador').html(OBJ_OPERADOR.RUN_EMPRESA);
}

function GET_DETALLE_INSCRIPCION(ID_INSCRIPCION) {
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/detalle-inscripcion-ajax",
        data: JSON.stringify({ ID_INSCRIPCION: parseInt(ID_INSCRIPCION) }),
        dataType: "json"
    });
}

function GENERAR_EXCEL_INSCRIPCION(ARRAY_ID)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/inscripcion/excel-inscripciones",
        data: JSON.stringify({ JSON_PARAMETRO: ARRAY_ID }),
        dataType: "json"
    });
}