﻿$(document).ready(function ()
{
    //SI NO EXISTE LA TABLA
    if (!$('#tabla-empresas').length) return;

    $('#tabla-empresas').DataTable({
        "ajax": {
            url: "/administrador/empresas-ajax",
            dataSrc: ''
        },
        "language": { "url": "/Scripts/datatables/Spanish.json" },
        "columns": [
            { "data": "ID_EMPRESA" },
            { "data": "NOMBRE_EMPRESA" },
            {
                "mRender": function (data, type, row)
                {
                    return "<a href='/administrador/empresas/" + row['ID_EMPRESA'] + "' class='btn btn-outline-primary btn-sm'><i class='fas fa-edit fa-sm fa-sm'></i></a>";
                },
                "searchable": false,
                "orderable": false
            }
        ]
    });
});

function CLICK_CREATE()
{
    var OBJETO = {};

    $("#datos-empresa").find('[data-propiedad]').each(function () {
        //VALOR EN STRING DEL CAMPO...
        var VALOR_CAMPO = $(this).val();

        //SI ES ENTERO
        if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

        //SI ES DECIMAL
        if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

        //SI ES UN RADIO BUTTON
        if ($(this).data('radio')) VALOR_CAMPO = $("#" + $(this).attr('id')).is(':checked') ? true : false;

        //SI ES UN CHECKBOX
        if ($(this).data('checkbox')) {
            VALOR_CAMPO = Array();
            $("input[name='" + $(this).attr("name") + "']:checked").each(function () {
                VALOR_CAMPO.push(parseInt(this.value));
            });
        }

        //SE CREA EL CAMPO CON EL VALOR
        OBJETO[$(this).data('propiedad')] = VALOR_CAMPO;

    });

    CREAR_EMPRESA(OBJETO, $('#btn-crear-empresa'));
}

function CREAR_EMPRESA(OBJETO, BTN)
{
    //BOTON EN CARGANDO...
    $(BTN).html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //TEXT EN BOTON
    var TEXT_BTN = "Crear empresa";

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/administrador/empresas/crear-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message, BTN, TEXT_BTN);
        },
        error: function (result)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos", BTN, TEXT_BTN);
        }
    });
}

function CLICK_EDIT()
{
    var OBJETO = {};

    $("#datos-empresa").find('[data-propiedad]').each(function () {
        //VALOR EN STRING DEL CAMPO...
        var VALOR_CAMPO = $(this).val();

        //SI ES ENTERO
        if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

        //SI ES DECIMAL
        if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

        //SI ES UN RADIO BUTTON
        if ($(this).data('radio')) VALOR_CAMPO = $("#" + $(this).attr('id')).is(':checked') ? true : false;

        //SI ES UN CHECKBOX
        if ($(this).data('checkbox'))
        {
            VALOR_CAMPO = Array();
            $("input[name='" + $(this).attr("name") + "']:checked").each(function () {
                VALOR_CAMPO.push(parseInt(this.value));
            });
        }

        //SE CREA EL CAMPO CON EL VALOR
        OBJETO[$(this).data('propiedad')] = VALOR_CAMPO;

    });

    EDITAR_EMPRESA(OBJETO, $('#btn-editar-empresa'));
}

function EDITAR_EMPRESA(OBJETO, BTN)
{
    //BOTON EN CARGANDO...
    $(BTN).html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //TEXT EN BOTON
    var TEXT_BTN = "Guardar empresa";

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/administrador/empresas/editar-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message, BTN, TEXT_BTN);

        },
        error: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos", BTN, TEXT_BTN);
        }
    });
}

function HTML_ALERTA(CLASS, MENS, BTN, TEXT_BTN)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PartialAlert",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (result)
        {
            $('#alerta-spiev').hide().html(result).fadeIn('slow');

            $(BTN).html(TEXT_BTN);
        }
    });
}





