﻿using System.Web;
using System.Web.Optimization;

namespace SPIEV.Web
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //SCRIPTS Y CSS PARA DASHBOARD
            bundles.Add(new StyleBundle("~/fonts/fontawesome").Include(
                        "~/fonts/fontawesome-free/css/all.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
                        "~/fonts/bootstrap/bootstrap.bundle.js",
                        "~/Scripts/bootstrap/bootstrap.bundle.js",
                        "~/Scripts/jquery-easing/jquery.easing.min.js",
                        "~/Scripts/dashboard/sb-admin-2.min.js",
                        "~/Scripts/pages/Home_All.js"));

            bundles.Add(new StyleBundle("~/Content/dashboard").Include(
                      "~/Content/sb-admin-2.css"));
        }
    }
}
