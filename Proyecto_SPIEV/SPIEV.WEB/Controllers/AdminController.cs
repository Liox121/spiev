﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.WEB.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;

namespace SPIEV.WEB.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("administrador")]
    public class AdminController : Controller
    {
        // GET: Admin
        [Route]
        [PERMISO_ACCION]
        public ActionResult INDEX()
        {
            return View();
        }

        [Route("usuarios/nuevo")]
        [PERMISO_ACCION]
        public ActionResult NUEVO_USUARIO()
        {
            //MODELITO PARA LA VISTA
            USUARIO_VM MODEL = new USUARIO_VM()
            {
                PERFILES = PERFIL_R.TODOS().ToList(),
                SUCURSALES = SUCURSAL_R.TODOS_WEB().ToList(),
            };

            return View(MODEL);
        }

        [HttpPost]
        [Route("usuarios/nuevo-ajax")]
        [PERMISO_ACCION]
        public string NUEVO_USUARIO_AJAX(USUARIO_VM MODEL)
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO (EN CASO DE EXCEPCION)
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SI NO SE ENVIAN CLAVES DE USUARIO
                    if (!MODEL.CLAVES_NO_NULAS()) ModelState.AddModelError("", "(*) Debe ingresar dos veces la clave");

                    //SI LAS CLAVES NO COINCIDEN
                    if (!MODEL.CLAVES_IGUALES()) ModelState.AddModelError("", "(*) Las claves deben coincidir");

                    //QUE EL NOMBRE DE USUARIO NO SE REPITA
                    if (USUARIO_R.TODOS().Any(X => X.NOMBRE_USUARIO == MODEL.NOMBRE_USUARIO))
                        ModelState.AddModelError("", "(*) El nombre de usuario ya esta siendo utilizado");

                     //SI EL MODELO NO ES VALIDO DEVUELVE LA LISTA DE ERRORES
                     if (!ModelState.IsValid) throw new Exception(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).First());

                     //CREAR EL USUARIO
                     USUARIO USUARIO_CREADO = USUARIO_R.CREATE(new USUARIO
                     {
                        CLAVE = MODEL.CLAVE,
                        CORREO_ELECTRONICO = MODEL.CORREO_ELECTRONICO,
                        ACTIVO = MODEL.ACTIVO,
                        NOMBRE_USUARIO = MODEL.NOMBRE_USUARIO,
                        NOMBRE_COMPLETO = MODEL.NOMBRE_COMPLETO
                     });

                     //CREAR LAS RELACIONES A PERFILES
                     foreach (PERFIL PERFIL in MODEL.PERFILES)
                     {
                        bool ES_POR_DEFECTO = (PERFIL.ID_PERFIL == MODEL.PERFIL_POR_DEFECTO) ? true : false;
                        USUARIO_PERFIL_R.CREATE(new USUARIO_PERFIL { ID_USUARIO = USUARIO_CREADO.ID_USUARIO, ID_PERFIL = PERFIL.ID_PERFIL, POR_DEFECTO = ES_POR_DEFECTO });
                     }

                    //SI SE ASOCIAN SUCURSALES... SE CREAN LAS RELACIONES
                     if (MODEL.SUCURSALES != null)
                     {
                        foreach (SUCURSAL SUCURSAL in MODEL.SUCURSALES)
                        {
                            SUCURSAL_USUARIO_R.CREATE(new SUCURSAL_USUARIO { ID_USUARIO = USUARIO_CREADO.ID_USUARIO, ID_SUCURSAL = SUCURSAL.ID_SUCURSAL });
                        }
                     }

                    //TERMINA LA EJECUCIÓN
                    scope.Complete();

                    //SI TODO SALIO CORRECTO SE ENVIA OK
                    SALIDA.status = true;
                    SALIDA.message = "Datos guardados exitosamente!";

                }
                catch (Exception ex)
                {
                    //SE ENVIA EL ERROR DE LA APLICACION
                    SALIDA.message = ex.Message;
                }
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

        [Route("usuarios")]
        public ActionResult TODO_USUARIOS()
        {
            return View();
        }

        [Route("usuarios/{ID_USUARIO:long}")]
        public ActionResult EDITAR_USUARIO(long ID_USUARIO)
        {
            USUARIO_VM MODEL = new USUARIO_VM();

            //DTOS DEL USUARIO SEGÚN ID
            MODEL.USUARIO_VM_DESDE_USUARIO(USUARIO_R.READ_WEB(ID_USUARIO));

            //PERFILES X USUARIO
            MODEL.MIS_PERFILES = USUARIO_PERFIL_R.TODOS_X_USUARIO(ID_USUARIO).Select(X => X.PERFIL).ToList();

            //SUCURSALES X USUARIO
            MODEL.MIS_SUCURSALES = SUCURSAL_USUARIO_R.TODOS_X_USUARIO(ID_USUARIO).Select(X => X.SUCURSAL).ToList();

            //TODOS LOS PERFILES Y SUCURSALES
            MODEL.PERFILES = PERFIL_R.TODOS().ToList();
            MODEL.SUCURSALES = SUCURSAL_R.TODOS().ToList();

            return View(MODEL);
        }

        [HttpGet]
        [Route("usuarios-ajax")]
        public ActionResult LISTA_USUARIOS_AJAX()
        {
            return Json(USUARIO_R.TODOS(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("usuarios/editar-ajax")]
        public string EDITAR_USUARIO_AJAX(USUARIO_VM MODEL)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                 try
                 {
                     //SI NO HAY PERFILES
                     if (MODEL.PERFILES == null || MODEL.PERFILES.Any() == false)
                        throw new Exception("Debe seleccionar al menos un perfil");

                     //SI SE SELECCIONA POR DEFECTO, UN PERFIL NO ELEGIDO
                     if (!MODEL.PERFILES.Select(X => X.ID_PERFIL).Contains(MODEL.PERFIL_POR_DEFECTO))
                        throw new Exception("Debe seleccionar el perfil por defecto");

                     //SI SE ENVIAN NUEVAS CLAVES DE USUARIO
                     if (MODEL.CLAVE != null)
                     {
                        //SI LAS CLAVES NO COINCIDEN
                        if (MODEL.CLAVE != MODEL.REPERTIR_CLAVE) throw new Exception("Las claves del usuario no coinciden");
                     }

                     //ACTUALIZAR EL USUARIO
                     USUARIO_R.UPDATE(new USUARIO
                     {
                         ID_USUARIO = MODEL.ID_USUARIO,
                         CLAVE = MODEL.CLAVE,
                         CORREO_ELECTRONICO = MODEL.CORREO_ELECTRONICO,
                         ACTIVO = MODEL.ACTIVO,
                         NOMBRE_USUARIO = MODEL.NOMBRE_USUARIO,
                         NOMBRE_COMPLETO = MODEL.NOMBRE_COMPLETO
                     });

                     //BORRAR LAS RELACIONES CON LOS PERFILES
                     USUARIO_PERFIL_R.DELETE_X_USUARIO(MODEL.ID_USUARIO);

                     //CREAR LAS RELACIONES A PERFILES
                     foreach (PERFIL PERFIL in MODEL.PERFILES)
                     {
                        bool ES_POR_DEFECTO = (PERFIL.ID_PERFIL == MODEL.PERFIL_POR_DEFECTO) ? true : false;
                        USUARIO_PERFIL_R.CREATE(new USUARIO_PERFIL { ID_USUARIO = MODEL.ID_USUARIO, ID_PERFIL = PERFIL.ID_PERFIL, POR_DEFECTO = ES_POR_DEFECTO });
                     }

                     //BORRAR LAS RELACIONES CON LAS SUCURSALES
                     SUCURSAL_USUARIO_R.DELETE_X_USUARIO(MODEL.ID_USUARIO);

                     //SI SE ASOCIAN SUCURSALES
                     if (MODEL.SUCURSALES != null)
                     {
                         //CREAR LAS RELACIONES A SUCURSALES
                         foreach (SUCURSAL SUCURSAL in MODEL.SUCURSALES)
                         {
                            SUCURSAL_USUARIO_R.CREATE(new SUCURSAL_USUARIO { ID_USUARIO = MODEL.ID_USUARIO, ID_SUCURSAL = SUCURSAL.ID_SUCURSAL });
                         }
                     }

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    //SI TODO SALIO CORRECTO SE ENVIA OK
                    salida.status = true;
                    salida.message = "Datos guardados exitosamente!";

                 }
                 catch (Exception ex)
                 {
                     //SE ENVIA EL ERROR DE LA APLICACION
                     salida.message = ex.Message;
                 }
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("empresas")]
        public ActionResult TODO_EMPRESAS()
        {
            return View();
        }

        [Route("empresas/nueva")]
        public ActionResult NUEVA_EMPRESA()
        {
            return View();
        }

        [HttpPost]
        [Route("empresas/crear-ajax")]
        public string NUEVA_EMPRESA_AJAX(EMPRESA_VM MODEL)
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            try
            {
                //SI EL MODELO NO ES VALIDO DEVUELVE LA LISTA DE ERRORES
                if (!ModelState.IsValid) throw new Exception(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).First());

                //ACTUALIZAR EL USUARIO
                EMPRESA_R.CREATE(MODEL);

                //SI TODO SALIO CORRECTO SE ENVIA OK
                SALIDA.status = true;
                SALIDA.message = "Datos guardados exitosamente!";
            }
            catch (Exception ex)
            {
                //SE ENVIA EL ERROR DE LA APLICACION
                SALIDA.message = ex.Message;
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

        [Route("empresas/{ID_EMPRESA:long}")]
        public ActionResult EDITAR_EMPRESA(long ID_EMPRESA)
        {
            //DTOS DE LA EMPRESA SEGÚN ID
            EMPRESA_VM MODEL = new EMPRESA_VM();
            MODEL.EMPRESA_VM_DESDE_EMPRESA(EMPRESA_R.READ(ID_EMPRESA));

            return View(MODEL);
        }

        [HttpGet]
        [Route("empresas-ajax")]
        public ActionResult LISTA_EMPRESAS_AJAX()
        {
            return Json(EMPRESA_R.TODOS(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("empresas/editar-ajax")]
        public string EDITAR_EMPRESA_AJAX(EMPRESA_VM MODEL)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SI EL MODELO NO ES VALIDO DEVUELVE LA LISTA DE ERRORES
                if (!ModelState.IsValid)
                    throw new Exception(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).First());

                //ACTUALIZAR EL USUARIO
                EMPRESA_R.UPDATE(MODEL);

                //SI TODO SALIO CORRECTO SE ENVIA OK
                salida.status = true;
                salida.message = "Datos guardados exitosamente!";
            }
            catch (Exception ex)
            {
                //SE ENVIA EL ERROR DE LA APLICACION
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpGet]
        [Route("sucursales-ajax")]
        public ActionResult LISTA_SUCURSAL_AJAX()
        {
            return Json(SUCURSAL_R.TODOS_WEB(), JsonRequestBehavior.AllowGet);
        }

        [Route("sucursales")]
        public ActionResult TODO_SUCURSALES()
        {
            return View();
        }

        [Route("sucursales/nueva")]
        public ActionResult NUEVA_SUCURSAL()
        {
            //DTOS DE LA SUCURSAL
            SUCURSAL_VM MODEL = new SUCURSAL_VM();

            MODEL.EMPRESAS = EMPRESA_R.TODOS().ToList();

            return View(MODEL);
        }

        [HttpPost]
        [Route("sucursales/crear-ajax")]
        public string NUEVA_SUCURSAL_AJAX(SUCURSAL_VM MODEL)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SI EL MODELO NO ES VALIDO DEVUELVE LA LISTA DE ERRORES
                if (!ModelState.IsValid)throw new Exception(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).First());

                //CREAR LA SUCURSAL
                SUCURSAL_R.CREATE(MODEL);

                //SI TODO SALIO CORRECTO SE ENVIA OK
                salida.status = true;
                salida.message = "Datos guardados exitosamente!";
            }
            catch (Exception ex)
            {
                //SE ENVIA EL ERROR DE LA APLICACION
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("sucursales/{ID_SUCURSAL:long}")]
        public ActionResult EDITAR_SUCURSAL(long ID_SUCURSAL)
        {
            //DTOS DE LA SUCURSAL SEGÚN ID
            SUCURSAL_VM MODEL = new SUCURSAL_VM();

            MODEL.SUCURSAL_VM_DESDE_SUCURSAL(SUCURSAL_R.READ(ID_SUCURSAL));
            MODEL.EMPRESAS = EMPRESA_R.TODOS().ToList();

            return View(MODEL);
        }

        [HttpPost]
        [Route("sucursales/editar-ajax")]
        public string EDITAR_SUCURSAL_AJAX(SUCURSAL_VM MODEL)
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            try
            {
                //SI EL MODELO NO ES VALIDO DEVUELVE LA LISTA DE ERRORES
                if (!ModelState.IsValid)
                    throw new Exception(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).First());

                //ACTUALIZAR LA SUCURSAL
                SUCURSAL_R.UPDATE(MODEL);

                //SI TODO SALIO CORRECTO SE ENVIA OK
                SALIDA.status = true;
                SALIDA.message = "Datos guardados exitosamente!";
            }
            catch (Exception ex)
            {
                //SE ENVIA EL ERROR DE LA APLICACION
                SALIDA.message = ex.Message;
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

        [Route("perfiles")]
        public ActionResult TODO_PERFILES()
        {
            return View();
        }

        [Route("perfiles-ajax")]
        public string LISTA_PERFILES_AJAX()
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            try
            {
                SALIDA.status = true;
                SALIDA.data = PERFIL_R.TODOS();
            }
            catch (Exception ex)
            {
                SALIDA.message = ex.Message;
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

        [Route("perfiles/{ID_PERFIL:long}")]
        public ActionResult EDITAR_PERFIL(long ID_PERFIL)
        {
            //DTOS DEL PERFIL SEGÚN ID
            PERFIL_VM MODEL = new PERFIL_VM();

            MODEL.PERFIL_VM_DESDE_PERFIL (PERFIL_R.READ(ID_PERFIL));
            MODEL.PAGINAS = PAGINA_PERFIL_R.TODOS(ID_PERFIL).ToList();

            return View(MODEL);
        }

        [Route("perfiles/editar-ajax")]
        public string EDITAR_PERFIL_AJAX(PERFIL MODEL, long PAGINA_POR_DEFECTO)
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //ACTUALIZAR EL PERFIL
                    PERFIL_R.UPDATE(MODEL);

                    //LA LISTA DE PAGINAS
                    List<PAGINA_PERFIL> PAG_PERFILES = PAGINA_PERFIL_R.TODOS(MODEL.ID_PERFIL).ToList();

                    //ACTUALIZACION DE LAS PAGINAS (QUE SON MENU A POR DEFECTO 0)
                    foreach (PAGINA_PERFIL ITEM in PAG_PERFILES.Where(n => n.PAGINA.ES_MENU))
                    {
                        ITEM.POR_DEFECTO = (PAGINA_POR_DEFECTO == ITEM.ID_PAGINA_PERFIL) ? true : false;
                        PAGINA_PERFIL_R.UPDATE(ITEM);
                    }

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    SALIDA.status = true;
                    SALIDA.message = "Perfil actualizado correctamente";
                }
                catch (Exception ex)
                {
                    SALIDA.message = ex.Message;
                }
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

        [Route("financieras")]
        public ActionResult TODO_FINANCIERAS()
        {
            return View();
        }

        [Route("financieras/nueva")]
        public ActionResult NUEVA_FINANCIERA()
        {
            return View();
        }

        [Route("financieras-ajax")]
        public string TODO_FINANCIERA_AJAX()
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            try
            {
                SALIDA.status = true;
                SALIDA.data = FINANCIERA_R.TODOS();
            }
            catch (Exception ex)
            {
                SALIDA.message = ex.Message;
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

        [Route("financieras/{ID_FINANCIERA:long}")]
        public ActionResult EDITAR_FINANCIERA(long ID_FINANCIERA)
        {
            //DTOS DE LA FINANCIERA SEGÚN ID
            FINANCIERA_VM MODEL = new FINANCIERA_VM();
            MODEL.FINANCIERA_VM_DESDE_FINANCIERA(FINANCIERA_R.READ(ID_FINANCIERA));

            return View(MODEL);
        }

        [Route("financieras/editar-ajax")]
        public string EDITAR_FINANCIERA_AJAX(FINANCIERA_VM MODEL)
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //ACTUALIZAR EL PERFIL
                    FINANCIERA_R.UPDATE(MODEL);

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    SALIDA.status = true;
                    SALIDA.message = "Financiera actualizada correctamente";

                }
                catch (Exception ex)
                {
                    SALIDA.message = ex.Message;
                }
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

        [Route("financieras/crear-ajax")]
        public string CREAR_FINANCIERA_AJAX(FINANCIERA_VM MODEL)
        {
            dynamic SALIDA = new ExpandoObject();
            SALIDA.status = false;

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //CREAR LA FINANCIERA
                    FINANCIERA_R.CREATE(MODEL);

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    SALIDA.status = true;
                    SALIDA.message = "Financiera creada correctamente";

                }
                catch (Exception ex)
                {
                    SALIDA.message = ex.Message;
                }
            }

            return JsonConvert.SerializeObject(SALIDA);
        }

    }

}