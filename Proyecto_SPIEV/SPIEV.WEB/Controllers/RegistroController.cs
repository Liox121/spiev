﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.WEB.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;

namespace SPIEV.WEB.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("registro")]
    public class RegistroController : Controller
    {

        // GET: Registro
        [Route]
        public ActionResult INDEX()
        {
            //DATOS PARA LA VISTA...
            PRIMERA_INSCRIPCION_VM model = new PRIMERA_INSCRIPCION_VM()
            {
                LISTA_COMUNAS = COMUNA_R.TODAS_COMUNA().ToList(),
                LISTA_CALIDAD_PERSONAS = CALIDAD_PERSONA_R.TODOS_CALIDAD_PERSONA().ToList(),

                VehiculoForm = new VEHICULO_VM()
                {
                    LISTA_TIPOS_COMBUSTIBLE = TIPO_COMBUSTIBLE_VEHICULO_R.TODOS_COMBUSTIBLE().ToList(),
                    LISTA_TIPOS_VEHICULO = TIPO_VEHICULO_R.TODOS_TIPOS_VEHICULOS().ToList(),
                    LISTA_TIPOS_MEDIDA = TIPO_MEDIDA_VEHICULO_R.TODOS_TIPOS_MEDIDA().ToList(),
                    LISTA_COLORES_VEHICULO = COLOR_VEHICULO_R.TODOS_COLOR_VEHICULO().ToList()
                },

                ComunidadForm = new COMUNIDAD_VM()
                {
                    LISTA_OPCIONES_COMUNIDAD = OPCION_COMUNIDAD_R.TODOS_OPCION_COMUNIDAD().ToList()
                },

                FacturaForm = new FACTURA_VM()
                {
                    LISTA_TIPOS_MONEDAS = TIPO_MONEDA_R.TODOS_TIPO_MONEDA().ToList()
                },

                OperadorForm = new OPERADOR_VM()
                {
                    LISTA_REGIONES = REGION_R.TODOS_REGION().ToList()
                }
            };

            return View(model);
        }

        // GET: Registro
        [Route("desde-solicitud/{ID_SOLICITUD:long}")]
        [HttpGet]
        public ActionResult INSCRIPCION_SOLICITUD(long? ID_SOLICITUD)
        {
            //DATOS PARA LA VISTA...
            PRIMERA_INSCRIPCION_VM model = new PRIMERA_INSCRIPCION_VM()
            {
                ID_SOLICITUD = ID_SOLICITUD,
                LISTA_COMUNAS = COMUNA_R.TODAS_COMUNA().ToList(),
                LISTA_CALIDAD_PERSONAS = CALIDAD_PERSONA_R.TODOS_CALIDAD_PERSONA().ToList(),

                VehiculoForm = new VEHICULO_VM()
                {
                    LISTA_TIPOS_COMBUSTIBLE = TIPO_COMBUSTIBLE_VEHICULO_R.TODOS_COMBUSTIBLE().ToList(),
                    LISTA_TIPOS_VEHICULO = TIPO_VEHICULO_R.TODOS_TIPOS_VEHICULOS().ToList(),
                    LISTA_TIPOS_MEDIDA = TIPO_MEDIDA_VEHICULO_R.TODOS_TIPOS_MEDIDA().ToList(),
                    LISTA_COLORES_VEHICULO = COLOR_VEHICULO_R.TODOS_COLOR_VEHICULO().ToList()
                },

                ComunidadForm = new COMUNIDAD_VM()
                {
                    LISTA_OPCIONES_COMUNIDAD = OPCION_COMUNIDAD_R.TODOS_OPCION_COMUNIDAD().ToList()
                },

                FacturaForm = new FACTURA_VM()
                {
                    LISTA_TIPOS_MONEDAS = TIPO_MONEDA_R.TODOS_TIPO_MONEDA().ToList()
                },

                OperadorForm = new OPERADOR_VM()
                {
                    LISTA_REGIONES = REGION_R.TODOS_REGION().ToList()
                }
            };

            return View(model);
        }

        [HttpPost]
        [Route("guardar-spiev-ajax")]
        public string NUEVO_SPIEV_AJAX(PRIMERA_INSCRIPCION_VM registro)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO (EN CASO DE EXCEPCION)
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SI EL MODELO NO ES VALIDO DEVUELVE LA LISTA DE ERRORES
                    if (!ModelState.IsValid) throw new Exception(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).First());

                    //CREAR VEHICULO
                    VEHICULO VEHICULO_CREADO = VEHICULO_R.CREATE(registro.VehiculoForm.OBJETO_CREAR());

                    //CREAR COMUNIDAD
                    COMUNIDAD COMUNIDAD_CREADA = COMUNIDAD_R.CREATE(registro.ComunidadForm.OBJETO_CREAR());

                    //CREAR ADQUIRIENTE
                    ADQUIRIENTE ADQUIRIENTE_CREADO = ADQUIRIENTE_R.CREATE(registro.AdquirienteForm.OBJETO_CREAR());

                    //CREAR SOLICITANTE
                    SOLICITANTE SOLICITANTE_CREADO = SOLICITANTE_R.CREATE(registro.SolicitanteForm.OBJETO_CREAR());

                    //CREAR ESTIPULANTE
                    ESTIPULANTE ESTIPULANTE_CREADO = ESTIPULANTE_R.CREATE(registro.EstipulanteForm.OBJETO_CREAR());

                    //CREAR IMPUESTO
                    IMPUESTO_ADICIONAL IMPUESTO_CREADO = IMPUESTO_ADICIONAL_R.CREATE(registro.ImpuestoForm.OBJETO_CREAR());

                    //CREAR FACTURA
                    FACTURA FACTURA_CREADA = FACTURA_R.CREATE(registro.FacturaForm.OBJETO_CREAR());

                    //CREAR OBSERVACION
                    OBSERVACION OBSERVACION_CREADA = OBSERVACION_R.CREATE(registro.ObservacionForm.OBJETO_CREAR());

                    //CREAR OPERADOR
                    OPERADOR OPERADOR_CREADO = OPERADOR_R.CREATE(registro.OperadorForm.OBJETO_CREAR());

                    //CREAR LA PRIMERA INSCRIPCIÓN
                    PRIMERA_INSCRIPCION NUEVA_INSCRIPCION = new PRIMERA_INSCRIPCION
                    {
                        ID_VEHICULO = VEHICULO_CREADO.ID_VEHICULO,
                        ID_COMUNIDAD = COMUNIDAD_CREADA.ID_COMUNIDAD,
                        ID_ADQUIRIENTE = ADQUIRIENTE_CREADO.ID_ADQUIRIENTE,
                        ID_SOLICITANTE = SOLICITANTE_CREADO.ID_SOLICITANTE,
                        ID_ESTIPULANTE = ESTIPULANTE_CREADO.ID_ESTIPULANTE,
                        ID_IMPUESTO = IMPUESTO_CREADO.ID_IMPUESTO_ADICIONAL,
                        ID_FACTURA = FACTURA_CREADA.ID_FACTURA,
                        ID_OBSERVACION = OBSERVACION_CREADA.ID_OBSERVACION,
                        ID_OPERADOR = OPERADOR_CREADO.ID_OPERADOR,
                        ID_ESTADO = 1, //ESTADO "Inscripción Ingresada"
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        FECHA = DateTime.Now
                    };

                    //CREAR PRIMERA INSCRIPCIÓN
                    PRIMERA_INSCRIPCION INSCRIPCION_CREADA = PRIMERA_INSCRIPCION_R.CREATE(NUEVA_INSCRIPCION);

                    //SI VIENE DESDE UNA SOLICITUD
                    if (registro.ID_SOLICITUD.HasValue)
                    {

                        //ACTUALIZA EL ID DE PRIMERA INSCRIPCION EN LA SOLICITUD (ASOCIA INSCRIPCION CON SOLICITUD)
                        SOLICITUD_SPIEV_R.UPDATE(new SOLICITUD_SPIEV_R.UPDATE_SOLICITUD
                        {
                           ID_SOLICITUD = registro.ID_SOLICITUD.Value,
                           ID_ESTADO = 2, // ESTADO "Documentación OK"
                           ID_PRIMERA_INSCRIPCION = INSCRIPCION_CREADA.ID_PRIMERA_INSCRIPCION,
                        });

                        //GENERA LA HISTORIA SOBRE LA SOLICITUD AL CAMBIAR ESTADO
                        HISTORIA_SOLICITUD_R.CREATE(new HISTORIA_SOLICITUD
                        {
                            ID_SOLICITUD = registro.ID_SOLICITUD.Value,
                            ID_ESTADO = 2,
                            ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                            COMENTARIO = "SOLICITUD SPIEV, CON DOCUMENTACIÓN OK",
                            FECHA = DateTime.Now
                         });
                    }

                    //GENERA LA HISTORIA SOBRE LA PRIMERA INSCRIPCIÓN
                    HISTORIA_INSCRIPCION_R.CREATE(new HISTORIA_INSCRIPCION
                    {
                        ID_PRIMERA_INSCRIPCION = INSCRIPCION_CREADA.ID_PRIMERA_INSCRIPCION,
                        ID_ESTADO = 1,
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        COMENTARIO = "PRIMERA INSCRIPCIÓN, INGRESADA",
                        FECHA = DateTime.Now
                    });

                    //TERMINA LA EJECUCIÓN
                    scope.Complete();

                    //SI TODO SALIO CORRECTO SE ENVIA OK
                    salida.status = true;
                    salida.data = new { ID_INSCRIPCION = INSCRIPCION_CREADA.ID_PRIMERA_INSCRIPCION };
                    salida.message = "Datos guardados exitosamente!";
                }
                catch (Exception ex)
                {
                   //SE ENVIA EL ERROR DE LA APLICACION
                   salida.message = ex.Message;
                }
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpGet]
        [Route("carga-vehiculo-ajax")]
        public string CargaVehiculo(int ID_TIPO_VEHICULO)
        {
            dynamic salida = new ExpandoObject();

            IEnumerable<CARGA_VEHICULO> lista_carga = CARGA_VEHICULO_R.TODOS_CARGA_VEHICULO().Where(n => n.ID_TIPO_VEHICULO == ID_TIPO_VEHICULO);

            salida.valor = lista_carga.Any() ? lista_carga.First().VALOR_POSIBLE : (float?)null;

            return JsonConvert.SerializeObject(salida);

        }

        [HttpGet]
        [Route("cantidad-comunidad-ajax")]
        public string CANTIDAD_COMUNIDAD_AJAX(int ID_OPCION_COMUNIDAD)
        {
            dynamic salida = new ExpandoObject();
            
            IEnumerable<CANTIDAD_COMUNIDAD> lista_cantidad = CANTIDAD_COMUNIDAD_R.TODOS_CANTIDAD_COMUNIDAD().Where(n => n.ID_OPCION_COMUNIDAD == ID_OPCION_COMUNIDAD);
            
            //SI EXISTE ALGUN REGISTRO CON EL ID TIPO VEHICULO
            salida.valor = lista_cantidad.Any() ? lista_cantidad.First().VALOR_POSIBLE : (long?)null;

            return JsonConvert.SerializeObject(salida);

        }

        [Route("solicitudes-ingresadas")]
        [HttpGet]
        public ActionResult SOLICITUD_INGRESO()
        {
            return View();
        }
        

        [Route("solicitudes-ingresadas-ajax")]
        [HttpPost]
        public string LISTA_SOLICITUD_INGRESO_AJAX()
        {
            return JsonConvert.SerializeObject(SOLICITUD_SPIEV_R.TODOS_SOLICITUD_X_ESTADO(1) /* 1 = ESTADO "Solicitud Ingresada" */ );
        }

    }
}