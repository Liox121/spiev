﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.UTIL;
using SPIEV.WEB.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SPIEV.WEB.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("inscripcion")]
    public class InscripcionController : Controller
    {

        [Route("cargar-documentacion/{Id_Inscripcion:long}")]
        [HttpGet]
        public ActionResult CARGAR_DOCUMENTOS(long ID_INSCRIPCION)
        {
            ViewBag.ID_INSCRIPCION = ID_INSCRIPCION;
            return View();
        }

        [Route("solicitud-limitacion/{Id_Inscripcion:long}")]
        [HttpGet]
        public ActionResult SOLICITUD_LIMITACION(long ID_INSCRIPCION)
        {
            ViewBag.ID_INSCRIPCION = ID_INSCRIPCION;
            return View();
        }

        [HttpPost]
        [Route("guardar-enviar-documentos-ajax")]
        public string CARGAR_DOCUMENTOS_AJAX(HttpPostedFileBase[] FILES, string JSON_PARAMETROS)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //DEFINICION DE VARIABLES
            List<string> PATH_UPLOAD_FILES = new List<string>();
            JSON_DOCUMENTO_INSCRIPCION JSON_DOCUMENTOS = new JSON_DOCUMENTO_INSCRIPCION();

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO SOBRE LAS TRANSACCIONES CONTENIDAS
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SE DESERIALIZA EL OBJETO RECIBIDO DESDE LA VISTA
                    JSON_DOCUMENTOS = JsonConvert.DeserializeObject<JSON_DOCUMENTO_INSCRIPCION>(JSON_PARAMETROS);

                    //LA RUTA DE DESTINO
                    String DESTINO = Server.MapPath("~/Multimedia/Fundantes");

                    //VALIDAR CARPETA...
                    if (!Directory.Exists(DESTINO)) throw new Exception($"La ruta de destino no existe en el servidor: {DESTINO}");

                    //LISTA DE ARCHIVOS A BORRARS
                    List<DOCUMENTO_INSCRIPCION> ARCHIVOS_BORRAR = DOCUMENTO_INSCRIPCION_R.DOCUMENTOS_X_INSCRIPCION(JSON_DOCUMENTOS.ID_INSCRIPCION).ToList();

                    //PROCESAR LOS ARCHIVOS (NUEVOS), SI EXISTEN
                    if (FILES != null)
                    {
                        for (int i = 0; i < FILES.Count(); i++)
                        {
                            //EL ARCHIVO
                            HttpPostedFileBase ARCHIVO = FILES[i];

                            //RUTA A GUARDAR
                            String FILE_NAME = $"{Guid.NewGuid()}{Path.GetExtension(ARCHIVO.FileName)}";

                            //EL ID DEL TIPO
                            int ID_TIPO_ARCHIVO = JSON_DOCUMENTOS.TIPO_ARCHIVO_NUEVO[i];

                            //GUARDAR EL ARCHIVO
                            ARCHIVO.SaveAs($"{DESTINO}/{FILE_NAME}");

                            //SE REGISTRA EL ARCHIVO SUBIDO
                            PATH_UPLOAD_FILES.Add($"{DESTINO}/{FILE_NAME}");

                            //CREAR EL ARCHIVO ASOCIADO A LA SOLICITUD
                            DOCUMENTO_INSCRIPCION_R.CREATE(new DOCUMENTO_INSCRIPCION
                            {
                                ID_INSCRIPCION = JSON_DOCUMENTOS.ID_INSCRIPCION,
                                ID_TIPO_ARCHIVO_INSCRIPCION = ID_TIPO_ARCHIVO,
                                DOCUMENTO = FILE_NAME
                            });

                        }
                    }

                    //PROCESAR LOS ARCHIVOS (ACTUALIZAR), SI EXISTEN
                    for (int i = 0; i < JSON_DOCUMENTOS.ARCHIVOS_ACTUALIZAR.Count(); i++)
                    {
                        int ID_DOCUMENTO = JSON_DOCUMENTOS.ARCHIVOS_ACTUALIZAR[i];

                        int ID_TIPO_ARCHIVO = JSON_DOCUMENTOS.TIPO_ARCHIVO_ACTUALIZAR[i];

                        //ACTUALIZAR EL ARCHIVO
                        DOCUMENTO_INSCRIPCION_R.UPDATE(new DOCUMENTO_INSCRIPCION_R.UPDATE_DOCUMENTO
                        {
                            ID_DOCUMENTO = ID_DOCUMENTO,
                            ID_INSCRIPCION = JSON_DOCUMENTOS.ID_INSCRIPCION,
                            ID_TIPO_ARCHIVO_INSCRIPCION = ID_TIPO_ARCHIVO,
                        });

                        //SE ELIMINA EL ARCHIVO, SI ESTA EN LA LISTA DE ACTUALIZACIÓN
                        DOCUMENTO_INSCRIPCION ARCHIVO_EXCLUIR = ARCHIVOS_BORRAR.SingleOrDefault(x => x.ID_DOCUMENTO == ID_DOCUMENTO);
                        ARCHIVOS_BORRAR.Remove(ARCHIVO_EXCLUIR);
                    }

                    for (int i = 0; i < ARCHIVOS_BORRAR.Count(); i++)
                    {
                        //BORRAR EL ARCHIVO ASOCIADO A LA INSCRIPCION
                        DOCUMENTO_INSCRIPCION_R.UPDATE(new DOCUMENTO_INSCRIPCION_R.UPDATE_DOCUMENTO
                        {
                            ID_DOCUMENTO = ARCHIVOS_BORRAR[i].ID_DOCUMENTO,
                            BORRADO = false // BORRADO = 1/TRUE, BORRADO = 0/FALSE
                        });
                    }

                    //ACTUALIZAR ESTADO PRIMERA INSCRIPCIÓN
                    PRIMERA_INSCRIPCION_R.UPDATE(new PRIMERA_INSCRIPCION_R.UPDATE_INSCRIPCION{
                        ID_PRIMERA_INSCRIPCION = JSON_DOCUMENTOS.ID_INSCRIPCION,
                        ID_ESTADO = 2 //ESTADO = "Documentación Cargada"
                    });

                    //GENERA LA HISTORIA SOBRE LA PRIMERA INSCRIPCIÓN
                    HISTORIA_INSCRIPCION_R.CREATE(new HISTORIA_INSCRIPCION
                    {
                        ID_PRIMERA_INSCRIPCION = JSON_DOCUMENTOS.ID_INSCRIPCION,
                        ID_ESTADO = 2, //ESTADO = "Documentación Cargada"
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        COMENTARIO = "PRIMERA INSCRIPCIÓN CON DOCUMENTACIÓN CARGADA",
                        FECHA = DateTime.Now
                    });

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    //SE SETEA LA SALIDA
                    salida.status = true;
                    salida.message = "Solicitud actualizada correctamente";
                }
                catch (Exception ex)
                {
                    //EN CASO DE ERROR BORRAR LOS ARCHIVOS SUBIDOS
                    Parallel.ForEach(PATH_UPLOAD_FILES, FILE =>
                    {
                        System.IO.File.Delete(FILE);
                    });

                    //SE ENVIA EL ERROR
                    salida.message = ex.Message;
                }

            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("inscripciones-ingresadas")]
        [HttpGet]
        public ActionResult INSCRIPCION_INGRESO()
        {
            return View();
        }

        [Route("inscripciones-ingresadas-ajax")]
        [HttpPost]
        public string LISTA_INSCRIPCION_INGRESO_AJAX()
        {
            return JsonConvert.SerializeObject(PRIMERA_INSCRIPCION_R.TODO_INSCRIPCION().Where(n => n.ID_ESTADO == 1) /* 1 = ESTADO "Inscripción Ingresada" */ );
        }

        [Route("inscripciones-todas")]
        [HttpGet]
        public ActionResult TODO_INSCRIPCIONES()
        {
            return View();
        }

        [Route("inscripciones-todas-ajax")]
        [HttpPost]
        public string LISTA_INSCRIPCION_AJAX()
        {
            return JsonConvert.SerializeObject(PRIMERA_INSCRIPCION_R.TODO_INSCRIPCION());
        }

        [Route("tipos-archivos-inscripcion-ajax")]
        [HttpPost]
        public string TIPOS_ARCHIVOS_INSCRIPCION_AJAX()
        {
            return JsonConvert.SerializeObject(TIPO_ARCHIVO_INSCRIPCION_R.TODOS_WEB());
        }

        [HttpPost]
        [Route("cargar-archivos-ajax")]
        public string CARGAR_ARCHIVOS_AJAX(long ID_INSCRIPCION)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO SOBRE LAS TRANSACCIONES CONTENIDAS
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SI NO EXISTE UNA SOLICITUD ASOCIADA...
                    if (!SOLICITUD_SPIEV_R.TODOS_SOLICITUD().Any(n => n.ID_PRIMERA_INSCRIPCION == ID_INSCRIPCION))
                    {
                        salida.message = "No existe solicitud, asociada a esta inscripción";
                        //SALIDA FORZADA...
                        return JsonConvert.SerializeObject(salida);
                    }

                    //LA SOLICITUD ASOCIADA A LA PRIMERA INSCRIPCIÓN
                    SOLICITUD_SPIEV SOLICITUD_ASOCIADA = SOLICITUD_SPIEV_R.TODOS_SOLICITUD().First(n => n.ID_PRIMERA_INSCRIPCION == ID_INSCRIPCION);

                    //LISTA DE ARCHIVOS (QUE CORRESPONDAN A PRIMERA INSCRIPCION)...
                    var TIPOS_INSCRIPCION = TIPO_ARCHIVO_INSCRIPCION_R.TODOS().Where(n => n.TIPO_WS_SPIEV.HasValue).Select(x => x.ID_TIPO_ARCHIVO);

                    List<ARCHIVO_SOLICITUD_SPIEV> ARCHIVOS_SOL = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(SOLICITUD_ASOCIADA.ID_SOLICITUD).ToList();
                    ARCHIVOS_SOL = ARCHIVOS_SOL.Where(n => TIPOS_INSCRIPCION.Contains(n.ID_TIPO_ARCHIVO_SOLICITUD)).ToList();

                    //LA RUTA DE ORIGEN
                    String ORIGEN = Server.MapPath("~/Multimedia/Solicitudes");

                    //LA RUTA DE DESTINO
                    String DESTINO = Server.MapPath("~/Multimedia/Fundantes");

                    //LISTA DE SALIDA...
                    List<DOCUMENTO_INSCRIPCION> DOC_SALIDA = new List<DOCUMENTO_INSCRIPCION>();

                    //SE COPIAN LOS ARCHIVOS DESDE LA SOLICITUD A LA INSCRIPCIÓN
                    foreach (ARCHIVO_SOLICITUD_SPIEV ARCHIVO_OBJ in ARCHIVOS_SOL)
                    {
                        //RUTA DE ORIGEN
                        String ORIGEN_PATH = $@"{ORIGEN}/{ARCHIVO_OBJ.ARCHIVO}";

                        //SI NO EXISTE EL ARCHIVO CONTINUA...
                        if (!System.IO.File.Exists(ORIGEN_PATH)) continue;

                        //NOMBRE DESTINO A GUARDAR
                        String FILE_NAME = $"{Guid.NewGuid()}{Path.GetExtension(ARCHIVO_OBJ.ARCHIVO)}";

                        //RUTA DE DESTINO
                        String DESTINO_PATH = $@"{DESTINO}/{FILE_NAME}";

                        //COPIAR EL ARCHIVO
                        System.IO.File.Copy(ORIGEN_PATH, DESTINO_PATH, true);

                        DOCUMENTO_INSCRIPCION DOC_CREADO = DOCUMENTO_INSCRIPCION_R.CREATE(new DOCUMENTO_INSCRIPCION
                        {
                            ID_INSCRIPCION = ID_INSCRIPCION,
                            ID_TIPO_ARCHIVO_INSCRIPCION = ARCHIVO_OBJ.ID_TIPO_ARCHIVO_SOLICITUD,
                            DOCUMENTO = FILE_NAME
                        });

                        //SE AGREGA A LA SALIDA
                        DOC_SALIDA.Add(DOC_CREADO);
                    };

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    salida.status = true;
                    salida.data = DOC_SALIDA;
                }
                catch (Exception ex)
                {
                    salida.message = ex.Message;
                }

            }

            return JsonConvert.SerializeObject(salida);

        }

        [HttpPost]
        [Route("documentos-inscripcion-ajax")]
        public string DOCUMENTOS_INSCRIPCION_AJAX(long ID_INSCRIPCION)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                List<DOCUMENTO_INSCRIPCION> DOCUMENTOS = DOCUMENTO_INSCRIPCION_R.DOCUMENTOS_X_INSCRIPCION(ID_INSCRIPCION).ToList();

                salida.status = true;
                salida.data = DOCUMENTOS;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("detalle-inscripcion-ajax")]
        public string DETALLE_INSCRIPCION_AJAX(long ID_INSCRIPCION)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                PRIMERA_INSCRIPCION INSCRIPCION = PRIMERA_INSCRIPCION_R.READ(ID_INSCRIPCION);
                INSCRIPCION = PRIMERA_INSCRIPCION_R.COMPLETAR_OBJETO(INSCRIPCION);

                salida.status = true;
                salida.data = INSCRIPCION;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("excel-inscripciones")]
        [HttpPost]
        public string GENERAR_EXCEL_INSCRIPCION(string JSON_PARAMETRO)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //REPOSITORIOS Y UTILIDADES
            ConvertList CONVERSOR = new ConvertList();
            GenerateExcel GENERA_EXCEL = new GenerateExcel();

            try
            {

                List<long> INSCRIPCIONES = JsonConvert.DeserializeObject<List<long>>(JSON_PARAMETRO);

                //RUTAS...
                String RUTA_PLANTILLA = Server.MapPath("~/Multimedia/Plantillas/EXPORTAR_INSCRIPCIONES.xlsx");

                //SE FILTRAN POR ID DE PRIMERA DE INSCRIPCION
                var LIST_FILTRADA = PRIMERA_INSCRIPCION_R.TODO_INSCRIPCION().Where(n => INSCRIPCIONES.Contains(n.ID_PRIMERA_INSCRIPCION));

                //LA LISTA PARA EXCEL
                var LIST_FOR_EXCEL = LIST_FILTRADA.Select(obj =>
                {
                    //COMPLETAR EL OBJETO
                    PRIMERA_INSCRIPCION_R.COMPLETAR_OBJETO(obj);

                    //RETORNAR LA LISTA PARA EL EXCEL
                    return obj.EXPORTAR_PRIMERA_INSCRIPCION();

                }).Select(X =>  new {
                    X.A, X.B, X.C, X.D, X.E, X.F, X.G, X.H, X.I, X.J, X.K, X.L, X.M, X.N, X.O, X.P, X.Q, X.R, X.S, X.T, X.U, X.V, X.W, X.X, X.Y, X.Z,
                    X.AA, X.AB, X.AC, X.AD, X.AE, X.AF, X.AG, X.AH, X.AI, X.AJ, X.AK, X.AL, X.AM, X.AN, X.AO, X.AP, X.AQ, X.AR, X.AS, X.AT, X.AU, X.AV, X.AW, X.AX, X.AY, X.AZ,
                    X.BA, X.BB, X.BC, X.BD, X.BE, X.BF, X.BG, X.BH, X.BI, X.BJ, X.BK, X.BL, X.BM, X.BN, X.BO, X.BP, X.BQ, X.BR, X.BS, X.BT, X.BU, X.BV, X.BW, X.BX, X.BY, X.BZ,
                    X.CA, X.CB, X.CC, X.CD
                }).ToList();

                //DATATABLE DESDE LISTA
                var DATA_LIST = CONVERSOR.ListToDataTable(LIST_FOR_EXCEL);

                //GENERAR ID UNICO, DESDE GUID
                string UNIQ_ID = Guid.NewGuid().ToString();

                //STREAM PARA LA SALIDA DEL EXCEL
                using (MemoryStream STREAM = new MemoryStream())
                {
                    //GENERAR EXCEL
                    GENERA_EXCEL.GENERAR_DESDE_PLANTILLA(3, RUTA_PLANTILLA, DATA_LIST, STREAM);
                    STREAM.Position = 0;
                    TempData[UNIQ_ID] = STREAM.ToArray();
                }
   
                salida.status = true;
                salida.data = new { FILE_GUID = UNIQ_ID, FILE_NAME = "EXPORTAR_INSCRIPCIONES.xlsx" };
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);

        }

        [HttpGet]
        public virtual ActionResult DescargarExcelInscripcion(string FILE_GUID, string FILE_NAME)
        {
            if (TempData[FILE_GUID] != null)
            {
                byte[] data = TempData[FILE_GUID] as byte[];
                return File(data, "application/vnd.ms-excel", FILE_NAME);
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return new EmptyResult();
            }
        }
    }

}