﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.UTIL;
using SPIEV.WEB.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SPIEV.WEB.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("solicitud")]
    public class SolicitudController : Controller
    {
        // GET: Solicitud
        [Route]
        public ActionResult INDEX()
        {
            SOLICITUD_VM MODEL = new SOLICITUD_VM
            {
                SUCURSALES = new List<LIB_WEB.SUCURSAL_USUARIO>(),
                FINANCIERAS = FINANCIERA_R.TODOS().ToList()
            };

            //AÑADE LA OPCION POR DEFECTO Y ORDENA POR ID DE FINANCIERA
            MODEL.DEFAULT_OPCION_FINANCIERA();

            return View(MODEL);
        }

        [Route("pagina-inicio")]
        public ActionResult Pagina_Inicio()
        {
            return View();
        }

        [Route("tipos-archivos-ajax")]
        [HttpPost]
        public string TiposArchivosAjax()
        {
            return JsonConvert.SerializeObject(TIPO_ARCHIVO_SOLICITUD_R.TODOS_WEB());
        }

        [Route("tipos-archivos-ajax-crear")]
        [HttpPost]
        public string TiposArchivosAjaxCrear()
        {
            return JsonConvert.SerializeObject(TIPO_ARCHIVO_SOLICITUD_R.TODOS_WEB_CREAR());
        }

        [Route("mis-solicitudes")]
        public ActionResult Mis_Solicitudes()
        {
            return View();
        }

        [Route("mis-solicitudes/{ID_SOLICITUD:long}")]
        [HttpGet]
        public ActionResult Mis_Solicitudes_Editar(long ID_SOLICITUD)
        {
            //SE ENVIA EL ID DE SOLICITUD A LA VISTA
            ViewBag.ID_SOLICITUD = ID_SOLICITUD;

            //LA LISTA DE SUCURSALES DISPONIBLES
            SOLICITUD_VM MODEL = new SOLICITUD_VM
            {
                SUCURSALES = new List<LIB_WEB.SUCURSAL_USUARIO>(),
                FINANCIERAS = FINANCIERA_R.TODOS().ToList()
            };

            //AÑADE LA OPCION POR DEFECTO Y ORDENA POR ID DE FINANCIERA
            MODEL.DEFAULT_OPCION_FINANCIERA();

            return View(MODEL);
        }

        [Route("mis-solicitudes-ajax")]
        [HttpPost]
        public string Mis_Solicitudes_Ajax()
        {
            return JsonConvert.SerializeObject(SOLICITUD_SPIEV_R.TODOS_SOLICITUD_X_USUARIO(SESSION_WEB.SESION_USER.ID_USUARIO));
        }

        [HttpPost]
        [Route("crear-ajax")]
        public string CrearSolicitudAjax(HttpPostedFileBase[] FILES, string JSON_PARAMETROS)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //VARIABLES
            List<string> PATH_UPLOAD_FILES = new List<string>();
            JSON_ARCHIVO_SOLICITUD JSON_ARCHIVOS = new JSON_ARCHIVO_SOLICITUD();

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO SOBRE LAS TRANSACCIONES CONTENIDAS
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SE CONVIERTE EL JSON RECIBIDO
                    JSON_ARCHIVOS = JsonConvert.DeserializeObject<JSON_ARCHIVO_SOLICITUD>(JSON_PARAMETROS);

                    //VALIDAR LOS DATOS
                    TryValidateModel(JSON_ARCHIVOS);

                    //ENVIAR VALIDACIÓN
                    if (!ModelState.IsValid) throw new Exception(ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).First());

                    //LA RUTA DE DESTINO
                    String DESTINO = Server.MapPath("~/Multimedia/Solicitudes");

                    //FILES NULOS
                    if (FILES == null) throw new Exception("No existen archivos para subir");

                    //TOTAL DE ARCHIVOS PDFs
                    var ARCHIVOS_PDF = FILES.Where(n => Path.GetExtension(n.FileName).ToUpper() == ".PDF");

                    //NO TODOS LOS ARCHIVOS SON PDFs
                    if (ARCHIVOS_PDF.Count() != FILES.Count()) throw new Exception("Solo se permiten archivos PDF");

                    int RUT_INT = Rut_Validar.ParteNumerica(JSON_ARCHIVOS.RUT_ADQUIRIENTE);
                    char DV = Rut_Validar.Digito(RUT_INT)[0];

                    //OBJETO A CREAR...
                    SOLICITUD_SPIEV solicitud = new SOLICITUD_SPIEV
                    {
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        ID_SUCURSAL = JSON_ARCHIVOS.ID_SUCURSAL,
                        ID_FINANCIERA = JSON_ARCHIVOS.ID_FINANCIERA,
                        RUT_ADQUIRIENTE = RUT_INT,
                        DV_ADQUIRIENTE = DV,
                        CORREO_ADQUIRIENTE = JSON_ARCHIVOS.CORREO_ADQUIRIENTE,
                        ID_ESTADO = 1, // ESTADO: Solicitud SPIEV Ingresada
                        COMENTARIO = JSON_ARCHIVOS.COMENTARIO,
                        FECHA_SOLICITUD = DateTime.Now,
                    };

                    //CREAR SOLICITUD SPIEV
                    SOLICITUD_SPIEV solicitud_creada = SOLICITUD_SPIEV_R.CREATE(solicitud);

                    //PROCESAR LOS ARCHIVOS
                    for (int i = 0; i < FILES.Count(); i++)
                    {
                        //EL ARCHIVO
                        HttpPostedFileBase ARCHIVO = FILES[i];

                        //RUTA A GUARDAR
                        String FILE_NAME = $"{Guid.NewGuid()}{Path.GetExtension(ARCHIVO.FileName)}";

                        //EL ID DEL TIPO
                        int ID_TIPO_ARCHIVO = JSON_ARCHIVOS.TIPO_ARCHIVO_NUEVO[i];

                        //GUARDAR EL ARCHIVO
                        ARCHIVO.SaveAs($"{DESTINO}/{FILE_NAME}");

                        //SE REGISTRA EL ARCHIVO SUBIDO
                        PATH_UPLOAD_FILES.Add($"{DESTINO}/{FILE_NAME}");

                        //CREAR EL ARCHIVO ASOCIADO A LA SOLICITUD
                        ARCHIVO_SOLICITUD_SPIEV_R.CREATE(new ARCHIVO_SOLICITUD_SPIEV
                        {
                            ID_SOLICITUD = solicitud_creada.ID_SOLICITUD,
                            ID_TIPO_ARCHIVO_SOLICITUD = ID_TIPO_ARCHIVO,
                            ARCHIVO = FILE_NAME
                        });

                    }

                    //CREAR LA HISTORIA DE LA SOLICITUD
                    HISTORIA_SOLICITUD_R.CREATE(new HISTORIA_SOLICITUD
                    {
                        ID_SOLICITUD = solicitud_creada.ID_SOLICITUD,
                        ID_ESTADO = 1,
                        COMENTARIO = "SOLICITUD SPIEV, INGRESADA",
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        FECHA = DateTime.Now
                    });

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    //SE SETEA LA SALIDA
                    salida.status = true;
                    salida.message = "Solicitud generada correctamente";
                }
                catch (Exception ex)
                {
                    //EN CASO DE ERROR BORRAR LOS ARCHIVOS SUBIDOS
                    Parallel.ForEach(PATH_UPLOAD_FILES, FILE =>
                    {
                        System.IO.File.Delete(FILE);
                    });

                    //SE ENVIA EL ERROR
                    salida.message = ex.Message;
                }

            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("mis-detalle-ajax")]
        public string MisDetalleAjax(int ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                SOLICITUD_SPIEV SOLICITUD = SOLICITUD_SPIEV_R.READ(ID_SOLICITUD);

                List<ARCHIVO_SOLICITUD_SPIEV> ARCHIVOS = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(ID_SOLICITUD).ToList();
                List<HISTORIA_SOLICITUD> HISTORIA_SOLICITUD = HISTORIA_SOLICITUD_R.HISTORIA_X_SOLICITUD(ID_SOLICITUD).ToList();
                List<HISTORIA_INSCRIPCION> HISTORIA_INSCRIPCION = new List<HISTORIA_INSCRIPCION>();

                //SI TIENE PRIMERA INSCRIPCIÓN ASOCIADA...
                if (SOLICITUD.ID_PRIMERA_INSCRIPCION.HasValue)
                {
                    HISTORIA_INSCRIPCION = HISTORIA_INSCRIPCION_R.HISTORIA_X_INSCRIPCION(SOLICITUD.ID_PRIMERA_INSCRIPCION.Value).ToList();
                }

                salida.status = true;
                salida.data = new
                {
                    ARCHIVOS,
                    HISTORIA_SOLICITUD,
                    HISTORIA_INSCRIPCION
                };
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("estados-solicitud-ajax")]
        public string EstadosSolicitudAjax()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                salida.status = true;
                salida.data = ESTADO_SOLICITUD_SPIEV_R.TODOS_ESTADOS_SOLICITUD();
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("cambio-estado-ajax")]
        public string CambioEstadoAjax(HISTORIA_SOLICITUD Historia)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //VALORES FALTANTES
                Historia.FECHA = DateTime.Now;
                Historia.ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO;

                //GENERAR EL CAMBIO DE ESTADO
                SOLICITUD_SPIEV_R.UPDATE(new SOLICITUD_SPIEV_R.UPDATE_SOLICITUD
                {
                    ID_SOLICITUD = Historia.ID_SOLICITUD,
                    ID_ESTADO = Historia.ID_ESTADO,
                });

                //GENERAR LA HISTORIA
                HISTORIA_SOLICITUD_R.CREATE(Historia);

                //LA RESPUESTA
                salida.status = true;
                salida.message = "Cambio de estado, aplicado correctamente";
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("detalle-solicitud-ajax")]
        public string DetalleSolicitudAjax(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                salida.status = true;
                salida.data = SOLICITUD_SPIEV_R.READ(ID_SOLICITUD);
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("archivos-solicitud-ajax")]
        public string ArchivosSolicitudAjax(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                List<ARCHIVO_SOLICITUD_SPIEV> ARCHIVOS = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(ID_SOLICITUD).ToList();

                salida.status = true;
                salida.data = ARCHIVOS;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpGet]
        [Route("mis-archivos-solicitud/{Id_Archivo:int}")]
        public FileResult MisArchivosDescargar(int Id_Archivo)
        {
            ARCHIVO_SOLICITUD_SPIEV archivo = ARCHIVO_SOLICITUD_SPIEV_R.READ(Id_Archivo);
            String MULTIMEDIA = Server.MapPath("~/Multimedia");
            String ARCHIVO = $"{MULTIMEDIA}/Solicitudes/{archivo.ARCHIVO}";
            
            //SI EL ARCHIVO NO EXISTE...
            ARCHIVO = (System.IO.File.Exists(ARCHIVO)) ? ARCHIVO : $"{MULTIMEDIA}/ARCHIVO_NO_ENCONTRADO.txt";

            byte[] fileBytes = System.IO.File.ReadAllBytes(ARCHIVO);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, archivo.ARCHIVO);
        }

        [HttpPost]
        [Route("editar-ajax")]
        public string EditarSolicitudAjax(HttpPostedFileBase[] FILES, string JSON_PARAMETROS)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            //DEFINICION DE VARIABLES
            List<string> PATH_UPLOAD_FILES = new List<string>();
            JSON_ARCHIVO_SOLICITUD JSON_ARCHIVOS = new JSON_ARCHIVO_SOLICITUD();

            //BLOQUE TRANSACCIONAL PERMITE EL COMMIT, GENERA UN ROLLBACK AUTOMATICO SOBRE LAS TRANSACCIONES CONTENIDAS
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                try
                {
                    //SE DESERIALIZA EL OBJETO RECIBIDO DESDE LA VISTA
                    JSON_ARCHIVOS = JsonConvert.DeserializeObject<JSON_ARCHIVO_SOLICITUD>(JSON_PARAMETROS);

                    //LA RUTA DE DESTINO
                    String DESTINO = Server.MapPath("~/Multimedia/Solicitudes");

                    //VALIDAR CARPETA...
                    if (!Directory.Exists(DESTINO)) throw new Exception($"La ruta de destino no existe en el servidor: {DESTINO}");

                    //VALIDAR EL RUT DEL DV
                    if (!Rut_Validar.ValidaRut(JSON_ARCHIVOS.RUT_ADQUIRIENTE)) throw new Exception("Rut ingresado no valido");

                    int RUT_INT = Rut_Validar.ParteNumerica(JSON_ARCHIVOS.RUT_ADQUIRIENTE);

                    //OBJETO A ACTUALIZAR...
                    SOLICITUD_SPIEV_R.UPDATE_SOLICITUD SOLICITUD_UPDATE = new SOLICITUD_SPIEV_R.UPDATE_SOLICITUD()
                    {
                        ID_SOLICITUD = JSON_ARCHIVOS.ID_SOLICITUD,
                        ID_SUCURSAL = JSON_ARCHIVOS.ID_SUCURSAL,
                        ID_FINANCIERA = JSON_ARCHIVOS.ID_FINANCIERA,
                        RUT_ADQUIRIENTE = RUT_INT,
                        DV_ADQUIRIENTE = Rut_Validar.Digito(RUT_INT)[0],
                        CORREO_ADQUIRIENTE = JSON_ARCHIVOS.CORREO_ADQUIRIENTE,
                        COMENTARIO = JSON_ARCHIVOS.COMENTARIO,
                        ID_ESTADO = 1 //SOLICITUD INGRESADA
                    };

                    //CREAR LA HISTORIA DE LA SOLICITUD
                    HISTORIA_SOLICITUD_R.CREATE(new HISTORIA_SOLICITUD
                    {
                        ID_SOLICITUD = JSON_ARCHIVOS.ID_SOLICITUD,
                        ID_ESTADO = 1,
                        COMENTARIO = "SOLICITUD SPIEV, ACTUALIZADA",
                        ID_USUARIO = SESSION_WEB.SESION_USER.ID_USUARIO,
                        FECHA = DateTime.Now
                    });

                    //ACTULIZACION SOLICITUD SPIEV
                    SOLICITUD_SPIEV_R.UPDATE(SOLICITUD_UPDATE);

                    //LISTA DE ARCHIVOS A BORRAR
                    List<ARCHIVO_SOLICITUD_SPIEV> ARCHIVOS_BORRAR = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(JSON_ARCHIVOS.ID_SOLICITUD).ToList();

                    //PROCESAR LOS ARCHIVOS (NUEVOS), SI EXISTEN
                    if (FILES != null)
                    {
                        //TOTAL DE ARCHIVOS PDFs
                        var ARCHIVOS_PDF = FILES.Where(n => Path.GetExtension(n.FileName).ToUpper() == ".PDF");

                        //NO TODOS LOS ARCHIVOS SON PDFs
                        if (ARCHIVOS_PDF.Count() != FILES.Count()) throw new Exception("Solo se permiten archivos PDF");

                        //CICLO SOBRE LOS ARCHIVOS...
                        for (int i = 0; i < FILES.Count(); i++)
                        {
                            //EL ARCHIVO
                            HttpPostedFileBase ARCHIVO = FILES[i];

                            //RUTA A GUARDAR
                            String FILE_NAME = $"{Guid.NewGuid()}{Path.GetExtension(ARCHIVO.FileName)}";

                            //EL ID DEL TIPO
                            int ID_TIPO_ARCHIVO = JSON_ARCHIVOS.TIPO_ARCHIVO_NUEVO[i];

                            //GUARDAR EL ARCHIVO
                            ARCHIVO.SaveAs($"{DESTINO}/{FILE_NAME}");

                            //SE REGISTRA EL ARCHIVO SUBIDO
                            PATH_UPLOAD_FILES.Add($"{DESTINO}/{FILE_NAME}");

                            //CREAR EL ARCHIVO ASOCIADO A LA SOLICITUD
                            ARCHIVO_SOLICITUD_SPIEV_R.CREATE(new ARCHIVO_SOLICITUD_SPIEV {
                                ID_SOLICITUD = JSON_ARCHIVOS.ID_SOLICITUD,
                                ID_TIPO_ARCHIVO_SOLICITUD = ID_TIPO_ARCHIVO,
                                ARCHIVO = FILE_NAME
                            });

                        }
                    }

                    //PROCESAR LOS ARCHIVOS (ACTUALIZAR), SI EXISTEN
                    for (int i = 0; i < JSON_ARCHIVOS.ARCHIVOS_ACTUALIZAR.Count(); i++)
                    {
                        int ID_ARCHIVO_SOLICITUD = JSON_ARCHIVOS.ARCHIVOS_ACTUALIZAR[i];

                        int ID_TIPO_ARCHIVO = JSON_ARCHIVOS.TIPO_ARCHIVO_ACTUALIZAR[i];

                        //ACTUALIZAR EL ARCHIVO
                        ARCHIVO_SOLICITUD_SPIEV_R.UPDATE(new ARCHIVO_SOLICITUD_SPIEV_R.UPDATE_ARCHIVO {
                            ID_ARCHIVO = ID_ARCHIVO_SOLICITUD,
                            ID_TIPO_ARCHIVO_SOLICITUD = ID_TIPO_ARCHIVO,
                        });

                        //SE ELIMINA EL ARCHIVO, SI ESTA EN LA LISTA DE ACTUALIZACIÓN
                        ARCHIVO_SOLICITUD_SPIEV ARCHIVO_EXCLUIR = ARCHIVOS_BORRAR.SingleOrDefault(x => x.ID_ARCHIVO == ID_ARCHIVO_SOLICITUD);
                        ARCHIVOS_BORRAR.Remove(ARCHIVO_EXCLUIR);
                    }

                    //BORRAMOS LOS ARCHIVOS QUE SE "BORRARON"
                    foreach (var ARCHIVO in ARCHIVOS_BORRAR)
                    {
                        //ACTUALIZAR EL ARCHIVO
                        ARCHIVO_SOLICITUD_SPIEV_R.UPDATE(new ARCHIVO_SOLICITUD_SPIEV_R.UPDATE_ARCHIVO
                        {
                            ID_ARCHIVO = ARCHIVO.ID_ARCHIVO,
                            BORRADO = false //AL DEFINIR FALSE SE BORRA, 0 = BORRADO, 1 = NO BORRADO
                        });
                    }

                    //SE COMPLETA LA OPERACION
                    scope.Complete();

                    //SE SETEA LA SALIDA
                    salida.status = true;
                    salida.message = "Solicitud actualizada correctamente";
                }
                catch (Exception ex)
                {
                    //EN CASO DE ERROR BORRAR LOS ARCHIVOS SUBIDOS
                    Parallel.ForEach(PATH_UPLOAD_FILES, FILE =>
                    {
                        System.IO.File.Delete(FILE);
                    });

                    //SE ENVIA EL ERROR
                    salida.message = ex.Message;
                }

            }

            return JsonConvert.SerializeObject(salida);
        }

        [HttpPost]
        [Route("merge-documentos")]
        public string CombinarPDF(long ID_SOLICITUD)
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            GeneratePDF PDF_UTIL = new GeneratePDF();

            try
            {
                //LISTA DE ARCHIVOS
                var ARCHIVOS = ARCHIVO_SOLICITUD_SPIEV_R.TODOS_BY_SOLICITUD(ID_SOLICITUD);

                //LA RUTA DE DESTINO
                String CARPETA_ARCHIVOS = Server.MapPath("~/Multimedia/Solicitudes");

                //DONDE EL ARCHIVO EXISTA
                var ARCHIVOS_PROCESAR = ARCHIVOS.Where(n => System.IO.File.Exists($"{CARPETA_ARCHIVOS}\\{n.ARCHIVO}"));

                //SI NO EXISTEN ARCHIVOS...
                if (ARCHIVOS_PROCESAR.Any() == false) throw new Exception("Archivos de solicitud, no encontrados en el servidor");

                //SOLO LOS PDFs
                ARCHIVOS_PROCESAR = ARCHIVOS_PROCESAR.Where(n => Path.GetExtension(n.ARCHIVO).ToUpper() == ".PDF");

                //SI NO EXISTEN ARCHIVOS...
                if (ARCHIVOS_PROCESAR.Any() == false) throw new Exception("Archivos de solicitud, no existen documentos en PDF");

                //SOLO LOS NOMBRES
                var ARCHIVOS_NOMBRES = ARCHIVOS_PROCESAR.Select(n => $"{CARPETA_ARCHIVOS}\\{n.ARCHIVO}").ToArray();

                //RUTA DESTINO
                String RUTA_SALIDA = Server.MapPath("~/Multimedia/Merge");

                //FUSIONAR DOC
                PDF_UTIL.CombineMultiplePDFs(ARCHIVOS_NOMBRES, $"{RUTA_SALIDA}\\{ID_SOLICITUD}.PDF");

                //SI SE GENERO CORRECTAMENTE ENVIO PARTE DE LA RUTA
                salida.status = true;
                salida.data = $"/Multimedia/Merge/{ID_SOLICITUD}.PDF";

            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

        [Route("tipos-requeridos-solicitud")]
        [HttpGet]
        public string Validar_Tipos_Solicitud()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SALIDA STATUS
                salida.status = true;
                salida.data = TIPO_ARCHIVO_SOLICITUD_R.VALIDACION_SOLICITUD();

            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }

    }
}