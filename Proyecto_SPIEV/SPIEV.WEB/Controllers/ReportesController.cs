﻿using Newtonsoft.Json;
using SPIEV.LIB_WEB;
using SPIEV.REPOS;
using SPIEV.WEB.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;

namespace SPIEV.WEB.Controllers
{
    [SESION_VALIDA]
    [RoutePrefix("reportes")]
    public class ReportesController : Controller
    {
        // GET: Reportes
        public ActionResult Index()
        {
            return View();
        }

        // GET: Reportes
        [HttpGet]
        [Route("mis-solicitudes")]
        public ActionResult Mis_Solicitudes()
        {
            return View();
        }

        [HttpPost]
        [Route("mis-estadisticas-inicio-ajax")]
        public string Mis_Estadisticas_Solicitud_Ajax()
        {
            dynamic salida = new ExpandoObject();
            salida.status = false;

            try
            {
                //SOLO MIS SOLICITUDES
                List<SOLICITUD_SPIEV> MIS_SOLICITUDES = SOLICITUD_SPIEV_R.TODOS_SOLICITUD_X_USUARIO(SESSION_WEB.SESION_USER.ID_USUARIO).ToList();

                //AGRUPAR SEGÚN ESTADO
                var GRUPO_POR_ESTADO = MIS_SOLICITUDES.GroupBy(
                        p => p.ESTADO.NOMBRE_ESTADO,
                        p => p,
                        (key, group) => new {  ESTADO = key, TOTAL = group.Count() });

                salida.status = true;
                salida.data = GRUPO_POR_ESTADO;
            }
            catch (Exception ex)
            {
                salida.message = ex.Message;
            }

            return JsonConvert.SerializeObject(salida);
        }
    }
}