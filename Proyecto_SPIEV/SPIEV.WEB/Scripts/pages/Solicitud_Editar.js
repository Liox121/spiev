﻿$(document).ready(function () {
    localStorage.removeItem('json-tipo-archivo-edit');

    //ID DE LA SOLICITUD
    var ID_SOLICITUD = $('#id-solicitud').val();

    //CARGAR DATOS DE LA SOLICITUD
    LOAD_SOLICITUD(ID_SOLICITUD);

});

function LOAD_SOLICITUD(ID_SOLICITUD)
{
    //BOTONES
    var BTN_ADD_FILE = $('#btn-add-file');
    var BTN_EDIT_SOL = $('#btn-editar-solicitud');

    //CARGAR TODA LA INFO NECESARIA
    $.when(TIPOS_ARCHIVOS_AJAX(), GET_DETALLE_SOLICITUD(ID_SOLICITUD), GET_ARCHIVOS_SOLICITUD(ID_SOLICITUD)).then(function (REQ_TIPOS_ARCHIVOS, REQ_DETALLE_SOLICITUD, REQ_ARCHIVOS_SOLICITUD)
    {
        //LOS DATOS
        var JSON_TIPOS = REQ_TIPOS_ARCHIVOS[0];
        var JSON_DETALLE = REQ_DETALLE_SOLICITUD[0];
        var JSON_ARCHIVOS = REQ_ARCHIVOS_SOLICITUD[0];

        //ALMACENAMOS LOS TIPOS DE ARCHIVOS EN SESSION HTML5
        localStorage.setItem('json-tipo-archivo-edit', JSON.stringify(JSON_TIPOS));

        //SETEAMOS LOS VALORES DE LA SOLICITUD
        $('#rut-adquiriente').val(JSON_DETALLE.data.RUT_ADQUIRIENTE + "-" + JSON_DETALLE.data.DV_ADQUIRIENTE);
        $('#correo-adquiriente').val(JSON_DETALLE.data.CORREO_ADQUIRIENTE);
        $('#comentario-solicitud').val(JSON_DETALLE.data.COMENTARIO);
        $('#Sucursales').val(JSON_DETALLE.data.ID_SUCURSAL);

        var FINANCIERA = JSON_DETALLE.data.ID_FINANCIERA === null ? 0 : JSON_DETALLE.data.ID_FINANCIERA;

        $('#Financieras').val(FINANCIERA);

        //TODOS LOS ARCHIVOS SUBIDOS
        $.each(JSON_ARCHIVOS.data, function (ID, ELEMENT) {
            ADD_FILE_UPD(ELEMENT);
        });

        //HABILITAR BOTONES
        $(BTN_ADD_FILE).removeAttr('disabled');
        $(BTN_ADD_FILE).removeClass('disabled');
        $(BTN_ADD_FILE).html("<i class='fas fa-plus-circle'></i> Agregar archivo");

        $(BTN_EDIT_SOL).removeAttr('disabled');
        $(BTN_EDIT_SOL).removeClass('disabled');
        $(BTN_EDIT_SOL).html("Guardar solicitud");
    });
}

function ADD_FILE_UPD(JSON_FILE)
{
    //CONTAMOS LOS ARCHIVOS AGREGADOS Y LE SUMAMOS UNO
    var NEW_ID = $(".caja-archivo").length + 1;

    //EL NUEVO ID DE LA CAJA
    var ID_ROW = "caja-archivo-" + NEW_ID;

    var DIV_COL = $('<div>')
        .addClass('col-lg-12 py-1 caja-archivo is-update')
        .attr('id', ID_ROW)
        .attr('data-archivo', JSON_FILE.ID_ARCHIVO);

    var DIV_ROW = $('<div>').addClass('row');

    $(DIV_ROW).append(GET_DIV_DOWNLOAD(JSON_FILE.ID_ARCHIVO));
    $(DIV_ROW).append(GET_DIV_SELECT_UPD(JSON_FILE.ID_TIPO_ARCHIVO_SOLICITUD));
    $(DIV_ROW).append(GET_DIV_DELETE(ID_ROW));

    $(DIV_COL).append(DIV_ROW);

    $("#files").append(DIV_COL);
}

function CLICK_ADD_FILE()
{
    //CONTAMOS LOS ARCHIVOS AGREGADOS Y LE SUMAMOS UNO
    var NEW_ID = $(".caja-archivo").length + 1;

    //EL NUEVO ID DE LA CAJA
    var ID_ROW = "caja-archivo-" + NEW_ID;

    var DIV_COL = $('<div>').addClass('col-lg-12 py-1 caja-archivo').attr('id', ID_ROW);
    var DIV_ROW = $('<div>').addClass('row');

    $(DIV_ROW).append(GET_DIV_INPUT());
    $(DIV_ROW).append(GET_DIV_SELECT());
    $(DIV_ROW).append(GET_DIV_DELETE(ID_ROW));

    $(DIV_COL).append(DIV_ROW);

    $("#files").append(DIV_COL);
}

function GET_DIV_INPUT()
{
    var DIV = $('<div>').addClass('col-6');
    var INPUT = $('<input>').attr('type', 'file').addClass('form-control-file form-control-sm px-0');

    $(DIV).append(INPUT);

    return DIV;
}

function GET_DIV_DOWNLOAD(ID_ARCHIVO)
{
    var DIV = $('<div>').addClass('col-6');
    var INPUT = $('<a>').addClass('btn btn-primary btn-sm btn-block').attr('href', '/solicitud/mis-archivos-solicitud/' + ID_ARCHIVO).html('<i class="fas fa-file-download"></i> Descargar');

    $(DIV).append(INPUT);

    return DIV;
}

function GET_DIV_SELECT()
{
    var DIV = $('<div>').addClass('col-4');
    var LABEL = $('<label>').addClass('col-2 col-form-label col-form-label-sm').html('Tipo');
    var SELECT = $('<select>').addClass('form-control form-control-sm');

    var DIV_SELECT = $('<div>').addClass('col-12').append(SELECT);
    var DIV_FORM_GROUP = $('<div>').addClass('form-group row mb-0').append(DIV_SELECT);

    COMPLETAR_SELECT_TIPO_ARCHIVO(SELECT);

    $(DIV).append(DIV_FORM_GROUP);

    return DIV;
}

function GET_DIV_SELECT_UPD(ID_TIPO_ARCHIVO)
{
    var DIV = $('<div>').addClass('col-4');
    //var LABEL = $('<label>').addClass('col-2 col-form-label col-form-label-sm').html('Tipo');
    var SELECT = $('<select>').addClass('form-control form-control-sm');

    var DIV_SELECT = $('<div>').addClass('col-12').append(SELECT);
    var DIV_FORM_GROUP = $('<div>').addClass('form-group row mb-0').append(DIV_SELECT);

    COMPLETAR_SELECT_TIPO_ARCHIVO_UPD(SELECT, ID_TIPO_ARCHIVO);

    $(DIV).append(DIV_FORM_GROUP);

    return DIV;
}

function GET_DIV_DELETE(ID)
{
    var DIV = $('<div>').addClass('col-2'); 
    var BTN = $('<button>').addClass('btn btn-danger btn-sm btn-block').attr({ 'type': 'button', 'onclick': 'CLICK_BORRAR_CAJA("' + ID + '")'}).html('<i class="fas fa-trash-alt"></i>');

    $(DIV).append(BTN);

    return DIV;
}

function CLICK_BORRAR_CAJA(ID)
{
    $('#' + ID).remove();
}

function COMPLETAR_SELECT_TIPO_ARCHIVO(SELECT)
{
    $(SELECT).append($('<option>').html('Cargando...'));

    //SE CARGA EL SELECT SI YA HAY DATOS EN ALMACEN HTML5                                                                    
    if (localStorage.getItem('json-tipo-archivo-edit') !== null)
    {
        var JSON_TIPO_STORE = JSON.parse(localStorage.getItem('json-tipo-archivo-edit'));
        CARGAR_SELECT(JSON_TIPO_STORE, SELECT);
        return;
    }

    //SI NO SE BUSCA AL SERVIDOR
    $.when(TIPOS_ARCHIVOS_AJAX()).then(function (data, textStatus, jqXHR) {

        localStorage.setItem('json-tipo-archivo-edit', JSON.stringify(data));

        CARGAR_SELECT(JSON.parse(localStorage.getItem('json-tipo-archivo-edit')), select);

    });
}

function COMPLETAR_SELECT_TIPO_ARCHIVO_UPD(SELECT, ID_TIPO_ARCHIVO)
{
    $(SELECT).append($('<option>').html('Cargando...'));

    //SE CARGA EL SELECT SI YA HAY DATOS EN ALMACEN HTML5                                                                    
    if (localStorage.getItem('json-tipo-archivo-edit') !== null)
    {
        var JSON_TIPO_STORE = JSON.parse(localStorage.getItem('json-tipo-archivo-edit'));
        CARGAR_SELECT(JSON_TIPO_STORE, SELECT);

        //PONER EL SELECT EN POSICION
        $(SELECT).val(ID_TIPO_ARCHIVO);
        return;
    }

    //SI NO SE BUSCA AL SERVIDOR
    $.when(TIPOS_ARCHIVOS_AJAX()).then(function (data, textStatus, jqXHR)
    {

        localStorage.setItem('json-tipo-archivo-edit', JSON.stringify(data));

        var JSON_TIPO_STORE = JSON.parse(localStorage.getItem('json-tipo-archivo-edit'));

        CARGAR_SELECT(JSON_TIPO_STORE, SELECT);

        //PONER EL SELECT EN POSICION
        $(SELECT).val(ID_TIPO_ARCHIVO);

    });
}

function CARGAR_SELECT(DATA, SELECT)
{
    $(SELECT).empty();

    $.each(DATA, function (ID, ELEMENT) {
        $(SELECT).append($('<option>').attr('value', ELEMENT.ID_TIPO_ARCHIVO_SOLICITUD).html(ELEMENT.TIPO_ARCHIVO.NOMBRE));
    });
}

function TIPOS_ARCHIVOS_AJAX()
{
    return $.ajax({
        url: "/solicitud/tipos-archivos-ajax",
        type: 'POST',
        dataType: 'json'
    });
}

function IsInArray(value, array)
{
    return array.indexOf(value) > -1;
}

function GET_DETALLE_SOLICITUD(ID_SOLICITUD)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/detalle-solicitud-ajax",
        data: JSON.stringify({ ID_SOLICITUD: parseInt(ID_SOLICITUD) }),
        dataType: "json"
    });
}

function GET_ARCHIVOS_SOLICITUD(ID_SOLICITUD) {
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/archivos-solicitud-ajax",
        data: JSON.stringify({ ID_SOLICITUD: parseInt(ID_SOLICITUD) }),
        dataType: "json"
    });
}

function CLICK_EDIT()
{
    var ID_SOLICITUD = parseInt($('#id-solicitud').val());
    var ID_SUCURSAL = parseInt($('#Sucursales').val());
    var RUT_ADQUIRIENTE = $('#rut-adquiriente').val();
    var CORREO_ADQUIRIENTE = $('#correo-adquiriente').val();
    var COMENTARIO = $('#comentario-solicitud').val();
    var ID_FINANCIERA = parseInt($('#Financieras').val());

    var OBJETO = new FormData();

    var JSON_OBJECT =
    {
        ID_SOLICITUD: ID_SOLICITUD,
        ID_SUCURSAL: ID_SUCURSAL,
        ID_FINANCIERA: ID_FINANCIERA === 0 ? null : ID_FINANCIERA,
        RUT_ADQUIRIENTE: RUT_ADQUIRIENTE,
        CORREO_ADQUIRIENTE: CORREO_ADQUIRIENTE,
        COMENTARIO: COMENTARIO,
        ARCHIVOS_ACTUALIZAR: null,
        TIPO_ARCHIVO_ACTUALIZAR: null,
        TIPO_ARCHIVO_NUEVO: null
    };

    //TIPOS REQUERIDOS AL SERVIDOR
    $.when(GET_TIPOS_REQUERIDOS()).then(function (JSON_REQ, textStatus, jqXHR)
    {
        //TIPOS A VALIDAR
        var JSON_TIPOS = JSON_REQ.data;

        //NOMBRES DE LOS ARCHIVOS REQUERIDOS
        var JSON_NOMBRES = JSON_TIPOS.map(function (obj) {
            return obj.TIPO_ARCHIVO.NOMBRE;
        });

        var VALIDAR = { valid: true, message: "" };

        var ARCHIVOS_ACTUALIZAR = [];
        var TIPOS_NUEVOS = [];
        var TIPOS_ACTUALIZAR = [];

        //LOS ARCHIVOS SUBIDOS
        $.each($('.caja-archivo'), function (ID, ELEMENT) {

            //EL TIPO DE ARCHIVO
            var TIPO_ARCHIVO = $(ELEMENT).find('select')[0];

            //SI ES UN ARCHIVO A ACTUALIZAR
            if ($(ELEMENT).hasClass('is-update'))
            {
                //EL ID DEL ARCHIVO A ACTUALIZAR
                var ID_ARCHIVO = parseInt($(ELEMENT).data('archivo'));

                //SE AÑADEN LOS ARCHIVOS AL ARREGLO
                ARCHIVOS_ACTUALIZAR.push(ID_ARCHIVO);
                TIPOS_ACTUALIZAR.push(parseInt($(TIPO_ARCHIVO).val()));
            }
            else
            {
                //EL ARCHIVO
                var ARCHIVO = $(ELEMENT).find('input[type="file"]')[0];

                //SE AÑADEN
                VALIDAR = VALIDAR_ARCHIVO(ARCHIVO);

                //SE AÑADEN LOS ARCHIVOS AL FORM
                OBJETO.append("FILES", ARCHIVO.files[0]);
                TIPOS_NUEVOS.push(parseInt($(TIPO_ARCHIVO).val()));
            }

        });

        //ARREGLO CONCATENEDO
        var TIPOS_CONCAT = TIPOS_ACTUALIZAR.concat(TIPOS_NUEVOS);

        //TIPOS DE ARCHIVOS QUE COINCIDEN CON LOS ADJUNTOS...
        var FILTRO = JSON_TIPOS.filter(function (ITEM) {
            return IsInArray(ITEM.ID_TIPO_ARCHIVO_SOLICITUD, TIPOS_CONCAT);
        });

        //SI FALTAN ARCHIVOS NECESARIOS
        if (FILTRO.length < JSON_TIPOS.length) VALIDAR = { valid: false, message: "Favor adjunte " + JSON_NOMBRES.join(', ') };

        //SE COMPLETA EL OBJETO
        JSON_OBJECT.ARCHIVOS_ACTUALIZAR = ARCHIVOS_ACTUALIZAR;
        JSON_OBJECT.TIPO_ARCHIVO_ACTUALIZAR = TIPOS_ACTUALIZAR;
        JSON_OBJECT.TIPO_ARCHIVO_NUEVO = TIPOS_NUEVOS;

        //SE PASA EL OBJETO A STRING
        var JSON_OBJECT_STRING = JSON.stringify(JSON_OBJECT);

        //SE AÑADEN EL JSON CON PARAMETROS
        OBJETO.append("JSON_PARAMETROS", JSON_OBJECT_STRING);

        //SI LA VALIDACION ES FALSE
        if (VALIDAR.valid === false)
        {
            alert(VALIDAR.message);
            return;
        }

        GUARDAR_SOLICITUD(OBJETO);

    });

   
}

function VALIDAR_ARCHIVO(ARCHIVO)
{
    var SALIDA = {};
    SALIDA.valid = true;
    SALIDA.message = "";

    if (ARCHIVO.files.length === 0) {
        SALIDA.valid = false;
        SALIDA.message = "Existe campo(s) sin archivo adjunto";
    }

    return SALIDA;
}

function GUARDAR_SOLICITUD(OBJETO)
{
    $.ajax({
        beforeSend: function (xhr)
        {
            $('#label-status').html('<i class="fas fa-spinner fa-spin"></i> Subiendo...').removeClass();
            $('#row-upload-ajax').css('display', 'flex');
        },
        url: "/solicitud/editar-ajax",
        type: "POST",
        data: OBJETO,
        dataType: 'json',
        contentType: false,
        cache: false,
        processData: false,
        success: function (RES)
        {
            var CLASE = "text-danger";

            if (RES.status) CLASE = "text-success";

            $('#label-status').addClass(CLASE);
            $('#label-status').text(RES.message);
        }
    });
}


function GET_TIPOS_REQUERIDOS()
{
    return $.ajax({
        url: "/solicitud/tipos-requeridos-solicitud",
        type: 'GET',
        dataType: 'json'
    });
}
