﻿$(document).ready(function ()
{
    //SI NO EXISTE LA TABLA
    if (!$('#tabla-usuarios').length) return;

    $('#tabla-usuarios').DataTable({
        "ajax": {
            url: "/administrador/usuarios-ajax",
            dataSrc: ''
        },
        "language": { "url": "/Scripts/datatables/Spanish.json" },
        "columns": [
            { "data": "ID_USUARIO" },
            { "data": "NOMBRE_USUARIO" },
            { "data": "CORREO_ELECTRONICO" },
            { "data": "ACTIVO" },
            {
                "mRender": function (data, type, row)
                {
                    return "<a href='/administrador/usuarios/" + row['ID_USUARIO'] + "' class='btn btn-outline-primary btn-sm'><i class='fas fa-edit fa-sm'></i></a>";
                },
                "searchable": false,
                "orderable": false
            }
        ]
    });
});

function CLICK_EDIT()
{
    var OBJETO = {};

    $("#datos-usuario").find('[data-propiedad]').each(function () {
        //VALOR EN STRING DEL CAMPO...
        var VALOR_CAMPO = $(this).val();

        //SI ES UN RADIO BUTTON
        if ($(this).data('radio')) VALOR_CAMPO = $("input[name='" + $(this).attr('name') + "']:checked").val();

        //SI ES UN CHECKBOX
        if ($(this).data('checkbox'))
        {
            VALOR_CAMPO = Array();
            $("input[name='" + $(this).attr("name") + "']:checked").each(function () {
                VALOR_CAMPO.push(parseInt(this.value));
            });
        }

        //SI ES ENTERO
        if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

        //SI ES DECIMAL
        if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

        //SI ES BOOLEANO
        if ($(this).data('boolean')) VALOR_CAMPO = VALOR_CAMPO.toString().toLowerCase() === 'true' ? true : false;

        //SE CREA EL CAMPO CON EL VALOR
        OBJETO[$(this).data('propiedad')] = VALOR_CAMPO;

    });

    //SI EL OBJETO ES VALIDO ES ENVIA
    if (VALIDAR_OBJETO(OBJETO))
    {
        EDITAR_USUARIO(OBJETO, $('#btn-editar-usuario'));
    }
}

function VALIDAR_OBJETO(OBJETO)
{
    if (OBJETO.NOMBRE_USUARIO === null || OBJETO.NOMBRE_USUARIO === "" || OBJETO.NOMBRE_USUARIO === undefined) {
        alert("(*) El campo nombre no puede estar vacio");
        return false;
    }

    if (OBJETO.CORREO_ELECTRONICO === null || OBJETO.CORREO_ELECTRONICO === 0 || OBJETO.CORREO_ELECTRONICO === undefined) {
        alert("(*) El campo correo no puede estar vacio");
        return false;
    }

    if (OBJETO.PERFIL_POR_DEFECTO === null || OBJETO.PERFIL_POR_DEFECTO === 0 || isNaN(OBJETO.PERFIL_POR_DEFECTO)) {
        alert("(*) Selecciona un perfil por defecto para continuar");
        return false;
    }

    if (OBJETO.PERFILES.length === 0)
    {
        alert("(*) Seleccione al menos un perfil");
        return false;
    }

    if (!IsInArray(OBJETO.PERFIL_POR_DEFECTO, OBJETO.PERFILES)) {
        alert("(*) Para elegir este perfil por defecto, marque el perfil correspondiente");
        return false;
    }

    return true;

}

function IsInArray(value, array)
{
    return array.indexOf(value) > -1;
}


function EDITAR_USUARIO(OBJETO, BTN)
{
    //BOTON EN CARGANDO...
    $(BTN).html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //TEXT EN BOTON
    var TEXT_BTN = "Crear usuario";

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/administrador/usuarios/editar-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT) {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message, BTN, TEXT_BTN);

        },
        error: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos", BTN, TEXT_BTN);
        }
    });
}

function CLICK_CREATE()
{
    var OBJETO = {};

    $("#datos-usuario").find('[data-propiedad]').each(function () {
        //VALOR EN STRING DEL CAMPO...
        var VALOR_CAMPO = $(this).val();

        //SI ES UN RADIO BUTTON
        if ($(this).data('radio')) VALOR_CAMPO = $("input[name='" + $(this).attr('name') + "']:checked").val();

        //SI ES UN CHECKBOX
        if ($(this).data('checkbox')) {
            VALOR_CAMPO = Array();
            $("input[name='" + $(this).attr("name") + "']:checked").each(function () {
                VALOR_CAMPO.push(parseInt(this.value));
            });
        }

        //SI ES ENTERO
        if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

        //SI ES DECIMAL
        if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

        //SI ES BOOLEANO
        if ($(this).data('boolean')) VALOR_CAMPO = VALOR_CAMPO.toString().toLowerCase() === 'true' ? true : false;

        //SE CREA EL CAMPO CON EL VALOR
        OBJETO[$(this).data('propiedad')] = VALOR_CAMPO;

    });

    CREAR_USUARIO(OBJETO, $('#btn-crear-usuario'));
}

function CREAR_USUARIO(OBJETO, BTN)
{
    //BOTON EN CARGANDO...
    $(BTN).html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //TEXT EN BOTON
    var TEXT_BTN = "Crear usuario";

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/administrador/usuarios/nuevo-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT) {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message, BTN, TEXT_BTN);

        },
        error: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos", BTN, TEXT_BTN);
        }
    });
}


function HTML_ALERTA(CLASS, MENS, BTN, TEXT_BTN)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PartialAlert",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');

            //BOTON NORMAL...
            $(BTN).html(TEXT_BTN);
        }
    });
}







