﻿$(document).ready(function () {

    //VALIDACION DE PASOS
    $('#nav-tab a').on('click', function (e) {
        e.preventDefault();

        var ELEMENTO_ACTIVO = $("#nav-tab").find("a.active:first");

        var TAB_ACTIVO = $(ELEMENTO_ACTIVO).attr('href');

        var FORM_ACTIVO = $(TAB_ACTIVO).find('form:first');

        var VALIDACION = $(FORM_ACTIVO).valid();

        return true;
    });

    $('#btn-generar-spiev').click(function () {

        var OBJETO = {};

        $('.form-spiev').each(function () {

            var FORM = $(this);
            var OBJETO_FORM = {};

            $(FORM).find('[data-propiedad]').each(function () {
                //VALOR EN STRING DEL CAMPO...
                var VALOR_CAMPO = $(this).val();

                //SI ES ENTERO
                if ($(this).data('entero')) VALOR_CAMPO = parseInt(VALOR_CAMPO);

                //SI ES DECIMAL
                if ($(this).data('decimal')) VALOR_CAMPO = parseFloat(VALOR_CAMPO);

                //SE CREA EL CAMPO CON EL VALOR
                OBJETO_FORM[$(this).data('propiedad')] = VALOR_CAMPO;
            });

            OBJETO[FORM.attr('name')] = OBJETO_FORM;

        });

        //SE AGREGA EL ID DE SOLICITUD
        OBJETO['ID_SOLICITUD'] = parseInt($('#id-solicitud').text());

        //SE ENVIAN A GUARDAR
        GUARDAR_SPIEV(OBJETO);
    });

    //CUANDO CARGA LA PAGINA SETEA LA CAPACIDAD DE CARGA
    GET_CARGA_VEHICULO_TIPO($('#VehiculoForm_ID_TIPO_VEHICULO').val());

    //CUANDO CAMBIA EL TIPO DE VEHICULO SE CAMBIA LA CAPACIDAD DE CARGA SI ES REQUERIDA
    $('#VehiculoForm_ID_TIPO_VEHICULO').change(function () {
        var ID = $(this).val();
        GET_CARGA_VEHICULO_TIPO(ID);
    });

    //CUANDO CARGA LA PAGINA SETEA LA CANTIDAD EN COMUNIDAD SEGÚN OPCION
    GET_CANTIDAD_OPCION($('#ComunidadForm_ID_OPCION_COMUNIDAD').val());

    //CUANDO CAMBIA LA OPCION DE COMUNIDAD CAMBIA LA CANTIDAD ASOCIADA
    $('#ComunidadForm_ID_OPCION_COMUNIDAD').change(function () {
        var ID = $(this).val();
        GET_CANTIDAD_OPCION(ID);
    });

});

function VER_PDF_MERGE(BTN)
{
    var SRC = $('#frame-pdf-merge').attr('src');

    //SI TIENE UNA URL VALIDA
    if (SRC !== "#")
    {
        //MOSTRAR
        $('#collapse-view-pdf').collapse('toggle');
        return;
    }

    //ID DE LA SOLICITUD
    var ID_SOL = parseInt($('#id-solicitud').text());

    //SE GENERA EL DOCUMENTO FUSIONADO
    $.when(PDF_MERGE_SOLICITUD(ID_SOL)).then(function (JSON)
    {
        //EN CASO DE ERROR...
        if (!JSON.status)
        {
            HTML_ALERTA("alert alert-dismissible fade show alert-danger", JSON.message);
            return;
        }

        //RUTA DEL PDF
        var RUTA_PDF = JSON.data;

        var URL_HOST = window.location.protocol + "//" + window.location.hostname;
        URL_HOST += window.location.port === "" ? "" : ":" + window.location.port;

        //RUTA DEL PDF
        var SRC_FRAME = $('#frame-pdf-merge').data('src');
        SRC_FRAME = SRC_FRAME.replace("{RUTA_PDF}", URL_HOST + RUTA_PDF);

        //SETEAR LA  URL DEL FRAME
        $('#frame-pdf-merge').attr('src', SRC_FRAME);
    
        //MINIZAR MENU SI CORRESPONDE
        if (!$('#accordionSidebar').hasClass('toggled') && !$('#collapse-view-pdf').hasClass('show')) $('#sidebarToggle').click();

        //MOSTRAR
        $('#collapse-view-pdf').collapse('toggle');

    });
}

function PDF_MERGE_SOLICITUD(ID)
{
    return $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/solicitud/merge-documentos",
        data: JSON.stringify({ ID_SOLICITUD: ID }),
        dataType: "json"
    });
}

function GET_CARGA_VEHICULO_TIPO(ID_TIPO)
{
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/registro/carga-vehiculo-ajax",
        data: { ID_TIPO_VEHICULO: ID_TIPO },
        dataType: "json",
        success: function (RESULT)
        {
            if (RESULT.valor === null)
            {
                $('#VehiculoForm_CARGA').val('');
                $('#VehiculoForm_CARGA').attr('readonly', false);
            }
            else
            {
                $('#VehiculoForm_CARGA').val(RESULT.valor);
                $('#VehiculoForm_CARGA').attr('readonly', true);
            }
        },
        error: function (RESULT)
        {

        }
    });
}

function GET_CANTIDAD_OPCION(ID_OPCION) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/registro/cantidad-comunidad-ajax",
        data: { ID_OPCION_COMUNIDAD: ID_OPCION },
        dataType: "json",
        success: function (RESULT)
        {
            if (RESULT.valor === null)
            {
                $('#ComunidadForm_CANTIDAD').val('');
                $('#ComunidadForm_CANTIDAD').attr('readonly', false);
            }
            else
            {
                $('#ComunidadForm_CANTIDAD').val(RESULT.valor);
                $('#ComunidadForm_CANTIDAD').attr('readonly', true);
            }
        },
        error: function (result)
        {

        }
    });
}

function GUARDAR_SPIEV(OBJETO)
{
    //BOTON EN CARGANDO...
    $('#btn-generar-spiev').html('<i class="fas fa-spinner fa-spin"></i> Cargando');

    //CLASE PARA ALERTA
    var CLASE = "alert alert-dismissible fade show ";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/registro/guardar-spiev-ajax",
        data: JSON.stringify(OBJETO),
        dataType: "json",
        success: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += RESULT.status ? "alert-success" : "alert-danger";

            //PINTAR ALERTA CON OPCIONES
            if (RESULT.status)
            {
                HTML_ALERTA_INSCR_OK(RESULT.data.ID_INSCRIPCION);
                return;
            }

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, RESULT.message);

        },
        error: function (RESULT)
        {
            //SI EL STATUS ES OK, SE ENVIA ALERTA VERDE, SI ES ROJA
            CLASE += "alert-danger";

            //SE INSERTA LA ALERTA EN PANTALLA
            HTML_ALERTA(CLASE, "Hay un problema al guardar los datos");
        }
    });
}

function HTML_ALERTA(CLASS, MENS)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PartialAlert",
        data: JSON.stringify({ Clase: CLASS, Mensaje: MENS }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');

            //BOTON NORMAL...
            $('#btn-generar-spiev').html('Generar 1era Inscripción');
        }
    });
}

function HTML_ALERTA_INSCR_OK(ID_INSCRIPCION)
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Home/PartialAlertInscripcionOK",
        data: JSON.stringify({ ID_INSCRIPCION: ID_INSCRIPCION }),
        dataType: "html",
        success: function (RESULT)
        {
            $('#alerta-spiev').hide().html(RESULT).fadeIn('slow');

            //BOTON NORMAL...
            $('#btn-generar-spiev').html('Generar 1era Inscripción');
        }
    });
}
